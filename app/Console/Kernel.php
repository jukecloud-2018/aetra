<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Http\Controllers\Controller;
use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\Customer;
use App\Models\Administrator\Workers;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Session;
use Storage;

include_once(app_path() . '/MyLibs/nusoap.php');
use DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //update status Open to Cancel monthly 26
        $schedule->call(function(){
            // //get datetime log
            // $file = storage_path('app/date26.log');
            // $getDateLog = fgets(fopen($file, 'r'));
            // $getDateLog = trim($getDateLog);
            // $url = "http://172.27.1.38:8280/services/DS_Get_Pending_FA?wsdl";
            // if(!filter_var($url, FILTER_VALIDATE_URL)){
            //   print_r("Site is offline");
            // }else{
            //   //echo $getDateLog;
            //   $client = new \nusoap_client($url, 'wsdl');
            //   $convert = Carbon::createFromFormat('Y-m-d H:i:s', now())->format('d/m/Y H:i:s');
            //   //-----//
            //   $pDate = isset($_GET["pDate"]) ? $_GET["pDate"] : $convert;
            //   $operation = 'getPendingFa';
            //   $param1 = array( 'pDate' => $pDate, );
            //   $hasil1 = $client->call($operation, $param1);
            //   foreach($hasil1 as $key => $value){
            //     $newData = array_filter($value, function ($var) use ($pDate) {
            //       return ($var['createDate'] <= $pDate);
            //     });
            //     foreach ($newData as $key => $row) {
            //       $findCus = FA_transaction::where('fa_transaction_id',trim($row['faId']))
            //                  ->where('status','P')
            //                  ->where('assign_status','Open')
            //                  ->first();
            //       if($findCus->fa_transaction_id != NULL){
            //         $data = array(
            //           'assign_status' => 'Cancel',
            //           'status' => 'X'
            //         );
            //         FA_transaction::where('created_at', '<=', now())
            //         ->where('fa_transaction_id',$findCus->fa_transaction_id)
            //         ->update($data);
            //       }
            //       //cek lagi yang di simpan datenya created_at atau update_at
            //       $maxDate = DB::table('fa_transaction')->max('created_at');
            //       $maxDate = substr($maxDate, 0, -4);
            //       $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
            //       Storage::prepend('date26.log', $getDate);//create log date -> Storage/app/date.log
            //     }
            //   }
            // }
        })->monthlyOn(26, '23:59');

        /** Get Panding FA New*/
        $schedule->call(function(){
            $file = storage_path('app/date.log');
            $getDateLog = fgets(fopen($file, 'r'));
            $getDateLog = trim($getDateLog);
            
            /** Function get data FA Pending from SIMPEL*/
            $url = 'http://172.27.1.38:8280/services/DS_Get_Pending_FA?wsdl';
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:get="http://aetra.co.id/getPendingFa">
                    <soapenv:Header/><soapenv:Body><get:getPendingFa><get:pDate>'.$getDateLog.'</get:pDate></get:getPendingFa></soapenv:Body>
                    </soapenv:Envelope>';
            $headers = array(
                "Content-Type: text/xml;charset=UTF-8",
                "SOAPAction:urn:getPendingFa",
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER =>  $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if($err){
                $error = "cURL Error #:" . $err;
                Storage::prepend('error.log', $error);
            }else{
                /** insert data to database */
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($getString ,"",-34);
                $getXML = simplexml_load_string($getString);
                $json  = json_encode($getXML);
                $configData = json_decode($json, true);
                // $path = storage_path('app/getPandingFA.json');
                // unlink($path);
                // Storage::prepend('getPandingFA.json', $json);
                foreach($configData as $value){
                    $insert_faTransaction = [];
                    $insert_customer      = [];
                    $update_FAtransaction = [];
                    $update_Customer      = [];
                    foreach($value as $row){
                        //Data FA Transaction and Customer
                        $nomen = trim($row['nomen']);
                        $customer_name = trim($row['entityName']);
                        $phone = encodePost($row['mobilePhone']);
                        $email = encodePost($row['emailId']);
                        if($email == '[ ]'){
                            $email = '';
                        }else{
                            $email = $row['emailId'];
                        }
                        $city = encodePost($row['city']);
                        if($city == '[ ]'){
                            $city = '';
                        }else{
                            $city = $row['city'];
                        }
                        $postal = encodePost($row['postal']);
                        $pcezbk = trim($row['pcezbk']);
                        $meterinfo = trim($row['meterInfo']);
                        $latitude = encodePost($row['gpslat']);
                        $longitude = encodePost($row['gpslong']);
                        $status = trim($row['custStatus']);
                        $date_aetra = strtr($row['createDate'], '/', '-');
                
                        //split created_at
                        if(isset($date_aetra)){
                            $created = date("Y-m-d h:i:s.u", strtotime($date_aetra));
                        }else{
                            $created = date("Y-m-d h:i:s.u", strtotime(now()));
                        }
                        $updated = date("Y-m-d h:i:s.u", strtotime(now()));
                
                        //split address
                        $get_rt = trim($row['RT']);
                        $rt = $get_rt ? $get_rt : '';
                        $get_rw = trim($row['RW']);
                        $rw = $get_rw ? $get_rw : '';
                
                        $get_address = trim($row['address']);
                        if($get_address){
                            $split_jl = explode("|", $get_address);
                            $jl = $split_jl[0];
                            $hasil_jl = $jl ? $jl : '';
                            $name_jl = $split_jl[1];
                            $hasil_name_jl = $name_jl ? $name_jl : '';
                            $gang = $split_jl[2];
                            $hasil_gang = $gang ? $gang : '';
                            if(isset($split_jl[3])){
                                $hasil_no = $split_jl[3];
                            }else{
                                $hasil_no = '';
                            }
                            $address = $hasil_jl." ".$hasil_name_jl." ".$hasil_gang." ".$hasil_no." RT ".$rt."RW ".$rw;
                        }else{
                            $address = '';
                        }
                        
                        //insert data to fa_transaction
                        $findCus = FA_transaction::where('fa_transaction_id',trim($row['faId']))->first();
                        if(!$findCus){
                            $faTransaction = [
                                //'No'    => $i,
                                'fa_transaction_id' => trim($row['faId']),
                                'nomen'             => $nomen,
                                'dispatch_group'    => trim($row['dispatchGrp']),
                                'descriptions'      => trim($row['descr']),
                                'fa_type_cd'        => trim($row['faTypeCd']),
                                'sending_status'    =>'Pending',
                                'assign_status'     => 'Open',
                                'status'            => trim($row['statusFlag']),
                                'created_at'        => substr($created, 0, -3),
                                'updated_at'        => substr($updated, 0, -3)
                            ];
                            $insert_faTransaction = collect($faTransaction);
                            $insert_faTransaction = $insert_faTransaction->chunk(500);
                            foreach ($insert_faTransaction as $row1){
                                DB::disableQueryLog();
                                DB::table('fa_transaction')->insert($row1->toArray());
                            }
                        }
                        $findCus = Customer::where('fa_transaction_id',trim($row['faId']))->first();
                        if(!$findCus){
                            $customer =[
                                'fa_transaction_id' => trim($row['faId']),
                                'nomen'         => trim($row['nomen']),
                                'customer_name' => trim($row['entityName']),
                                'phone'         => $phone,
                                'email'         => $email,
                                'address'       => $address,
                                'city'          => $city,
                                'postal'        => $postal,
                                'pcezbk'        => $pcezbk,
                                'meterinfo'     => $meterinfo,
                                'latitude'      => $latitude,
                                'longitude'     => $longitude,
                                'status'        => $status
                            ];
                            $insert_customer = collect($customer);
                            $insert_customer = $insert_customer->chunk(500);
                            foreach ($insert_customer as $row2){
                            DB::disableQueryLog();
                            DB::table('customer')->insert($row2->toArray());
                            }
                        }else{
                            //Update data
                            FA_transaction::where('fa_transaction_id', trim($row['faId']))
                                            ->update([
                                                'nomen' => trim($row['nomen']),
                                                'dispatch_group' => trim($row['dispatchGrp']),
                                                'fa_type_cd' => trim($row['faTypeCd']),
                                                'status' => trim($row['statusFlag'])
                                            ]);
                        
                            Customer::where('fa_transaction_id', trim($row['faId']))
                                    ->update([
                                        'nomen'         => trim($row['nomen']),
                                        'phone'         => $phone,
                                        'address'       => $address,
                                        'email'         => $email,
                                        'city'          => $city,
                                        'postal'        => $postal,
                                        'pcezbk'        => $pcezbk,
                                        'meterinfo'     => $meterinfo,
                                        'latitude'      => $latitude,
                                        'longitude'     => $longitude,
                                        'status'        => $status
                                    ]);
                        }
                    }
                    //buat query max date di fa_transaction//
                    $maxDate = DB::table('fa_transaction as a')->select('created_at')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
                    $maxDate = $maxDate->created_at;
                    $maxDate = substr($maxDate, 0, -4);
                    $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
                    Storage::prepend('date.log', $getDate);//create log date -> Storage/app/date.log
                }
            }
        //})->dailyAt('00:00');
        })->dailyAt('11:48');


        /** Get WO New */
        $schedule->call(function(){
            $file = storage_path('app/wo.log');
            $getDateLog = fgets(fopen($file, 'r'));
            $getDateLog = trim($getDateLog);
            $url = "http://172.27.1.38:8280/services/DS_Get_Active_WO?wsdl";
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:get="http://aetra.co.id/getActiveWo"><soapenv:Header/>
                        <soapenv:Body>
                            <get:getActiveWoOperation>
                                <get:pDate>'.$getDateLog.'</get:pDate>
                            </get:getActiveWoOperation>
                        </soapenv:Body>
                    </soapenv:Envelope>';
            $headers = array("Content-Type: text/xml;charset=UTF-8", "SOAPAction:urn:getActiveWoOperation",);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER =>  $headers,
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if($err){
                $error = "cURL Error #:" . $err;
                Storage::prepend('error.log', $error);
            }else{
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($getString ,"",-34);
                $getXML = simplexml_load_string($getString);
                $json  = json_encode($getXML);
                $configData = json_decode($json, true);
            
                foreach ($configData as $key => $value){
                    foreach ($value as $row) {
                        $date_aetra = encodePost($row['lastUpdateDate']);
                        $date_aetra = strtr($row['lastUpdateDate'], '/', '-');
                        $date = date("Y-m-d h:i:s.u", strtotime($date_aetra));
                        $findWO = WO_transaction::where('no_wo',trim($row['workOrderNo']))
                                ->where('asset_id',trim($row['assetId']))
                                ->where('no_wo_task',trim($row['workOrderTaskNo']))
                                ->first();
                        if(!$findWO){
                            $data = [
                                'plant' => encodePost($row['plant']),
                                'no_wo' => encodePost($row['workOrderNo']),
                                'no_wo_task' => encodePost($row['workOrderTaskNo']),
                                'task_status' => encodePost($row['taskStatus']),
                                'task_desc' => encodePost($row['taskDesc']),
                                'asset_id' => encodePost($row['assetId']),
                                'no_account' => encodePost($row['accountNo']),
                                'no_specification' => encodePost($row['specificationNo']),
                                'asset_desc' => encodePost($row['assetDesc']),
                                'area' => encodePost($row['area']),
                                'department' => encodePost($row['department']),
                                'crew' => encodePost($row['crew']),
                                'jobCode' => encodePost($row['jobCode']),
                                'created_by' => encodePost($row['createdBy']),
                                'taskPhase' => encodePost($row['taskPhase']),
                                'sending_status' => 'Pending',
                                'assigned_status' => 'Open',
                                'created_at' => substr($date, 0, -3)
                            ];
                            $wo_transaction = DB::table('wo_transaction')->insert($data);
                        }else{
                            WO_transaction::where('no_wo',trim($row['workOrderNo']))
                            ->where('asset_id',trim($row['assetId']))
                            ->where('no_wo_task',trim($row['workOrderTaskNo']))
                            ->update([
                                'plant' => encodePost($row['plant']),
                                'no_wo' => encodePost($row['workOrderNo']),
                                'no_wo_task' => encodePost($row['workOrderTaskNo']),
                                'task_status' => encodePost($row['taskStatus']),
                                'task_desc' => encodePost($row['taskDesc']),
                                'asset_id' => encodePost($row['assetId']),
                                'no_account' => encodePost($row['accountNo']),
                                'no_specification' => encodePost($row['specificationNo']),
                                'asset_desc' => encodePost($row['assetDesc']),
                                'area' => encodePost($row['area']),
                                'department' => encodePost($row['department']),
                                'crew' => encodePost($row['crew']),
                                'jobCode' => encodePost($row['jobCode']),
                                'created_by' => encodePost($row['createdBy']),
                                'taskPhase' => encodePost($row['taskPhase'])
                            ]);
                        }
                    }
                    // creat log date wo 
                    $maxDate = DB::table('wo_transaction')->select('created_at')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
                    $maxDate = substr($maxDate->created_at, 0, -4);
                    $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
                    Storage::prepend('wo.log', $getDate);//create log date -> Storage/app/wo.log
                    return response()->json([ 'data' => [ 'status' => 'Update' ] ]);
                }
            }
        })->dailyAt('11:37');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
