<?php

namespace App\Imports;

use App\Models\Administrator\Workers;
use App\Models\Administrator\Users;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class Import implements ToModel, WithHeadingRow
{
  use Importable;
  /**
  * @param array $row
  *
  * @return \Illuminate\Database\Eloquent\Model|null
  */
  public function model(array $row)
  {
    $data = new Users([
        'name' => $row['name'],
        'password' => $row['password'],
        'username' => $row['username'],
        'no_hp' => $row['no_hp']
    ]);

    return $data;
  }
}
