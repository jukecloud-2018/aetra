<?php

namespace App\Imports;

use App\Models\Transaction\WO_transaction;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AssignmentWOImport implements ToModel
{
	use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       return new WO_transaction([
       		'plant'=> $row['plant'],
	      	'no_wo' => $row['no_wo'],
		    'no_wo_task' => $row['no_wo_task'],
		    'jobCode' => $row['jobCode'],
		    'task_status' => $row['task_status'],
		    'asset_id' => $row['asset_id'],
		    'worker_id' => $row['worker_id'],
		    'assign_date' => $row['assign_status'],
		    'reassign_date' => $row['reassign_date'],
		    'assign_status' => $row['assign_status'],
		    'no_account' => $row['no_account'],
		    'no_specification' => $row['no_specification'],
		    'area' => $row['area'],
		    'department' => $row['department'],
		    'crew' => $row['crew'],
		    'urgent_status' => $row['urgent_status'],
		    'priority_status' => $row['priority_status'],
		    'sending_status' => $row['sending_status']
	    ]);
    }
}
