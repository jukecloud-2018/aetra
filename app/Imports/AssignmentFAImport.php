<?php

namespace App\Imports;

use App\Models\Transaction\FA_transaction;
use App\Models\Administrator\Workers;
use App\Models\Administrator\Users;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AssignmentFAImport implements ToModel, WithHeadingRow
{
  use Importable;
  /**
  * @param array $row
  *
  * @return \Illuminate\Database\Eloquent\Model|null
  */
  public function model(array $row)
  {
    return new FA_transaction([
      'fa_transaction_id' => $row['fa_transaction_id'],
      'nomen' => $row['nomen'],
      'dispatch_group' => $row['dispatch_group'],
      'descriptions' => $row['descriptions'],
      'fa_type_cd' => $row['fa_type_cd'],
      'urgent_status' => $row['urgent_status'],
      'worker_id' => $row['worker_id'],
      'assign_status' => $row['assign_status'],
      'priority_status' => $row['priority_status'],
      'status' => $row['status'],
      'sending_status' => $row['sending_status']
    ]);
  }
}
