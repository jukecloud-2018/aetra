<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Skill_wo extends Model
{
    //protected table
    protected $table = 'skill_wo';

    public function worker()
    {
    	return $this->hasOne(Workers::class,'worker_id');
    }

    public function skill_wo_detail()
    {
        return $this->hasMany(Skill_wo_detail::class,'skill_wo_id');
    }
}
