<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class HHManagement extends Model
{
    protected $table = 'hh_management';
}
