<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    //

    public function worker()
    {
    	return $this->hasOne(Workers::class,'vendor_id');
    }
}
