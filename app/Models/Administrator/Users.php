<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Traits\LaratrustUserTrait;
use App\Models\Administrator\Admin;
use App\Models\Administrator\Groups;


class Users extends Model
{
    use LaratrustUserTrait;
    //
    public function groups()
    {
    	return $this->belongsTo(Groups::class,'group_id');	
    }

    public function workers()
    {
        return $this->hasOne(Workers::class,'user_id');
    }

    public function admin()
    {
    	return $this->belongsTo(Admin::class,'user_id');
    }
}
