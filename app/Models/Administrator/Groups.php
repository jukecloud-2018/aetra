<?php

namespace App\Models\Administrator;
use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Users;

class Groups extends Model
{
    
    protected $table = 'group_access';

    public static function boot() {
        parent::boot();

        static::deleting(function($groups) { 
            $groups->survey_group_access()->delete();
            $groups->fa_group_access()->delete(); 
        });
    }

    public function users()
    {
    	return $this->hasOne(Users::class);
    }

    public function fa_group_access()
    {
        return $this->hasMany(FA_Group_Access::class,'group_access_id','id');
    }

    public function survey_group_access()
    {
        return $this->hasMany(Survey_group_access::class,'group_access_id','id');
    }
}
