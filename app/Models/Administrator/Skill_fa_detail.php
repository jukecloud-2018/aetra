<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Skill_fa_detail extends Model
{
    //
    protected $table = 'skill_fa_detail';

    public function skill_fa()
    {
        return $this->belongsTo(Skill_fa::class,'skill_fa_id');
    }

    public function fa_type()
    {
        return $this->belongsTo(Fa_type::class);
    }


}
