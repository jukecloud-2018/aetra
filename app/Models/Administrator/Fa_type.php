<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction\FA_transaction;

class Fa_type extends Model
{
    //

    protected $table = 'fa_type';

    public function fa_transaction()
    {
    	return $this->belongsTo(FA_transaction::class);
    }

    public function fa_type_lang()
    {
        return $this->hasOne(FA_type_lang::class,'fa_type_cd','id');
    }

}
