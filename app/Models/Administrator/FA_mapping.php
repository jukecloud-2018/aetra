<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction\FA_transaction;


class FA_mapping extends Model
{
    //
    protected $table = 'fa_mapping';

     public function fa_transaction()
    {
    	return $this->belongsTo(FA_transaction::class);
    }
}
