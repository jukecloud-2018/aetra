<?php

namespace App\Models\Administrator;

use App\Models\Transaction\AssignmentSurvey;
use Illuminate\Database\Eloquent\Model;

class Workers extends Model
{
    protected $table = 'workers';

    public function vendor()
    {
    	return $this->belongsTo(Vendors::class);
    }

    public function skill_fa()
    {
    	return $this->belongsTo(Skill_fa::class);
    }

    public function skill_wo()
    {
        return $this->belongsTo(Skill_wo::class);
    }

    public function users()
    {
//        return $this->belongsTo(Users::class,'id');
        return $this->hasOne(Users::class,'id','user_id');
    }


    public function AssignmentSurvey()
    {
//    	return $this->hasOne(Transaction\AssignmentSurvey::class,'worker_id');
    	return $this->hasOne(AssignmentSurvey::class,'worker_id');
    }

    public function worker_dispatch()
    {
        return $this->hasMany(Worker_dispatch::class,'worker_id','id');
    }
}
