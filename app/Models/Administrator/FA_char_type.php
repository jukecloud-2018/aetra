<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class FA_char_type extends Model
{
    protected $table = 'fa_char_type';
    public function fa_char_value()
    {
        return $this->belongsTo(FA_char_value::class,'char_type_cd','char_type_cd');
    }

    public function fa_alg_validation()
    {
        return $this->belongsTo(FA_alg_validation::class,'alg_cd','adhoc_val_alg_cd');
    }
}
