<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Skill_fa extends Model
{
    //
    protected $table = 'skill_fa';

    public function worker()
    {
    	return $this->hasOne(Workers::class,'worker_id');
    }

    public function skill_fa_detail()
    {
        return $this->hasMany(Skill_fa_detail::class,'skill_fa_id');
    }


}
