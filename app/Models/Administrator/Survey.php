<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Survey_detail;
use App\Models\Administrator\Survey;

class Survey extends Model
{
    //
    protected  $table = 'survey';

    public function survey_detail()
    {
    	return $this->hasMany(Survey_detail::class,'survey_id');
    }

    public function AssignmentSurvey()
    {
    	return $this->hasOne(Transaction\AssignmentSurvey::class,'survey_id');
    }
}
