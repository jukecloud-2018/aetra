<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Admin;
use App\Models\Administrator\Groups;


class Admin extends Model
{
    //

    protected $table = 'admin';


    public function users()
    {
    	return $this->hasOne(Users::class,'id');
    }

   
}
