<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\WO_char_type;
use App\Models\Administrator\WO_transaction;

class Wo_task extends Model
{
    //protected table
    protected $table = 'wo_task';

    public function wo_char_type()
    {
        return $this->hasOne(WO_char_type::class,'job_code','job_code');
    }

    public function wo_transaction()
    {
    	return $this->belongsTo(WO_transaction::class);
    }
}
