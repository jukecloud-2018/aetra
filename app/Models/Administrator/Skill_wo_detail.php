<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class Skill_wo_detail extends Model
{
    //protected
    protected $table = 'skill_wo_detail';

    public function skill_wo()
    {
        return $this->belongsTo(Skill_wo::class,'skill_wo_id');
    }

    public function wo_task()
    {
        return $this->belongsTo(Wo_task::class,'job_code_id');
    }
}
