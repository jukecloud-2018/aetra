<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;

class FA_mapping_char_type extends Model
{
    protected $table = 'fa_mapping_char_type';
    public function fa_char_type()
    {
        return $this->belongsTo(FA_char_type::class,'char_type_cd','char_type_cd');
    }

    public function fa_type()
    {
        return $this->belongsTo(Fa_type::class,'fa_type_cd','fa_type_cd');
    }
}
