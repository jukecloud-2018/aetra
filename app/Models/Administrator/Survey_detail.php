<?php

namespace App\Models\Administrator;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Survey;
use App\Models\Administrator\Survey_detail;


class Survey_detail extends Model
{
    //
    protected $table = 'survey_detail';


    public function survey()
    {
    	$this->belongsTo(Survey::class,'survey_id');	
    }
}
