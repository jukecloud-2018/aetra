<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class FA_Transaction_material_usage extends Model
{
    protected $table = 'fa_transaction_material_usage';

    public function transaction(){
        return $this->belongsTo(FA_transaction::class,'fa_transaction_id','fa_transaction_id');
    }
}
