<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //

    protected $table = 'customer';

    public function fa_transaction()
    {
    	return $this->belongsTo(FA_transaction::class);
    }
}
