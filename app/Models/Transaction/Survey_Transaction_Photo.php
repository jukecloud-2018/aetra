<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Survey_Transaction_Photo extends Model
{
    protected $table = 'survey_transaction_photo';
}
