<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class FA_Template_BOM_Characteristic extends Model
{
    protected $table = 'fa_template_bom_characteristic';
}
