<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class FA_Template_BOM_code extends Model
{
    protected $table = 'fa_template_BOM_code';
    protected $primaryKey = 'id';
    protected $fillable = ['code', 'name', 'category', 'unit_of_purch'];
    protected $guarded = [];
}
