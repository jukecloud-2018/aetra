<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class WO_Template_BOM_Characteristic extends Model
{
    protected $table = 'wo_template_bom_characteristic';
}
