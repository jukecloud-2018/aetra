<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Survey_Transaction_Characteristic extends Model
{
    protected $table = 'survey_transaction_characteristic';
    protected $fillable = ['user_id', 'survey_code', 'type_data', 'characteristic_name', 'characteristic_value', 'created_by', 'updated_by'];
}
