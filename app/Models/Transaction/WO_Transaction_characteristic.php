<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class WO_Transaction_characteristic extends Model
{
    protected $table = 'wo_transaction_characteristic';
}
