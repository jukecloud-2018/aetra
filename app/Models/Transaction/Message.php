<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $table = 'messages';

  public function message_to()
  {
    return $this->hasMany(Message_To::class,'id','id_send');
  }
}
