<?php

namespace App\Models\Transaction;

use App\Models\Administrator\Users;
use App\Models\Administrator\Workers;
use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\FA_type;
use App\Models\Administrator\FA_mapping;



class FA_transaction extends Model
{
    //

    protected $table = 'fa_transaction';
    protected $fillable = ['created_at'];

 	public function customer()
 	{
 		//return $this->hasOne(Customer::class,'nomen','nomen');
 		return $this->hasOne(Customer::class,'fa_transaction_id','fa_transaction_id');
 	}

 	public function fa_type()
 	{
 		return $this->hasOne(FA_type::class,'fa_type_cd','fa_type_cd');
 	}

 	public function fa_mapping()
 	{
 		return $this->hasOne(FA_mapping::class,'fa_type_cd','fa_type_cd');
 	}

 	public function worker()
    {
        return $this->belongsTo(Workers::class,'worker_id','id');
    }

 	public function fa_template_BOM()
    {
        return $this->belongsTo(FA_Template_BOM::class,'fa_type_cd','fa_type_cd');
    }

    public function fa_transaction_photo()
    {
    	require $this->belongsTo(FA_Transaction_photo::class, 'user_id','user_id');
    }

    public function fa_transaction_ttd()
    {
    	require $this->belongsTo(FA_Transaction_ttd::class, 'user_id','user_id');
    }


}
