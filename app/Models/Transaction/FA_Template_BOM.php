<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class FA_Template_BOM extends Model
{
    protected $table = 'fa_template_BOM';
    public function fa_template_BOM_detail()
    {
        return $this->hasMany(FA_Template_BOM_detail::class,'fa_template_BOM_id');
    }
}
