<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Workers;
use App\Models\Administrator\Wo_task;
use App\Models\Transaction\WO_Template_BOM;

class WO_transaction extends Model
{
    protected $table = 'wo_transaction';
    protected $fillable = ['created_at'];

    public function worker()
    {
        return $this->belongsTo(Workers::class,'worker_id','id');
    }

    public function wo_task()
 	{
 		return $this->hasOne(Wo_task::class,'job_code','jobCode');
 	}

 	public function wo_template_BOM()
    {
        return $this->belongsTo(WO_Template_BOM::class,'jobCode','jobCode');
    }
}
