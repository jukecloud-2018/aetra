<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class WO_Template_BOM extends Model
{
    protected $table = 'wo_template_BOM';

    public function wo_template_BOM_detail()
    {
        return $this->hasMany(WO_Template_BOM_detail::class,'wo_template_BOM_id');
    }
}
