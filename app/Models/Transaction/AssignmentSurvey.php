<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Administrator\Survey;
use App\Models\Administrator\Workers;


class AssignmentSurvey extends Model
{

    protected $table = 'survey_transaction';

    public function survey()
    {
    	return $this->belongsTo(Survey::class,'survey_id','id');
    }

    public function worker()
    {
        return $this->belongsTo(Workers::class,'worker_id','id');
    }
}
