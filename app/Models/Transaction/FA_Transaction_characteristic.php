<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;

class FA_Transaction_characteristic extends Model
{
    protected $table = 'fa_transaction_characteristic';
    protected $fillable = ['user_id', 'fa_transaction_id', 'fa_type_cd', 'name_characteristic', 'value_characteristic','sequence','char_type_cd','char_type_flg', 'created_by', 'updated_by'];

    public function transaction(){
        return $this->belongsTo(FA_transaction::class,'fa_transaction_id','fa_transaction_id');
    }
}
