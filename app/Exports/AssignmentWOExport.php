<?php

namespace App\Exports;

use App\Models\Transaction\WO_transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Session;

class AssignmentWOExport implements FromCollection, WithHeadings
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table('wo_transaction as a')
                ->where('a.assign_status', 'Open')
                ->select(
                    'a.plant',
                    'a.no_wo', 
                    'a.no_wo_task', 
                    'a.jobCode', 
                    'a.task_status', 
                    'a.asset_id', 
                    'a.worker_id', 
                    'a.assign_date', 
                    'a.reassign_date',
                    'a.assign_status', 
                    'a.no_account',
                    'a.no_specification',
                    'a.area',
                    'a.department',
                    'a.crew',
                    'a.urgent_status',
                    'a.priority_status', 
                    'a.sending_status')
                ->get(); 
        
        return $data;
    }

    public function headings():array
    {
        return[
            'plant',
            'no_wo',
            'no_wo_task',
            'jobCode',
            'task_status',
            'asset_id',
            'worker_id',
            'assign_date',
            'reassign_date',
            'assign_status',
            'no_account',
            'no_specification',
            'area',
            'department',
            'crew',
            'urgent_status',
            'priority_status',
            'sending_status'
        ];
    }
}
