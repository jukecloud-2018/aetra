<?php

namespace App\Exports;

use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;
use Session;

class AssignmentFAExportDate implements FromCollection, WithHeadings
{
    use Exportable;

    public function __construct($start_date, $end_date)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;

        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         $user = Session::get('userinfo'); 
         $data = DB::table('fa_transaction as a')
            ->whereNotIn('a.assign_status', ['Complete'])
            ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
            ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
            ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
            ->leftJoin('workers as e','a.worker_id','=','e.id')
            ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
            ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
            ->whereBetween('a.assign_date', [$this->start_date, $this->end_date])
            ->where('g.group_access_id', '=', $user['group_access_id'])
            ->select(
              'a.fa_transaction_id','a.nomen','a.dispatch_group','a.descriptions','a.fa_type_cd',
              'a.assign_date','a.reassigned_date','a.urgent_status','a.worker_id','a.assign_status',
              'a.priority_status','a.status','a.sending_status','b.customer_name','b.phone','b.address',
              'b.pcezbk','b.meterinfo','b.latitude','b.longitude','b.email','b.city','b.postal'
            )->get();
      return $data;
    }

    public function headings():array
    {
        return[
            'fa_transaction_id',
            'nomen',
            'dispatch_group',
            'descriptions',
            'fa_type_cd',
            'assign_date',
            'reassigned_date',
            'urgent_status',
            'worker_id',
            'assign_status',
            'priority_status',
            'status',
            'sending_status',
            'customer_name',
            'phone',
            'address',
            'pcezbk',
            'meterinfo',
            'latitude',
            'longitude',
            'email',
            'city',
            'postal'
        ];
    }
}
