<?php

namespace App\Exports;

use App\Models\Transaction\WO_transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use DB;
use Session;

class AssignmentWOExportDate implements FromCollection, WithHeadings
{
    use Exportable;

    public function __construct($start_date, $end_date)
    {
        $this->start_date = $start_date;
        $this->end_date = $end_date;

        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         $data = DB::table('wo_transaction as a')
                ->whereBetween('a.assign_date', [$this->start_date, $this->end_date])
                ->select(
                    'a.plant',
                    'a.no_wo', 
                    'a.no_wo_task', 
                    'a.jobCode', 
                    'a.task_status', 
                    'a.asset_id', 
                    'a.worker_id', 
                    'a.assign_date', 
                    'a.reassign_date',
                    'a.assign_status', 
                    'a.no_account',
                    'a.no_specification',
                    'a.area',
                    'a.department',
                    'a.crew',
                    'a.urgent_status',
                    'a.priority_status', 
                    'a.sending_status')
                ->get();

        return $data;
    }

    public function headings():array
    {
        return[
            'plant',
            'no_wo',
            'no_wo_task',
            'jobCode',
            'task_status',
            'asset_id',
            'worker_id',
            'assign_date',
            'reassign_date',
            'assign_status',
            'no_account',
            'no_specification',
            'area',
            'department',
            'crew',
            'urgent_status',
            'priority_status',
            'sending_status'
        ];
    }
}
