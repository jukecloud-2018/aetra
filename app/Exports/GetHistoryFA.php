<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class GetHistoryFA implements FromCollection, WithHeadings
{
    public function __construct(string $fa_id)
    {
        $this->fa_id = $fa_id;
    }

    public function collection()
    {
        $data = DB::table('log_fa_status')
                ->select('fa_transaction_id','status','message','created_at')
                ->where('fa_transaction_id','=', $this->fa_id)->get();
        return $data;
    }

    public function headings():array
    {
        return[
            'fa_transaction_id',
            'status',
            'message',
            'date_log'
        ];
    }
}
