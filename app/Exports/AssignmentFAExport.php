<?php

namespace App\Exports;

use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\Customer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use Session;

class AssignmentFAExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      /*
          Export menggunakan join table dengan menggunakan Query builder
      */
      //$data = DB::table('fa_transaction')
              // ->join('customer', 'fa_transaction.fa_transaction_id', '=', 'customer.fa_transaction_id')
      //       ->select('fa_transaction.nomen', 'fa_transaction.dispatch_group', 'fa_transaction.dispatch_group', 'fa_transaction.fa_transaction_id', 'fa_transaction.fa_type_cd', 'fa_transaction.status', 'fa_transaction.created_at', 'fa_transaction.worker_id', 'fa_transaction.assign_date', 'fa_transaction.reassigned_date', 'fa_transaction.assign_status', 'fa_transaction.urgent_status', 'fa_transaction.priority_status', 'fa_transaction.sending_status')
      //       ->get();

      // $getaccess = DB::table('group_access as a')
        //        ->join('fa_group_access as b','a.id','=','b.group_access_id')
      //          ->where('a.id','=', $user['group_access_id'])
      //          ->select('a.id', 'a.group_name', 'b.dispatch_group')->get();
    //   $sql = DB::table('fa_transaction as a');
    //             foreach ($getaccess as $value) {
    //               $sql->orWhere('dispatch_group',$value->dispatch_group);
    //             }
    //   $sql->where('a.assign_status','<>', 'Complete')
    //       ->where('a.assign_status','<>', 'Started')
    //       ->select(
    //                 'a.nomen', 'a.dispatch_group', 'a.fa_transaction_id', 'a.fa_type_cd', 
    //                 'a.status', 'a.created_at', 'a.worker_id', 'a.assign_date', 'a.reassigned_date',
    //                 'a.assign_status', 'a.urgent_status', 'a.priority_status', 'a.sending_status'
    //               );
    //   $data = $sql->get();
      
      $user = Session::get('userinfo'); 
      $data = DB::table('fa_transaction as a')
            ->whereNotIn('a.assign_status', ['Complete'])
            ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
            ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
            ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
            ->leftJoin('workers as e','a.worker_id','=','e.id')
            ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
            ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
            ->where('g.group_access_id', '=', $user['group_access_id'])
            ->select(
              'a.fa_transaction_id','a.nomen','a.dispatch_group','a.descriptions','a.fa_type_cd',
              'a.assign_date','a.reassigned_date','a.urgent_status','a.worker_id','a.assign_status',
              'a.priority_status','a.status','a.sending_status','b.customer_name','b.phone','b.address',
              'b.pcezbk','b.meterinfo','b.latitude','b.longitude','b.email','b.city','b.postal'
            )->get();
      
      return $data;
    }

    public function headings():array
    {
        return[
            'fa_transaction_id',
            'nomen',
            'dispatch_group',
            'descriptions',
            'fa_type_cd',
            'assign_date',
            'reassigned_date',
            'urgent_status',
            'worker_id',
            'assign_status',
            'priority_status',
            'status',
            'sending_status',
            'customer_name',
            'phone',
            'address',
            'pcezbk',
            'meterinfo',
            'latitude',
            'longitude',
            'email',
            'city',
            'postal'
        ];
    }
}
