<?php

namespace App\Http\Middleware;

use Closure;
use Cache;
use Session;
use Illuminate\Http\Request;

class TokenAllMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Session::get('userinfo'))
        {
            return redirect('/login');
        }
        else
        {
            $userinfo = Session::get('userinfo');
            $acces = Session::get('accessModule');
            $moduleUrl = \Request::segment(1).'/'.\Request::segment(2);


            if(!empty($acces))
            {
                if(empty($acces[$userinfo['user_id']][$moduleUrl]))
                {
                    return redirect('/home'); //awalnya dulu /dashboard
                }
            }
            else
            {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
