<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\Message;
use App\Models\Transaction\Message_To;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Validator;
use Session;
use DB;

use App\Models\Administrator\Workers;
use App\Models\Administrator\Users;



class MessagingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = Session::get('userinfo');
      $data['message'] = DB::table('messages as a')
                        //->join('message_to as b','a.id_send','=','b.id_send')
                        //->join('workers as c','b.worker_id','=','c.worker_id')
                        //->join('worker_dispatch as d','b.id','=','d.worker_id')
                        //->join('fa_group_access as e', 'd.dispatch_group', '=', 'e.dispatch_group')
                        //->select('a.id', 'a.id_send', 'b.worker_id',  'c.worker_name', 'a.subject', 'a.message', 'a.status', 'a.created_at')
                        //->where('worker_status', '=', 'Active')
                        //->where('e.group_access_id', '=', $user['group_access_id'])
                        ->orderBy('a.created_at', 'desc')
                        ->where('a.status', '=', 'Send')
                        ->get();
      $data['kesiapa'] = DB::table('message_to');
      return view('transaction.messaging.v_index_messaging', $data);
    }

    public function draf($stat)
    {
      if($stat == 'draf'){
        
      $user = Session::get('userinfo');
      $data['stat'] = 'draf';
      $data['message'] = DB::table('messages as a')
                        //->join('workers as b','a.worker_id','=','b.worker_id')
                        //->join('users as c','b.user_id','=','c.id')
                        //->join('worker_dispatch as d','b.id','=','d.worker_id')
                        //->join('fa_group_access as e', 'd.dispatch_group', '=', 'e.dispatch_group')
                        //->select('a.id', 'a.worker_id',  'b.worker_name', 'a.subject', 'a.message', 'a.status', 'a.created_at')
                        //->where('worker_status', '=', 'Active')
                        ->orderBy('a.created_at', 'desc')
                        ->where('a.status', '=', 'Draf')
                        //->where('e.group_access_id', '=', $user['group_access_id'])
                        ->get();
      return view('transaction.messaging.v_draf_messaging', $data);
      }else{
        return redirect()->route('transaction.messaging.index');
      }
    }

    public function compose()
    {
      $user = Session::get('userinfo');
      $data['worker'] = DB::table('workers as a')
                          ->join('worker_dispatch as c','a.id','=','c.worker_id')
                          ->join('fa_group_access as d', 'c.dispatch_group', '=', 'd.dispatch_group')
                          ->where('worker_status', '=', 'Active')
                          ->where('d.group_access_id', '=', $user['group_access_id'])
                          ->select('a.worker_id','a.worker_name','a.user_id')
                          ->get();
	    return view('transaction.messaging.v_compose',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(),[
        // 'message'=>'required',
        'subject'=>'required',
      ]);

      if($validator->fails())
      {
        return response()->json(['errors'=>$validator->errors()->all()]);
      }

      if($request->allGroup == "all"){
        $user   = Session::get('userinfo');
        $worker = DB::table('workers as a')
                          ->join('users as b','a.user_id','=','b.id')
                          ->join('worker_dispatch as c','a.id','=','c.worker_id')
                          ->join('fa_group_access as d', 'c.dispatch_group', '=', 'd.dispatch_group')
                          ->where('worker_status', '=', 'Active')
                          ->where('d.group_access_id', '=', $user['group_access_id'])
                          ->select('a.worker_id','a.worker_name','a.user_id')
                          ->get();
        // echo json_encode($worker);
        // exit();

        //save message            
        $messages = new Message;
        $messages->subject = $request->subject;
        $messages->message = $request->message;
        $messages->status = $request->status;
        $messages->status_all = '1';
        $messages->save();
        $LastInsertId = $messages->id;

        //seva message to
        foreach($worker as $value){
          $messages = DB::table('message_to')->insert(['id_send'=>$LastInsertId, 'worker_id'=> $value->worker_id,'user_id'=>$value->user_id, 'status'=> 'Send']);
        }
      }else{
        //save message
        $worker = Workers::where('worker_id', $request->worker_id)->first();

        $messages = new Message;
        $messages->subject = $request->subject;
        $messages->message = $request->message;
        $messages->status = $request->status;
        $messages->status_all = '0';
        $messages->save();
        $LastInsertId = $messages->id;

        //save message_to
        $messages = DB::table('message_to')->insert(['id_send'=>$LastInsertId, 'worker_id'=> $request->worker_id,'user_id'=>$worker->user_id, 'status'=> 'Send']);
        }

      if($messages)
      {
        return response()->json(['status'=>'success']);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['getData']=  DB::table('messages as a')->join('message_to as b', 'a.id','b.id_send')->where('a.id', $id)->first();
      $data['worker'] = DB::table('workers as a')->join('users as b','a.user_id','=','b.id')->select('*')->get();
      return view('transaction.messaging.v_edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      
      $validator = Validator::make($request->all(),[
        'subject'=>'required',  
      ]);

      if($validator->fails())
      {
        return response()->json(['errors'=>$validator->errors()->all()]);
      }
      
      $message =  Message_To::where('id_send', $request->id)->delete();
      $message =  Message::where('id', $request->id)->delete();
      
       /*$data = array(
        'subject' => $request->subject,
        'message' => $request->message,
        'status' => $request->status,
        'worker_id'=> $request->worker_id
      );
      print_r($data);*/
      
      if($request->allGroup == "all"){
        $user   = Session::get('userinfo');
        $worker = DB::table('workers as a')
                          ->join('users as b','a.user_id','=','b.id')
                          ->join('worker_dispatch as c','a.id','=','c.worker_id')
                          ->join('fa_group_access as d', 'c.dispatch_group', '=', 'd.dispatch_group')
                          ->where('worker_status', '=', 'Active')
                          ->where('d.group_access_id', '=', $user['group_access_id'])
                          ->select('a.worker_id','a.worker_name','a.user_id')
                          ->get();
        //save message            
        $messages = new Message;
        $messages->subject = $request->subject;
        $messages->message = $request->message;
        $messages->status = $request->status;
        $messages->status_all = '1';
        $messages->save();
        $LastInsertId = $messages->id;

        //seva message to
        foreach($worker as $value){
          $messages = DB::table('message_to')->insert(['id_send'=>$LastInsertId, 'worker_id'=> $value->worker_id,'user_id'=>$value->user_id, 'status'=> 'Send']);
        }
      }else{
        //save message
        $worker = Workers::where('worker_id', $request->worker_id)->first();

        $messages = new Message;
        $messages->subject = $request->subject;
        $messages->message = $request->message;
        $messages->status = $request->status;
        $messages->status_all = '0';
        $messages->save();
        $LastInsertId = $messages->id;

        //save message_to
        $messages = DB::table('message_to')->insert(['id_send'=>$LastInsertId, 'worker_id'=> $request->worker_id,'user_id'=>$worker->user_id,'status'=> 'Send']);
      }

      if($messages)
      {
        return response()->json(['status'=>'success']);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
      $message =  Message_To::where('id_send', $request->id)->delete();
      $message =  Message::where('id', $request->id)->delete();
      if($message){
        return response()->json(['status'=>'success']);    
      }
    }

    public function detail($id)
    {
      $data['message'] = DB::table('messages as a')
                        //->join('message_to as b','a.id_send','=','b.id_send')
                        //->join('workers as c','b.worker_id','=','c.worker_id')
                        ->where('a.id', $id)
                        //->select('a.id', 'a.worker_id',  'b.worker_name', 'a.subject', 'a.message', 'a.status', 'a.created_at')
                        ->get();
      return view('transaction.messaging.v_detail', $data);
    }
}