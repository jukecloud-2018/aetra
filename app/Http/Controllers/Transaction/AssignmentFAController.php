<?php
namespace App\Http\Controllers\Transaction;
use App\Exports\AssignmentFAExport;
use App\Exports\AssignmentFAExportDate;
use App\Imports\AssignmentFAImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\Customer;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Redirect;

include_once(app_path() . '/MyLibs/nusoap.php');

class AssignmentFAController extends Controller
{
	/**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
	public function index(Request $request)
	{
		//just remove start date and end date in session
        $request->session()->forget('start');
        $request->session()->forget('end');

		//query fa_transaction berdasarkan dispatch_group
		$user = Session::get('userinfo');
		//GET WORKER
		$show['workers'] = DB::table('workers as a')
					->join('worker_dispatch as b','a.id','=','b.worker_id')
					->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
					->where('status', '=', 'fa')
					->where('worker_status', '=', 'Active')
					->where('c.group_access_id', '=', $user['group_access_id'])
					->get();

		$show['getWorkers'] = DB::table('worker_dispatch as a')
					->leftjoin('workers as b','a.worker_id','=','b.id')
					->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
					->distinct('a.worker_id')
					->select('a.worker_id', 'b.worker_name')
					->where('worker_status', '=', 'Active')
					->where('c.group_access_id', '=', $user['group_access_id'])
					->get();
		// END GET WORKER

		if($user['role_id'] == 1){
			$show['open'] = FA_transaction::where('assign_status','=','Open')->count();
			$show['assigned'] = FA_transaction::where('assign_status','=','Assigned')->count();
			$show['reAssigned'] = FA_transaction::where('assign_status','=','Re-Assigned')->count();
			$show['ontheway'] = FA_transaction::where('assign_status','=','On The Way')->count();
			$show['pickup'] = FA_transaction::where('assign_status','=','Pick Up')->count();
			$show['started'] = FA_transaction::where('assign_status','=','Started')->count();
			$show['cancel'] = FA_transaction::where('assign_status','=','Cancel Worker')->count();
			$show['complete'] = FA_transaction::where('assign_status','=','Complete')->count();
			$show['total'] = FA_transaction::count();
		}else{
	  		//open
			$open = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Open')
				->get();
			//assigned
			$assigned = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Assigned')
				->get();
			//assigned
			$reassigned = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Re-Assigned')
				->get();
			//ontheway
			$ontheway = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'On The Way')
				->get();
			//pickup
			$pickup = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Pick Up')
				->get();
			//started
			$started = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Started')
				->get();
			//cancel worker
			$cancel = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Cancel Worker')
				->get();
			//complete
			$complete = DB::table('fa_transaction as a')
				->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
				->distinct()
				->select('a.*')
				->where('b.group_access_id', '=', $user['group_access_id'])
				->where('assign_status', '=', 'Complete')
				->get();
			//total
			$data = DB::table('fa_transaction as a')
				->whereNotIn('a.assign_status', ['Complete'])
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->get();
		}

		$show['data'] = DB::table('fa_transaction as a')
			->whereNotIn('a.assign_status', ['Complete'])
			->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
			->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
			->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
			->leftJoin('workers as e','a.worker_id','=','e.id')
			->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
			->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
			->where('g.group_access_id', '=', $user['group_access_id'])
			->select(
			  'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
			  'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
			  'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description'
			)
			->orderBy('a.updated_at', 'desc')
			->paginate(300);

		$limit = 300;//limit untuk continue page number
		$show['num'] = num_row($request->input('page'), $limit);
		$show['open'] = count($open);
		$show['assigned'] = count($assigned);
		$show['reAssigned'] = count($reassigned);
		$show['ontheway'] = count($ontheway);
		$show['pickup'] = count($pickup);
		$show['started'] = count($started);
		$show['complete'] = count($complete);
		$show['cancel'] = count($cancel);
		$show['total'] = count($data);
		return view('transaction.assignmentFA.v_index_assignmentFA', $show);
	}


	public function getData(Request $request)
	{
		if($request->startDateTrans == null or $request->endDateTrans == null){
			return redirect()->route('transaction.assignmentFA.index');
		}else{
			$user = Session::get('userinfo');
			$start_date = Carbon::createFromFormat('d-m-Y', $request->startDateTrans)->format('Y-m-d');
			$end_date = Carbon::createFromFormat('d-m-Y', $request->endDateTrans)->format('Y-m-d');

			//put it in session?
      		Session::put('start', $start_date);
      		Session::put('end', $end_date);

			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER
			//total row
			$total = DB::table('fa_transaction as a')
				->whereNotIn('a.assign_status', ['Complete'])
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->whereBetween('a.assign_date', [$start_date, $end_date])
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->whereNotIn('a.assign_status', ['Complete'])
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->whereBetween('a.assign_date', [$start_date, $end_date])
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['cariDari'] = $start_date;
			$show['cariSmp'] = $end_date;
			$show['ket'] = 'Search Date';
			return view('transaction.assignmentFA.v_search_assignmentFA', $show);
		}
	}

	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/

	public function refresh_all_fa(){
        $url = "http://172.27.1.38:8280/services/DS_Get_Pending_FA?wsdl";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // cek koneksi url
        if($httpcode>=200 && $httpcode<300){
//            $file = storage_path('app/date.log');
//            $getDateLog = fgets(fopen($file, 'r'));
//            $getDateLog = trim($getDateLog);
            $getDate = DB::table('fa_transaction')
                        ->select(DB::raw('max(created_at) as created_at'))
                        ->get();
           $getMaxDate = $getDate[0]->created_at;
           $maxDate = substr($getDate[0]->created_at, 0, -4);
           $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
            //$getDate = '26/02/2018 01:01:01';
            /** Function get data FA Pending from SIMPEL*/
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:get="http://aetra.co.id/getPendingFa">
                  <soapenv:Header/><soapenv:Body><get:getPendingFa><get:pDate>'.$getDate.'</get:pDate></get:getPendingFa></soapenv:Body>
                  </soapenv:Envelope>';
            $headers = array(
                "Content-Type: text/xml;charset=UTF-8",
                "SOAPAction:urn:getPendingFa",
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER =>  $headers,
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);

            if ($err) {
                $error = "cURL Error #:" . $err;
                Storage::prepend('error.log', $error);
            } else {
                /** insert data to database */
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($getString ,"",-34);
                $getXML = simplexml_load_string($getString);
                $json  = json_encode($getXML);
                $configData = json_decode($json, true);
                $path = storage_path('app/getPandingFA.json');
                unlink($path);
                Storage::prepend('getPandingFA.json', $json);
                foreach($configData as $value){
                    $insert_faTransaction = [];
                    $insert_customer      = [];
                    $update_FAtransaction = [];
                    $update_Customer      = [];
                    foreach($value as $row){
                        //Data FA Transaction and Customer
                        $nomen = trim($row['nomen']);
                        $customer_name = trim($row['entityName']);
                        $phone = encodePost($row['mobilePhone']);
                        $email = encodePost($row['emailId']);
                        if($email == '[ ]'){
                            $email = '';
                        }else{
                            $email = $row['emailId'];
                        }
                        $city = encodePost($row['city']);
                        if($city == '[ ]'){
                            $city = '';
                        }else{
                            $city = $row['city'];
                        }
                        $postal = encodePost($row['postal']);
                        $pcezbk = trim($row['pcezbk']);
                        $meterinfo = trim($row['meterInfo']);
                        $latitude = encodePost($row['gpslat']);
                        $longitude = encodePost($row['gpslong']);
                        $status = trim($row['custStatus']);
                        $date_aetra = strtr($row['createDate'], '/', '-');

                        //split created_at
                        if(isset($date_aetra)){
                            $created = date("Y-m-d h:i:s.u", strtotime($date_aetra));
                        }else{
                            $created = date("Y-m-d h:i:s.u", strtotime(now()));
                        }
                        $updated = date("Y-m-d h:i:s.u", strtotime(now()));

                        //split address
                        $get_rt = trim($row['RT']);
                        $rt = $get_rt ? $get_rt : '';
                        $get_rw = trim($row['RW']);
                        $rw = $get_rw ? $get_rw : '';

                        $get_address = trim($row['address']);
                        if($get_address){
                            $split_jl = explode("|", $get_address);
                            $jl = $split_jl[0];
                            $hasil_jl = $jl ? $jl : '';
                            $name_jl = $split_jl[1];
                            $hasil_name_jl = $name_jl ? $name_jl : '';
                            $gang = $split_jl[2];
                            $hasil_gang = $gang ? $gang : '';

                            if(isset($split_jl[3])){
                                $hasil_no = $split_jl[3];
                            }else{
                            $hasil_no = '';
                            }
                            $address = $hasil_jl." ".$hasil_name_jl." ".$hasil_gang." ".$hasil_no." RT ".$rt."RW ".$rw;
                        }else{
                            $address = '';
                        }

                        //insert data to fa_transaction
                        $findCus = FA_transaction::where('fa_transaction_id',trim($row['faId']))->first();
                        if(!$findCus){
                            $faTransaction = [
                                'fa_transaction_id' => trim($row['faId']),
                                'nomen'             => $nomen,
                                'dispatch_group'    => trim($row['dispatchGrp']),
                                'descriptions'      => trim($row['descr']),
                                'fa_type_cd'        => trim($row['faTypeCd']),
                                'sending_status'    =>'Pending',
                                'assign_status'     => 'Open',
                                'status'            => trim($row['statusFlag']),
                                'created_at'        => substr($created, 0, -3),
                                'updated_at'        => substr($updated, 0, -3)
                            ];
                            $insert_faTransaction = collect($faTransaction);
                            $insert_faTransaction = $insert_faTransaction->chunk(500);
                            foreach ($insert_faTransaction as $row1){
                                DB::disableQueryLog();
                                DB::table('fa_transaction')->insert($row1->toArray());
                            }
                        }
                        $findCus = Customer::where('fa_transaction_id',trim($row['faId']))->first();
                        if(!$findCus){
                            $customer =[
                                'fa_transaction_id' => trim($row['faId']),
                                'nomen'         => trim($row['nomen']),
                                'customer_name' => trim($row['entityName']),
                                'phone'         => $phone,
                                'email'         => $email,
                                'address'       => $address,
                                'city'          => $city,
                                'postal'        => $postal,
                                'pcezbk'        => $pcezbk,
                                'meterinfo'     => $meterinfo,
                                'latitude'      => $latitude,
                                'longitude'     => $longitude,
                                'status'        => $status
                            ];
                            $insert_customer = collect($customer);
                            $insert_customer = $insert_customer->chunk(500);
                            foreach ($insert_customer as $row2){
                                DB::disableQueryLog();
                                DB::table('customer')->insert($row2->toArray());
                            }
                        }else{
                            FA_transaction::where('fa_transaction_id', trim($row['faId']))
                                ->update([
                                    'nomen' => trim($row['nomen']),
                                    'dispatch_group' => trim($row['dispatchGrp']),
                                    'fa_type_cd' => trim($row['faTypeCd']),
                                    'status' => trim($row['statusFlag'])
                                ]);

                            Customer::where('fa_transaction_id', trim($row['faId']))
                                ->update([
                                    'nomen'         => trim($row['nomen']),
                                    'phone'         => $phone,
                                    'address'       => $address,
                                    'email'         => $email,
                                    'city'          => $city,
                                    'postal'        => $postal,
                                    'pcezbk'        => $pcezbk,
                                    'meterinfo'     => $meterinfo,
                                    'latitude'      => $latitude,
                                    'longitude'     => $longitude,
                                    'status'        => $status
                                ]);
                        }
                    }
                  //buat query max date di fa_transaction//
//                  $maxDate = DB::table('fa_transaction as a')->select('created_at')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
//                  $maxDate = $maxDate->created_at;
//                  $maxDate = substr($maxDate, 0, -4);
//                  $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
//                  Storage::prepend('date.log', $getDate);//create log date -> Storage/app/date.log
                  return response()->json([ 'data' => [ 'status' => 'Update' ] ]);
                }
            }
        }else{
          return response()->json([ 'data' => [ 'status' => 'offline' ] ]);
        }
	}

	/*  public function refresh_all_fa()
	{
	//get akses user dispatchgroup
	$user = Session::get('userinfo');

	//get datetime log
	$file = storage_path('app/date.log');
	$getDateLog = fgets(fopen($file, 'r'));
	$getDateLog = trim($getDateLog);

	$url = "http://172.27.1.38:8280/services/DS_Get_Pending_FA?wsdl";
	if(!filter_var($url, FILTER_VALIDATE_URL))
	{
		print_r("Site is offline");
	}else{
	  $client = new \nusoap_client($url, 'wsdl');
	  $pDate = isset($_GET["pDate"]) ? $_GET["pDate"] : $getDateLog;
	  $operation = 'getPendingFa';
	  $param1 = array( 'pDate' => $pDate, );
	  $hasil1 = $client->call($operation, $param1);
	  foreach ($hasil1 as $key => $value){
		foreach($value as $row){
		  $findCus = FA_transaction::where('fa_transaction_id',trim($row['faId']))->first();
		  if(!$findCus){
			$fa_transaction = new FA_transaction;
			$fa_transaction->fa_transaction_id = trim($row['faId']);
			$fa_transaction->nomen = trim($row['nomen']);
			$fa_transaction->dispatch_group = trim($row['dispatchGrp']);
			$fa_transaction->descriptions = trim($row['descr']);
			$fa_transaction->fa_type_cd = trim($row['faTypeCd']);
			$fa_transaction->sending_status = 'Pending';
			$fa_transaction->assign_status = 'Open';
			$fa_transaction->status = trim($row['statusFlag']);
			$date_aetra = trim($row['createDate']);

			//split created_at
			if(isset($date_aetra)){
			  $convert = Carbon::createFromFormat('d/m/Y H:i:s', $date_aetra);
			  $fa_transaction->created_at = $convert;
			}else{
			  $fa_transaction->created_at = '';
			}
			$fa_transaction->save();
		  }

		  //split address
		  $get_rt = trim($row['RT']);
		  $rt = $get_rt ? $get_rt : '';
		  $get_rw = trim($row['RW']);
		  $rw = $get_rw ? $get_rw : '';

		  $get_address = trim($row['address']);
		  if($get_address){
			$split_jl = explode("|", $get_address);
			$jl = $split_jl[0];
			$hasil_jl = $jl ? $jl : '';
			$name_jl = $split_jl[1];
			$hasil_name_jl = $name_jl ? $name_jl : '';
			$gang = $split_jl[2];
			$hasil_gang = $gang ? $gang : '';

			if(isset($split_jl[3])){
			  $hasil_no = $split_jl[3];
			}else{
			  $hasil_no = '';
			}
			$address = $hasil_jl." ".$hasil_name_jl." ".$hasil_gang." ".$hasil_no." RT ".$rt."RW ".$rw;
		  }else{
			$address = '';
		  }

		  //$findCus = Customer::where('nomen',$value['nomen'])->first();
		  $findCus = Customer::where('fa_transaction_id',trim($row['faId']))->first();
		  if(!$findCus)
		  {
			$customer = new Customer;
			$customer->fa_transaction_id = trim($row['faId']);
			$customer->nomen = trim($row['nomen']);
			$customer->customer_name = trim($row['entityName']);
			$customer->phone = trim($row['mobilePhone']);
			$customer->address = $address;
			$customer->email = trim($row['emailId']);
			$customer->city = trim($row['city']);
			$customer->postal = trim($row['postal']);
			$customer->pcezbk = trim($row['pcezbk']);
			$customer->meterinfo = trim($row['meterInfo']);
			$customer->latitude = trim($row['gpslat']);
			$customer->longitude = trim($row['gpslong']);
			$customer->status = trim($row['custStatus']);
			$customer->save();
		  }else{
			//Update data
			FA_transaction::where('fa_transaction_id', trim($row['faId']))
							->update([
								'nomen' => trim($row['nomen']),
								'dispatch_group' => trim($row['dispatchGrp']),
								'fa_type_cd' => trim($row['faTypeCd']),
								'status' => trim($row['statusFlag'])
							]);

			Customer::where('fa_transaction_id', trim($row['faId']))
					  ->update([
						  'nomen' => trim($row['nomen']),
						  'phone' => trim($row['mobilePhone']),
						  'address' => $address,
						  'email' => trim($row['emailId']),
						  'city' => trim($row['city']),
						  'postal' => trim($row['postal']),
						  'pcezbk' => trim($row['pcezbk']),
						  'meterinfo' => trim($row['meterInfo']),
						  'latitude' => trim($row['gpslat']),
						  'longitude' => trim($row['gpslong']),
						  'status' => trim($row['custStatus'])
					  ]);
		  }
		}
		//buat query max date di fa_transaction//
		$maxDate = DB::table('fa_transaction as a')->select('created_at')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
		$maxDate = $maxDate->created_at;
		$maxDate = substr($maxDate, 0, -4);
		$getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
		Storage::prepend('date.log', $getDate);//create log date -> Storage/app/date.log
		return response()->json([ 'data' => [ 'status' => 'Update' ] ]);
	  }
	}
	}
	*/
	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function create()
	{
	  //
	}

	public function multiSaveWorker(Request $request)
	{
		// print_r($request->worker);
		$cek = $_POST['checkFA'];
		$user = Session::get('userinfo');
		foreach ($cek as $value => $get) {
			$data = FA_transaction::find($get);
			$data->worker_id = $request->worker;
			if($data->assign_date == null){
				$data->assign_date = date('Y-m-d H:i:s');
				$data->assign_status = 'Assigned';
				$data->sending_status = 'Recieved';
			}else{
				$data->reassigned_date = date('Y-m-d H:i:s');
				$data->assign_status = 'Re-Assigned';
				$data->sending_status = 'Recieved';
			}
			$data->save();
		}
		
		if($data)
		{
			return response()->json([
				'status'=>'success',
				'data' => [
					'assign' => $data->assign_date,
					'reassigned' => $data->reassigned_date,
				]
			]);
		}

	}

	public function saveWorker(Request $request,$id)
	{
		$data = FA_transaction::find($id);
		$data->worker_id = $request->worker;
		if($data->assign_date == null){
			$data->assign_date = date('Y-m-d H:i:s');
			$data->assign_status = 'Assigned';
			$data->sending_status = 'Recieved';
		}else{
			$data->reassigned_date = date('Y-m-d H:i:s');
			$data->assign_status = 'Re-Assigned';
			$data->sending_status = 'Recieved';
		}
		$data->save();

		if($data)
		{
			return response()->json([
				'status'=>'success',
				'data' => [
					'assign' => $data->assign_date,
					'reassigned' => $data->reassigned_date,
				]
			]);
		}
	}

	public function getWorkerOne(Request $request,$id)
	{
	$data = FA_transaction::where('id',$id)->first();
	}

	public function saveUrgent(Request $request,$id)
	{
	  $data = FA_transaction::find($id);

	  $data->urgent_status = $request->urgent_status;

	  $data->save();

	  if($data)
	  {
		  return response()->json(['status'=>'success']);
	  }

	}
	public function savePriority(Request $request,$id)
	{
	$data = FA_transaction::find($id);
	$data->priority_status = $request->priority;
	$data->save();
	if($data)
	{
		return response()->json(['status'=>'success']);
	}
	}

	public function saveAssignDate(Request $request,$id)
	{
		$data = FA_transaction::find($id);
		$data->assign_date =  Carbon::createFromFormat('d-m-Y', $request->assignDate)->format('Y-m-d');
		$data->save();
		if($data){
			return response()->json(['status'=>'success']);
		}
	}

	/*public function saveReAssignedDate($id)
	{
	  $data = FA_transaction::find($id);
	  $data->reassigned_date = date('Y-m-d H:i:s');
	  $data->assign_status = 'Re-Assigned';
	  $data->save();

	  if($data)
	  {
		  return response()->json(['status'=>'success']);
	  }
	}*/

	public function getWorker()
	{
	  $worker = DB::table('users as a')->join('workers as b','a.id','=','b.user_id')->select('*')->get();

	  return response()->json(['worker'=>$worker]);
	}

	public function export($type)
	{
		//return Excel::download(new AssignmentFAExport, 'assignmentFA.xlsx');
		//$fa_transaction = FA_transaction::get()->toArray();
		// $now = date("Ymd H:i:s");
		// $startDate = Carbon::createFromFormat('d-m-Y', '31-12-2016')->format('Y-m-d');
		// $export = new AssignmentFAExport();
		// $export->setDate(2019);
		// return Excel::download(new AssignmentFAExport, 'assignmentFA'.$now.'.xlsx');
		// return Excel::download($export, 'users.xlsx');


	 	$startDate = Session::get('start');
	    $endDate = Session::get('end');
	    if ($startDate && $endDate) {
	      $now = date("Ymd H:i:s");
	      return Excel::download(new AssignmentFAExportDate($startDate,$endDate), 'assignmentFA-'.$now.'.xlsx');
	    }else{
	      $now = date("Ymd H:i:s");
	      return Excel::download(new AssignmentFAExport(), 'assignmentFA-'.$now.'.xlsx');
	    }
	}

	public function import(){
		$data = Excel::toArray(new AssignmentFAImport, request()->file('file'));
		if ($data) {
		  	collect(head($data))
		  	->each(function ($row, $key) {
		  		//cek data jika ada apa tidak
		  		// echo json_encode($row);
		  		// exit();
		  		$cek = FA_transaction::where('fa_transaction_id', $row['fa_transaction_id'])->get();
			  	//jika hasilnya null maka create baru
			  	//jika tidak null update
		  		if ($cek == '[]') {
		  			$new_fa = new FA_transaction();
		  			$new_fa->fa_transaction_id = $row['fa_transaction_id'];
		  			$new_fa->nomen = $row['nomen'];
				    $new_fa->dispatch_group = $row['dispatch_group'];
				    $new_fa->descriptions = $row['descriptions'];
				    $new_fa->fa_type_cd = $row['fa_type_cd'];
				    $new_fa->assign_date = Carbon::now();
				    $new_fa->reassigned_date = null;
				    $new_fa->urgent_status = $row['urgent_status'];
				    $new_fa->worker_id = $row['worker_id'];
				    $new_fa->assign_status = $row['assign_status'];
				    $new_fa->priority_status = $row['priority_status'];
				    $new_fa->status = $row['status'];
				    $new_fa->sending_status = $row['sending_status'];
				    $new_fa->created_at = Carbon::now();
				    $new_fa->save();

				    $new_cus = new Customer();
				    $new_cus->fa_transaction_id = $row['fa_transaction_id'];
				    $new_cus->nomen = $row['nomen'];
				    $new_cus->customer_name = $row['customer_name'];
				    $new_cus->phone = $row['phone'];
				    $new_cus->address = $row['address'];
				    $new_cus->pcezbk = $row['pcezbk'];
				    $new_cus->meterinfo = $row['meterinfo'];
				    $new_cus->latitude = $row['latitude'];
				    $new_cus->longitude = $row['longitude'];
				    $new_cus->email = $row['email'];
				    $new_cus->city = $row['city'];
				    $new_cus->postal = $row['postal'];
				    $new_cus->save();
		  		}else{

		  			//update table fa_transaction
			  		DB::table('fa_transaction')
					  ->where('fa_transaction_id', $row['fa_transaction_id'])
					  ->update([
					  		'fa_transaction_id'=>$row['fa_transaction_id'],'nomen'=>$row['nomen'], 
					  		'dispatch_group'=>$row['dispatch_group'],'descriptions'=>$row['descriptions'],
					  		'fa_type_cd'=>$row['fa_type_cd'],'assign_date'=>$row['assign_date'],
					  		'reassigned_date'=>$row['reassigned_date'],'urgent_status'=>$row['urgent_status'],
					  		'worker_id'=>$row['worker_id'],'assign_status'=>$row['assign_status'],
					  		'priority_status'=>$row['priority_status'],'status'=>$row['status'],
					  		'sending_status'=>$row['sending_status'],'updated_at'=>Carbon::now(),
					  	]);

					//update table customer
					 DB::table('customer')
					 	->where('fa_transaction_id', $row['fa_transaction_id'])
					 	->update([
					 		'fa_transaction_id'=>$row['fa_transaction_id'],'nomen' => $row['nomen'],
						    'customer_name'=>$row['customer_name'],'phone'=>$row['phone'],
						    'address'=>$row['address'],'pcezbk'=>$row['pcezbk'],
						    'meterinfo'=>$row['meterinfo'],'latitude'=>$row['latitude'],
						    'longitude'=>$row['longitude'],'email'=>$row['email'],'city'=>$row['city'],
						    'postal'=>$row['postal']
					 	]);
		  		}
		  	});
		return Redirect::to('transaction/assignmentFA');
		}
	}

	public function getDataByFilter(Request $request)
	{
	  $input_name = $request->name_search;
	  if($request->option_checked == 'fa_type')
	  {
		  $data = FA_transaction::with('customer')
			  ->with('fa_type.fa_type_lang')
			  ->where('fa_type_cd','like','%'.$input_name.'%')
			  ->get();
	  }else{
		  $data = FA_transaction::with('customer')
			  ->with('fa_type.fa_type_lang')
			  ->whereHas('customer', function ($query) use ($input_name){
				  $query->where('pcezbk','like','%'.$input_name.'%');
			  })
			  ->get();
	  }
	  return Datatables::of($data)
		  ->editColumn('descr', function($data) {
			  return $data->fa_type->fa_type_lang->descr;
		  })
		  ->editColumn('created_date_fa', function($data) {
			  return $data->created_at;
		  })
		  /*->editColumn('pcezbk', function($data) {
			  return $data->customer->pcezbk ? $data->customer->pcezbk : '';
		  })*/
		  ->editColumn('assign_date', function($data) {
			  $assign_date = ($data->assign_date==null) ? "" : date('d-m-Y H:i:s', strtotime($data->assign_date));
			  return  $assign_date;

		  })
		  ->make(true);
	}

	public function detailNomen($nomen){
	  $data = Customer::where('nomen','=', $nomen)->first();
	  return response()->json([
		  'status' => '200',
		  'alert' => 'success',
		  'title' => 'Success!',
		  'description' => 'Get data nomen success',
		  'success' => true,
		  'data' => $data
	  ]);
	}

  	public function search(Request $request){
		$user = Session::get('userinfo');

		//GET WORKER
		$show['workers'] = DB::table('workers as a')
			->join('worker_dispatch as b','a.id','=','b.worker_id')
			->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
			->where('worker_status', '=', 'Active')
			->where('c.group_access_id', '=', $user['group_access_id'])
			->get();

		$show['getWorkers'] = DB::table('worker_dispatch as a')
			->leftjoin('workers as b','a.worker_id','=','b.id')
			->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
			->distinct('a.worker_id')
			->select('a.worker_id', 'b.worker_name')
			->where('worker_status', '=', 'Active')
			->where('c.group_access_id', '=', $user['group_access_id'])
			->get();
		// END GET WORKER
		//get data search
		$group = $user['group_access_id'];
		//total row
		$total = DB::table('fa_transaction as a')
			->distinct()
			->select(
				'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
				'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
				'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
			)
			->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
			->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
			->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
			->leftJoin('workers as e','a.worker_id','=','e.id')
			->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
			->join('fa_group_access as g', function($q) use ($group)
			{
				$q->on('a.dispatch_group', '=', 'g.dispatch_group')
					->where('g.group_access_id', '=', "$group");
			})
			->where('a.nomen','LIKE','%'.$request->search."%")
			->orWhere('a.fa_transaction_id','LIKE','%'.$request->search."%")
			->orWhere('a.dispatch_group','LIKE','%'.$request->search."%")
			->orWhere('a.fa_type_cd','LIKE','%'.$request->search."%")
			->orWhere('a.assign_status','LIKE','%'.$request->search."%")
			->orWhere('b.customer_name','LIKE','%'.$request->search."%")
			->orWhere('b.pcezbk','LIKE','%'.$request->search."%")
			->orWhere('e.worker_name','LIKE','%'.$request->search."%")
			->orWhere('f.description','LIKE','%'.$request->search."%")
			->orderBy('a.updated_at', 'desc')->get();

		//result search
		$show['data'] = DB::table('fa_transaction as a')
			->distinct()
			->select(
				'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
				'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
				'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
			)
			->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
			->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
			->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
			->leftJoin('workers as e','a.worker_id','=','e.id')
			->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
			->join('fa_group_access as g', function($q) use ($group)
			{
				$q->on('a.dispatch_group', '=', 'g.dispatch_group')
					->where('g.group_access_id', '=', "$group");
			})
			->where('a.nomen','LIKE','%'.$request->search."%")
			->orWhere('a.fa_transaction_id','LIKE','%'.$request->search."%")
			->orWhere('a.dispatch_group','LIKE','%'.$request->search."%")
			->orWhere('a.fa_type_cd','LIKE','%'.$request->search."%")
			->orWhere('a.assign_status','LIKE','%'.$request->search."%")
			->orWhere('b.customer_name','LIKE','%'.$request->search."%")
			->orWhere('b.pcezbk','LIKE','%'.$request->search."%")
			->orWhere('e.worker_name','LIKE','%'.$request->search."%")
			->orWhere('f.description','LIKE','%'.$request->search."%")
			->orderBy('a.updated_at', 'desc')
			->paginate(300);

		$limit = 300;//limit untuk continue page number
		$show['num'] = num_row($request->input('page'), $limit);
		$show['total'] = count($total);
		$show['group'] = $user['group_access_id'];
		$show['cari'] = $request->search;
		$show['ket'] = 'Search Data';
		return view('transaction.assignmentFA.v_search_assignmentFA', $show);
	}

	public function filterAssignmentFA(Request $request){
		$fa_type_cd = trim($request->fa_type_cd);
		$pc_ez_bk = trim($request->pc_ez_bk);
		$assign_status = trim($request->assign_status);
		$user = Session::get('userinfo');

		if ($request->fa_type == null && $request->pcezbk == null && $request->status == null) {
			return redirect()->route('transaction.assignmentFA.index');

		}else if ($request->fa_type && $request->pcezbk && $request->status) {
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('a.assign_status', '=', $assign_status)
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('a.assign_status', '=', $assign_status)
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['fa_type_cd'] = $fa_type_cd;
			$show['pc_ez_bk'] = $pc_ez_bk;
			$show['status'] = $assign_status;
			$show['ket'] = 'Search All';

		}else if ($request->fa_type && $request->pcezbk) {
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['ket'] = 'Search Duo';

		}else if ($request->fa_type && $request->status) {
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('a.assign_status', '=', $assign_status)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('a.assign_status', '=', $assign_status)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['ket'] = 'Search Duo';

		}
		else if ($request->fa_type) {		
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.fa_type_cd', '=', $fa_type_cd)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['FA_Type'] = $fa_type_cd;
			$show['ket'] = 'Search Fa Type';
			
		}else if ($request->pcezbk) {
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			// $query = '
			// SELECT * FROM customer a
			// CROSS APPLY (
			// 	SELECT pcezbk, LEFT(pcezbk,2) AS col
			// ) b
			// LEFT JOIN fa_transaction AS c ON a.fa_transaction_id = c.fa_transaction_id
			// LEFT JOIN fa_type AS d ON c.fa_type_cd = d.fa_type_cd
			// LEFT JOIN fa_type_lang AS e ON d.id = e.fa_type_cd
			// WHERE b.col = 09';
			// $tes = DB::select($query);
			// echo json_encode($tes);
			// echo json_encode(count($tes));
			// exit();

			// SELECT * FROM dbo.customer a
			// CROSS APPLY (
			// 	SELECT pcezbk, LEFT(pcezbk,2) AS col
			// ) b
			// WHERE b.col = 09s

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['PC_EZ_BK'] = $pc_ez_bk;
			$show['ket'] = 'Search PC_EZ_BK';

		}else if ($request->status) {
			//GET WORKER
			$show['workers'] = DB::table('workers as a')
				->join('worker_dispatch as b','a.id','=','b.worker_id')
				->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();

			$show['getWorkers'] = DB::table('worker_dispatch as a')
				->leftjoin('workers as b','a.worker_id','=','b.id')
				->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
				->distinct('a.worker_id')
				->select('a.worker_id', 'b.worker_name')
				->where('worker_status', '=', 'Active')
				->where('c.group_access_id', '=', $user['group_access_id'])
				->get();
			// END GET WORKER

			//total row
			$total = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.assign_status', '=', $assign_status)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('a.assign_status', '=', $assign_status)
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
					'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
					'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
				)
				->orderBy('a.updated_at', 'desc')
				->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['total'] = count($total);
			$show['Status'] = $assign_status;
			$show['ket'] = 'Search Status';

		}
		return view('transaction.assignmentFA.v_search_assignmentFA', $show);
	}

}
