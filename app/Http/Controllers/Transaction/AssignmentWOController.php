<?php
namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\WO_transaction;
use App\Exports\AssignmentWOExport;
use App\Exports\AssignmentWOExportDate;
use App\Imports\AssignmentWOImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Administrator\Users;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

//sementara
use App\Models\Administrator\Admin;
use App\Models\Administrator\Access_modules;
use App\Models\Administrator\Workers;
use App\Models\Administrator\Worker_dispatch;
use App\Models\Administrator\Worker_date;

class AssignmentWOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //just remove start date and end date in session
        $request->session()->forget('start');
        $request->session()->forget('end');

        $data['workers'] = DB::table('workers as a')
            ->where('status', '=', 'wo')
            ->where('worker_status', '=', 'Active')
            ->get();

        $data['getWorkers'] = DB::table('worker_dispatch as a')
            ->leftjoin('workers as b','a.worker_id','=','b.id')
            ->distinct('a.worker_id')
            ->select('a.worker_id', 'b.worker_name')
            ->where('worker_status', '=', 'Active')
            ->get();

        $data['getAllWO'] = DB::table('wo_transaction as a')
            ->whereNotIn('a.assign_status', ['Complete'])
            ->leftJoin('workers as b','a.worker_id','=','b.id')
            ->select('a.*', 'b.id as id_worker', 'b.worker_name')
            ->orderBy('a.updated_at', 'desc')
            ->paginate(200);

        $limit = 200;//limit untuk continue page number
        $data['num'] = num_row($request->input('page'), $limit);
        $data['open'] = WO_transaction::where('assign_status', '=', 'Open')->count();
        $data['assign'] = WO_transaction::where('assign_status', '=', 'Assigned')->count();
        $data['reassign'] = WO_transaction::where('assign_status', '=', 'Re-Assigned')->count();
        $data['ontheway'] = WO_transaction::where('assign_status', '=', 'On The Way')->count();
        $data['started'] = WO_transaction::where('assign_status', '=', 'Started')->count();
        $data['cancel'] = WO_transaction::where('assign_status', '=', 'Cancel Worker')->count();
        $data['complete'] = WO_transaction::where('assign_status', '=', 'Complete')->count();
        $data['total'] = count($data['getAllWO']);
        //get worker WO
        $data['workers'] = DB::table('workers as a')->where('status', '=', 'wo')->where('worker_status', '=', 'Active')->get();
        return view('transaction.assignmentWO.v_index_assignmentWO', $data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        $url = "http://172.27.1.38:8280/services/DS_Get_Active_WO?wsdl";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // cek koneksi url
        if($httpcode>=200 && $httpcode<300){
            /** Function get data FA Pending from SIMPEL*/
            /** get date log wo */
            $file = storage_path('app/wo.log');
            $getDateLog = fgets(fopen($file, 'r'));
            $getDateLog = trim($getDateLog);
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:get="http://aetra.co.id/getActiveWo"><soapenv:Header/>
                      <soapenv:Body>
                        <get:getActiveWoOperation>
                          <get:pDate>'.$getDateLog.'</get:pDate>
                        </get:getActiveWoOperation>
                      </soapenv:Body>
                    </soapenv:Envelope>';
            $headers = array("Content-Type: text/xml;charset=UTF-8", "SOAPAction:urn:getActiveWoOperation",);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $xml,
                CURLOPT_HTTPHEADER =>  $headers,
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if($err){
                $error = "cURL Error #:" . $err;
                Storage::prepend('error.log', $error);
            }else{
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($response ,"", 38,90);
                $getString = substr_replace($getString ,"",-34);
                $getXML = simplexml_load_string($getString);
                $json  = json_encode($getXML);
                $configData = json_decode($json, true);
                foreach ($configData as $key => $value){
                    $i=1;
                    foreach ($value as $row) {
                        $date_aetra = encodePost($row['lastUpdateDate']);
                        $date_aetra = strtr($row['lastUpdateDate'], '/', '-');
                        $date = date("Y-m-d h:i:s.u", strtotime($date_aetra));
                        $findWO = WO_transaction::where('no_wo',trim($row['workOrderNo']))
                            ->where('asset_id',trim($row['assetId']))
                            ->where('no_wo_task',trim($row['workOrderTaskNo']))
                            ->first();
                        if(!$findWO){
                            $data = [
                                'plant' => encodePost($row['plant']),
                                'no_wo' => encodePost($row['workOrderNo']),
                                'no_wo_task' => encodePost($row['workOrderTaskNo']),
                                'task_status' => encodePost($row['taskStatus']),
                                'task_desc' => encodePost($row['taskDesc']),
                                'asset_id' => encodePost($row['assetId']),
                                'asset_type' => encodePost($row['asset_type']),
                                'no_account' => encodePost($row['accountNo']),
                                'no_specification' => encodePost($row['specificationNo']),
                                'asset_desc' => encodePost($row['assetDesc']),
                                'area' => encodePost($row['area']),
                                'supervisor' => encodePost($row['supervisor']),
                                'department' => encodePost($row['department']),
                                'crew' => encodePost($row['crew']),
                                'jobCode' => encodePost($row['jobCode']),
                                'created_by' => encodePost($row['createdBy']),
                                'taskPhase' => encodePost($row['taskPhase']),
                                'sending_status' => 'Pending',
                                'assign_status' => 'Open',
                                'created_at' => substr($date, 0, -3)
                            ];
                            $wo_transaction = DB::table('wo_transaction')->insert($data);
                        }else{
                            WO_transaction::where('no_wo',trim($row['workOrderNo']))
                            ->where('asset_id',trim($row['assetId']))
                            ->where('no_wo_task',trim($row['workOrderTaskNo']))
                            ->update([
                                'plant' => encodePost($row['plant']),
                                'no_wo' => encodePost($row['workOrderNo']),
                                'no_wo_task' => encodePost($row['workOrderTaskNo']),
                                'task_status' => encodePost($row['taskStatus']),
                                'task_desc' => encodePost($row['taskDesc']),
                                'asset_id' => encodePost($row['assetId']),
                                'asset_type' => encodePost($row['asset_type']),
                                'no_account' => encodePost($row['accountNo']),
                                'no_specification' => encodePost($row['specificationNo']),
                                'asset_desc' => encodePost($row['assetDesc']),
                                'area' => encodePost($row['area']),
                                'supervisor' => encodePost($row['supervisor']),
                                'department' => encodePost($row['department']),
                                'crew' => encodePost($row['crew']),
                                'jobCode' => encodePost($row['jobCode']),
                                'created_by' => encodePost($row['createdBy']),
                                'taskPhase' => encodePost($row['taskPhase'])
                            ]);
                        }
                    }
                    // creat log date wo
                    $maxDate = DB::table('wo_transaction')->select('created_at')->orderBy('created_at', 'desc')->skip(1)->take(1)->first();
                    $maxDate = substr($maxDate->created_at, 0, -4);
                    $getDate = Carbon::createFromFormat('Y-m-d H:i:s', $maxDate)->format('d/m/Y H:i:s');//convert log date
                    Storage::prepend('wo.log', $getDate);//create log date -> Storage/app/wo.log
                    return response()->json([ 'data' => [ 'status' => 'Update' ] ]);
                }
            }
        }else{
            return response()->json([ 'data' => [ 'status' => 'offline' ] ]);
        }
    }

    public function saveWorker(Request $request,$id)
    {
        $data = WO_transaction::find($id);
        $data->worker_id = $request->worker;
        if($data->assign_date == null){
            $data->assign_date = date('Y-m-d H:i:s');
            $data->assign_status = 'Assigned';
            $data->sending_status = 'Recieved';
        }else{
            $data->reassign_date = date('Y-m-d H:i:s');
            $data->assign_status = 'Re-Assigned';
            $data->sending_status = 'Recieved';
        }
        $data->save();

        if($data)
        {
            return response()->json([
                'status'=>'success',
                'data' => [
                    'assign' => $data->assign_date,
                    'reassign' => $data->reassign_date,
                ]
            ]);
        }
    }

    //save urgent
    public function saveUrgent(Request $request,$id)
    {
        $data = WO_transaction::find($id);
        $data->urgent_status = $request->urgent_status;
        $data->save();
        if($data)
        {
            return response()->json(['status'=>'success']);
        }
    }

    //save priority
    public function savePriority(Request $request,$id)
    {
        $data = WO_transaction::find($id);
        $data->priority_status = $request->priority;
        $data->save();
        if($data)
        {
            return response()->json(['status'=>'success']);
        }
    }

    // export data wo to excel
    public function export($type)
    {
        $startDate = Session::get('start');
        $endDate = Session::get('end');
        if ($startDate && $endDate) {
            $now = date("Ymd H:i:s");
            return Excel::download(new AssignmentWOExportDate($startDate,$endDate), 'assignmentWO-'.$now.'.xlsx');
        }else{
            $now = date("Ymd H:i:s");
            return Excel::download(new AssignmentWOExport(), 'assignmentWO-'.$now.'.xlsx');
        }
    }

    // import data wo to excel
    public function import(){
        $data = Excel::toArray(new AssignmentWOImport, request()->file('file'));
        if ($data) {
            collect(head($data))
            ->each(function ($row, $key) {
                //cek data ada apa enggak
                $cek = WO_transaction::where('asset_id', $row['asset_id'])->get();
                // echo json_encode($row);
                // exit();
                //jika hasil cek null maka create baru
                //jika tidak null maka update
                if ($cek == '[]') {
                    $new_wo = new WO_transaction();
                    $new_wo->plant = $row['plant'];
                    $new_wo->no_wo = $row['no_wo'];
                    $new_wo->no_wo_task = $row['no_wo_task'];
                    $new_wo->jobCode = $row['jobcode'];
                    $new_wo->task_status = $row['task_status'];
                    $new_wo->asset_id = $row['asset_id'];
                    $new_wo->worker_id = $row['worker_id'];
                    $new_wo->assign_date = $row['assign_date'];
                    $new_wo->reassign_date = $row['reassign_date'];
                    $new_wo->assign_status = $row['assign_status'];
                    $new_wo->no_account = $row['no_account'];
                    $new_wo->no_specification = $row['no_specification'];
                    $new_wo->area = $row['area'];
                    $new_wo->department = $row['department'];
                    $new_wo->crew = $row['crew'];
                    $new_wo->urgent_status = $row['urgent_status'];
                    $new_wo->priority_status = $row['priority_status'];
                    $new_wo->sending_status = $row['sending_status'];
                    $new_wo->created_at = Carbon::now();
                    $new_wo->updated_at = Carbon::now();
                    $new_wo->save();
                }else{
                    foreach ($cek as $key => $value) {
                        DB::table('wo_transaction')
                        ->where('asset_id', $row['asset_id'])
                        ->update(array_except($row, ['asset_id']));
                    }
                }
            });
            return Redirect::to('transaction/assignmentWO');
        }
    }

    //search data berdasarkan date
    public function getData(Request $request){
        $user = Session::get('userinfo');

        //cek apakah data date yang dimasukkan kosong atau tidak
        //jika kosong alihkan ke halaman index dari Assignment WO

        if($request->startDateTrans == null or $request->endDateTrans == null){
            return redirect()->route('transaction.assignmentWO.index');
        }else{
            $start_date = Carbon::createFromFormat('d-m-Y', $request->startDateTrans)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->endDateTrans)->format('Y-m-d');

            //put it in session?
            Session::put('start', $start_date);
            Session::put('end', $end_date);

            //get total search
            $total = DB::table('wo_transaction as a')
                ->leftJoin('workers as b','a.worker_id','=','b.id')
                ->whereBetween('a.assign_date', [$start_date, $end_date])
                ->select('a.*','b.worker_name')
                ->orderBy('updated_at', 'desc')
                ->get();

            //get result
            $data['getAllWO'] = DB::table('wo_transaction as a')
                ->leftJoin('workers as b','a.worker_id','=','b.id')
                ->whereBetween('a.assign_date', [$start_date, $end_date])
                ->select('a.*', 'b.id as id_worker', 'b.worker_name')
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $data['workers'] = DB::table('workers as a')
                ->where('status', '=', 'wo')
                ->where('worker_status', '=', 'Active')
                ->get();

            $limit = 300;//limit untuk continue page number
            $data['num'] = num_row($request->input('page'), $limit);
            $data['cariDari'] = $start_date;
            $data['cariSmp'] = $end_date;
            $data['total'] = count($total);
            $data['ket'] = 'Search Date';
            return view('transaction.assignmentWO.v_search_assignmentWO', $data);
        }
    }

    public function search(Request $request){
        //get total search

        $total = DB::table('wo_transaction as a')
            ->leftJoin('workers as b','a.worker_id','=','b.id')
            ->select('a.*','b.id as id_worker','b.worker_name')
            ->where('a.asset_id','LIKE','%'.$request->search."%")
            ->orWhere('a.jobCode','LIKE','%'.$request->search."%")
            ->orWhere('a.no_wo','LIKE','%'.$request->search."%")
            ->orWhere('a.no_wo_task','LIKE','%'.$request->search."%")
            ->orWhere('a.asset_desc','LIKE','%'.$request->search."%")
            ->orWhere('a.task_status','LIKE','%'.$request->search."%")
            ->orWhere('b.worker_name','LIKE','%'.$request->search."%")
            ->orWhere('a.assign_status','LIKE','%'.$request->search."%")
            ->orWhere('a.assign_date','LIKE','%'.$request->search."%")
            ->orWhere('a.asset_type','LIKE','%'.$request->search."%")
            ->orWhere('a.supervisor','LIKE','%'.$request->search."%")
            ->orderBy('updated_at', 'desc')
            ->get();

        //get result
        $data['getAllWO'] = DB::table('wo_transaction as a')
            ->leftJoin('workers as b','a.worker_id','=','b.id')
            ->select('a.*','b.id as id_worker','b.worker_name')
            ->where('a.asset_id','LIKE','%'.$request->search."%")
            ->orWhere('a.jobCode','LIKE','%'.$request->search."%")
            ->orWhere('a.no_wo','LIKE','%'.$request->search."%")
            ->orWhere('a.no_wo_task','LIKE','%'.$request->search."%")
            ->orWhere('a.asset_desc','LIKE','%'.$request->search."%")
            ->orWhere('a.task_status','LIKE','%'.$request->search."%")
            ->orWhere('b.worker_name','LIKE','%'.$request->search."%")
            ->orWhere('a.assign_status','LIKE','%'.$request->search."%")
            ->orWhere('a.assign_date','LIKE','%'.$request->search."%")
            ->orWhere('a.asset_type','LIKE','%'.$request->search."%")
            ->orWhere('a.supervisor','LIKE','%'.$request->search."%")
            ->orderBy('updated_at', 'desc')
            ->paginate(300);

        $data['workers'] = DB::table('workers as a')
                            ->where('status', '=', 'wo')
                            ->where('worker_status', '=', 'Active')
                            ->get();


        $limit = 300;//limit untuk continue page number
        $data['num'] = num_row($request->input('page'), $limit);
        $data['total'] = count($total);
        $data['cari'] = $request->search;
        $data['total'] = count($data['getAllWO']);
        $data['ket'] = 'Search Data';
        return view('transaction.assignmentWO.v_search_assignmentWO', $data);
    }


    public function saveAssignDate(Request $request,$id)
    {
        $data = WO_transaction::find($id);
        $data->assign_date =  Carbon::createFromFormat('d-m-Y', $request->assignDate)->format('Y-m-d');
        $data->save();
        if($data){
            return response()->json(['status'=>'success']);
        }
    }

    public function filterAssignmentWO(Request $request){
        $jobCode = trim($request->jobCode);
        if ($jobCode == null) {
        return redirect()->route('transaction.assignmentWO.index');
        }
        else{
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->where('status', '=', 'wo')
                ->where('worker_status', '=', 'Active')
                ->get();

            //total row
            $total = DB::table('wo_transaction as a')
                ->leftJoin('workers as b','a.worker_id','=','b.id')
                ->where('a.jobCode', $jobCode)
                ->select('a.*','b.worker_name')
                ->orderBy('updated_at', 'desc')
                ->get();

            //result search
            $show['getAllWO'] = DB::table('wo_transaction as a')
                ->leftJoin('workers as b','a.worker_id','=','b.id')
                ->where('a.jobCode', $jobCode)
                ->select('a.*', 'b.id as id_worker', 'b.worker_name')
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

              $limit = 300;//limit untuk continue page number
              $show['num'] = num_row($request->input('page'), $limit);
              $show['total'] = count($total);
              $show['jobCode'] = $jobCode;
              $show['ket'] = 'Search Filter';
              return view('transaction.assignmentWO.v_search_assignmentWO', $show);
        }
    }

    public function multiSaveWorker(Request $request)
    {
        // print_r($request->worker);
        $cek = $_POST['checkFA'];
        foreach ($cek as $value => $get) {
            $data = WO_transaction::find($get);
            $data->worker_id = $request->worker;
            if($data->assign_date == null){
                $data->assign_date = date('Y-m-d H:i:s');
                $data->assign_status = 'Assigned';
                $data->sending_status = 'Recieved';
            }else{
                $data->reassigned_date = date('Y-m-d H:i:s');
                $data->assign_status = 'Re-Assigned';
                $data->sending_status = 'Recieved';
            }
            $data->save();
        }

        if($data)
        {
            return response()->json([
                'status'=>'success',
                'data' => [
                    'assign' => $data->assign_date,
                    'reassigned' => $data->reassigned_date,
                ]
            ]);
        }

    }
}
