<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Survey;
use App\Models\Transaction\AssignmentSurvey;
use App\Models\Administrator\Workers;
use App\Models\Administrator\Survey_detail;
use App\Models\Transaction\Code_AB;
use App\Models\Transaction\Code_PC;
use App\Models\Transaction\Code_EZ;
use App\Models\Transaction\Code_BK;
use Yajra\Datatables\Datatables;
use Validator;
use DB;
use Carbon\Carbon;
use Session;


class AssignmentSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data to list
        $data['list_survey'] = DB::table('survey_transaction as a')
        ->join('survey as b','a.survey_id','b.id')
        ->join('workers as c','c.id','a.worker_id')
        ->leftjoin('code_ab as d','d.id','a.AB_id')
        ->leftjoin('code_pc as e','e.id','a.PC_id')
        ->leftjoin('code_ez as f','f.id','a.EZ_id')
        // ->join('code_bk as g','g.id','a.BK_id')
        ->whereNotIn('a.survey_status',['Finished'])
        ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
            'a.survey_code','a.survey_date',
            'c.worker_name','a.sending_status','a.survey_status','d.code_ab','e.code_pc','f.code_ez','a.worker_id','a.nomen','a.asset')
        ->get();
        return view('transaction.assignmentSurvey.v_index_assignmentSurvey',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //get user info
        $user = Session::get('userinfo');
        $workers_fa = DB::table('workers as a')
              ->join('worker_dispatch as b','a.id','=','b.worker_id')
              ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
              ->where('worker_status', '=', 'Active')
              ->where('c.group_access_id', '=', $user['group_access_id'])
              ->select('a.id', 'a.worker_id', 'a.worker_name')
              ->get();

        $workers_wo = DB::table('workers as a')
              ->where('status', '=', 'wo')
              ->get();
        $data['workers'] = $workers_fa->merge($workers_wo);
        $data['workers']->all();      
        
        $data['survey'] = Survey::all();
        $data['ab'] = Code_AB::all();
        return view('transaction.assignmentSurvey.v_add_assignmentSurvey',$data);
    }

    public function pc($id){
        $pc = Code_PC::where('code_ab_id', '=', $id)->get();
        return response()->json($pc);
    }

    public function ez($id){
        $ez = Code_EZ::where('code_pc_id', '=', $id)->get();
        return response()->json($ez);
    }

    public function bk($id){
        $bk = Code_BK::where('code_ez_id', '=', $id)->get();
        return response()->json($bk);
    }

    public function selectWorker($id){
        $obj_survey = Survey::where('id', $id)->first();
        
        //cek object survey
        //jika object survey == Asset muncul worker WO
        //jika object survey == Nomen muncul worker FA
        //jika object survey == Area muncul semua worker

        if ($obj_survey->object_survey == 'Asset') {
            $worker = Workers::where('status', 'wo')
                              ->where('worker_status', 'Active')
                              ->get();
            return response()->json($worker);
        }
        if ($obj_survey->object_survey == 'Nomen') {
            $worker = Workers::where('status', 'fa')
                              ->where('worker_status', 'Active')
                              ->get();
            return response()->json($worker);
        }
        if ($obj_survey->object_survey == 'Area') {
            $worker = Workers::where('worker_status', 'Active')
                              ->get();
            return response()->json($worker);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $validator = Validator::make($request->all(),[
                'survey_id'=>'required',
                'start_date'=>'required',
                'end_date'=>'required',
                'ab_id'=>'required',
                'pc_id'=>'required',
                'ez_id'=>'required',
                'worker_id'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        //save user
        $date  = date("y");
        $now = date('d-m-Y h:i:s');
        $survey_transaction = new AssignmentSurvey;
        $survey_transaction->survey_code = $date.rand(1000000,9999999);
        $survey_transaction->survey_date = Carbon::createFromFormat('d-m-Y h:i:s', $now)->format('Y-m-d h:i:s');
        $survey_transaction->survey_id = $request->survey_id;
        $survey_transaction->worker_id = $request->worker_id;
        $survey_transaction->start_date =  Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
        $survey_transaction->end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');
        $survey_transaction->AB_id = $request->ab_id;
        $survey_transaction->PC_id = $request->pc_id;
        $survey_transaction->EZ_id = $request->ez_id;
        $survey_transaction->nomen = $request->nomen;
        $survey_transaction->asset = $request->asset;
        $survey_transaction->survey_status = 'Assigned';
        $survey_transaction->sending_status = 'Recieved';
        $survey_transaction->save();
        
        if($survey_transaction)
        {
            return response()->json(['status'=>'success']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get user info
        $user = Session::get('userinfo');

        $survey = AssignmentSurvey::where('id', $id)->first();
        //cek status survey untuk mengambil worker sesuai dengan object survey
        $objSurvey = Survey::where('id', $survey->survey_id)->first();

        if ($objSurvey->object_survey == 'Asset') {
          $data['workers'] = Workers::where('status', 'wo')
                                    ->where('worker_status', 'Active')
                                    ->get();
        }
        if ($objSurvey->object_survey == 'Nomen') {
          $data['workers'] = Workers::where('status', 'fa')
                                    ->where('worker_status', 'Active')
                                    ->get();
        }
        if ($objSurvey->object_survey == 'Area') {
          $workers_fa = DB::table('workers as a')
              ->join('worker_dispatch as b','a.id','=','b.worker_id')
              ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
              ->where('worker_status', '=', 'Active')
              ->where('c.group_access_id', '=', $user['group_access_id'])
              ->select('a.id', 'a.worker_id', 'a.worker_name')
              ->get();

          $workers_wo = DB::table('workers as a')
                ->where('status', '=', 'wo')
                ->get();
          $data['workers'] = $workers_fa->merge($workers_wo);
          $data['workers']->all(); 
        }

         

        $data['assignmentSurvey']  = AssignmentSurvey::where('id', $id)->first();
        $data['survey'] = Survey::all();
        //$data['workers'] = Workers::all();
        $data['ab'] = Code_AB::all();
        $data['pc'] = Code_PC::where('code_ab_id', $data['assignmentSurvey']->AB_id)->get();
        $data['ez'] = Code_EZ::where('code_pc_id', $data['assignmentSurvey']->PC_id)->get();
        // $data['bk'] = Code_BK::where('code_ez_id', $data['assignmentSurvey']->EZ_id)->get();
        return view('transaction.assignmentSurvey.v_edit_assignmentSurvey',$data);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

         $validator = Validator::make($request->all(),[
                'survey_id'=>'required',
                'start_date'=>'required',
                'end_date'=>'required',
                'worker_id'=>'required'
                
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

           //save user
        $survey_transaction = AssignmentSurvey::find($id);

        $survey_transaction->survey_id = $request->survey_id;
        $survey_transaction->worker_id = $request->worker_id;
        $survey_transaction->start_date = $request->start_date;
        $survey_transaction->end_date = $request->end_date;
        $survey_transaction->AB_id = $request->ab_id;
        $survey_transaction->PC_id = $request->pc_id;
        $survey_transaction->EZ_id = $request->ez_id;
        $survey_transaction->nomen = $request->nomen;
        $survey_transaction->asset = $request->asset;
        $survey_transaction->survey_status = 'Assigned';
        $survey_transaction->sending_status = 'Recieved';
        $survey_transaction->save();


        if($survey_transaction)
        {
            return response()->json(['status'=>'success']);
        }
    }

    public function viewAB($ab_id){
        $data['detail_ab'] = DB::table('code_ab')
                ->where('id', $ab_id)
                ->get();

        return view('transaction.assignmentSurvey.view_detail_ab', $data);
    }

    public function viewPC($id){
        $data['detail_pc'] = DB::table('survey_transaction as a')
                ->where('a.id', $id)
                ->join('code_pc as b','a.PC_id','b.id')
                ->join('code_ez as c','a.EZ_id','c.id')
                ->select('b.code_pc','c.code_ez')
                ->get();

        return view('transaction.assignmentSurvey.view_detail_pc', $data);
    }
}
