<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\WO_Template_BOM;
use App\Models\Transaction\WO_Template_BOM_detail;
use DB;
use Validator;

class MappingWOTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['list_wo_task'] = DB::table('wo_task')->get();
      // echo json_encode($data);
      // exit();
      return view('administrator.mappingWOTask.v_index_mappingWOTask', $data);
    }

    public function view_character($id)
    {

      $dataMapping = DB::table('wo_task as a')
                    ->join('wo_char_type as b', 'a.job_code', 'b.job_code')
                    ->where('b.job_code', '=', $id)
                    ->select('b.char_wo_type','b.char_type_flg','b.char_value','a.priority')
                    ->get();

      // echo json_encode($dataMapping);
      // exit();
      
      return response()->json(['data'=>$dataMapping]);
    }

    public function savePriority(Request $request, $id)
    {
      // Update property in table wo_task
      $update = DB::table('wo_task')->where('id',$id)->update(['priority'=>$_GET['priority_status']]);

      // Update priority in table wo transaction
      DB::table('wo_transaction')->where('jobCode',$request->job_code)->update(['priority_status'=>$_GET['priority_status']]);

      if($update)
      {
        return response()->json(['status'=>'success']);
      } 
    }

    public function saveUrgent(Request $request,$id)
    {
      $update = DB::table('wo_task')->where('id',$id)->update(['urgent_status'=>$_GET['urgent_status']]);
      //update urgent in assignment WO
      DB::table('wo_transaction')
            ->where('jobCode',$request->job_code)
            ->update(['urgent_status'=>$_GET['urgent_status']]);
      
      if($update)
      {
        return response()->json(['status'=>'success']);
      }
    }  

    public function saveLockWO($id)
    {   
      $date = date('Y-m-d H:i:s');
      $update = DB::table('wo_task')->where('id',$id)->update(['lock_status'=>$_GET['val_lock_wo'], 'updated_at'=>$date]);

      if($update)
      {
        return response()->json(['status'=>'success']);
      }  
    }

    public function viewBOM($id)
    {
        $data = DB::table('wo_template_bom')->where('jobCode',$id)->get();
        return response()->json(['data'=>$data]);
    } 

    public function create()
    {
        $data['jobCode'] = $_GET['jobCode'];
        $data['code'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['coba'] = DB::select('select id,code,name from fa_template_BOM_code');
        return view('administrator.mappingWOTask.v_add_bom', $data);
    }

    public function getCode($id)
    {
        $name = DB::table('fa_template_BOM_code')->where('id',$id)->first();
      return json_encode($name);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['template_name'=>'required']);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $insert_template = DB::table('wo_template_BOM')->insert(['template_name'=>$request->template_name,'jobCode'=>$request->jobCode,'created_at'=>now()]);
        $last_id = WO_Template_BOM::orderBy('id', 'desc')->first()->id;
        if($last_id != 0){
          if(count($request->template_code)>0){
            $product = count($request->template_code);
            for($i= 0; $i < $product; $i++){ // multiple insert
              $data = array(
                                'wo_template_BOM_id' => $last_id,
                                'code' => $request->template_code[$i],
                                'created_at' => now()
                                );
                $insert = DB::table('wo_template_BOM_detail')->insert(array($data));
            } 
          }
        }
        if($insert)
        {
           return response()->json(['status'=>'success','jobCode'=>$request->jobCode]);
        }
    } 

    public function show($id)
    {
        $data['wo_template_BOM'] = DB::table('wo_template_BOM')->where('id',$id)->first();
        $data['detail'] = DB::table('wo_template_BOM')
                ->join('wo_template_BOM_detail', 'wo_template_BOM.id', '=', 'wo_template_BOM_detail.wo_template_BOM_id')
                ->join('fa_template_BOM_code', 'wo_template_BOM_detail.code', '=', 'fa_template_BOM_code.id')
                        ->select('fa_template_BOM_code.id','fa_template_BOM_code.code as code','fa_template_BOM_code.name')
                        ->where('wo_template_BOM.id', $id)
                        ->get();
        if($data['detail'])
        {
            return view('administrator.mappingWOTask.v_detail_bom',$data);
        }
    }

    public function edit($id)
    {
        //
        $data['code'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['coba'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['wo_template_BOM'] = DB::table('wo_template_BOM')->where('id',$id)->first();
        $data['detail'] = DB::table('wo_template_BOM')
                ->join('wo_template_BOM_detail', 'wo_template_BOM.id', '=', 'wo_template_BOM_detail.wo_template_BOM_id')
                ->join('fa_template_BOM_code', 'wo_template_BOM_detail.code', '=', 'fa_template_BOM_code.id')
                        ->select('fa_template_BOM_code.id','wo_template_BOM_detail.id as id_detail','fa_template_BOM_code.code as code','fa_template_BOM_code.name')
                        ->where('wo_template_BOM.id', $id)
                        ->get();
        $data['count'] = $data['detail']->count();
        if($data['detail'])
        {
           return view('administrator.mappingWOTask.v_edit_bom',$data);
        }
    }

    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),['template_name'=>'required']);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $updateNamaTemplate = DB::table('wo_template_BOM')->where('id',$id)->update(['template_name'=>$request->template_name]);
        $cek = DB::table('wo_template_BOM_detail')->where('wo_template_BOM_id', $id)->first();
        if(empty($cek))
        {
            if(count($request->template_code)>0){
                $product = count($request->template_code);
                for($i= 0; $i < $product; $i++){ // multiple insert
                    $data = array(
                                'wo_template_BOM_id' => $id,
                                'code' => $request->template_code[$i],
                                'created_at' => now()
                                );
                    $insert = DB::table('wo_template_BOM_detail')->insert(array($data));
                }
            }
        }else{
            $delete = DB::table('wo_template_BOM_detail')->where('wo_template_BOM_id', $id)->delete();
            if($delete)
            {
                if(count($request->template_code)>0){
                    $product = count($request->template_code);
                    for($i= 0; $i < $product; $i++){ // multiple insert
                        $data = array(
                                    'wo_template_BOM_id' => $id,
                                    'code' => $request->template_code[$i],
                                    'created_at' => now()
                                    );
                        $insert = DB::table('wo_template_BOM_detail')->insert(array($data));
                    }
                }
            }
        }     
        if($insert)
        {
            return response()->json(['status'=>'success','jobCode'=>$request->jobCode]);
        }
    }
}
