<?php
/*
    for manage user 
    @author arief manggala putra
    created date : 23 10 2018
*/

namespace App\Http\Controllers\Administrator;

use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Users;
use App\Models\Administrator\Groups;
use App\Models\Administrator\Modules;
use App\Models\Administrator\Admin;
use App\Models\Administrator\workers;
use App\Models\Administrator\Access_modules;
use Illuminate\Support\Facades\Hash;

use Validator;
use DB;

class UsersController extends Controller
{

    // for view index
    public function index()
    {
        $show['groups'] = Groups::all();
        $show['modules'] = Modules::where('moduleParent',0)->get();
        $show['data'] = DB::table('users  as a')
                        ->join('admin as b','a.id','b.user_id')
                        ->join('group_access as c','c.id','b.group_access_id')
                        ->select('a.username','a.name','b.phone','c.group_name','b.admin_status','a.id as user_id' ,'b.id as admin_id','c.id as group_access_id')
                        ->orderBy('a.created_at', 'desc')
                        ->get();
        return view('administrator.users.v_index_users',$show);
    }

    /*for getdatatable
    public function getData()
    {   
         $data = DB::table('users  as a')
                ->join('admin as b','a.id','b.user_id')
                ->join('group_access as c','c.id','b.group_access_id')
                ->select('a.username','a.name','b.phone','c.group_name','b.admin_status','a.id as user_id' ,'b.id as admin_id','c.id as group_access_id')
                ->get();
         
         return Datatables::of($data)
          ->editColumn('username', function($data) {
                return $data->username;
            })
          ->editColumn('name', function($data) {
                return $data->name;
            })
          ->editColumn('phone', function($data) {
                return $data->phone;
            })
          ->editColumn('group_access_id', function($data) {
                return $data->group_name;
            })
            ->addColumn('admin_status', function ($data) {
                if($data->admin_status == 'Active')
                {
                    $dt_non = 'Non Active';
                    return '<input  checked type="checkbox" name="status_check" id="status_active" onClick="changeStatus(\'' .$data->admin_id. '\',\'' .$dt_non. '\')"  /> Active';
                }
                else
                {
                     return '<input  type="checkbox" name="status_check" id="status_active"  onclick=changeStatus("'.$data->admin_id.'","Active") /> Non Active';    
                }
                
            }) 
            ->rawColumns(['admin_status'])
         ->make(true);
    }
    */

    // for update status witch checkbox
    public function updateStatus($id)
    {
        $user = Admin::find($id);
        $user->admin_status = $_GET['status'];
        $user->save(); 
        return response()->json(['status'=>'success']);
    }

    // for save users
    public function store(Request $request)
    {
        //
         $validator = Validator::make($request->all(),[
                'name'=>'required',
                'username'=>'required',
                'group_id'=>'required',
                // 'phone'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }


        //save user
        $group = new Users;

        $group->username = $request->username;
        $group->name = $request->name;
        $group->is_login = 1 ;
        $group->password = bcrypt($request->password);
        $group->save();

        //save admin
        $admin = new Admin;

        $admin->phone = $request->phone;
        $admin->user_id = $group->id;
        $admin->admin_status = 'Active';
        $admin->role_id = 2;
        $admin->group_access_id = $request->group_id;
        $admin->save();

        if(isset($_POST['module_id'])){
            foreach($_POST['module_id'] as $m)
            {
                //save Access_modules
                $Access_modules = new Access_modules;
                $Access_modules->module_id = $m;
                $Access_modules->admin_id = $admin->id;
                $Access_modules->save();
            }
        }


        if($group and $admin)
        {
            return response()->json(['status'=>'success']);
        }

    }

    // for modal detail user
    public function detailUser($id)
    {
        $data['id']  = $id;
        $data['groups'] = Groups::all();
        $data['modules'] = Modules::where('moduleParent',0)->get();
        $data['action'] = $_GET['action'];

        $data['dataForm'] =  DB::table('users  as a')
                ->join('admin as b','a.id','b.user_id')
                ->where('b.id',$id)
                ->select('a.username','a.name','b.phone','b.admin_status','a.id as user_id' ,'b.id as admin_id','b.group_access_id')
                ->first();

        return view('administrator.users.v_detail_user',$data);
    }

    // for update user
    public function updateUser(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'name'=>'required',
                'username'=>'required',
                'group_id'=>'required',
                'phone'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        
        //save user
        $group = Users::find($request->user_id);
        if($request->password != null){
          $group->username = $request->username;
          $group->name = $request->name;
          $group->password = bcrypt($request->password);
          $group->save();
        }else{
          $group->username = $request->username;
          $group->name = $request->name;
          $group->save();
        }
        //save admin
        $admin = Admin::where('id',$request->admin_id)->first();

        $admin->phone = $request->phone;
        $admin->group_access_id = $request->group_id;
        $admin->save();

        // delete all module by user id
        $delete_module = Access_modules::where('admin_id',$request->admin_id);
        $delete_module->delete();
        foreach($_POST['module_id'] as $m)
        {
            //save Access_modules
            $Access_modules = new Access_modules;
            $Access_modules->module_id = $m;
            $Access_modules->admin_id = $admin->id;
            $Access_modules->save();
        }

        if($group)
        {
            return response()->json(['status'=>'success']);
        }
    }

    //for get datamodule
    public function getDataModule()
    {
        $data = Modules::where('moduleParent',0)->get();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

    // for get datadetail module
    public function getDetailModule(Request $request)
    {
        $dataDetail = Modules::where('moduleParent',$request->id)->get();
        return response()->json(['dataDetail'=>$dataDetail,'status'=>'success']);
    }

    public function destroy($id)
    {
        $admin = Admin::where('id', $id)->first();

        if($admin->delete())
        {
            Users::where('id', $admin->user_id)->delete();
            return response()->json(['status'=>'success']);    
        }   
    }
}
