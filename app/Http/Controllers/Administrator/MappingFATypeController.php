<?php

namespace App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\Administrator\FA_type;
use App\Models\Transaction\FA_Template_BOM;
use App\Models\Transaction\FA_Template_BOM_detail;
use App\Models\Administrator\FA_mapping_char_type;
use DB;
use Validator;

class MappingFATypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $show['data'] = DB::table('fa_type as a')
                ->distinct('c.fa_type_cd')
                ->join('fa_type_lang as b','a.id','=','b.fa_type_cd')
                ->leftjoin('fa_transaction as c','a.fa_type_cd','=','c.fa_type_cd')
                ->select('a.id','a.fa_type_cd','a.lock_fa','b.descr','c.urgent_status','a.fa_priority_flg')->get();
        return view('administrator.mappingFAType.v_index_mappingFAType', $show);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['fa_type_cd'] = $_GET['fa_type_cd'];
        $data['code'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['coba'] = DB::select('select id,code,name from fa_template_BOM_code');
        return view('administrator.mappingFAType.v_add_bom', $data);
    }

    public function getCode($id)
    {
        $name = DB::table('fa_template_BOM_code')->where('id',$id)->first();
	    return json_encode($name);
    }	

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),['template_name'=>'required']);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $fa_type_cd = trim($request->fa_type_cd);
        $insert_template = DB::table('fa_template_BOM')->insert(['template_name'=>$request->template_name,'fa_type_cd'=>$fa_type_cd,'created_at'=>now()]);
        $last_id = FA_Template_BOM::orderBy('id', 'desc')->first()->id;
        if($last_id != 0){
	        if(count($request->template_code)>0){
	        	$product = count($request->template_code);
	        	for($i= 0; $i < $product; $i++){ // multiple insert
	        	 	$data = array(
                                'fa_template_BOM_id' => $last_id,
                                'code' => $request->template_code[$i],
                                'created_at' => now()
                                );
	        	    $insert = DB::table('fa_template_BOM_detail')->insert(array($data));
	        	} 
	        }
        }
        if($insert)
        {
           return response()->json(['status'=>'success','fa_type_cd'=>$fa_type_cd]);
        }else
        {
            return response()->json(['status'=>'error','message'=>'Jangan Ada Field Yang KOSONG!']);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['fa_template_BOM'] = DB::table('fa_template_BOM')->where('id',$id)->first();
        $data['detail'] = DB::table('fa_template_BOM')
        				->join('fa_template_BOM_detail', 'fa_template_BOM.id', '=', 'fa_template_BOM_detail.fa_template_BOM_id')
        				->join('fa_template_BOM_code', 'fa_template_BOM_detail.code', '=', 'fa_template_BOM_code.id')
                        ->select('fa_template_BOM_code.id','fa_template_BOM_code.code as code','fa_template_BOM_code.name')
                        ->where('fa_template_BOM.id', $id)
                        ->get();
        if($data['detail'])
        {
            return view('administrator.mappingFAType.v_detail_bom',$data);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['code'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['coba'] = DB::select('select id,code,name from fa_template_BOM_code');
        $data['fa_template_BOM'] = DB::table('fa_template_BOM')->where('id',$id)->first();
        $data['detail'] = DB::table('fa_template_BOM')
        				->join('fa_template_BOM_detail', 'fa_template_BOM.id', '=', 'fa_template_BOM_detail.fa_template_BOM_id')
        				->join('fa_template_BOM_code', 'fa_template_BOM_detail.code', '=', 'fa_template_BOM_code.id')
                        ->select('fa_template_BOM_code.id','fa_template_BOM_detail.id as id_detail','fa_template_BOM_code.code as code','fa_template_BOM_code.name')
                        ->where('fa_template_BOM.id', $id)
                        ->get();
        $data['count'] = $data['detail']->count();
        if($data['detail'])
        {
           return view('administrator.mappingFAType.v_edit_bom',$data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make($request->all(),['template_name'=>'required']);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $updateNamaTemplate = DB::table('fa_template_BOM')->where('id',$id)->update(['template_name'=>$request->template_name]);
        $cek = DB::table('fa_template_BOM_detail')->where('fa_template_BOM_id', $id)->first();
        if(empty($cek))
        {
            if(count($request->template_code)>0){
                $product = count($request->template_code);
                for($i= 0; $i < $product; $i++){ // multiple insert
                    $data = array(
                                'fa_template_BOM_id' => $id,
                                'code' => $request->template_code[$i],
                                'created_at' => now()
                                );
                    $insert = DB::table('fa_template_BOM_detail')->insert(array($data));
                }
            }
        }else{
            $delete = DB::table('fa_template_BOM_detail')->where('fa_template_BOM_id', $id)->delete();
            if($delete)
            {
                if(count($request->template_code)>0){
                    $product = count($request->template_code);
                    for($i= 0; $i < $product; $i++){ // multiple insert
                        $data = array(
                                    'fa_template_BOM_id' => $id,
                                    'code' => $request->template_code[$i],
                                    'created_at' => now()
                                    );
                        $insert = DB::table('fa_template_BOM_detail')->insert(array($data));
                    }
                }
            }
        }     
        

        if($insert)
        {
            return response()->json(['status'=>'success','fa_type_cd'=>trim($request->fa_type_cd)]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getData()
    {
      //$data = FA_type::with('fa_type_lang')->with('fa_transaction');
      $data = DB::table('fa_type as a')
                 	->join('fa_type_lang as b','a.id','=','b.fa_type_cd')
                 	//->join('fa_transaction as c','a.fa_type_cd','=','c.fa_type_cd')
               		->select('a.fa_type_cd','b.descr')->get();

      return Datatables::of($data)->make(true);
    }

    public function view_character($id)
    {

         $dataMapping = getFACharacter($id);
        // print_r($dataMapping);exit();
        
        // $data = DB::table('fa_mapping_char_type as a')
        //         ->join('fa_char_type as b','a.char_type_cd','=','b.char_type_cd')
        //         ->select('*')->where('fa_type_cd',$id)->get();
        
        return response()->json(['data'=>$dataMapping]);
    }

    public function viewBOM($id)
    {
        $data = DB::table('fa_template_bom')->where('fa_type_cd',$id)->get();
        return response()->json(['data'=>$data]);
    } 

    public function viewSeqChar($id)
    {
        $data = DB::table('fa_mapping_char_type as a')
                    ->join('fa_char_type_lang as b', 'a.char_type_cd', 'b.char_type_cd')
                    ->where('fa_type_cd',$id)
                    ->select('a.id','a.char_type_cd','a.sort_seq','b.descr')
                    ->get();
        return response()->json(['data'=>$data]);
    }   

    public function saveUrgent(Request $request,$id)
    {
        $update = DB::table('fa_type')->where('id',$id)->update(['urgent_status'=>$_GET['urgent_status']]);
        $updateFATrans = DB::table('fa_transaction')
                            ->where('fa_type_cd',$request->fa_type_cd)
                            ->update(['urgent_status'=>$_GET['urgent_status']]);
        if($update)
        {
            return response()->json(['status'=>'success']);
        }
    }  

    public function saveLockFA($id)
    {   

        //$update = FA_type::find($id);
        //$update->lock_fa = $_GET['val_lock_fa'];
        //$update->save(); 

        $update = DB::table('fa_type')->where('id',$id)->update(['lock_fa'=>$_GET['val_lock_fa']]);

        if($update)
        {
            return response()->json(['status'=>'success']);
        }
               
    }   

    public function savePriority(Request $request, $id)
    {
        $update = DB::table('fa_transaction')->where('fa_type_cd',$request->fa_type_cd)->update(['priority_status'=>$_GET['priority_status']]);
        $update = DB::table('fa_type')->where('id',$id)->update(['fa_priority_flg'=>$_GET['priority_status']]);
        if($update)
        {
            return response()->json(['status'=>'success']);
        } 
    }

    public function editSeqChar($id)
    {
        $data['seqChar'] = DB::table('fa_mapping_char_type as a')
                            ->join('fa_char_type_lang as b', 'a.char_type_cd', 'b.char_type_cd')
                            ->where('a.id', $id)
                            ->select('a.id','a.char_type_cd','a.sort_seq','b.descr')
                            ->first();

        return view('administrator.mappingFAType.v_edit_seq_char',$data);
    }

    public function updateCharSeq(Request $request, $id){
        $data = FA_mapping_char_type::find($id);
        $data->sort_seq = $request->sort_seq;
        if($data->update()){
            return response()->json(['status'=>'success']);
        }
    }
}
