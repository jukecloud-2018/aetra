<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\System_parameter;
use DB;


class SystemParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $system_parameter = System_parameter::whereNotNull('id')->first();

        if(empty($system_parameter))
        {

            $data['system_parameter'] ="";
        }
        else
        {   

            $data['system_parameter'] = System_parameter::whereNotNull('id')->first();
        }

        return view('administrator.systemParameter.v_index_systemParameter',$data);
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
         //save 

        $data = System_parameter::whereNotNull('id')->first();

        if(empty($data))
        {        


            $system_parameter = new System_parameter;

            $system_parameter->range_periode = $request->range_periode;
            $system_parameter->assign_task = $request->range_periode;
            $system_parameter->lock_fa = $request->lock_fa;
            $system_parameter->log_off_web = $request->log_off_web;
            $system_parameter->log_off_mobile = $request->log_off_mobile;
            $system_parameter->auto_refresh = $request->auto_refresh;
            $system_parameter->pickup_task = (empty($request->pickup_task) || $request->pickup_task == 'complete' ) ? "complete":$request->pickup_task;
            $system_parameter->v_code = $request->v_code;
            $system_parameter->v_name = $request->v_name;
            $system_parameter->close_time = $request->close_time;
            $system_parameter->close_time_status = $request->close_time_status;

            $system_parameter->save();
        }
        else
        {

            System_parameter::truncate();

            $system_parameter = new System_parameter;

            $system_parameter->range_periode = $request->range_periode;
            $system_parameter->assign_task = $request->assign_task;
            $system_parameter->lock_fa = $request->lock_fa;
            $system_parameter->log_off_web = $request->log_off_web;
            $system_parameter->log_off_mobile = $request->log_off_mobile;
            $system_parameter->auto_refresh = $request->auto_refresh;
            $system_parameter->pickup_task = (empty($request->pickup_task) || $request->pickup_task == 'complete' ) ? "complete":$request->pickup_task;
            $system_parameter->v_code = $request->v_code;
            $system_parameter->v_name = $request->v_name;
            $system_parameter->close_time = $request->close_time;
            $system_parameter->close_time_status = $request->close_time_status;

            $system_parameter->save();
        }



        if($system_parameter)
        {
            return response()->json(['status'=>'success']);    
        }
    }

}
