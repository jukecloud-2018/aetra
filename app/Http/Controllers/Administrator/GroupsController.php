<?php

namespace App\Http\Controllers\Administrator;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Groups;
use App\Models\Administrator\FA_Group;
use App\Models\Administrator\FA_Group_Access;
use App\Models\Administrator\Survey_group_access;
use Validator;
use DB;

class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['faGroup']  = FA_Group::all();
        $data['count'] = count($data['faGroup']);
        $data['surveyGroup'] = DB::table('code_ab')->get();
        return view('administrator.groups.v_index_groups',$data);
    }

    // get Data ajax datatable
    public function getData()
    {
        $data = Groups::with('fa_group_access')->get();
        return Datatables::of($data)->make(true);
    }

    public function saveSurveyGroup()
    {

        echo 'tes';
        
    }

    public function detailWOGroup()
    {
        $data['faGroup'] = FA_Group::all();
        return view('administrator.groups.v_detail_FA_Group',$data);
    }

    public function editGroupAccess($id)
    {
        $data['groups'] = Groups::where('id',$id)->first();
        $data['faGroup'] = FA_Group::all();
        return view('administrator.groups.v_edit_groups',$data);
    } 

    public function editFAGroup($id)
    {
        $data['faGroup'] = FA_Group::all();
        $data['id'] = $id;
        $data['count_dispatch_group_master'] = count($data['faGroup']);
        $data['count_dispatch_group_user'] = FA_Group_Access::where('group_access_id',$id)->count();
        return view('administrator.groups.v_edit_fa_group',$data);
    }

    public function editSurveyGroup($id)
    {
        $data['code_ab'] = DB::table('code_ab')->get();
        $data['id'] = $id;
        return view('administrator.groups.v_edit_survey_group',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['group_name' =>'required | unique:group_access,group_name']);
        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $group_access = new Groups;
        $group_access->group_name = $request->group_name;
        $group_access->save();

        if(!empty($_POST['dispatch_group']))
        {
            for($i=0; $i < count($_POST['dispatch_group']); $i++)
            {

                $fa_group_access = new FA_Group_Access;
//                $fa_group_access->dispatch_group = empty($_POST['dispatch_group'][$i]) ? false : true;
                $fa_group_access->dispatch_group = $_POST['dispatch_group'][$i];
                $fa_group_access->group_access_id = $group_access->id;
//                $fa_group_access->transaction = empty($_POST['fa_group_type_trn'][$i]) ? false : true;
                $fa_group_access->transaction = $_POST['fa_group_type_trn'][$i];
                $fa_group_access->monitoring = $_POST['fa_group_type_mnt'][$i];
                $fa_group_access->message= $_POST['fa_group_type_msg'][$i];
                //chrome_log("ini data inputan = $fa_group_access");
                /*print_r("masuk sini - ".$i." = ".$_POST['dispatch_group'][$i]."<pre>".
                    "transaction = ".$_POST['fa_group_type_trn'][$i]."<pre>".
                    "monitoring = ".$_POST['fa_group_type_mnt'][$i]."<pre>".
                    "message = ".$_POST['fa_group_type_msg'][$i]."<pre>".
                    "<pre>");*/
                $fa_group_access->save();
            }
        }

        if(!empty($_POST['code_ab']))
        {
            for($i=0;$i<count($_POST['code_ab']);$i++)
            {                
                DB::table('survey_group_access')->insert(
                    [
                        'code_ab'=>$_POST['code_ab'][$i],
                        'group_access_id'=>$group_access->id,
                        'transaction'=>$_POST['survey_group_type_trn'][$i],
                        'monitoring'=>$_POST['survey_group_type_mnt'][$i],
                        'message'=>$_POST['survey_group_type_msg'][$i],
                        'created_at'=>now(),
                        'updated_at'=>now(),
                    ]
                );
            }
        }
        
        if($group_access || $fa_group_access)
        {
            return response()->json(['success'=>'success']);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function savemodalEditFAGroup(Request $request)
    {
      $countgroup = count($request->dispatch_group);
      $count = $countgroup + 1;
      $delete_fa_group_access = FA_Group_Access::where('group_access_id',$request->group_access_id);
      $delete_fa_group_access->delete();
      if($countgroup != 0){
        $faGroup = FA_Group::all();
          for ($i=0; $i < $countgroup; $i++) {
            $data = array(
              'group_access_id' => $_POST['group_access_id'],
              'dispatch_group' => $_POST['dispatch_group'][$i],
              'transaction' => $_POST['fa_group_type_trn'][$i],
              'monitoring' => $_POST['fa_group_type_mnt'][$i],
              'message' => $_POST['fa_group_type_msg'][$i]
            );
            $insert = DB::table('fa_group_access')->insert(array($data));
          }
      }else{
          return response()->json(['gagal'=>'gagal']);
      }

      if($insert)
      {
        return response()->json(['success'=>'success']);
      }
    }

    public function savemodalEditSurveyGroup(Request $request)
    {
         DB::table('survey_group_access')->where('group_access_id', '=', $request->group_access_id)->delete();
        for($i=0;$i<count($_POST['code_ab']);$i++)
        {
            //save Survey Detail
          $delete_survey_group_access =  DB::table('survey_group_access')->insert(
                [
                    'code_ab'=>$_POST['code_ab'][$i],
                    'group_access_id'=>$request->group_access_id,
                    'transaction'=>$_POST['survey_group_type_trn'][$i],
                    'monitoring'=>$_POST['survey_group_type_mnt'][$i],
                    'message'=>$_POST['survey_group_type_msg'][$i],
                    'created_at'=>now(),
                    'updated_at'=>now(),
                ]
            );
        }


        if($delete_survey_group_access)
        {
            return response()->json(['success'=>'success']);
        }

    }

  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

         $validator = Validator::make($request->all(),[
                'group_name'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $groups = Groups::find($id);
        $groups->group_name = $request->group_name;
        $groups->save();

        if($groups)
        {
            return response()->json(['status'=>'success']);    
        }
        
    }

    
    public function destroy($id)
    {

        // $group_access = Groups::find($id);

        // // delete related   
        // $group_access->fa_group_access()->delete();
        // $group_access->survey_group_access()->delete();

        // if($group_access->delete()){
        //     return response()->json(['status'=>'success']);
        // } 

        $group = Groups::findOrFail($id);

        if($group->delete()) {  
            return response()->json(['status'=>'success']);    
        }   
    }

    
}
