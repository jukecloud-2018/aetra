<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Wo_task;
use App\Models\Administrator\Skill_wo;
use App\Models\Administrator\Skill_wo_detail;
use Yajra\Datatables\Datatables;
use Validator;

class SkillWOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get WO type
        $data['wo'] = Skill_wo::orderBy('created_at','desc')->get();
        $data['task'] = WO_task::all();
        return view('administrator.skillWO.v_index_skillWO', $data);
    }

    //get data and send to Datatable
    public function getData()
    {
        $data = Skill_wo::all();
         return Datatables::of($data)
         ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['skill_wo_name'=>'required',]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $skillWO = new Skill_wo;
        $skillWO->skill_wo_name = $_POST['skill_wo_name'];
        $skillWO->save();

        if(!empty($_POST['woTask'])){
            for ($i=0; $i < count($_POST['woTask']); $i++) {
                $detail = new Skill_wo_detail;
                $detail->job_code_id = $_POST['woTask'][$i];
                $detail->skill_wo_id = $skillWO->id;
                $detail->save();
            }
        }
        
        if($skillWO)
        {
            return response()->json(['status'=>'success']);    
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete skill wo
        $skillWO = Skill_wo::findOrFail($id);

        if($skillWO->delete())
        {
            Skill_wo_detail::where('skill_wo_id', $id)->delete();
            return response()->json(['status'=>'success']);    
        }   
    }

    //Detail Group
    public function detailWOSkill($id)
    {
        $data['skill_wo'] = Skill_wo::where('id',$id)->first();
        $data['action'] = $_POST['action'];
        return view('administrator.skillWO.v_detail_skill_wo',$data);   
    }

    //detail WO Type
    public function detailWoType($id)
    {
        $data['wo_type'] = Wo_task::all();
        $data['id'] = $id;
        return view('administrator.skillWO.v_detail_wo_type',$data);   
    }

    public function updateWOSkill(Request $request){
        $validator = Validator::make($request->all(),[
            'skill_wo_name'=>'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $skillWO = Skill_wo::where('id',$request->wo_id)->first();

        $skillWO->skill_wo_name = $request->skill_wo_name;
        $skillWO->save();

         $del_details_wo = Skill_wo_detail::where('skill_wo_id',$request->wo_id);
         $del_details_wo->delete();

        $valDetail = explode(',',$request->detail_value);
        foreach($valDetail as $val)
        {
            if($val != 0)
            {
                $detail = new Skill_wo_detail;
                $detail->job_code_id = $val;
                $detail->skill_wo_id = $skillWO->id;
                $detail->save();    
            }
        }

        if($valDetail)
        {
            return response()->json(['status'=>'success']);    
        }
    }

    public function saveWOType(Request $request){
        $del_details_wo = Skill_wo_detail::where('skill_wo_id',$request->skill_wo_id);
        $del_details_wo->delete();

        $valDetail = explode(',',$request->detail_value);
        foreach($valDetail as $val)
        {
            if($val != 0)
            {
                $detail = new Skill_wo_detail;
                $detail->job_code_id = $val;
                $detail->skill_wo_id = $request->skill_wo_id;
                $detail->save();    
            }
        }

        if($valDetail)
        {
            return response()->json(['status'=>'success']);    
        }
    }
}
