<?php

namespace App\Http\Controllers\Administrator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Workers;
use App\Models\Administrator\Users;
use App\Models\Administrator\Vendors;
use App\Models\Administrator\Skill_fa;
use App\Models\Administrator\Skill_wo;
use App\Models\Administrator\FA_Group;
use App\Models\Transaction\FA_transaction;
use App\Models\Administrator\Worker_date;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Validator;
use Redirect;
use DB;

class WorkerLoginController extends Controller
{
    public function index()
    {   
        //check end date for worker and is_login status
        $now = date('Y-m-d');
        $date_worker = DB::table('worker_date as a')
                            ->leftJoin('workers as b', 'a.worker_id', 'b.id')
                            ->leftJoin('users as c', 'b.user_id', 'c.id')
                            ->select('a.worker_id','a.start_date','a.end_date','c.is_login')
                            ->get();

        foreach ($date_worker as $key => $value) {
            if ($now > $value->end_date && $value->is_login == 1) {
                DB::table('workers')->where('id', $value->worker_id)
                    ->update(['worker_status'=>'Non Active']);
            }
            if ($now < $value->end_date && $value->is_login == 1) {
                DB::table('workers')->where('id', $value->worker_id)
                    ->update(['worker_status'=>'Active']);
            }
        }

        $data['vendors'] = Vendors::all();
        $data['skill_fa'] = Skill_fa::all();
        $data['faGroup']  = FA_Group::all();
        $data['count'] = count($data['faGroup']);
        $data['surveyGroup'] = DB::table('code_ab')->get();
        $data['worker'] = DB::table('workers as a')
                          ->rightJoin('users as e','e.id','a.user_id')
                          ->join('vendors as b','a.vendor_id','b.id')
                          ->join('skill_fa as c','c.id','a.skill_fa_id')
                          ->join('worker_date as d','d.worker_id','a.id')
                          ->select('a.worker_name','e.username','a.id','a.no_hp','a.no_ktp','a.worker_status','a.email',
                                  'b.vendor_name','b.id as vendor_id','c.id as skill_fa_id','c.skill_fa_name','d.start_date','d.end_date', 'a.user_id')
                          ->orderBy('a.created_at', 'desc')
                          ->where('a.status', 'fa')
                          ->get();

        $data['count'] = count($data['worker']);
        return view('administrator.workerLogin.v_index_workerLogin',$data);
    }

    public function workerWO($type)
    {
        if($type == 'wo'){
            $data['vendors'] = Vendors::all();
            $data['faGroup']  = FA_Group::all();
            $data['surveyGroup'] = DB::table('code_ab')->get();
            $data['worker'] = DB::table('workers as a')
                            ->rightJoin('users as e','e.id','a.user_id')
                            ->join('vendors as b','a.vendor_id','b.id')
                            ->join('worker_date as d','d.worker_id','a.id')
                            ->join('skill_wo as f', 'f.id', 'a.skill_wo_id')
                            ->select('a.worker_name','e.username','a.id','a.no_hp','a.no_ktp','a.worker_status','a.email',
                                    'b.vendor_name','b.id as vendor_id','d.start_date','d.end_date', 'a.user_id', 'f.id as skill_wo_id')
                            ->orderBy('a.created_at', 'desc')
                            ->where('a.status', 'wo')
                            ->get();
            $data['count'] = count($data['worker']);
            return view('administrator.workerLogin.v_index_workerWO',$data);
        }else{
            return Redirect::to('administrator/workerLogin');
        }
    }

    public function create()
    {
        $data['vendors'] = Vendors::all();
        $data['skill_fa'] = Skill_fa::all();
        $data['skill_wo'] = Skill_wo::all();
        return view('administrator.workerLogin.v_add_workerLogin',$data);
    }
    
    public function store(Request $request)
    {
        if($request->status == 'fa'){
            $validator = Validator::make($request->all(),[
                'username'=>'required',
                'worker_name'=>'required',
                'worker_id' => 'required',
                'no_ktp' => 'required',
                'no_hp' => 'required',
                'email' => 'required',
                'vendor'=>'required',
                'skillFA'=>'required',
                'dispatch_group'=>'required'
            ]);
        
            if($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            //save data user
            $username = $request->username;
            $dataUser = [
                'name' => $request->worker_name,
                'username' => $username,
                'is_login' => 0,
                'created_at' => Carbon::now()
            ];
            $user = DB::table('users')->insertGetId($dataUser);

            //save worker
            $data = [
                'status' => $request->status,
                'worker_name' => $request->worker_name,
                'worker_id' => $request->worker_id,
                'no_ktp' => $request->no_ktp,
                'no_hp' => $request->no_hp,
                'email' => $request->email,
                'user_id' => $user,
                'vendor_id' => $request->vendor,
                'skill_fa_id' => $request->skillFA,
                'created_at' => Carbon::now()
            ];
            $worker =DB::table('workers')->insertGetId($data);

            //date worker
            $worker_date = new Worker_date;
            $worker_date->worker_id = $worker;
            $worker_date->start_date = Carbon::createFromFormat('d-m-Y', $request->worker_start_date)->format('Y-m-d');
            $worker_date->end_date = Carbon::createFromFormat('d-m-Y', $request->worker_end_date)->format('Y-m-d');
            $worker_date->save();
            
            if(!empty($_POST['dispatch_group']))
            {
                for($ui = 0; $ui < count($_POST['dispatch_group']);$ui++ )
                {
                    $worker_dispatch = DB::table('worker_dispatch')->insert(['worker_id'=> $worker,'dispatch_group'=>$_POST['dispatch_group'][$ui],
                        // 'start_date'=> Carbon::createFromFormat('d-m-Y', $_POST['start_date'][$ui])->format('Y-m-d'),
                        // 'end_date'=>Carbon::createFromFormat('d-m-Y', $_POST['end_date'][$ui])->format('Y-m-d'),
                        'created_at' => Carbon::now(),
                    ]);   
                }
            }
            
            if(!empty($_POST['code_ab']))
            {
                for($ai = 0; $ai < count($_POST['code_ab']);$ai++ )
                {
                    $worker_survey =  DB::table('worker_survey')
                                        ->insert([
                                            'worker_id'=>$worker,
                                            'code_ab'=>$_POST['code_ab'][$ai],
                                            'created_at' => Carbon::now()
                                        ]);
                }
            }
    
        }elseif($request->status == 'wo'){
            $validator = Validator::make($request->all(),[
                    'username'=>'required',
                    'worker_name'=>'required',
                    'worker_id' => 'required',
                    'no_ktp' => 'required',
                    'no_hp' => 'required',
                    'email' => 'required',
                    'vendor'=>'required',
                    'skillWO'=>'required'
            ]);
            if($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            //save data user
            $username = $request->username;
            $dataUser = [
                'name' => $request->worker_name,
                'username' => $username,
                'is_login' => 0,
                'created_at' => Carbon::now()
            ];
            $user = DB::table('users')->insertGetId($dataUser);

            //save data worker
            $data = [
                'status' => $request->status,
                'worker_name' => $request->worker_name,
                'worker_id' => $request->worker_id,
                'no_ktp' => $request->no_ktp,
                'no_hp' => $request->no_hp,
                'email' => $request->email,
                'user_id' => $user,
                'vendor_id' => $request->vendor,
                'skill_wo_id' => $request->skillWO,
                'created_at' => Carbon::now()
            ];
            $worker =DB::table('workers')->insertGetId($data);

            //save date worker
            $worker_date = new Worker_date;
            $worker_date->worker_id = $worker;
            $worker_date->start_date = Carbon::createFromFormat('d-m-Y', $request->worker_start_date)->format('Y-m-d');
            $worker_date->end_date = Carbon::createFromFormat('d-m-Y', $request->worker_end_date)->format('Y-m-d');
            $worker_date->save();

            //save data survey worker
            if(!empty($_POST['code_ab']))
            {
                for($ai = 0; $ai < count($_POST['code_ab']);$ai++ )
                {
                    $worker_survey =  DB::table('worker_survey')
                                        ->insert([
                                            'worker_id'=>$worker,
                                            'code_ab'=>$_POST['code_ab'][$ai],
                                            'created_at' => Carbon::now()
                                        ]);   
                }
            }
        }

        if($worker)
        {
            return response()->json(['status'=>'success']);
        }
    }

    public function update(Request $request, $id)
    {
        if($request->status == 'fa'){
            $validator = Validator::make($request->all(),[
                'worker_name'=>'required',
                'vendor'=>'required',
                'skillFA'=>'required'
            ]);

            if($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            //save users
            $worker = Workers::find($id);
            $id_user_in_worker = $worker->user_id;
            $users = new Users;
            $users = Users::find($id_user_in_worker);
            $users->name = $request->worker_name;
            $users->username = $request->username;
            $users->is_login = 1;
            $users->save();

            //save worker
            $worker = new Workers;
            $worker = Workers::find($id);

            $worker->worker_name = $request->worker_name;
            $worker->user_id = $request->user_id;
            //$worker->worker_id = $request->worker_id;
            $worker->no_ktp = $request->no_ktp;
            $worker->no_hp = $request->no_hp;
            $worker->email = $request->email;
            $worker->vendor_id = $request->vendor;
            $worker->skill_fa_id = $request->skillFA;
            $worker->skill_wo_id = null;
            $worker->status = 'fa';
            $worker->save();
            
            $worker_date = new Worker_date;
            $worker_date = Worker_date::where('worker_id',$id)->firstOrFail();
            
            $worker_date->worker_id = $id;
            $worker_date->start_date = Carbon::createFromFormat('d-m-Y', $request->worker_start_date)->format('Y-m-d');
            $worker_date->end_date = Carbon::createFromFormat('d-m-Y', $request->worker_end_date)->format('Y-m-d');
            $worker_date->save();
        }elseif($request->status == 'wo'){
            $validator = Validator::make($request->all(),[
                'worker_name'=>'required',
                'vendor'=>'required',
                'skillWO'=>'required'
            ]);

            if($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            //save users
            $worker = Workers::find($id);
            $id_user_in_worker = $worker->user_id;
            $users = new Users;
            $users = Users::find($id_user_in_worker);
            $users->name = $request->worker_name;
            $users->username = $request->username;
            $users->is_login = 1;
            $users->save();

            //save worker
            $worker = new Workers;
            $worker = Workers::find($id);
            $worker->worker_name = $request->worker_name;
            $worker->user_id = $request->user_id;
            //$worker->worker_id = $request->worker_id;
            $worker->no_ktp = $request->no_ktp;
            $worker->no_hp = $request->no_hp;
            $worker->email = $request->email;
            $worker->vendor_id = $request->vendor;
            $worker->skill_fa_id = null;
            $worker->skill_wo_id = $request->skillWO;
            $worker->status = 'wo';
            $worker->save();
            
            DB::table('worker_dispatch')->where('worker_id', '=', $id)->delete();

            $worker_date = new Worker_date;
            $worker_date = Worker_date::where('worker_id',$id)->firstOrFail();
            $worker_date->worker_id = $id;
            $worker_date->start_date = Carbon::createFromFormat('d-m-Y', $request->worker_start_date)->format('Y-m-d');
            $worker_date->end_date = Carbon::createFromFormat('d-m-Y', $request->worker_end_date)->format('Y-m-d');
            $worker_date->save();
        }
        
        if($worker)
        {
            return response()->json(['status'=>'success']);
        }
    }

    public function editSurvey($id)
    {
        $data['vendors'] = Vendors::all();
        $data['skill_fa'] = Skill_fa::all();
        //$data['skill_wo'] = Skill_wo::all();
        $data['faGroup']  = FA_Group::all();
        $data['count'] = count($data['faGroup']);
        $data['surveyGroup'] = DB::table('code_ab')->get();
        $data['worker_id'] = $id;
        return view('administrator.workerLogin.v_edit_survey',$data);
    }

    public function updateListSurvey($id)
    {        
        if(!empty($_POST['code_ab']))
        {
            $delete_survey_group_access = DB::table('worker_survey')->where('worker_id','=',$id)->delete();
            for($ei = 0; $ei < count($_POST['code_ab']);$ei++ )
            {
                $worker_survey =  DB::table('worker_survey')->insert(['worker_id'=>$id,'code_ab'=>$_POST['code_ab'][$ei]])  ;   
            }
        }

        if($delete_survey_group_access)
        {
            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['error'=>'error']);   
        }

    }

    public function editDispatch($id)
    {
        $data['faGroup']  = FA_Group::all();
        $data['count'] = count($data['faGroup']);
        $data['workGroup']  = DB::table('worker_dispatch')->where('worker_id', $id)->get();
        $data['worker_id'] = $id;
        return view('administrator.workerLogin.v_edit_dispatch',$data);
    }


    public function updateListFA($id)
    {        
        if(!empty($_POST['dispatch_group']))
        {
            $delete_dispatch = DB::table('worker_dispatch')->where('worker_id','=',$id)->delete();
            
            for($ei = 0; $ei < count($_POST['dispatch_group']);$ei++ )
            {
                
                $worker_dispatch =  DB::table('worker_dispatch')->insert(['worker_id'=>$id,'dispatch_group'=>$_POST['dispatch_group'][$ei]])  ;   
            }

        }

        if($delete_dispatch)
        {
            return response()->json(['success'=>'success']);
        }
        else
        {
            return response()->json(['error'=>'error']);   
        }

    }

  
    public function show($id)
    {
        $data['vendors'] = Vendors::all();
        $data['skill_fa'] = Skill_fa::all();
        $data['skill_wo'] = Skill_wo::all();
        $data['worker'] = Workers::find($id);
        $userid = $data['worker']->user_id;
        $data['users'] = Users::find($userid);
        $data['worker_date'] = Worker_date::where('worker_id',$id)->firstOrFail();
        return view('administrator.workerLogin.v_edit_workerLogin',$data);
    }
    
    public function destroy($id)
    {
        $worker = Workers::findOrFail($id);
        $data_fa = FA_transaction::where('worker_id',$id)->first();
        if(!empty($data_fa))
        {
            return response()->json(['status'=>'error','message'=>'Mohon maaf worker tersebut telah di assigment ke pekerjaan']);
        }

        DB::table('users')->where('id', '=', $worker->user_id)->delete();
        DB::table('Worker_date')->where('worker_id', '=', $id)->delete();
        DB::table('worker_dispatch')->where('worker_id', '=', $id)->delete();
        DB::table('worker_survey')->where('worker_id', '=', $id)->delete();

        if($worker->delete())
        {
            return response()->json(['status'=>'success']);    
        }   
    }


    // for get data -> datatables
    public function getData()
    {
        $data = DB::table('workers as a')
        ->rightJoin('users as e','e.id','a.user_id')
        ->join('vendors as b','a.vendor_id','b.id')
        ->join('skill_fa as c','c.id','a.skill_fa_id')
        ->join('worker_date as d','d.worker_id','a.id')
        ->select('a.worker_name','e.username','a.id','a.no_hp','a.no_ktp','a.worker_status','a.email',
                 'b.vendor_name','b.id as vendor_id','c.id as skill_fa_id','c.skill_fa_name','d.start_date','d.end_date', 'a.user_id')
        ->orderBy('a.created_at', 'desc')
        ->get();
        
         return Datatables::of($data)
          ->editColumn('vendor', function($data) {
                return $data->vendor_name;
            })
         ->make(true);
    }


    public function viewDispatch($id)
    {
        $data['faGroup'] = DB::table('worker_dispatch')->where('worker_id',$id)->get();
        return view('administrator.workerLogin.view_dispatch',$data);
    } 

    public function viewSkill($id)
    {
        // $data['skill'] = DB::table('skill_fa_detail as a')->join('fa_type as b','a.fa_type_id','=','b.id')->select('*')->where('skill_fa_id',$id)->get();
        $data['skill'] = DB::table('skill_fa as a')
                              //->join('skill_fa_detail as b', 'b.skill_fa_id', 'a.id')
                              //->join('workers as c', 'c.skill_fa_id', 'a.id')
                              ->where('a.id', $id)
                              ->get();
        // //Debug                       
        // echo json_encode($data);
        // exit();
        return view('administrator.workerLogin.view_skill',$data);
    } 

    public function viewSkillWO($id){
        $data['skill'] = DB::table('skill_wo as a')
                           //->join('skill_wo_detail as b', 'b.skill_wo_id', 'a.id')
                           ->where('a.id', $id)
                           ->get();
        // //debug
        // echo json_encode($data);
        // exit();

        return view('administrator.workerLogin.view_skill_wo', $data);
    }


    public function viewSurvey($id)
    {
        $data['survey'] = DB::table('worker_survey as a')->join('code_ab as b','a.code_ab','=','b.code_ab')->select('*')->where('worker_id',$id)->get();
        $data['count'] = count($data['survey']);
        return view('administrator.workerLogin.view_survey',$data);
    }

    public function editPassword($user_id)
    {
        $data['pass'] = DB::table('users')
                        ->select('*')->where('id',$user_id)->get();
        return view('administrator.workerLogin.view_password',$data);    
    }

    public function updatePassword(Request $request, $id){
        $data = Users::find($id);
        $data->password = bcrypt ($request->password);
        if($data->update()){
            return response()->json(['status'=>'success']);
        }
    }
}
