<?php
/*
    this function for manage Vendor
    @author Arief Manggala putra
    created date : 22 10 2018
*/

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Administrator\Vendors;
use Yajra\Datatables\Datatables;



class VendorsController extends Controller
{
    
    // for view index
    public function index()
    {
        return view('administrator.vendors.v_index_vendors');
    }

    // for save vendor
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'vendor_name'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $vendor = new Vendors;

        $vendor->vendor_name = $request->vendor_name;
        $vendor->save();
        if($vendor)
        {
            return response()->json(['status'=>'success']);
        }
    }

    // for get data -> datatables
    public function getData()
    {
         $data = Vendors::all();
         return Datatables::of($data)->make(true);;
    }

    // for put edit Vendor
    public function edit($id)
    {
        $data = Vendors::where('id',$id)->first();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

    // for getData Vendor
    public function viewVendor($id)
    {
        $data = Vendors::where('id',$id)->first();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

   //for update vendor
    public function update(Request $request, $id)
    {
        $data = Vendors::find($id);
        $data->vendor_name = $request->vendor_name;
        if($data->save())
        {
            return response()->json(['status'=>'success']);    
        }
    }

    // for deleete vendor
    public function destroy($id)
    {
        $vendor = Vendors::findOrFail($id);

        if($vendor->delete())
        {
            return response()->json(['status'=>'success']);    
        }   
    }
}
