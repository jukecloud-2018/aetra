<?php

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use App\Models\Administrator\HHManagement;
use Validator;

class HHManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('administrator.hhManagement.v_index_hhManagement');
    }

    public function getData(){
        $data = HHManagement::all();
        return Datatables::of($data)->make(true);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
                'no_imei'=>'required',
                'username'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $data = new HHManagement;

        $data->no_imei = $request->no_imei;
        $data->username = $request->username;
        $data->mode = "Active";
        $data->save();

        if($data)
        {
            return response()->json(['status'=>'success']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = HHManagement::where('id',$id)->first();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = HHManagement::find($id);
        $data->no_imei = $request->no_imei;
        $data->username = $request->username;

        if($data->save())
        {
            return response()->json(['status'=>'success']);    
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = HHManagement::findOrFail($id);

        if($data->delete())
        {
            return response()->json(['status'=>'success']);    
        }   
    }
}
