<?php
/*
    this function for skill fa 
    @author arief manggala putra
    date 25 10 2018

    please comment your code .
*/


namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Skill_fa;
use App\Models\Administrator\Fa;
use App\Models\Administrator\Fa_type;
use App\Models\Administrator\Skill_fa_detail;
use Yajra\Datatables\Datatables;
use Validator;
use DB;


class SkillFAController extends Controller
{
    /**
        for view index
     */
    public function index()
    {
        $data['fa'] = skill_fa::orderBy('created_at', 'desc')->get();
        $data['type'] = fa_type::orderBy('fa_type_cd', 'asc')->get();
        return view('administrator.skillFA.v_index_skillFA',$data);
    }

    #getdata for datatable
    public function getData()
    {
        $data = Skill_fa::all();
         return Datatables::of($data)
         ->make(true);
    }
   
   // for modal detail fa skill
   public function detailFASkill($id)
   {
        $data['skill_fa'] = Skill_fa::where('id',$id)->first();
        $data['action'] = $_POST['action'];
        return view('administrator.skillFA.v_detail_skill_fa',$data);   
   }

   // for modal detail fa type
   public function detailFaType($id)
   {
        $data['fa_type'] = Fa_type::all();
        $data['id'] = $id;
        return view('administrator.skillFA.v_detail_fa_type',$data);   
   }

   // for update fa skill
   public function updateFASkill(Request $request)
   {
       $validator = Validator::make($request->all(),[
            'skill_fa_name'=>'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $skillFA = Skill_fa::where('id',$request->fa_id)->first();

        $skillFA->skill_fa_name = $request->skill_fa_name;
        $skillFA->save();

         $delete_survey_detail = Skill_fa_detail::where('skill_fa_id',$request->fa_id);
         $delete_survey_detail->delete();

        $valDetail = explode(',',$request->detail_value);
        foreach($valDetail as $val)
        {
            if($val != 0)
            {
                $detail = new Skill_fa_detail;
                $detail->fa_type_id = $val;
                $detail->skill_fa_id = $skillFA->id;
                $detail->save();    
            }
        }

        if($valDetail)
        {
            return response()->json(['status'=>'success']);    
        }
   }


   // for save fa skill
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(),['skill_fa_name'=>'required',]);
        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $skillFA = new Skill_fa;
        $skillFA->skill_fa_name = $_POST['skill_fa_name'];
        $skillFA->save();
        if(!empty($_POST['faType'])){
            for ($i=0; $i < count($_POST['faType']); $i++) { 
                $detail = new Skill_fa_detail;
                $detail->fa_type_id = $_POST['faType'][$i];
                $detail->skill_fa_id = $skillFA->id;
                $detail->save();
            }
        }
        if($skillFA)
        {
            return response()->json(['status'=>'success']);    
        }
    }

    public function saveFAType(Request $request)
    {
        $delete_survey_detail = Skill_fa_detail::where('skill_fa_id',$request->skill_fa_id);
        $delete_survey_detail->delete();

        $valDetail = explode(',',$request->detail_value);
        foreach($valDetail as $val)
        {
            if($val != 0)
            {
                $detail = new Skill_fa_detail;
                $detail->fa_type_id = $val;
                $detail->skill_fa_id = $request->skill_fa_id;
                $detail->save();    
            }
        }

        if($valDetail)
        {
            return response()->json(['status'=>'success']);    
        }
    }


    /**
        for delete fa skill
     */
    public function destroy($id)
    {
        //
        $survey = Skill_fa::findOrFail($id);

        if($survey->delete())
        {
            Skill_fa_detail::where('skill_fa_id', $id)->get();
            return response()->json(['status'=>'success']);    
        }   
    }
}
