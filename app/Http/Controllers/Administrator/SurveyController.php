<?php

/*
    for manage survey 
    @arief manggala putra
    created date 23 10 2018
*/

namespace App\Http\Controllers\Administrator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Type_data;
use App\Models\Administrator\Modules;
use App\Models\Administrator\Survey;
use App\Models\Administrator\Survey_detail;
use Yajra\Datatables\Datatables;
use Validator;
use DB;


class SurveyController extends Controller
{
    
    // for view index
    public function index()
    {
        //
        $data['type_data'] = Type_data::all();
        return view('administrator.survey.v_index_survey',$data);
    }

    // for save survey
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(),[
            'survey_name'=>'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }


        //save user
        $survey = new Survey;
        // $survey_code=null;

        // $date  = date('Ym');
        // $query = DB::table('survey')->select(DB::raw('max(right(survey_code,3)) as max'))->where('survey_code','like','%'.$date.'%')->first();
        // if(empty($query->max))
        // {
        //     $survey_code = $date.rand(100,999);

        // }
        // else
        // {

        //     $tmp = ((int)$query->max)+1;
        //     $survey_code = $date.sprintf("%03s",$tmp);
        // }


        $survey->survey_name = $request->survey_name;
        $survey->object_survey = $request->object_survey;

        $survey->status = (!empty($request->status_survey)) ?  'Non Active': 'Active' ;
        $survey->save();


        for($i=0;$i < count($_POST['survey_detail_name']);$i++)
        {
            $detail = new Survey_detail;
            $detail->survey_detail_name = $_POST['survey_detail_name'][$i];
            $detail->survey_id = $survey->id;
            $detail->status = (!empty($_POST['status_detail_survey'][$i])) ? 'Active':'Non Active';
            $detail->type_data_id = $_POST['type_data_id'][$i];
            $detail->save();
        }

        if($detail)
        {
            return response()->json(['status'=>'success']);    
        }
    }

  
    // for delete survey
    public function destroy($id)
    {
        //
        $survey = Survey::findOrFail($id);
        if($survey->delete())
        {
            Survey_detail::where('survey_id', '=', $id)->delete();
            return response()->json(['status'=>'success']);    
        }   

    }

    // for detail survey
    public function detailSurvey($id)
    {
        $data['survey'] = Survey::with('survey_detail')->where('id',$id)->first();
        $data['detail'] =  Survey::with('survey_detail')->where('id',$id)->get();
        $data['type_data'] =  Type_data::all();
        $data['page'] =  $_GET['page'];
        return view('administrator.survey.v_detail_survey',$data);   
    }

    // for get datatable
    public function getData()
    {
        $data = Survey::all();
         return Datatables::of($data)
          ->addColumn('status', function ($data) {
                if($data->status == 'Active')
                {
                    $dt_non = 'Non Active';
                    return '<input  checked type="checkbox" name="status_check" id="status_active" onClick="changeStatus(\'' .$data->id. '\',\'' .$dt_non. '\')"  /> Active';
                }
                else
                {
                     return '<input  type="checkbox" name="status_check" id="status_active"  onclick=changeStatus("'.$data->id.'","Active") /> Non Active';    
                }
                
            }) 
         ->rawColumns(['status'])
         ->make(true);
    }

    // get type data 
    public function getTypeData()
    {
        $data = Type_data::all();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

    // for modal edit
    public function updateStatus($id)
    {
        $survey = Survey::find($id);
        $survey->status = $_GET['status'];
        $survey->save();
        return response()->json(['status'=>'success']);
    }

    // for delete detail
    public function deleteDetail($id)
    {
         $vendor = Survey_detail::findOrFail($id);
        if($vendor->delete())
        {
            return response()->json(['status'=>'success']);    
        }   
    }

    // for update survey
    public function updateSurvey(Request $request)
    {
         $validator = Validator::make($request->all(),[
                'survey_name'=>'required',
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        //save Survey
        $survey = Survey::find($request->survey_id);

        $survey->survey_name = $request->survey_name;
        $survey->object_survey = $request->object_survey;
        $survey->status = (!empty($request->status_survey)) ?  'Active': 'Non Active' ;
        $survey->save();
            

        //save Survey Detail

         $delete_survey_detail = Survey_detail::where('survey_id',$request->survey_id);
         $delete_survey_detail->delete();
        foreach($_POST['survey_detail_name'] as $i=> $m)
        {
            //save Survey Detail
            $Survey_detail = new Survey_detail;
            $Survey_detail->survey_detail_name = $_POST['survey_detail_name'][$i];
            $Survey_detail->survey_id = $survey->id;
            $Survey_detail->status = (!empty($_POST['status_detail_survey'][$i])) ? 'Active':'Non Active';
            $Survey_detail->type_data_id = $_POST['type_data_id'][$i];
            $Survey_detail->save();
        }

        if($Survey_detail)
        {
            return response()->json(['status'=>'success']);
        }
    }
}
