<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\Administrator\Users;
use App\Models\Administrator\Workers;
use App\Models\Transaction\AssignmentSurvey;
use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\FA_Transaction_photo;
use App\Models\Transaction\FA_Transaction_ttd;
use App\Models\Administrator\FA_mapping_char_type;
use App\Models\Transaction\FA_Transaction_characteristic;
use App\Models\Transaction\WO_Transaction_characteristic;
use App\Models\Transaction\WO_Transaction_photo;
use App\Models\Transaction\WO_Transaction_ttd;
use App\Models\Transaction\WO_Template_BOM_Characteristic;
use App\Models\Transaction\Customer;
use App\Models\Administrator\Fa_type;
use App\models\Administrator\Log_fa_status;
use App\Models\Administrator\Log_wo_status;
use App\Models\Administrator\System_parameter;
use App\Models\Transaction\Survey_Transaction_Characteristic;
use App\Models\Transaction\Survey_Transaction_Photo;
use App\Models\Transaction\Survey_Transaction;
use App\Models\Transaction\WO_transaction;
use App\Models\Administrator\Wo_task;
use App\Models\Administrator\Worker_date;
use App\Models\Administrator\Worker_dispatch;
use App\Models\Transaction\FA_Template_BOM_Characteristic;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL; 
use GuzzleHttp\Exception\GuzzleException;   
use GuzzleHttp\Client;
use SoapClient;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\Import;

use Carbon\Carbon;

use Validator;
use DB;

include_once(app_path() . '/MyLibs/nusoap.php');

Class ApiController extends Controller
{
	public function signup_hh(Request $request)
    {
        $validator = Validator::make($request->all(),['ktp'=>'required','username'=>'required','phone'=>'required','pin'=>'required']); //last imei ya belum

		if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'']);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        //cek apakah versi sudah terupdate atau belum
        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }else{
            $userReady = DB::table('workers as a')->join('users as b','a.user_id','=','b.id')->select('*')->where('username',$request->username)->first();


            if(!empty($userReady) && $userReady->worker_status == 'Active' && $userReady->is_login == 1)
            {   
                return response()->json(['success'=>0,'message'=>'Maaf username anda telah terdaftar silahkan login','user_id'=>'']);
            }

            $users = Users::where('username',$request->username)->first();
            $w_id = $users->id;
            $worker_data = Workers::where('user_id', $w_id)->first();

            if(!empty($users))
            {   
                
                DB::table('workers')
                ->where('user_id', $users->id)
                ->update(['no_hp'=>$request->phone,'no_ktp'=>$request->ktp,'worker_id'=>$worker_data->worker_id,'worker_status'=>'Active']); //'imei'=>$request->imei,
                
                DB::table('users')
                ->where('id', $users->id)
                ->update(['password' =>bcrypt($request->pin), 'is_login'=>1]);

                return response()->json(['success'=>1,'message'=>'','user_id'=>$users->id,'worker_id'=>$worker_data->worker_id,'type'=>0]);    
            }
            else
            {
                return response()->json(['success'=>0,'message'=>'Maaf username belum di daftarkan oleh admin','user_id'=>'']);
            } 
        }
    }

    public function login_hh(Request $request)
    {

    	$validator = Validator::make($request->all(),['username'=>'required','password'=>'required']);
    	$gagal = response()->json(['message'=>'Login Gagal Silahkan Coba Lagi','success'=>0,'userid'=>'','name'=>'','pict'=>'','worker_id'=>'','type'=>'']);

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }else{
            $users = DB::table('users as a')
                ->join('workers as b','a.id','=','b.user_id')
                ->select('a.id','b.worker_status','a.name','b.picture','a.password','b.worker_id')
                ->where('a.username',$request->username)
                ->where('b.worker_status','Active')
                // ->where('a.is_login',FALSE)
                ->first();

            if($users)
            {
                $username = $request->username;
                $password =  Hash::check($request->password,$users->password);
            }
            else
            {
                return $gagal;
            }
            
            if($password)
            {
                 $update = Users::find($users->id);
                 $update->is_login = '1';
                 $update->save();
                 return response()->json(['message'=>'','success'=>1,'userid'=>$users->id,'name'=>$users->name,'pict'=>'','worker_id'=>$users->worker_id,'type'=>0]);
            }
            else
            {   
                return $gagal;
            }
        }
    }

    public function logout_hh(Request $request)
    {
        // $version_code = $request->version_code; //version code yang dikirim dari android
        // $check_version = System_parameter::first();//get system parameter data 
        // $v_code_db = $check_version->v_code;//echo version code yang ada di db

        // if ($version_code != $v_code_db) {
        //     return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        // }

    	if(!empty($request->userid))
    	{
    		$user = Users::find($request->userid);
	    	$user->is_login = 0;
	    	$user->save();
    		return response()->json(['message'=>'','success'=>1]);
    	}
    	else
    	{
    		return response()->json(['message'=>'Paramater tidak ditemukan','success'=>0]);	
    	}

    }
	
	public function message_hh(Request $request)
	{
        $validator = Validator::make($request->all(),['userid'=>'required']);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $worker = Workers::where('user_id', $request->userid)->first();

		if(!empty($worker))
    	{

    		$message = DB::table('message_to as a')
            ->join('workers as b','a.user_id','=','b.user_id')
            ->join('users as c','b.user_id','=','c.id')
            ->join('messages as d', 'a.id_send', 'd.id')
            ->select('d.id','d.subject','d.created_at','d.message')
            ->where('c.id',$request->userid)->get();

            // echo json_encode($message);
            // exit();
			foreach($message as $msg)
			{
				$row = array();
				$row['id'] = $msg->id;
				$row['subject'] = $msg->subject;
				$row['created_at']  = $msg->created_at;
				$row['message']  = $msg->message;
				$data[] = $row;
			}
			
			return response()->json($data);
    	}
    	else
    	{
    		return response()->json(['message'=>'User dan Pesan Tidak Di Temukan','success'=>0]);	
    	}
	}
	
	public function send_start_status_hh(Request $request)
	{
		$validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
		$gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
		
		if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $level_battery = $request->input('level_battery');
        $level_signal = $request->input('level_signal');
        $fa_transaction_id = $request->input('fa_transaction_id');
        $fa_id = FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)->first();
        $fa_type_cd = trim($fa_id->fa_type_cd);

        $lock_status = Fa_type::where('fa_type_cd', $fa_type_cd)->first();

        if ($lock_status->lock_fa == 'Started') 
        {
            $url = "http://172.27.1.38:8280/services/DS_Get_FA_Status_Single?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $pFaId = $request->input('fa_transaction_id');
                $operation = 'getFaStatusSingle';
                $params = array(
                    'pFaId' => $pFaId
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['getFaStatusSingle'] as $key => $value) {
                    $status = trim($value);
                    if ($status == 'P') {
                        
                    $getWorker = Workers::where('user_id',$request->userid)->first();

                    //Kirim Lock Fa Ke Simpel
                    $xmlrequest = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c1c="http://splwg.com/C1CustomerContactMaintenance.xsd">
                    <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                        <wsse:Security>
                            <wsse:UsernameToken>       
                                <wsse:Username>SIMPEL2</wsse:Username>
                                <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>        
                            </wsse:UsernameToken>    
                        </wsse:Security>  
                    </SOAP-ENV:Header>
                        <SOAP-ENV:Body>
                            <C1FieldActivityMaintenance dateTimeTagFormat="CdxDateTime"  transactionType ="UPDATE" >
                                <C1FieldActivityMaintenanceService>
                                    <C1FieldActivityMaintenanceHeader
                                        FieldActivityID="'.$fa_transaction_id.'"     
                                        />
                                        <C1FieldActivityMaintenanceDetails 
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            ActivityType="'.$fa_type_cd.'"  
                                            > 
                                            <FASteps>
                                                <FAStepsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />  
                                            </FASteps>
                                            <FACharacteristics>
                                                <FACharacteristicsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                                <FACharacteristicsRow
                                                    rowAction="Add" 
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    CharacteristicType="CMFALOCK" 
                                                    Sequence="100"
                                                    SearchCharacteristicValue="Y"
                                                    CharacteristicValue="Y" >
                                                </FACharacteristicsRow>
                                            </FACharacteristics>
                                    <FaRem>
                                        <FaRemHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />
                                    </FaRem>
                                    <FaLog>
                                        <FaLogHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />
                                    </FaLog>
                                        </C1FieldActivityMaintenanceDetails>
                                    </C1FieldActivityMaintenanceService>
                                </C1FieldActivityMaintenance>
                            </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>';

                        // print_r($xmlrequest);
                        // exit();

                        $action = 'C1FieldActivityMaintenance';
                        //$url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                        $url = 'http://172.27.1.38:8280/services/PS_Field_Activity?wsdl';
                        $client = new Client(); // Create the Guzzle Client        
                        $response = $client->post($url, [         
                                        'headers' => [           
                                        'SOAPAction' => $action, // Add the action              
                                        'Content-Type' => 'text/xml'          
                                    ],          
                                    'body' => $xmlrequest // Put the xml in the body        
                                ]);
                        // print_r($response);
                        // exit();
                        if ($response) {
                            //return $response->getBody()->getContents();
                            $res = $response->getBody()->getContents();
                            $doc = new \DOMDocument();
                            $doc->loadXML($res);
                            $status = $doc->getElementsByTagName('faultcode')->length;
                            // print_r($status);
                            // print_r($res);
                            // exit();

                            if ($status == 1) {
                                //$status = $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue;
                                //$err = $doc->getElementsByTagName('ResponseText')->item(0)->nodeValue;
                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Fail Send CMFALOCK';
                                $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                                $log->save();

                                return response()->json(['message'=>'Gagal Lock FA Ke Simpel', 'success'=>0]); 
                            }else{
                                $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                                    ->update(['assign_status'=>'Started']);

                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Started';
                                $log->message = 'Worker Starting a job';
                                $log->save();

                                //update level batery dan signal di table worker
                                $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                         ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 

                                return response()->json(['message'=>'','success'=>1]); 
                            }
                        }
                        
                    }else if($status == 'X'){
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Cancel';
                        $log->message = 'FA Transaction ID has been canceled';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'Fa Sudah Di Close','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Fa Close Gagal Di Update','success'=>0]);
                        }
                    }else if($status == 'C'){
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Compelete','status'=>'C', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Compelete';
                        $log->message = 'FA Transaction ID has been completed';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'Fa Sudah Complete','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Fa Complete Gagal Di Update','success'=>0]);
                        }
                    }else{
                        return response()->json(['message'=>'Silahkan Cek Kembali FA dan Id Worker','success'=>0]);
                    }
                }
            }
        }
        else
        {
            $url = "http://172.27.1.38:8280/services/DS_Get_FA_Status_Single?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');

                $pFaId = $request->input('fa_transaction_id');
                $operation = 'getFaStatusSingle';
                $params = array(
                    'pFaId' => $pFaId
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['getFaStatusSingle'] as $key => $value) {
                    $status = trim($value);
                    if ($status == 'P') {
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Started', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Started';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                             return response()->json(['message'=>'','success'=>1]); 
                        }
                        else
                        {
                             return response()->json(['message'=>'Gagal Save Status Start Di WMS','success'=>0]); 
                        }
                    }else if($status == 'X'){
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Cancel';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'Fa Sudah Di Close','success'=>true]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Fa Close Gagal Di Update','success'=>false]);
                        }
                    }else{
                        return response()->json(['message'=>'Silahkan Cek Kembali FA dan Id Worker','success'=>false]);
                    }
                }
            }
        }
	}

	public function send_pickup_status_hh(Request $request)
	{
		$validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
		$gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
		
		if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }
		
        $getWorker = Workers::where('user_id',$request->userid)->first();
	
		$trans  = FA_transaction::where('worker_id',$getWorker->id)
                    ->where('fa_transaction_id',$request->fa_transaction_id)
                    ->update(['assign_status'=>'Pickup']);
		
		if($trans)
		{
			 return response()->json(['message'=>'','success'=>1]);	
		}
		else
		{
			 return $gagal;
		}

	}

	public function send_cancel_status_hh(Request $request)
	{
		$validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
		$gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
		
		if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }
		
        $getWorker = Workers::where('user_id',$request->userid)->first();

		$trans  = FA_transaction::where('worker_id',$getWorker->id)
                    ->where('fa_transaction_id',$request->fa_transaction_id)
                    ->update(['assign_status'=>'Cancel Worker']);

        //insert log status fa transaction
        $log = new Log_fa_status;
        $log->fa_transaction_id= $request->fa_transaction_id;
        $log->status = 'Cancel Worker';
        $log->save();

		if($trans)
		{
			 return response()->json(['message'=>'','success'=>1]);	
		}
		else
		{
			 return $gagal;
		}

	}

    public function send_ontheway_status_hh(Request $request)
    {
        $validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db
        
        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $level_battery = $request->input('level_battery');
        $level_signal = $request->input('level_signal');
        $fa_transaction_id = $request->input('fa_transaction_id');
        $fa_id = FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)->first();
        $fa_type_cd = trim($fa_id->fa_type_cd);

        /**
            1. select worker berdasarkan userid untuk mengambil id nya
            1. select fa transaction berdasarkan id worker
            2. cek assign_status
            3. jika ada yang on the way / started
            4. kasih response gagal
        */
        $worker = Workers::where('user_id', '=', $request->input('userid'))->first();

        $cek_otw = FA_transaction::where('worker_id','=',$worker->id)
                                        ->where('assign_status', '=', 'On The Way')
                                        ->first();

        $cek_start = FA_transaction::where('worker_id','=',$worker->id)
                                        ->where('assign_status', '=', 'Started')
                                        ->first();

        //cek status lock dari mapping type lock fa
        $lock_status = Fa_type::where('fa_type_cd', $fa_type_cd)->first();

         

        // if ($cek_otw) {
        //     return response()->json(['message'=>'Sedang Ada Fa Id Dalam Status On The Way', 'success'=>0]);
        // }else if($cek_start){
        //     return response()->json(['message'=>'Sedang Ada Fa Id Dalam Status Started, Silahkan Selesaikan Pekerjaan Tersebut Terlebih Dahulu', 'success'=>0]);
        // }else 

        if ($lock_status->lock_fa == 'On The Way' || $lock_status->lock_fa == 'Unlock'){
            $url = "http://172.27.1.38:8280/services/DS_Get_FA_Status_Single?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $pFaId = $request->input('fa_transaction_id');
                $operation = 'getFaStatusSingle';
                $params = array(
                    'pFaId' => $pFaId
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['getFaStatusSingle'] as $key => $value) {
                    $status = trim($value);
                    if ($status == 'P') {
                        
                    $getWorker = Workers::where('user_id',$request->userid)->first();

                    //Kirim Lock Fa Ke Simpel
                    $xmlrequest = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c1c="http://splwg.com/C1CustomerContactMaintenance.xsd">
                    <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                        <wsse:Security>
                            <wsse:UsernameToken>       
                                <wsse:Username>SIMPEL2</wsse:Username>
                                <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>        
                            </wsse:UsernameToken>    
                        </wsse:Security>  
                    </SOAP-ENV:Header>
                        <SOAP-ENV:Body>
                            <C1FieldActivityMaintenance dateTimeTagFormat="CdxDateTime"  transactionType ="UPDATE" >
                                <C1FieldActivityMaintenanceService>
                                    <C1FieldActivityMaintenanceHeader
                                        FieldActivityID="'.$fa_transaction_id.'"     
                                        />
                                        <C1FieldActivityMaintenanceDetails 
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            ActivityType="'.$fa_type_cd.'"  
                                            > 
                                            <FASteps>
                                                <FAStepsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />  
                                            </FASteps>
                                            <FACharacteristics>
                                                <FACharacteristicsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                                <FACharacteristicsRow
                                                    rowAction="Add" 
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    CharacteristicType="CMFALOCK" 
                                                    Sequence="100"
                                                    SearchCharacteristicValue="Y"
                                                    CharacteristicValue="Y" >
                                                </FACharacteristicsRow>
                                            </FACharacteristics>
                                    <FaRem>
                                        <FaRemHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />
                                    </FaRem>
                                    <FaLog>
                                        <FaLogHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />
                                    </FaLog>
                                        </C1FieldActivityMaintenanceDetails>
                                    </C1FieldActivityMaintenanceService>
                                </C1FieldActivityMaintenance>
                            </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>';

                        // print_r($xmlrequest);
                        // exit();

                        $action = 'C1FieldActivityMaintenance';
                        //$url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                        $url = 'http://172.27.1.38:8280/services/PS_Field_Activity?wsdl';
                        $client = new Client(); // Create the Guzzle Client        
                        $response = $client->post($url, [         
                                        'headers' => [           
                                        'SOAPAction' => $action, // Add the action              
                                        'Content-Type' => 'text/xml'          
                                    ],          
                                    'body' => $xmlrequest // Put the xml in the body        
                                ]);
                        // print_r($response);
                        // exit();
                        if ($response) {
                            //return $response->getBody()->getContents();
                            $res = $response->getBody()->getContents();
                            $doc = new \DOMDocument();
                            $doc->loadXML($res);
                            $status = $doc->getElementsByTagName('faultcode')->length;
                            // print_r($status);
                            // print_r($res);
                            // exit();

                            if ($status == 1) {
                                //$status = $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue;
                                //$err = $doc->getElementsByTagName('ResponseText')->item(0)->nodeValue;
                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Fail Lock FA to SIMPEL';
                                $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                                $log->save();

                                return response()->json(['message'=>'Gagal Lock FA Ke Simpel', 'success'=>0]); 
                            }else{
                                $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                                    ->update(['assign_status'=>'On The Way']);

                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'On The Way';
                                $log->message = 'Worker on the way to job location';
                                $log->save();

                                //update level batery dan signal di table worker
                                $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                         ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 

                                return response()->json(['message'=>'','success'=>1]); 
                            }
                        }
                        
                    }else if($status == 'X'){
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Cancel';
                        $log->message = 'FA Transaction ID has been canceled';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'Fa Sudah Di Close','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Fa Close Gagal Di Update','success'=>0]);
                        }
                    }else if($status == 'C'){
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Compelete','status'=>'C', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Compelete';
                        $log->message = 'FA Transaction ID has been Compeleted';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'Fa Sudah Complete','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'Fa Complete Gagal Di Update','success'=>0]);
                        }
                    }else{
                        return response()->json(['message'=>'Silahkan Cek Kembali FA dan Id Worker','success'=>0]);
                    }
                }
            }
        }
        else
        {
            $url = "http://172.27.1.38:8280/services/DS_Get_FA_Status_Single?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $pFaId = $request->input('fa_transaction_id');
                $operation = 'getFaStatusSingle';
                $params = array(
                    'pFaId' => $pFaId
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['getFaStatusSingle'] as $key => $value){
                    $status = trim($value);
                    if ($status == 'P') {
                        $getWorker = Workers::where('user_id',$request->userid)->first();
                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'On The Way']);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'On The Way';
                        $log->message = 'Worker on the way to job location';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                            ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 

                        if ($trans) {
                            return response()->json(['message'=>'','success'=>1]);
                        }else{
                            return response()->json(['message'=>'Status On The Way Gagal Di Simpan','success'=>0]);
                        }
                         
                    }else if ($status == 'X') {
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Cancel';
                        $log->message = 'FA Transaction ID has been canceled';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans){
                            return response()->json(['message'=>'Fa Sudah Di Close','success'=>1]);
                        }else{
                            return response()->json(['message'=>'Fa Close Gagal Di Update','success'=>0]);
                        }
                    }else if ($status == 'C') {
                        $getWorker = Workers::where('user_id',$request->userid)->first();

                        $latitude = $request->input('latitude');
                        $longitude = $request->input('longitude');

                        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                                    ->where('fa_transaction_id',$request->fa_transaction_id)
                                    ->update(['assign_status'=>'Compelete','status'=>'C', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Compelete';
                        $log->message = 'FA Transaction ID has been completed';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans){
                            return response()->json(['message'=>'Fa Sudah Di Complete','success'=>1]);
                        }else{
                            return response()->json(['message'=>'Fa Complete Gagal Di Update','success'=>0]);
                        }
                    }
                }
            }
        }
    }

    public function send_reassigned_status_hh(Request $request)
    {
        $validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }
        
        $getWorker = Workers::where('user_id',$request->userid)->first();
        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                    ->where('fa_transaction_id',$request->fa_transaction_id)
                    ->update(['assign_status'=>'Re-Assigned']);

        //insert log status fa transaction
        $log = new Log_fa_status;
        $log->fa_transaction_id= $request->fa_transaction_id;
        $log->status = 'Re-Assigned';
        $log->message = 'Worker has been Re-Assigned';
        $log->save();

        if($trans)
        {
             return response()->json(['message'=>'','success'=>1]); 
        }
        else
        {
             return $gagal;
        }

    }

    public function send_assigned_status_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }
        
        $getWorker = Workers::where('user_id',$request->userid)->first();

        $trans  = FA_transaction::where('worker_id',$getWorker->id)
                    ->where('fa_transaction_id',$request->fa_transaction_id)
                    ->update(['assign_status'=>'Assigned']);

        //insert log status fa transaction
        $log = new Log_fa_status;
        $log->fa_transaction_id= $request->fa_transaction_id;
        $log->status = 'Assigned';
        $log->message = 'Worker has been Assigned';
        $log->save();

        if($trans)
        {
             return response()->json(['message'=>'','success'=>1]); 
        }
        else
        {
             return $gagal;
        }

    }

    public function send_open_status_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required', 'fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found', 'success'=>0]);

        if ($validator->fails()) 
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $getWorker = Workers::where('user_id', $request->userid)->first();

        $trans = FA_transaction::where('worker_id', $getWorker->id)
                    ->where('fa_transaction_id', $request->fa_transaction_id)
                    ->update(['assign_status'=>'Open']);

        if($trans)
        {
            return response()->json(['message'=>'','success'=>1]);
        }
        else
        {
            return $gagal;
        }
    }
	
	public function list_fa_hh(Request $request)
	{
		$validator = Validator::make($request->all(),['userid'=>'required']);

		$gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);

		//$fa_transaction = DB::table('fa_transaction as a')
// 			->join('workers as b','a.worker_id','=','b.id')
// 			->join('users as c','b.user_id','=','c.id')
// 			->join('customer as d','d.nomen','=','a.nomen')
// 			->join('fa_type as e','e.fa_type_cd','=','a.fa_type_cd')
// //            ->join('fa_template_BOM as f','f.fa_type_cd','=','a.fa_type_cd')
//            /*->join('fa_template_BOM as f', function ($join){
//                $join->on('a.fa_type_cd','=','f.fa_type_cd');
//            })*/
// //            ->join('fa_template_BOM_detail as g','g.fa_template_BOM_id','=','f.id')
// 			->select('a.id as id','a.fa_type_cd as type','e.descr as desc','a.fa_transaction_id as number','d.customer_name as name',
//                'd.address as address','d.phone as phone','a.created_at as date','d.latitude as latitude','d.longitude as longitude',
//                'a.urgent_status as urgent','a.assign_status as status','a.status_worker as status_worker')
// 			->where('c.id',$request->userid)
// 		    ->get();

        //status yang tidak boleh muncul
        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $now = date('H:i:s');
 
        //check status tutup tugas
        $close_work = System_parameter::first();
        if ($close_work->close_time <= $now && $close_work->close_time_status == 'Active') {
            $close_fa_id = FA_transaction::with('worker.users')
                            ->whereHas('worker', function($query) use ($userid){
                                $query->where('user_id', '=', $userid);
                            })->get();
            
            foreach ($close_fa_id as $key => $value) {
                if ($value->assign_status == 'Assigned' or $value->assign_status == 'Re-Assigned') {
                    $close = FA_transaction::whereIn('assign_status', ['Assigned','Re-Assigned'])
                                    ->update(['assign_date'=>null, 
                                              'reassigned_date'=> null,
                                              'worker_id'=>null,
                                              'assign_status'=>'Open',
                                              'sending_status'=>'Pending'
                                          ]);

                    if ($close) {
                        return response()->json(['message'=>'', 'success'=>1]);
                    }else{
                        return response()->json(['message'=>'Tutup Tugas Gagal', 'success'=>0]);
                    }
                }
            }
        }

        // get today
        $today = Carbon::now();
        $today = $today->toDateString(); 

        //Status 
        $assign_status = ['Complete','Uncompleted'];

		$fa_transaction = FA_transaction::with('worker.users')
            ->whereNotIn('fa_transaction.assign_status', $assign_status)
            //->whereDate('fa_transaction.assign_date', $today) 
            ->with('customer')
            ->with('fa_type.fa_type_lang')
            ->with('fa_template_BOM.fa_template_BOM_detail')
            ->whereHas('worker', function ($query) use ($userid){
                $query->where('user_id','=',$userid);
            })->get();

        // echo json_encode($fa_transaction);
        // exit();

		if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        //jika jam 17.00 (jam 5 sore) kirim response kosong
        
        foreach($fa_transaction as $fa)
        {
            $row = array();
        	$row['id'] = $fa->id;
        	$row['fa_transaction_id'] = $fa->fa_transaction_id;
        	$row['type'] = $fa->fa_type_cd;
        	$row['desc'] = $fa->descriptions;//dispatch_group_descr;//$fa->fa_type->fa_type_lang->descr;
        	$row['number'] = $fa->fa_transaction_id;
        	$row['name'] = $fa->customer->customer_name;
            $row['customer_status'] = trim($fa->customer->status);
        	$row['address'] = $fa->customer->address." ".$fa->customer->city;
        	$row['phone'] = ($fa->customer->phone == null) ? '' : $fa->customer->phone;
            $row['meterinfo'] = $fa->customer->meterinfo;
        	$row['date'] = $fa->created_at ? $fa->created_at->format('Y-m-d H:i:s') : '';
        	$row['latitude'] = $fa->customer->latitude;
        	$row['longitude'] = $fa->customer->longitude;
        	$row['urgent'] = ($fa->urgent_status == null) ? 0: (int)$fa->urgent_status;

            // echo json_encode($fa->fa_template_BOM->id);
            // exit();

            /*status: int,
                1=> Open,
                2=> Assigned,
                3=> Re-Assigned,
                4=> On The Way,
                5=> Pickup,
                6=> Start,
                7=> Cancel Worker*/
            if($fa->assign_status == 'Open'){
                $assign_status = 1;
            }if($fa->assign_status == 'Assigned'){
                $assign_status = 2;
            }if($fa->assign_status == 'Re-Assigned'){
                $assign_status = 3;
            }if($fa->assign_status == 'On The Way'){
                $assign_status = 4;
            }if($fa->assign_status == 'Pickup'){
                $assign_status = 5;
            }if($fa->assign_status == 'Started'){
                $assign_status = 6;
            }if($fa->assign_status == 'Cancel Worker'){
                $assign_status = 7;
            }if($fa->assign_status == 'Complete'){
                $assign_status = 8;
            }
        	$row['status'] = $assign_status;

            /** Status worket : int
                0 => worker belum melakukan upload data 
                1 => foto sebelum pekerjaan diupload 
                2 => foto sesudah pekerjaan sudah diupload  
                3 => remarks/characteristic di upload 
                4 => bom diupload 
                5 => ttd diupload
                6 => complete 
                7 => cancel 
            */

            $row['status_worker'] = (int)$fa->status_worker;

            //$cek_posisiton = FA_Transaction_photo::where('fa_transaction_id', '=', $fa->fa_transaction_id);
            $cek_posisiton = DB::table('fa_transaction_photo')
                             ->where('fa_transaction_id', '=', $fa->fa_transaction_id)
                             ->get();

            $photo1 = [];
            $photo2 = [];
            $photo3 = [];
            $photo4 = [];

            foreach ($cek_posisiton as $posisiton) {
                if ($posisiton->photo1) {
                    
                    array_push($photo1, $posisiton->photo1);
                }
                else
                {
                    array_push($photo1, '');
                }

                if($posisiton->photo2)
                {
                    array_push($photo2, $posisiton->photo2);   
                }
                else
                {
                    array_push($photo2, '');
                }

                if ($posisiton->photo3) 
                {
                    array_push($photo3, $posisiton->photo3);
                }
                else
                {
                    array_push($photo3, '');
                }

                if ($posisiton->photo4) 
                {
                    array_push($photo4, $posisiton->photo4);
                }
                else
                {
                    array_push($photo4, '');
                }        
            }
            
            $photo_sebelum1 = implode('', $photo1);
            $photo_sebelum2 = implode('', $photo2);
            $photo_sesudah1 = implode('', $photo3);
            $photo_sesudah2 = implode('', $photo4);

            if ($photo_sebelum1 && $photo_sebelum2) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 1]);
            }

            if ($photo_sesudah1 && $photo_sesudah2) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 2]);
            }

            $row['worker_data']['photo']['photo_sebelum1'] = $photo_sebelum1;
            $row['worker_data']['photo']['photo_sebelum2'] = $photo_sebelum2;
            $row['worker_data']['photo']['photo_sesudah1'] = $photo_sesudah1;
            $row['worker_data']['photo']['photo_sesudah2'] = $photo_sesudah2;
            $characteristic = getFACharacter($fa->fa_type_cd);

//            return $characteristic->count();
            $characteristic = $characteristic->groupBy('char_type_cd');

            $field_detail = [];
            $desc = '';
            foreach ($characteristic as $key =>$item) {
                //$data['name'][] = $characteristic[$key];
                $row2 = [];
                $desc = $characteristic[$key][0]->descr;
                $row2['name'] = $characteristic[$key][0]->charac_desc;
                $row2['sort_seq'] = trim($characteristic[$key][0]->sort_seq);
                $row2['char_type_cd'] = trim($characteristic[$key][0]->char_type_cd);
                $row2['char_type_flg'] = trim($characteristic[$key][0]->char_type_flg);
                $adhoc_val_alg_cd = trim($characteristic[$key][0]->adhoc_val_alg_cd);

                $row2['layout']['type'] = $this->trim_type($characteristic[$key][0]->char_type_flg, $adhoc_val_alg_cd);
                $row2['mandatory'] = ($characteristic[$key][0]->required_sw) == 'Y' ? true : false;
                //$row2['field']['layout']['count_value'] = count($characteristic[$key]);

                $value = [];
                $var_chal = '';
                foreach ($characteristic[$key] as $field){

                    if($field->char_val != null && trim($field->char_val) != 'PS'){
                        $var_chal = trim($field->char_val);
                        $value[] = [
                            'code' => $var_chal,
                            'desc' => $field->description
                        ];
                    }
                }
//                $row2['layout']['value'] = implode(',', $value);
                

                $row2['layout']['value'] = $value;

                $list_name = $row2['name'] = $characteristic[$key][0]->charac_desc;
                $fa_id = FA_Transaction_characteristic::where('fa_transaction_id', $fa->fa_transaction_id)
                        ->get();
                
                $user_value = '';
                foreach ($fa_id as $key => $value) {
                    if($list_name == $value->name_characteristic) {
                        $user_value = $value->value_characteristic;
                        //print_r($user_value);
                    }
                }    
                         
                $row2['layout']['user_value'] = $user_value;
                $field_detail[] = $row2;
            }
            //exit();

            $row['worker_data']['characteristic'] = [
                'desc' => $desc,
                'field' => $field_detail
            ];


            // cek apakah fa_type memiliki data fa_template_BOM
            // jika kosong maka munculkan nilai nya String kosong
            // jika ada data, munculkan sesuai dengan keinginan
            // masukkan fa_template_bom ke sebuah variable 

            $field_bom = [];
            $characteristic_bom = getBOMCharacter($fa->fa_type_cd);
            $characteristic_bom = $characteristic_bom->groupBy('template_name');

            // echo json_encode($characteristic_bom);
            // exit();

            foreach ($characteristic_bom as $key => $item)
            {
                $row3['id'] = $characteristic_bom[$key][0]->fa_template_BOM_id;
                $row3['name'] = $characteristic_bom[$key][0]->template_name;

                $value = [];
                $var_chal = '';
                foreach ($characteristic_bom[$key] as $field){
                    $var_char = trim($field->code);
                    $var_decs = trim($field->name);
                    $value[] = [
                        'itemcode' => $var_char,
                        'itemname' => $var_decs
                    ];
                }

                $row3['bom']['value'] = $value;
                $row3['bom']['user_value'] = '';
                $row3['mandatory'] = true;

                $field_bom[] = $row3;
            }            
            
            $row['worker_data']['bomtemplate'] = $field_bom;

            // $row['worker_data']['bomtemplate'][] = [
            //     'id' => '', //reference : $fa->fa_template_BOM->id,
            //     'name' => '', //reference : $fa->fa_template_BOM->template_name,
            //     'bom' => '',//reference : $bom
            // ];

            //cek table foto ttd berdasarkan fa_transaction
            $cekdata = FA_Transaction_ttd::where('fa_transaction_id','=', $fa->fa_transaction_id)->first();
            if($cekdata){
                $signed = $cekdata->photo_url;
            }else{
                $signed = '';
            }

            if ($signed) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 6]);
            }

            $row['worker_data']['signed'] = $signed;
            $data[] = $row;

//            return $row;

            /*$field_detail = [];
            $name_descr = '';
            $layout_char = [];
            //$array_baru = array();

            foreach ($characteristic as $item)
            {
                $name_descr = $item->descr;

                if(trim($item->char_type_flg) == 'DFV'){
                    $type = 'dropdown';
                }
                if(trim($item->char_type_flg) == 'ADV'){
                    $type = 'text/numeric/time';
                }
                if(trim($item->char_type_flg) == 'FKV'){
                    $type = 'disable';
                }

                $layout_char = [
                    'type' => $type,
                    'value' => $item->char_val,
//                    'COBAAA' => $array_baru
                ];

//                $field_detail[] = [
//                    'name' => $item->charac_desc,
//                    'layout' => $layout_char,
//                    'mandatory' => ($item->required_sw == 'Y') ? true : false
//                ];

                $field_detail[] = [
                    'name' => $item->charac_desc,
                    'layout' => $layout_char,
                    'mandatory' => ($item->required_sw == 'Y') ? true : false
                ];

                $array_baru[$item->charac_desc][$type][] = $item->char_val;
                //$array_baru[$item->charac_desc] = $item->charac_desc;*/

            //}

            /*$row['aaaaaaaaaaaaa'] = $array_baru;

            $row['worker_data']['characteristic'][] = [
                'desc' => $name_descr,
                'field' => $field_detail
            ];*/


        }

        if(empty($data)){
            return response()->json([
                'message'=>'Data FA Transaction Tidak Ditemukan',
                'success'=>0,
                'userid'=>$userid,
                'name'=>'',
                'pict'=>''
            ]);
        }

        return response()->json($data);
	}

	protected function trim_type($char_type_flg, $adhoc_val_alg_cd)
    {
        if(trim($char_type_flg) == 'DFV'){
            $type = 'dropdown';
        }
        if(trim($char_type_flg) == 'ADV')
        {
            /*
            ADHV-DDMMYY = Validate that the Date Format is DD/MM/YYYY
            CMVALINSDTTM = Validate Date Time Format dd/MM/yyyy-hh:mm
            ADH >= 0   = A number greater than or equal to 0
            CM-ADHVNUM  = Validate Numeric Field
            YEAR        = A number with 4 digit
            ADH > 0     = A number greater than 0
            ADH-VALDTTM = Validate Date Time Format DD/MM/YYYY-HH:MI
            CM-VALILDRT2 = Validate Illegal Duration ( 12 - 72 )
            CM-VALILDRTN = Validate Illegal Duration ( 6 - 72 )
            */
            if($adhoc_val_alg_cd == "ADHV-DDMMYY"){
                $type = 'date'; //Validate that the Date Format is DD/MM/YYYY
            }if($adhoc_val_alg_cd == "CMVALINSDTTM"){
                $type = 'datetime'; //Validate Date Time Format dd/MM/yyyy-hh:mm
            }if($adhoc_val_alg_cd == "ADH >= 0"){
                $type = 'numeric'; //A number greater than or equal to 0
            }if($adhoc_val_alg_cd == "CM-ADHVNUM"){
                $type = 'numeric'; //Validate Numeric Field
            }if($adhoc_val_alg_cd == "YEAR"){
                $type = 'numeric'; //A number with 4 digit
            }if($adhoc_val_alg_cd == "ADH > 0"){
                $type = 'numeric'; //A number greater than 0
            }if($adhoc_val_alg_cd == "ADH-VALDTTM"){
                $type = 'datetime'; //Validate Date Time Format DD/MM/YYYY-HH:MI
            }if($adhoc_val_alg_cd == "CM-VALILDRT2"){
                $type = 'numeric'; //Validate Illegal Duration ( 12 - 72 )
            }if($adhoc_val_alg_cd == "CM-VALILDRTN"){
                $type = 'numeric'; //Validate Illegal Duration ( 6 - 72 )
            }if($adhoc_val_alg_cd == ""){
                $type = 'text';
            }
        }


        if(trim($char_type_flg) == 'FKV'){
            $type = 'disable';
        }

        return $type;
    }

	public function profile(Request $request)
    {
	    $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        if(!empty($request->userid))
        {
            //cek apakah user yang di input adalah user FA/WO
            $cek_user = Workers::where('user_id', $request->userid)->first();

            if ($cek_user->status == 'fa') {
                $datas = Workers::where('user_id','=', $request->userid)
                ->with('users')
                ->with('vendor')
                ->with('worker_dispatch')
                ->with('skill_fa.skill_fa_detail.fa_type.fa_type_lang')
                ->with('AssignmentSurvey')
                ->get();

                foreach ($datas as $value){
                    $row['worker_id'] = $value->worker_id;
                    $row['username'] = $value->users->username;
                    $row['name'] = $value->users->name;

                    if($value->vendor){
                        $row['vendor'] = $value->vendor->vendor_name;
                    }else{
                        $row['vendor'] = '';
                    }

                    $detail= [];
                    foreach ($value->skill_fa->skill_fa_detail as $item){
                        $detail[] = [
                            'id' => $item->id,
                            'fa_type_id' => $item->fa_type_id,
                            'skill_fa_id' => $item->skill_fa_id,
                            'name' => $item->fa_type->fa_type_lang->descr
                        ];
                    }
                    $row['skill_fa_type'] = [
                        'id' => $value->skill_fa->id,
                        'group' => $value->skill_fa->skill_fa_name,
                        'skill_fa_detail' => $detail
                    ];

                    $skil_wo = [];
                    $row['skill_wo_task'] = $skil_wo;
                    $row['survey_type'] = '';
                    $row['fa_group'] = $value->worker_dispatch;

                    $wo_group = [];
                    $row['wo_group'] = $wo_group;
                    $data[] = $row;
                }

                return response()->json($data);

            }elseif ($cek_user->status == 'wo') {
                $datas = Workers::where('user_id','=', $request->userid)
                                ->with('users')
                                ->with('vendor')
                                ->with('skill_wo.skill_wo_detail.wo_task')
                                ->with('AssignmentSurvey')
                                ->get();
                
                // return $datas;
                // exit();

                foreach ($datas as $value){

                    $row['worker_id'] = $value->worker_id;
                    $row['username'] = $value->users->username;
                    $row['name'] = $value->users->name;

                    if($value->vendor){
                        $row['vendor'] = $value->vendor->vendor_name;
                    }else{
                        $row['vendor'] = '';
                    }

                    $detail = [];
                    $row['skill_fa_type'] = $detail;
                    
                    $detail= [];
                    foreach ($value->skill_wo->skill_wo_detail as $item){
                        $detail[] = [
                            'id' => $item->id,
                            'wo_type_id' => $item->job_code_id,
                            'skill_wo_id' => $item->skill_wo_id,
                            'name' => $item->wo_task->descr
                        ];
                    }
                    $row['skill_wo_task'] = [
                        'id' => $value->skill_wo->id,
                        'group' => $value->skill_wo->skill_wo_name,
                        'skill_wo_detail' => $detail
                    ];
                    $row['survey_type'] = '';
                    $wo_group = [];
                    $row['wo_group'] = $wo_group;
                    $data[] = $row;
                }

                return response()->json($data);
            }
            

            if(empty($cek_user))
            {
                return response()->json([
                    'message' => 'Data kosong',
                    'success' => 1
                ]);
            }
        }else{
	        return response()->json([
	            'message' => 'Parameter tidak ditemukan',
                'success' => 0
            ]);
        }
    }

    public function post_characteristic(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required', 'fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found', 'success'=>0]);

        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->input('userid');
        $fa_transaction_id = $request->input('fa_transaction_id');
        $data = $request->input('data');
        $decode_data = json_decode($data);

        //cek status kirim ke simpel
        $cek_status_simple = FA_transaction::where('fa_transaction_id', $fa_transaction_id)->first();

        //cek apakah fa_id sudah ada di table post_characteristic
        $cek_fa_id = FA_Transaction_characteristic::where('fa_transaction_id', $fa_transaction_id)->first();

        //masukkan data dari API ke table fa_transaction_characteristic
        $url = 'https://mantis08.aetra.co.id:8243/services/DS_GET_FA_SEQ?wsdl';
        $client = new \nusoap_client($url, 'wsdl');
        $pFAID = $fa_transaction_id;
        $operation = 'GetFaSeqOperation';
        $params = array(
            'pFAID' => $pFAID
        );

        $hasil1 = $client->call($operation, $params);

        if ($cek_fa_id) {
            foreach ($hasil1['getFaSeq'] as $key => $value){
                $insertSeq = FA_Transaction_characteristic::where('fa_transaction_id', $fa_transaction_id)
                            ->where('char_type_cd', $value['chartypecd'])
                            ->update([
                            'user_id' => $userid,
                            'fa_transaction_id' => $fa_transaction_id,
                            'char_type_cd'=> $value['chartypecd'],
                            'sequence' => $value['seqnum']
                    ]);
            }
        }else{
            foreach ($hasil1['getFaSeq'] as $key => $value){
                $insertSeq = DB::table('fa_transaction_characteristic')
                    ->insert([
                        [
                            'user_id' => $userid,
                            'fa_transaction_id' => $fa_transaction_id,
                            'char_type_cd'=> $value['chartypecd'],
                            'sequence' => $value['seqnum']
                        ]
                    ]);
            }
        }

        if ($insertSeq) {
            foreach ($decode_data as $item => $value) {
                    $data = FA_Transaction_characteristic::where('fa_transaction_id', $fa_transaction_id)
                            ->where('char_type_cd', $value->char_type_cd)
                            ->update([
                            'user_id'=>$userid, 
                            'fa_transaction_id' => $fa_transaction_id, 
                            'fa_type_cd' => $value->fa_type_cd,
                            'name_characteristic' => $value->name,
                            'value_characteristic'=> $value->user_value, 
                            'char_type_flg' => $value->char_type_flg,
                            'updated_by' => $userid
                    ]);
            }
        }

        //jika data bernilai true
        if ($data) {
            //cek fa id di table post karakteristik, jika benar hanya kirim karakteristik ke simpel tanpa log
            if ($cek_status_simple->status_char_simpel == 'Success') {
                //update status wms to table fa_transaction
                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                ->update(['status_wms'=>'Success']);

                $date = date("Y-m-d h:m:i");
                $getCharWMS = FA_Transaction_characteristic::where('fa_transaction_id', $fa_transaction_id)
                                                            ->whereNotIn('char_type_cd',['CASE','CMFALOCK'])
                                                            ->get();

                $xmlrequest = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c1c="http://splwg.com/C1CustomerContactMaintenance.xsd">
                   <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                    <wsse:Security>
                        <wsse:UsernameToken>       
                            <wsse:Username>SIMPEL2</wsse:Username>
                            <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>        
                        </wsse:UsernameToken>    
                    </wsse:Security>  
                   </SOAP-ENV:Header>
                    <SOAP-ENV:Body>
                        <C1FieldActivityMaintenance dateTimeTagFormat="CdxDateTime"  transactionType ="UPDATE" >
                            <C1FieldActivityMaintenanceService>
                                <C1FieldActivityMaintenanceHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    
                                /><C1FieldActivityMaintenanceDetails 
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    ActivityType="'.$cek_status_simple->fa_type_cd.'"  
                                    > 
                                    <FASteps>
                                        <FAStepsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />  
                                    </FASteps>
                                    <FACharacteristics>
                                        <FACharacteristicsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />';
                            foreach ($getCharWMS as $key => $value) {
                            $xmlrequest .= '<FACharacteristicsRow
                                        rowAction="Change" 
                                        FieldActivityID="'.$fa_transaction_id.'" 
                                        CharacteristicType="'.trim($value->char_type_cd).'" 
                                        Sequence="'.trim($value->sequence).'"
                                        SearchCharacteristicValue="'.$value->value_characteristic.'" 
                                        ';
                                        if ($value->char_type_flg == 'ADV') {
                                            $xmlrequest .='AdhocCharacteristicValue="'.$value->value_characteristic.'">
                                            </FACharacteristicsRow>';
                                        }else{
                                            $xmlrequest .='CharacteristicValue="'.$value->value_characteristic.'">
                                            </FACharacteristicsRow>';
                                        }
                            }                                 
                            $xmlrequest .=  '</FACharacteristics>
                            <FaRem>
                                <FaRemHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            </FaRem>
                            <FaLog>
                                <FaLogHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                                
                                    </FaLog>
                                </C1FieldActivityMaintenanceDetails>
                            </C1FieldActivityMaintenanceService>
                        </C1FieldActivityMaintenance>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>';

                // print_r($xmlrequest);
                // exit();

                $action = 'C1FieldActivityMaintenance';
                //$url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                $url = 'http://172.27.1.38:8280/services/PS_Field_Activity?wsdl';
                $client = new Client(); // Create the Guzzle Client        
                $response = $client->post($url, [         
                                'headers' => [           
                                    'SOAPAction' => $action, // Add the action              
                                    'Content-Type' => 'text/xml'          
                                ],          
                                'body' => $xmlrequest // Put the xml in the body        
                            ]); 

                if ($response) {
                    //return $response->getBody()->getContents();
                    $res = $response->getBody()->getContents();
                    $doc = new \DOMDocument();
                    $doc->loadXML($res);
                    $status = $doc->getElementsByTagName('faultcode')->length;
                    // print_r($doc);
                    //print_r($res);
                    // exit();

                    if ($status == 1) {
                        //$status = $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue;
                        //$err = $doc->getElementsByTagName('ResponseText')->item(0)->nodeValue;
                        FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                        ->update(['assign_status'=>'Uncompleted','status_char_simpel'=>'Gagal']);

                        //save log status fa
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Fail Send Characteristic';
                        $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                        $log->save();

                        return response()->json(['message'=>'Gagal Kirim Data Ke SIMPEL, Status Uncomplete, Silahkan Hubungi Admin', 'success'=>1]); 
                    }else{
                        FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                    ->update(['status_char_simpel'=>'Success']);

                        //return response()->json(['message'=>'Send Data To SIMPEL Success']);
                        //$toSimpelStatus = 'Send Data To SIMPEL Success';
                        //Add XML Completion FA Here

                        $worker = Workers::where('user_id', '=', $userid)->get();
                        $worker_id = trim($worker[0]->worker_id);
                        $deskrip = 'Has been completed by '.$worker[0]->worker_id;

                        $xmlclosefa = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:c1f="http://splwg.com/C1FACompletionExtSysStruct.xsd">
                           <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">

                            <wsse:Security>
                                <wsse:UsernameToken>
                                    <wsse:Username>SIMPEL2</wsse:Username>
                                    <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>
                                </wsse:UsernameToken>
                            </wsse:Security>
                           
                           </SOAP-ENV:Header>
                           
                           <SOAP-ENV:Body>
                              <C1FACompletionExtSysStruct dateTimeTagFormat="CdxDateTime"  transactionType ="ADD" >
                                    <C1FACompletionExtSysStructService>
                                        <C1FACompletionExtSysStructHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            
                                        /><C1FACompletionExtSysStructDetails 
                                            UploadFieldActivityID="'.$fa_transaction_id.'" 
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            Workedby="'.$worker_id.'" 
                                            WorkDateTime="'.$date.'" 
                                            LongDescription="'.$deskrip.'" 
                                            > 
                                            <FAUploadCharacteristics>
                                                <FAUploadCharacteristicsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                            </FAUploadCharacteristics>
                                            
                                            <FAUploadRemarks>
                                                <FAUploadRemarksHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                            </FAUploadRemarks>
                                            <MeterInfo>
                                                
                                            </MeterInfo>
                                            <ItemInfo>
                                                
                                            </ItemInfo>
                                        </C1FACompletionExtSysStructDetails>
                                    </C1FACompletionExtSysStructService>
                                </C1FACompletionExtSysStruct>
                           </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>';

                        $action = 'C1FACompletionExtSysStruct';
                        $url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                        $client = new Client(); // Create the Guzzle Client        
                        $response = $client->post($url, [         
                                        'headers' => [           
                                            'SOAPAction' => $action, // Add the action              
                                            'Content-Type' => 'text/xml'          
                                        ],          
                                        'body' => $xmlclosefa // Put the xml in the body        
                                    ]);

                        if ($response) {
                            //return $response->getBody()->getContents();
                            $res = $response->getBody()->getContents();
                            $doc = new \DOMDocument();
                            $doc->loadXML($res);
                            $statusComp = $doc->getElementsByTagName('faultcode')->length;
                            // print_r($res);
                            // exit();

                            if ($statusComp == 1) {
                                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                                ->update(['assign_status'=>'Uncompleted','status_complation_fa'=>'Gagal']);

                                //save log status fa
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Fail Completion FA';
                                $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                                $log->save();

                                return response()->json(['message'=>'Gagal Melakukan Completion FA, Status Uncomplete, Silahkan Hubungi Admin', 'success'=>1]); 
                            }else{
                                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                                ->update(['status_complation_fa'=>'Success', 'status'=>'C', 'assign_status'=>'Complete']);

                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Complete';
                                $log->message = 'Berhasil Completion FA';
                                $log->save();

                                return response()->json(['message'=>'Data Berhasil Di Kirim Ke SIMPEL, WMS, dan Completion FA','success'=>1]);
                            }
                        }else{
                            return response()->json(['message'=>'API Completion Tidak Ada Response, Status Uncomplete, Silahkan Hubungi Admin','success'=>1]);
                        }
                    }
                }else{
                    return response()->json(['message'=>'API Kirim Ke SIMPEL Tidak Ada Response, Status Uncomplete, Silahkan Hubungi Admin','success'=>1]); 
                }

            }else{
                //kirim pakai log

                //update status wms to table fa_transaction
                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                ->update(['status_wms'=>'Success']);

                $getCharWMS = FA_Transaction_characteristic::where('fa_transaction_id', $fa_transaction_id)
                                                            ->whereNotIn('char_type_cd',['CASE','CMFALOCK'])
                                                            ->get();
                $date = date("Y-m-d h:m:i");

                $xmlrequest = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c1c="http://splwg.com/C1CustomerContactMaintenance.xsd">
                   <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                    <wsse:Security>
                        <wsse:UsernameToken>       
                            <wsse:Username>SIMPEL2</wsse:Username>
                            <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>        
                        </wsse:UsernameToken>    
                    </wsse:Security>  
                   </SOAP-ENV:Header>
                    <SOAP-ENV:Body>
                        <C1FieldActivityMaintenance dateTimeTagFormat="CdxDateTime"  transactionType ="UPDATE" >
                            <C1FieldActivityMaintenanceService>
                                <C1FieldActivityMaintenanceHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    
                                /><C1FieldActivityMaintenanceDetails 
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    ActivityType="'.$cek_status_simple->fa_type_cd.'"  
                                    > 
                                    <FASteps>
                                        <FAStepsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />  
                                    </FASteps>
                                    <FACharacteristics>
                                        <FACharacteristicsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />';
                            foreach ($getCharWMS as $key => $value) {
                            $xmlrequest .= '<FACharacteristicsRow
                                        rowAction="Change" 
                                        FieldActivityID="'.$fa_transaction_id.'" 
                                        CharacteristicType="'.trim($value->char_type_cd).'" 
                                        Sequence="'.trim($value->sequence).'"
                                        SearchCharacteristicValue="'.$value->value_characteristic.'" 
                                        ';
                                        if ($value->char_type_flg == 'ADV') {
                                            $xmlrequest .='AdhocCharacteristicValue="'.$value->value_characteristic.'">
                                            </FACharacteristicsRow>';
                                        }else{
                                            $xmlrequest .='CharacteristicValue="'.$value->value_characteristic.'">
                                            </FACharacteristicsRow>';
                                        }
                            }                                 
                            $xmlrequest .=  '</FACharacteristics>
                            <FaRem>
                                <FaRemHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            </FaRem>
                            <FaLog>
                                <FaLogHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />';
                            $no_seq = 2; //default 2
                            foreach ($decode_data as $key => $row) {
                            $xmlrequest .='<FaLogRow 
                                    rowAction="Add" 
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    Sequence="'.$no_seq++.'"
                                    DateTime="'.$date.'" 
                                    Details="'.$row->name.'" 
                                    User="SIMPEL2" 
                                    LogType="EMSG">
                                </FaLogRow>';
                            }
                                    $xmlrequest .='
                                    </FaLog>
                                </C1FieldActivityMaintenanceDetails>
                            </C1FieldActivityMaintenanceService>
                        </C1FieldActivityMaintenance>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>';

                // print_r($xmlrequest);
                // exit();

                $action = 'C1FieldActivityMaintenance';
                //$url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                $url = 'http://172.27.1.38:8280/services/PS_Field_Activity?wsdl';
                $client = new Client(); // Create the Guzzle Client        
                $response = $client->post($url, [         
                                'headers' => [           
                                    'SOAPAction' => $action, // Add the action              
                                    'Content-Type' => 'text/xml'          
                                ],          
                                'body' => $xmlrequest // Put the xml in the body        
                            ]); 

                if ($response) {
                    //return $response->getBody()->getContents();
                    $res = $response->getBody()->getContents();
                    $doc = new \DOMDocument();
                    $doc->loadXML($res);
                    $status = $doc->getElementsByTagName('faultcode')->length;
                    // print_r($doc->getElementsByTagName('ResponseText')[0]->nodeValue);
                    // print_r($res);
                    // exit();

                    if ($status == 1) {
                        //$status = $doc->getElementsByTagName('ResponseStatus')->item(0)->nodeValue;
                        //$err = $doc->getElementsByTagName('ResponseText')->item(0)->nodeValue;
                        FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                        ->update(['assign_status'=>'Uncompleted','status_char_simpel'=>'Gagal']);

                        //insert log status fa transaction
                        $log = new Log_fa_status;
                        $log->fa_transaction_id= $request->fa_transaction_id;
                        $log->status = 'Fail Send Characteristic';
                        $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                        $log->save();

                        return response()->json(['message'=>'Gagal Kirim Data Ke SIMPEL, Status Uncomplete, Silahkan Hubungi Admin', 'success'=>1]); 
                    }else{
                        FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                    ->update(['status_char_simpel'=>'Success']);

                        //return response()->json(['message'=>'Send Data To SIMPEL Success']);
                        //$toSimpelStatus = 'Send Data To SIMPEL Success';
                        //Add XML Completion FA Here

                        $worker = Workers::where('user_id', '=', $userid)->get();
                        $worker_id = trim($worker[0]->worker_id);
                        $deskrip = 'Has been completed by '.$worker[0]->worker_name;

                        $xmlclosefa = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:c1f="http://splwg.com/C1FACompletionExtSysStruct.xsd">
                           <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">

                            <wsse:Security>
                                <wsse:UsernameToken>
                                    <wsse:Username>SIMPEL2</wsse:Username>
                                    <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>
                                </wsse:UsernameToken>
                            </wsse:Security>
                           
                           </SOAP-ENV:Header>
                           
                           <SOAP-ENV:Body>
                              <C1FACompletionExtSysStruct dateTimeTagFormat="CdxDateTime"  transactionType ="ADD" >
                                    <C1FACompletionExtSysStructService>
                                        <C1FACompletionExtSysStructHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            
                                        /><C1FACompletionExtSysStructDetails 
                                            UploadFieldActivityID="'.$fa_transaction_id.'" 
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            Workedby="'.$worker_id.'" 
                                            WorkDateTime="'.$date.'" 
                                            LongDescription="'.$deskrip.'" 
                                            > 
                                            <FAUploadCharacteristics>
                                                <FAUploadCharacteristicsHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                            </FAUploadCharacteristics>
                                            
                                            <FAUploadRemarks>
                                                <FAUploadRemarksHeader
                                                    FieldActivityID="'.$fa_transaction_id.'" 
                                                    />
                                            </FAUploadRemarks>
                                            <MeterInfo>
                                                
                                            </MeterInfo>
                                            <ItemInfo>
                                                
                                            </ItemInfo>
                                        </C1FACompletionExtSysStructDetails>
                                    </C1FACompletionExtSysStructService>
                                </C1FACompletionExtSysStruct>
                           </SOAP-ENV:Body>
                        </SOAP-ENV:Envelope>';

                        $action = 'C1FACompletionExtSysStruct';
                        $url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                        $client = new Client(); // Create the Guzzle Client        
                        $response = $client->post($url, [         
                                        'headers' => [           
                                            'SOAPAction' => $action, // Add the action              
                                            'Content-Type' => 'text/xml'          
                                        ],          
                                        'body' => $xmlclosefa // Put the xml in the body        
                                    ]);

                        if ($response) {
                            //return $response->getBody()->getContents();
                            $res = $response->getBody()->getContents();
                            $doc = new \DOMDocument();
                            $doc->loadXML($res);
                            $statusComp = $doc->getElementsByTagName('faultcode')->length;
                            // print_r($res);
                            // exit();

                            if ($statusComp == 1) {
                                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->update(['assign_status'=>'Uncompleted', 'status_complation_fa'=>'Gagal']);

                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Fail Completion FA';
                                $log->message = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                                $log->save();

                                return response()->json(['message'=>'Gagal Completion FA, Status Uncomplete, Silahkan Hubungi Admin', 'success'=>1]);
                            }else{
                                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->update(['status_complation_fa'=>'Success', 'status'=>'C', 'assign_status'=>'Complete']);

                                //insert log status fa transaction
                                $log = new Log_fa_status;
                                $log->fa_transaction_id= $request->fa_transaction_id;
                                $log->status = 'Complete';
                                $log->message = 'Berhasil Melakukan Completion FA';
                                $log->save();


                                return response()->json(['message'=>'Data Berhasil Di Kirim Ke SIMPEL dan WMS','success'=>1]);
                            }
                        }else{
                            return response()->json(['message'=>'API Completion FA Tidak Ada Response, Status Uncomplete, Silahkan Hubungi Admin','success'=>1]);
                        } 
                    }
                }else{
                    return response()->json(['message'=>'API Send To SIMPEL Tidak Ada Response, Status Uncomplite, Silahkan Hubungi Admin','success'=>1]);
                }
            }
        }else{
            FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                            ->update(['status_wms'=>'Gagal']);

            return response()->json(['message'=>'Data Tidak Berhasil di tambahkan ke wms, silahkan hubungi admin','success'=>1]);
        }
    }
		
    public function upload_photo_fa_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $trans = FA_Transaction_photo::where('fa_transaction_id', $request->fa_transaction_id)->first();

        $this->validate($request, [
            'photo1' => 'image|mimes:jpeg,png,jpg',
            'photo2' => 'image|mimes:jpeg,png,jpg',
            'photo3' => 'image|mimes:jpeg,png,jpg',
            'photo4' => 'image|mimes:jpeg,png,jpg'
        ]);

        $fa_id = $request->input('fa_transaction_id');

        if($trans){

            //Hapus Image Sebelumnya 
            $usersImage1 = public_path("uploads/workphotos/{$trans->photo_name1}");
            $usersImage2 = public_path("uploads/workphotos/{$trans->photo_name2}");
            $usersImage3 = public_path("uploads/workphotos/{$trans->photo_name3}");
            $usersImage4 = public_path("uploads/workphotos/{$trans->photo_name4}");

            if (File::exists($usersImage1)) { 
                unlink($usersImage1);
            }
            if (File::exists($usersImage2)) { 
                unlink($usersImage2);
            }
            if (File::exists($usersImage3)) { 
                unlink($usersImage3);
            }
            if (File::exists($usersImage4)) { 
                unlink($usersImage4);
            }
            
            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($request->hasfile('photo1'))
            {
                $image = $request->file('photo1');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name1 = $request->input('fa_transaction_id').'-01'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name1);
                $photo_url = asset('/uploads/workphotos/'.$name1);

                array_push($images1, $photo_url);
            }

            if($request->hasfile('photo2'))
            {
                $image = $request->file('photo2');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name2 = $request->input('fa_transaction_id').'-02'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name2);
                $photo_url_2 = asset('/uploads/workphotos/'.$name2);

                array_push($images2, $photo_url_2);
            }

            if($request->hasfile('photo3'))
            {
                $image = $request->file('photo3');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name3 = $request->input('fa_transaction_id').'-03'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name3);
                $photo_url_3 = asset('/uploads/workphotos/'.$name3);

                array_push($images3, $photo_url_3);
            }

            if($request->hasfile('photo4'))
            {
                $image = $request->file('photo4');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name4 = $request->input('fa_transaction_id').'-04'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name4);
                $photo_url_4 = asset('/uploads/workphotos/'.$name4);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            DB::table('fa_transaction_photo')
                ->where('fa_transaction_id', $fa_id)
                ->update([  'photo_name1' => $name1,
                            'photo_name2' => $name2,
                            'photo_name3' => $name3,
                            'photo_name4' => $name4,
                            'photo1' => $image1, 
                            'photo2' => $image2, 
                            'photo3' => $image3, 
                            'photo4' => $image4]);

            return response()->json(['message'=>'','success'=>1, 'img_url'=>['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]]);

        }else{

            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($request->hasfile('photo1'))
            {
                $image = $request->file('photo1');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name1 = $request->input('fa_transaction_id').'-01'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name1);
                $photo_url = asset('/uploads/workphotos/'.$name1);

                array_push($images1, $photo_url);
            }

            if($request->hasfile('photo2'))
            {
                $image = $request->file('photo2');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name2 = $request->input('fa_transaction_id').'-02'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name2);
                $photo_url_2 = asset('/uploads/workphotos/'.$name2);

                array_push($images2, $photo_url_2);
            }

            if($request->hasfile('photo3'))
            {   
                $image = $request->file('photo3');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name3 = $request->input('fa_transaction_id').'-03'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name3);
                $photo_url_3 = asset('/uploads/workphotos/'.$name3);

                array_push($images3, $photo_url_3);
            }

            if($request->hasfile('photo4'))
            {
                $image = $request->file('photo4');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name4 = $request->input('fa_transaction_id').'-04'.'.png';
                $destinationPath = public_path('/uploads/workphotos');
                $image->move($destinationPath, $name4);
                $photo_url_4 = asset('/uploads/workphotos/'.$name4);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            $data = new FA_Transaction_photo();
            $data->fa_transaction_id = $request->input('fa_transaction_id');
            $data->user_id = $request->input('userid');
            $data->type = $request->input('type');
            $data->photo_name1 = $name1;
            $data->photo_name2 = $name2;
            $data->photo_name3 = $name3;
            $data->photo_name4 = $name4;
            $data->photo1 = $image1;
            $data->photo2 = $image2;
            $data->photo3 = $image3;
            $data->photo4 = $image4;
            $data->save();

            if ($data) 
            {
                return response()->json(['message'=>'','success'=>1, 'img_url'=>['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]]);
            }
            else
            {
                return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
            }    
        }
    }

    public function upload_ttd_fa_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }


        $trans = FA_transaction::where('fa_transaction_id', $request->input('fa_transaction_id'))->first();
        
        if($trans){
            //cek data fa_id di table fa_transaction_ttd
            $cek_ttd = FA_Transaction_ttd::where('fa_transaction_id', $request->input('fa_transaction_id'))->first();

            //decode string base64 image to image 
            $image = base64_decode($request->input('ttd'));

            //jika sudah ada
            if (empty($cek_ttd)) {
                //jika gambar nya kosong
                if (!isset($image) || empty($image)) {
                    return response()->json(['message'=>'Tanda Tangan Belum Ada','success'=>0]);
                }else{
                    //create image name
                    $photo_name = time().'_'.$request->input('fa_transaction_id').'.png';
                    $destinationPath = public_path().'/uploads/signedworkers'.$photo_name;
                    //save image to folder
                    //$image->move($destinationPath, $photo_name);
                    //$save_image = file_get_contents($image)->save($destinationPath);
                    Storage::disk('public')->put($photo_name, $image);
                    $img_url = asset('/storage/signedworkers/'.$photo_name);


                    $data = new FA_Transaction_ttd();
                    $data->fa_transaction_id = $request->input('fa_transaction_id');
                    $data->user_id = $request->input('userid');
                    $data->photo_name = $photo_name;
                    $data->photo_url = $img_url;
                    $data->save();

                    if ($data) {
                        return response()->json(['message'=>'','success'=>1,'img_url'=>$img_url]);
                    }else{
                        return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
                    }
                }
            }else{
                //jika gambar nya kosong
                if (!isset($image) || empty($image)) {
                    return response()->json(['message'=>'Tanda Tangan Belum Ada','success'=>0]);
                }else{
                    //create image name
                    $photo_name = time().'_'.$request->input('fa_transaction_id').'.png';
                    $destinationPath = public_path().'/uploads/signedworkers'.$photo_name;
                    //save image to folder
                    //$image->move($destinationPath, $photo_name);
                    //$save_image = file_get_contents($image)->save($destinationPath);
                    Storage::disk('public')->put($photo_name, $image);
                    $img_url = asset('/storage/signedworkers/'.$photo_name);

                    //update table berdasarkan id
                    $data = DB::table('fa_transaction_ttd')
                                ->where('fa_transaction_id', $request->input('fa_transaction_id'))
                                ->update([
                                    'fa_transaction_id'=>$request->input('fa_transaction_id'),
                                    'user_id'=>$request->input('userid'),
                                    'photo_name'=>$photo_name,
                                    'photo_url'=>$img_url
                                ]);

                    if ($data) {
                        return response()->json(['message'=>'','success'=>1,'img_url'=>$img_url]);
                    }else{
                        return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
                    }
                }
            }
        }
    }

    public function list_pickup_fa_hh(Request $request)
    {
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $fa_transaction = FA_transaction::with('worker.users')
            ->with('customer')
            ->with('fa_type.fa_type_lang')
            ->with('fa_template_BOM.fa_template_BOM_detail')
            ->whereHas('worker', function ($query) use ($userid){
                $query->where('user_id','=',$userid);
            })
            ->where('urgent_status', 1)
            ->get();

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        foreach($fa_transaction as $fa)
        {
            $row = array();
            $row['id'] = $fa->id;
            $row['fa_transaction_id'] = $fa->fa_transaction_id;
            $row['type'] = $fa->fa_type_cd;
            $row['desc'] = $fa->fa_type->fa_type_lang->descr;
            $row['number'] = $fa->fa_transaction_id;
            $row['name'] = $fa->customer->customer_name;
            $row['address'] = $fa->customer->address." ".$fa->customer->city;
            $row['phone'] = ($fa->customer->phone == null) ? '' : $fa->customer->phone;
            $row['meterInfo'] = $fa->customer->meterinfo;
            $row['date'] = $fa->created_at ? $fa->created_at->format('Y-m-d H:i:s') : '';
            $row['latitude'] = $fa->customer->latitude;
            $row['longitude'] = $fa->customer->longitude;
            $row['urgent'] = ($fa->urgent_status == null) ? 0: (int)$fa->urgent_status;

            /*status: int,
                1=> Open,
                2=> Assigned,
                3=> Re-Assigned,
                4=> On The Way,
                5=> Pickup,
                6=> Start,
                7=> Cancel Worker*/
            if($fa->assign_status == 'Open'){
                $assign_status = 1;
            }if($fa->assign_status == 'Assigned'){
                $assign_status = 2;
            }if($fa->assign_status == 'Re-Assigned'){
                $assign_status = 3;
            }if($fa->assign_status == 'On The Way'){
                $assign_status = 4;
            }if($fa->assign_status == 'Pickup'){
                $assign_status = 5;
            }if($fa->assign_status == 'Started'){
                $assign_status = 6;
            }if($fa->assign_status == 'Cancel Worker'){
                $assign_status = 7;
            }
            $row['status'] = $assign_status ;
            $row['status_worker'] = (int)$fa->status_worker;

            //$cek_posisiton = FA_Transaction_photo::where('fa_transaction_id', '=', $fa->fa_transaction_id);
            $cek_posisiton = DB::table('fa_transaction_photo')
                             ->where('fa_transaction_id', '=', $fa->fa_transaction_id)
                             ->get();

            $photo1 = [];
            $photo2 = [];
            $photo3 = [];
            $photo4 = [];

            foreach ($cek_posisiton as $posisiton) {
                if ($posisiton->photo1) {
                    
                    array_push($photo1, $posisiton->photo1);
                }
                else
                {
                    array_push($photo1, '');
                }

                if($posisiton->photo2)
                {
                    array_push($photo2, $posisiton->photo2);   
                }
                else
                {
                    array_push($photo2, '');
                }

                if ($posisiton->photo3) 
                {
                    array_push($photo3, $posisiton->photo3);
                }
                else
                {
                    array_push($photo3, '');
                }

                if ($posisiton->photo4) 
                {
                    array_push($photo4, $posisiton->photo4);
                }
                else
                {
                    array_push($photo4, '');
                }        
            }
            
            $photo_sebelum1 = implode('', $photo1);
            $photo_sebelum2 = implode('', $photo2);
            $photo_sesudah1 = implode('', $photo3);
            $photo_sesudah2 = implode('', $photo4);

            if ($photo_sebelum1 && $photo_sebelum2) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 1]);
            }

            if ($photo_sesudah1 && $photo_sesudah2) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 2]);
            }

            $row['worker_data']['photo']['photo_sebelum1'] = $photo_sebelum1;
            $row['worker_data']['photo']['photo_sebelum2'] = $photo_sebelum2;
            $row['worker_data']['photo']['photo_sesudah1'] = $photo_sesudah1;
            $row['worker_data']['photo']['photo_sesudah2'] = $photo_sesudah2;
            $characteristic = getFACharacter($fa->fa_type_cd);

//            return $characteristic->count();
            $characteristic = $characteristic->groupBy('char_type_cd');

            $field_detail = [];
            $desc = '';
            foreach ($characteristic as $key =>$item) {
                //$data['name'][] = $characteristic[$key];
                $row2 = [];
                $desc = $characteristic[$key][0]->descr;
                $row2['name'] = $characteristic[$key][0]->charac_desc;
                $adhoc_val_alg_cd = trim($characteristic[$key][0]->adhoc_val_alg_cd);

                $row2['layout']['type'] = $this->trim_type($characteristic[$key][0]->char_type_flg, $adhoc_val_alg_cd);
                $row2['mandatory'] = ($characteristic[$key][0]->required_sw) == 'Y' ? true : false;
                //$row2['field']['layout']['count_value'] = count($characteristic[$key]);

                $value = [];
                $var_chal = '';
                foreach ($characteristic[$key] as $field){

                    if($field->char_val != null && trim($field->char_val) != 'PS'){
                        $var_chal = trim($field->char_val);
                        $value[] = [
                            'code' => $var_chal,
                            'desc' => $field->description
                        ];
                    }
                }
                

                $row2['layout']['value'] = $value;

                $list_name = $row2['name'] = $characteristic[$key][0]->charac_desc;
                $fa_id = FA_Transaction_characteristic::where('fa_transaction_id', $fa->fa_transaction_id)
                        ->get();
                
                $user_value = '';
                foreach ($fa_id as $key => $value) {
                    if($list_name == $value->name_characteristic) {
                        $user_value = $value->value_characteristic;
                        //print_r($user_value);
                    }
                }    
                         
                $row2['layout']['user_value'] = $user_value;
                $field_detail[] = $row2;
            }
            //exit();

            $row['worker_data']['characteristic'] = [
                'desc' => $desc,
                'field' => $field_detail
            ];

            $bom = [];
            foreach ($fa->fa_template_BOM->fa_template_BOM_detail as $value)
            {
                $bom[] = [
                    'itemname' => $value->name,
                    'itemcode' => $value->code
                ];
            }

            $row['worker_data']['bomtemplate'][] = [
                'id' => $fa->fa_template_BOM->id,
                'name' => $fa->fa_template_BOM->template_name,
                'bom' => $bom
            ];

            //cek table foto ttd berdasarkan fa_transaction
            $cekdata = FA_Transaction_ttd::where('fa_transaction_id','=', $fa->fa_transaction_id)->first();
            if($cekdata){
                $signed = $cekdata->photo_url;
            }else{
                $signed = '';
            }

            if ($signed) {
                DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa->fa_transaction_id)
                    ->update(['status_worker' => 6]);
            }

            $row['worker_data']['signed'] = $signed;
            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'Data FA Transaction Tidak Ditemukan',
                'success'=>0,
                'userid'=>$userid,
                'name'=>'',
                'pict'=>''
            ]);
        }
        return response()->json($data);
    }

    public function check_status_joblist_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);
        $gagal = response()->json(['message'=>'Worker Not Found', 'success'=>0]);

        if ($validator->fails()) 
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $getWorker = Workers::where('user_id', $request->userid)->first();

        $trans = DB::table('fa_transaction')
                    ->where('worker_id', $getWorker->id) 
                    ->where('assign_status', 'like', '%Complete%') 
                    ->where(function($query){
                        $query->whereDate('assign_date', '=', Carbon::today())
                              ->orWhereDate('reassigned_date', '=', Carbon::today());
                    })
                    ->get();
        $transCount = $trans->count();

        if ($transCount == 10) {
            return response()->json(['complete'=>true,'message'=>'Semua Pekerjaan di Joblist Complete']);
        }else{
            return response()->json(['complete'=>false,'message'=>'Semua Pekerjaan di Joblist Belum Complete']);
        }
    }

    public function list_survey_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        //Array survey status for filter in query get
        $survey_status = ['Finished'];

        $userid = $request->userid;
        $survey = AssignmentSurvey::with('worker.users')
            ->whereNotIn('survey_transaction.survey_status', $survey_status)
            ->with('survey')
            ->whereHas('worker', function ($query) use ($userid){
                $query->where('user_id','=',$userid);
            })->get();

        // echo json_encode($survey);
        // exit();

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        foreach($survey as $su)
        {
            $row = array();
            $row['id'] = $su->id;
            $row['survey_name'] = $su->survey->survey_name;
            $row['survey_code'] = $su->survey_code;
            $row['start_date'] = $su->start_date;
            $row['end_date'] = $su->end_date;

            /*status: int,
                1=> Assigned,
                2=> Started,
                3=> Finished,
                4=> Expired*/
            $survey_status = trim($su->survey_status);
            if($survey_status == 'Assigned'){
                $survey_status = 1;
            }if($survey_status == 'Started'){
                $survey_status = 2;
            }if($survey_status == 'Finished'){
                $survey_status = 3;
            }if($survey_status == 'Expired'){
                $survey_status = 4;
            }

            $row['status'] = $survey_status ;
            $row['status_worker'] = (int)$su->status_worker;

            $cek_posisiton = DB::table('survey_transaction_photo')
                             ->where('survey_code', '=', $su->survey_code)
                             ->get();
            // echo json_encode($cek_posisiton);
            // exit();

            $photo1 = [];
            $photo2 = [];
            $photo3 = [];
            $photo4 = [];

            foreach ($cek_posisiton as $posisiton) {
                if ($posisiton->photo1) {
                    
                    array_push($photo1, $posisiton->photo1);
                }
                else
                {
                    array_push($photo1, '');
                }

                if($posisiton->photo2)
                {
                    array_push($photo2, $posisiton->photo2);   
                }
                else
                {
                    array_push($photo2, '');
                }

                if ($posisiton->photo3) 
                {
                    array_push($photo3, $posisiton->photo3);
                }
                else
                {
                    array_push($photo3, '');
                }

                if ($posisiton->photo4) 
                {
                    array_push($photo4, $posisiton->photo4);
                }
                else
                {
                    array_push($photo4, '');
                }        
            }
            
            $photo_sebelum1 = implode('', $photo1);
            $photo_sebelum2 = implode('', $photo2);
            $photo_sesudah1 = implode('', $photo3);
            $photo_sesudah2 = implode('', $photo4);
            
            $row['worker_data']['photo']['photo_sebelum1'] = $photo_sebelum1;
            $row['worker_data']['photo']['photo_sebelum2'] = $photo_sebelum2;
            $row['worker_data']['photo']['photo_sesudah1'] = $photo_sesudah1;
            $row['worker_data']['photo']['photo_sesudah2'] = $photo_sesudah2;        

            /**
                Get Characteristic Survey
                + Get from table survey_details order by id survey template from table survey
                + foreach each element 
                + Check type characteristic using if conditional

                Type Characteristic :
                - String
                - Text
                - Integer
            */

            $characteristic = DB::table('survey_detail')
                                ->where('survey_id', $su->survey_id)
                                ->get();

            $field_detail = [];
            $desc = $su->survey->survey_name;
            foreach ($characteristic as $key => $value) {
                $row2 = [];
                $row2['name'] = $value->survey_detail_name;

                if ($value->type_data_id == 1) {
                    $type_data = 'String';
                }if ($value->type_data_id == 2) {
                    $type_data = 'Text';
                }if ($value->type_data_id == 3) {
                    $type_data = 'Integer';
                }

                $row2['layout']['type'] = $type_data;
                $row2['mandatory'] = ($value->status) == 'Active' ? true : false;

                $list_name = $value->survey_detail_name;
                $survey_id = Survey_Transaction_Characteristic::where('survey_code', $su->survey_code)
                        ->get();

                // echo json_encode($survey_id);
                // exit();
                
                $user_value = '';
                foreach ($survey_id as $key => $value) {
                    if($list_name == $value->characteristic_name) {
                        $user_value = $value->characteristic_value;
                        //print_r($user_value);
                    }
                }   

                $row2['layout']['user_value'] = $user_value;
                $field_detail[] = $row2;
            }

            $row['worker_data']['characteristic'] = [
                'desc' => $desc,
                'field' => $field_detail
            ];

            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'Kode Surver Tidak Ditemukan',
                'success'=>0,
                'userid'=>$userid,
                'name'=>'',
                'pict'=>''
            ]);
        }

        return response()->json($data);
    }

    public function post_characteristic_survey_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required', 'survey_code'=>'required']);
        $gagal = response()->json(['message'=>'Survey Code Not Found', 'success'=>0]);

        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->input('userid');
        $survey_code = $request->input('survey_code');
        $data = $request->input('data');
        $decode_data = json_decode($data);
        $lat = $request->input('latitude');
        $long = $request->input('longitude');

        //cek apakah data sudah ada di table survey transaction characteristic
        $cek_survey = Survey_Transaction_Characteristic::where('survey_code', $survey_code)->first();

        //jika ada, update
        //jika tidak, create
        if (empty($cek_survey)) {
            //Save Data To WMS
            foreach ($decode_data as $item => $value) {
                $data = new Survey_Transaction_Characteristic();
                $data->user_id = $userid;
                $data->survey_code = $survey_code;
                $data->type_data = $value->type_data;
                $data->characteristic_name = $value->name;
                $data->characteristic_value = $value->user_value;
                $data->save();
            }
        }else{
            //update Data To WMS
            foreach ($decode_data as $item => $value) {
                $data = Survey_Transaction_Characteristic::where('survey_code', $survey_code)
                            ->where('characteristic_name', $value->name)
                            ->update([
                            'user_id'=>$userid, 
                            'survey_code'=>$survey_code, 
                            'type_data'=>$value->type_data,
                            'characteristic_value'=>$value->user_value, 
                    ]);
            }
        }

        //update or create in table survey transaction
        $data = Survey_Transaction::where('survey_code', $survey_code)
                                   ->update(['lat'=>$lat, 'long'=>$long]);

        if ($data)
        {
            AssignmentSurvey::where('survey_code', $survey_code)->update(['status_worker'=>2,'survey_status'=>'Finished']);
            return response()->json(['message'=>'Data Berhasil Di Kirim Ke WMS','success'=>1]); 
        }
        else
        {
            return response()->json(['message'=>'Gagal Kirim Data Ke WMS','success'=>0]);
        }
    }

    public function upload_photo_survey_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','survey_code'=>'required']);
        $gagal = response()->json(['message'=>'Survey Code Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $trans = Survey_Transaction_Photo::where('survey_code', $request->survey_code)->first();

        $this->validate($request, [
            'photo1' => 'image|mimes:jpeg,png,jpg',
            'photo2' => 'image|mimes:jpeg,png,jpg',
            'photo3' => 'image|mimes:jpeg,png,jpg',
            'photo4' => 'image|mimes:jpeg,png,jpg'
        ]);

        $survey_code = $request->input('survey_code');

        if($trans){
            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($request->hasfile('photo1'))
            {
                $image = $request->file('photo1');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url = asset('/uploads/surveyphotos/'.$name);

                array_push($images1, $photo_url);
            }

            if($request->hasfile('photo2'))
            {
                $image = $request->file('photo2');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_2 = asset('/uploads/surveyphotos/'.$name);

                array_push($images2, $photo_url_2);
            }

            if($request->hasfile('photo3'))
            {
                $image = $request->file('photo3');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_3 = asset('/uploads/surveyphotos/'.$name);

                array_push($images3, $photo_url_3);
            }

            if($request->hasfile('photo4'))
            {
                $image = $request->file('photo4');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_4 = asset('/uploads/surveyphotos/'.$name);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            DB::table('survey_transaction_photo')
                ->where('survey_code', $survey_code)
                ->update(['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]);

            return response()->json(['message'=>'','success'=>1, 'img_url'=>['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]]);

        }else{

            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($request->hasfile('photo1'))
            {
                $image = $request->file('photo1');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url = asset('/uploads/surveyphotos/'.$name);

                array_push($images1, $photo_url);
            }

            if($request->hasfile('photo2'))
            {
                $image = $request->file('photo2');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_2 = asset('/uploads/surveyphotos/'.$name);

                array_push($images2, $photo_url_2);
            }

            if($request->hasfile('photo3'))
            {   
                $image = $request->file('photo3');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_3 = asset('/uploads/surveyphotos/'.$name);

                array_push($images3, $photo_url_3);
            }

            if($request->hasfile('photo4'))
            {
                $image = $request->file('photo4');
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name = $name_file.'_'.time().'_'.$request->input('survey_code').'.png';
                $destinationPath = public_path('/uploads/surveyphotos');
                $image->move($destinationPath, $name);
                $photo_url_4 = asset('/uploads/surveyphotos/'.$name);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            $data = new Survey_Transaction_Photo();
            $data->survey_code = $request->input('survey_code');
            $data->user_id = $request->input('userid');
            $data->photo1 = $image1;
            $data->photo2 = $image2;
            $data->photo3 = $image3;
            $data->photo4 = $image4;
            $data->save();

            if ($data) 
            {
                AssignmentSurvey::where('survey_code', $survey_code)
                                ->update(['status_worker'=>1]);
                return response()->json(['message'=>'','success'=>1, 'img_url'=>['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]]);
            }
            else
            {
                return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
            }    
        }
    }

    public function get_all_job_map(Request $request){
        $dispatch_group = $request->input('dispatch_group');
        $filter_group = $request->input('filter_group');
        $filter_status = $request->input('filter_status');

        if ($filter_group && $filter_status) {
            //cek grup apakah wo atau fa
            if ($filter_group == "fa_group") {
                //get filter status and decode it
                $decode = json_decode($filter_status);

                //foreach each element in $decode
                foreach ($decode as $key => $value) {
                    if ($value == 1) {
                        $ass_status[] = 'Open';
                    }if ($value == 2) {
                        $ass_status[] = 'Assigned';
                    }if ($value == 3) {
                        $ass_status[] = 'Re-Assigned';
                    }if ($value == 4) {
                        $ass_status[] = 'On The Way';
                    }if ($value == 5) {
                        $ass_status[] = 'Pickup';
                    }if ($value == 6) {
                        $ass_status[] = 'Started';
                    }if ($value == 7) {
                        $ass_status[] = 'Cancel Worker';
                    }if ($value == 8) {
                        $ass_status[] = 'Complete';
                    }
                }

                $dispatch = json_decode(str_replace("'", '"', $dispatch_group));
                //get data berdasarkan group dan status
                $fa_id = DB::table('fa_transaction')
                            ->whereIn('assign_status', $ass_status)
                            ->whereIn('dispatch_group', $dispatch)
                            ->join('customer','fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                            ->get();

                //foreach setiap element yang ada dalan $fa_id dan masukkan ke dalam array
                foreach($fa_id as $fa)
                {
                    $row = array();
                    $row['id'] = $fa->fa_transaction_id;
                    $row['type'] = $fa->fa_type_cd;

                    if($fa->assign_status == 'Open'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else if($fa->assign_status == 'Assigned'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else if($fa->assign_status == 'Re-Assigned'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else{
                        $latitude = $fa->lat;
                        $longitude = $fa->long;
                    }
                    
                    $row['lat'] = $latitude;
                    $row['long'] = $longitude;

                    if($fa->assign_status == 'Open'){
                        $assign_status = 1;
                    }if($fa->assign_status == 'Assigned'){
                        $assign_status = 2;
                    }if($fa->assign_status == 'Re-Assigned'){
                        $assign_status = 3;
                    }if($fa->assign_status == 'On The Way'){
                        $assign_status = 4;
                    }if($fa->assign_status == 'Pickup'){
                        $assign_status = 5;
                    }if($fa->assign_status == 'Started'){
                        $assign_status = 6;
                    }if($fa->assign_status == 'Cancel Worker'){
                        $assign_status = 7;
                    }if($fa->assign_status == 'Complete'){
                        $assign_status = 8;
                    }

                    $row['status'] = $assign_status;

                    $data[] = $row;
                }            
            }

            if ($filter_group == "wo_group") {
                /**
                    WO GROUP BELUM ADA DATA NYA
                */
            }

        }else{
            //Ambil semua Data FA dan WO tanpa Filter status dan Group
            //Ambil data berdasarkan Dispath_group
            $dispatch = json_decode(str_replace("'", '"', $dispatch_group));

            $all_data = DB::table('fa_transaction')
                            ->join('customer','fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                            ->whereIn('fa_transaction.dispatch_group', $dispatch)
                            ->get();
            /*$all_data = FA_transaction::where('dispatch_group', '=', $dispatch_group)
                                      ->join('customer', 'fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                                      ->get();*/

            //foreach setiap element $all_data dan masukkan ke dalam array
            foreach($all_data as $fa)
            {
                $row = array();
                $row['id'] = $fa->fa_transaction_id;
                $row['type'] = $fa->fa_type_cd;

                if($fa->assign_status == 'Open'){
                    $latitude = $fa->latitude;
                    $longitude = $fa->longitude;
                }else if($fa->assign_status == 'Assigned'){
                    $latitude = $fa->latitude;
                    $longitude = $fa->longitude;
                }else if($fa->assign_status == 'Re-Assigned'){
                    $latitude = $fa->latitude;
                    $longitude = $fa->longitude;
                }else{
                    $latitude = $fa->lat;
                    $longitude = $fa->long;
                }
                
                $row['lat'] = $latitude;
                $row['long'] = $longitude;

                if($fa->assign_status == 'Open'){
                    $assign_status = 1;
                }if($fa->assign_status == 'Assigned'){
                    $assign_status = 2;
                }if($fa->assign_status == 'Re-Assigned'){
                     $assign_status = 3;
                }if($fa->assign_status == 'On The Way'){
                    $assign_status = 4;
                }if($fa->assign_status == 'Pickup'){
                    $assign_status = 5;
                }if($fa->assign_status == 'Started'){
                    $assign_status = 6;
                }if($fa->assign_status == 'Cancel Worker'){
                    $assign_status = 7;
                }if($fa->assign_status == 'Complete'){
                    $assign_status = 8;
                }

                $row['status'] = $assign_status;
                //masukkan ke dalam array
                $data[] = $row;
            } 
        }

        //Cek data array apakah ada isi atau tidak
        if(empty($data)){
            return response()->json([
                'message'=>'Silahkan Cek Data Kembali',
                'success'=>0
            ]);
        }

        //tampilkan data array dalam bentuk json
        return response()->json($data);
    }

    public function search_job_map(Request $request){
        /**
        params: 
            id: String
        response:
            id: fa transaction id, String
            type: fa type, String
            lat: String
            lon: String
            status: integer
        */

        $validator = Validator::make($request->all(),['id'=>'required']);

        $fa_transaction_id = $request->input('id');
        //get data berdasarkan ID => Fa_transaction_id
        $fa = FA_transaction::join('customer', 'fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                            ->where('customer.fa_transaction_id', $fa_transaction_id)
                            ->first();

        // echo json_encode($fa);
        // exit();

        if ($fa) {

            $row = array();
            $row['id'] = $fa->fa_transaction_id;
            $row['type'] = $fa->fa_type_cd;
            if($fa->assign_status == 'Open'){
                $latitude = $fa->latitude;
                $longitude = $fa->longitude;
            }else if($fa->assign_status == 'Assigned'){
                $latitude = $fa->latitude;
                $longitude = $fa->longitude;
            }else if($fa->assign_status == 'Re-Assigned'){
                $latitude = $fa->latitude;
                $longitude = $fa->longitude;
            }else{
                $latitude = $fa->lat;
                $longitude = $fa->long;
            }
                
            $row['lat'] = $latitude;
            $row['long'] = $longitude;

            if($fa->assign_status == 'Open'){
                $assign_status = 1;
            }if($fa->assign_status == 'Assigned'){
                $assign_status = 2;
            }if($fa->assign_status == 'Re-Assigned'){
                $assign_status = 3;
            }if($fa->assign_status == 'On The Way'){
                $assign_status = 4;
            }if($fa->assign_status == 'Pickup'){
                $assign_status = 5;
            }if($fa->assign_status == 'Started'){
                $assign_status = 6;
            }if($fa->assign_status == 'Cancel Worker'){
                $assign_status = 7;
            }if($fa->assign_status == 'Complete'){
                $assign_status = 8;
            }

            $row['status'] = $assign_status;
            //masukkan ke dalam array
            $data[] = $row;

            return response()->json($data);

        }else{
            return response()->json(['message'=>'FA ID yang anda masukkan tidak ada', 'success'=>0]);
        }
    }

    public function get_all_worker(Request $request){
        $validator = Validator::make($request->all(),['dispatch_group'=>'required']);

        $dispatch_group = $request->input('dispatch_group');
        // $level_battery = $request->input('level_battery');
        // $signal = $request->input('signal');
        $filter = $request->input('filter');
        $worker_name = $request->input('worker_name');

        /**
            1. Ambil fa transaction id berdasarkan dispatch group
            2. join table fa transaction dan worker untuk mengambil data workernya
        */
        $dispatch = json_decode(str_replace("'", '"', $dispatch_group));
        //get fa
        $fa = DB::table('fa_transaction')
                ->join('customer','fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                ->join('workers', 'fa_transaction.worker_id','=','workers.id')
                ->whereIn('fa_transaction.dispatch_group', $dispatch)
                ->get();
        //get wo
        // $wo = FA_transaction::where('dispatch_group', '=', $dispatch_group)
        //                     ->join('workers', 'fa_transaction.worker_id','=','workers.id')
        //                     ->get();

        /**
            cek apakah filter berfungsi atau tidak dengan mengecek inputan yang diberikan
            - jika inputan hanya dispatch_group saja, maka munculkan semua data tanpa filter
            - jike inputan berupa 2 variable yang berbeda : filter dan worker_name, maka keluarkan 
              data sesuai dengan inputan yang diberikan
        */

        if ($filter && $worker_name) {
            if ($filter == "fa_group") {

                $dispatch = json_decode(str_replace("'", '"', $dispatch_group));

                //get fa transaction using worker_id
                $fa_filter = DB::table('fa_transaction')
                                ->join('customer','fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                                ->join('workers', 'fa_transaction.worker_id','=','workers.id')
                                ->whereIn('fa_transaction.dispatch_group', $dispatch)
                                ->where('workers.worker_id', '=', $worker_name)
                                ->get();

                // echo json_encode($fa_filter);
                // exit();

                foreach ($fa_filter as $fa) {
                    $row = array();
                    $row['crew'] = '';
                    $row['name'] = $fa->worker_id;
                    $row['id'] = $fa->fa_transaction_id;
                    $row['type'] = $fa->fa_type_cd;

                    if($fa->assign_status == 'Open'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else if($fa->assign_status == 'Assigned'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else if($fa->assign_status == 'Re-Assigned'){
                        $latitude = $fa->latitude;
                        $longitude = $fa->longitude;
                    }else{
                        $latitude = $fa->lat;
                        $longitude = $fa->long;
                    }

                    $row['lat'] = $latitude;
                    $row['long'] = $longitude;

                    if($fa->assign_status == 'Open'){
                        $assign_status = 1;
                    }if($fa->assign_status == 'Assigned'){
                        $assign_status = 2;
                    }if($fa->assign_status == 'Re-Assigned'){
                        $assign_status = 3;
                    }if($fa->assign_status == 'On The Way'){
                        $assign_status = 4;
                    }if($fa->assign_status == 'Pickup'){
                        $assign_status = 5;
                    }if($fa->assign_status == 'Started'){
                        $assign_status = 6;
                    }if($fa->assign_status == 'Cancel Worker'){
                        $assign_status = 7;
                    }if($fa->assign_status == 'Complete'){
                        $assign_status = 8;
                    }

                    $row['status'] = $assign_status;

                    //kalkulasi persen signal
                    if ($fa->level_signal == 1) {
                        $signal_lvl = '25%';
                    }else if ($fa->level_signal == 2) {
                        $signal_lvl = '50%';
                    }else if ($fa->level_signal == 3) {
                        $signal_lvl = '75%';
                    }else if ($fa->level_signal == 4) {
                        $signal_lvl = '100%';
                    }else{
                        $signal_lvl = '0%';
                    }

                    //kalkulasi batery
                    if ($fa->level_battery == '') {
                        $row['battery'] = '0%';
                    }else{
                        $row['battery'] = $fa->level_battery.'%';
                    }

                    $row['signal'] = $signal_lvl;
                    
                    //masukkan ke dalam array
                    $data[] = $row;
                }
            }

            else if ($filter == "wo_group") {
                /**
                    DATA BELOM ADA
                */
            }
            
        }else{
            //FA
            foreach ($fa as $val) {
                $row = array();
                $row['crew'] = '';
                $row['name'] = $val->worker_id;
                $row['id'] = $val->fa_transaction_id;
                $row['type'] = $val->fa_type_cd;
                
                if($val->assign_status == 'Open'){
                    $latitude = $val->latitude;
                    $longitude = $val->longitude;
                }else if($val->assign_status == 'Assigned'){
                    $latitude = $val->latitude;
                    $longitude = $val->longitude;
                }else if($val->assign_status == 'Re-Assigned'){
                    $latitude = $val->latitude;
                    $longitude = $val->longitude;
                }else{
                    $latitude = $val->lat;
                    $longitude = $val->long;
                }
                    
                $row['lat'] = $latitude;
                $row['long'] = $longitude;

                if($val->assign_status == 'Open'){
                    $assign_status = 1;
                }if($val->assign_status == 'Assigned'){
                    $assign_status = 2;
                }if($val->assign_status == 'Re-Assigned'){
                    $assign_status = 3;
                }if($val->assign_status == 'On The Way'){
                    $assign_status = 4;
                }if($val->assign_status == 'Pickup'){
                    $assign_status = 5;
                }if($val->assign_status == 'Started'){
                    $assign_status = 6;
                }if($val->assign_status == 'Cancel Worker'){
                    $assign_status = 7;
                }if($val->assign_status == 'Complete'){
                    $assign_status = 8;
                }

                $row['status'] = $assign_status;

                //kalkulasi persen signal
                if ($val->level_signal == 1) {
                    $signal_lvl = '25%';
                }else if ($val->level_signal == 2) {
                    $signal_lvl = '50%';
                }else if ($val->level_signal == 3) {
                    $signal_lvl = '75%';
                }else if ($val->level_signal == 4) {
                    $signal_lvl = '100%';
                }else{
                    $signal_lvl = '0%';
                }

                //kalkulasi batery
                if ($val->level_battery == '') {
                    $row['battery'] = '0%';
                }else{
                    $row['battery'] = $val->level_battery.'%';
                }

                $row['signal'] = $signal_lvl;
                
                //masukkan ke dalam array
                $data[] = $row;
                
            }//end foreach

            //WO
            // foreach ($wo as $val) {
            //     $row = array();
            //     $row['crew'] = 'wo';
            //     $row['name'] = 'wo';
            //     $row['id'] = 'wo';
            //     $row['type'] = 'wo'; 
            //     $row['lat'] = 'wo';
            //     $row['long'] = 'wo';
            //     $row['status'] = 'wo';
            //     $row['battery'] = 'wo';
            //     $row['signal'] = 'wo';

            //     $data[] = $row;
            // }
            
        }
        
        if (empty($data)) {
            return response()->json(['message'=>'Data Tidak Ada','success'=>0]);
        }else{
            return response()->json($data);
        }
    }

    public function search_worker(Request $request){
        /**
        params: 
            id: String/worker_name is worker_id
        response:
            id: fa transaction id, String
            type: fa type, String
            lat: String
            lon: String
            status: integer
        */

        $validator = Validator::make($request->all(),['id'=>'required']);

        $worker_name = $request->input('id');
        // $level_battery = $request->input('level_battery');
        // $signal = $request->input('signal');
        //get data berdasarkan ID => Fa_transaction_id
        $worker = FA_transaction::join('workers', 'fa_transaction.worker_id','=', 'workers.id')
                                ->join('customer', 'fa_transaction.fa_transaction_id','=','customer.fa_transaction_id')
                                ->where('workers.worker_id', '=', $worker_name)
                                ->get();

        // echo json_encode($worker);
        // exit();

        if ($worker) {
            foreach ($worker as $key => $value) {
                $row = array();
                $row['id'] = $value->fa_transaction_id;
                $row['type'] = $value->fa_type_cd;
                if($value->assign_status == 'Open'){
                    $latitude = $value->latitude;
                    $longitude = $value->longitude;
                }else if($value->assign_status == 'Assigned'){
                    $latitude = $value->latitude;
                    $longitude = $value->longitude;
                }else if($value->assign_status == 'Re-Assigned'){
                    $latitude = $value->latitude;
                    $longitude = $value->longitude;
                }else{
                    $latitude = $value->lat;
                    $longitude = $value->long;
                }
                    
                $row['lat'] = $latitude;
                $row['long'] = $longitude;

                if($value->assign_status == 'Open'){
                    $assign_status = 1;
                }if($value->assign_status == 'Assigned'){
                    $assign_status = 2;
                }if($value->assign_status == 'Re-Assigned'){
                    $assign_status = 3;
                }if($value->assign_status == 'On The Way'){
                    $assign_status = 4;
                }if($value->assign_status == 'Pickup'){
                    $assign_status = 5;
                }if($value->assign_status == 'Started'){
                    $assign_status = 6;
                }if($value->assign_status == 'Cancel Worker'){
                    $assign_status = 7;
                }if($value->assign_status == 'Complete'){
                    $assign_status = 8;
                }

                $row['status'] = $assign_status;

                //kalkulasi persen signal
                if ($value->level_signal == 1) {
                    $signal_lvl = '25%';
                }else if ($value->level_signal == 2) {
                    $signal_lvl = '50%';
                }else if ($value->level_signal == 3) {
                    $signal_lvl = '75%';
                }else if ($value->level_signal == 4) {
                    $signal_lvl = '100%';
                }else{
                    $signal_lvl = '0%';
                }

                //kalkulasi batery
                if ($value->level_battery == '') {
                    $row['battery'] = '0%';
                }else{
                    $row['battery'] = $value->level_battery.'%';
                }
                
                $row['signal'] = $signal_lvl;

                //masukkan ke dalam array
                $data[] = $row;
            }

            if (empty($data)) {
                return response()->json(['message'=>'Worker Tidak Tersedia','success'=>0]);
            }else{
                return response()->json($data);
            }

        }else{
            return response()->json(['message'=>'Data Worker Tidak Dapat Di Ambil', 'success'=>0]);
        }
    }

    public function update_batery_signal_level(Request $request){

        $validator = Validator::make($request->all(),['worker_id'=>'required', 'level_battery'=>'required', 'level_signal'=>'required']);

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $worker_id = $request->input('worker_id');
        $level_battery = $request->input('level_battery');
        $level_signal = $request->input('level_signal');

        //cek worker apakah tersedia atau tidak
        $worker_cek = Workers::where('worker_id','=', $worker_id)->first();

        if ($worker_cek) {
            //update level batery dan signal di table workers
            $data = Workers::where('worker_id', '=', $worker_id)
                           ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]);
            if($data){
                return response()->json(['level_battery'=>$level_battery,'level_signal'=>$level_signal,'success'=>1]);
            }else{
                return response()->json(['level_battery'=>0,'level_signal'=>0,'success'=>0]);
            }

        }else{
            return response()->json(['message'=>'Worker Tidak Ditemukan Silahkan Cek Ke Admin', 'success'=>1]);
        }
    }

    public function post_bom_hh(Request $request)
    {
        $validator = Validator::make($request->all(),['userid'=>'required', 'fa_transaction_id'=>'required']);
        $gagal = response()->json(['message'=>'FA Transaction Not Found', 'success'=>0]);

        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->input('userid');
        $fa_transaction_id = $request->input('fa_transaction_id');
        $template_id = $request->input('template_id');
        $data = $request->input('data');
        $decode_data = json_decode($data);

        //cek apakah karakteristik bom sudah ada di dalam table bom karakteristik atau belum
        //jika belum maka lakukan create data baru
        //jika sudah maka lakukan update data lama

        $cek_bom_status = FA_Template_BOM_Characteristic::where('fa_transaction_id', $fa_transaction_id)->first();

        if (empty($cek_bom_status)) {
            foreach ($decode_data as $key => $value) {
                $data = new FA_Template_BOM_Characteristic();
                $data->user_id = $userid;
                $data->fa_transaction_id = $fa_transaction_id;
                $data->template_bom_id = $template_id;
                $data->code = trim($value->code);
                $data->qty = $value->qty;
                $data->save();
            }
        }else{
            //update characteristic di table characteristic bom
            foreach ($decode_data as $item => $value) {
                    $data = FA_Template_BOM_Characteristic::where('fa_transaction_id', $fa_transaction_id)
                            ->where('code', trim($value->code))
                            ->update([
                            'user_id'=>$userid, 
                            'fa_transaction_id'=>$fa_transaction_id, 
                            'template_bom_id'=>$template_id, 
                            'code'=>$value->code,
                            'qty'=>$value->qty
                    ]);
            }
        }
        
        if ($data) {
        
            DB::table('fa_transaction')
                    ->where('fa_transaction_id', $fa_transaction_id)
                    ->update(['status_worker' => 4]);

            //SEND TO SUN SERVER
            return response()->json(['status'=>1, 'message'=>'']);
        }else{
            return response()->json(['status'=>1, 'message'=>'Gagal Save Data BOM Ke WMS']);
        }
    }

    public function history_worker(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'FA Transaction Not Found','success'=>0]);

        //status yang tidak boleh muncul
        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $fa_transaction = FA_transaction::with('worker.users')
            ->whereIn('fa_transaction.assign_status', ['Complete', 'Uncompleted'])
            ->with('fa_type.fa_type_lang')
            ->with('fa_template_BOM.fa_template_BOM_detail')
            ->whereHas('worker', function ($query) use ($userid){
                $query->where('user_id','=',$userid);
            })->get();

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        foreach($fa_transaction as $fa)
        {
            $row['fa_id'] = $fa->id;
            $row['fa_transaction_id'] = $fa->fa_transaction_id;
            $row['fa_type'] = $fa->fa_type_cd;
            $row['descr'] = $fa->descriptions;
            $row['name'] = $fa->customer->customer_name;
            $row['address'] = $fa->customer->address." ".$fa->customer->city;
            $row['phone'] = $fa->customer->phone;
            $row['status'] = $fa->assign_status;

            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'History Transaction Tidak Ditemukan',
                'success'=>0,
            ]);
        }

        return response()->json($data);
    }

    public function list_wo_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);

        //status yang tidak boleh muncul
        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $now = date('H:i:s');
 
        //check status tutup tugas
        $close_work = System_parameter::first();
        if ($close_work->close_time <= $now && $close_work->close_time_status == 'Active') {
            $close_fa_id = WO_transaction::with('worker.users')
                            ->whereHas('worker', function($query) use ($userid){
                                $query->where('user_id', '=', $userid);
                            })->get();
            
            foreach ($close_fa_id as $key => $value) {
                if ($value->assign_status == 'Assigned' or $value->assign_status == 'Re-Assigned') {
                    $close = WO_transaction::whereIn('assign_status', ['Assigned','Re-Assigned'])
                                    ->update(['assign_date'=>null, 
                                              'reassign_date'=> null,
                                              'worker_id'=>null,
                                              'assign_status'=>'Open',
                                              'sending_status'=>'Pending'
                                          ]);

                    if ($close) {
                        return response()->json(['message'=>'', 'success'=>1]);
                    }else{
                        return response()->json(['message'=>'Tutup Tugas Gagal', 'success'=>0]);
                    }
                }
            }
        }

        // get today
        $today = Carbon::now();
        $today = $today->toDateString(); 

        //Status 
        $assign_status = ['Complete','Uncompleted'];

        $wo_transaction = WO_transaction::with('worker.users')
                    ->whereNotIn('wo_transaction.assign_status', $assign_status)
                    //->whereDate('wo_transaction.assign_date', $today) 
                    ->with('wo_task.wo_char_type')
                    ->with('wo_template_BOM.wo_template_BOM_detail')
                    ->whereHas('worker', function ($query) use ($userid){
                        $query->where('user_id','=',$userid);
                    })->get();


        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }
        
        foreach($wo_transaction as $wo)
        {
            $row = array();
            $row['id'] = $wo->id;
            $row['asset_id'] = $wo->asset_id;
            $row['asset_desc'] = $wo->asset_desc;
            $row['jobCode'] = $wo->jobCode;
            $row['descr'] = $wo->descr;
            $row['area'] = $wo->area;
            $row['department'] = $wo->department;
            $row['no_account'] = $wo->no_account;
            $row['no_specification'] = $wo->no_specification;
            $row['plant'] = $wo->plant;
            $row['no_wo'] = $wo->no_wo;
            $row['no_wo_task'] = $wo->no_wo_task;
            $row['task_status'] = $wo->task_status;
            $row['task_decs'] = $wo->task_desc;
            $row['date'] = $wo->created_at ? $wo->created_at->format('Y-m-d H:i:s') : '';
            $row['latitude'] = $wo->lat;
            $row['longitude'] = $wo->long;
            $row['urgent'] = ($wo->urgent_status == null) ? 0: (int)$wo->urgent_status;

            /*status: int,
                1=> Open,
                2=> Assigned,
                3=> Re-Assigned,
                4=> On The Way,
                5=> Pickup,
                6=> Start,
                7=> Cancel Worker*/
            if($wo->assign_status == 'Open'){
                $assign_status = 1;
            }if($wo->assign_status == 'Assigned'){
                $assign_status = 2;
            }if($wo->assign_status == 'Re-Assigned'){
                $assign_status = 3;
            }if($wo->assign_status == 'On The Way'){
                $assign_status = 4;
            }if($wo->assign_status == 'Pickup'){
                $assign_status = 5;
            }if($wo->assign_status == 'Started'){
                $assign_status = 6;
            }if($wo->assign_status == 'Cancel Worker'){
                $assign_status = 7;
            }if($wo->assign_status == 'Complete'){
                $assign_status = 8;
            }
            $row['status'] = $assign_status;

            /** Status worket : int
                0 => worker belum melakukan upload data 
                1 => foto sebelum pekerjaan diupload 
                2 => foto sesudah pekerjaan sudah diupload  
                3 => remarks/characteristic di upload 
                4 => bom diupload 
                5 => ttd diupload
                6 => complete 
                7 => cancel 
            */

            $row['status_worker'] = (int)$wo->status_worker;

            $cek_posisiton = DB::table('wo_transaction_photo')
                             ->where('no_wo', $wo->no_wo)
                             ->where('no_wo_task', $wo->no_wo_task)
                             ->get();

            // echo json_encode($cek_posisiton);
            // exit();

            $photo1 = [];
            $photo2 = [];
            $photo3 = [];
            $photo4 = [];

            foreach ($cek_posisiton as $posisiton) {
                if ($posisiton->photo1) {
                    
                    array_push($photo1, $posisiton->photo1);
                }
                else
                {
                    array_push($photo1, '');
                }

                if($posisiton->photo2)
                {
                    array_push($photo2, $posisiton->photo2);   
                }
                else
                {
                    array_push($photo2, '');
                }

                if ($posisiton->photo3) 
                {
                    array_push($photo3, $posisiton->photo3);
                }
                else
                {
                    array_push($photo3, '');
                }

                if ($posisiton->photo4) 
                {
                    array_push($photo4, $posisiton->photo4);
                }
                else
                {
                    array_push($photo4, '');
                }        
            }
            
            $photo_sebelum1 = implode('', $photo1);
            $photo_sebelum2 = implode('', $photo2);
            $photo_sesudah1 = implode('', $photo3);
            $photo_sesudah2 = implode('', $photo4);

            $row['worker_data']['photo']['photo_sebelum1'] = $photo_sebelum1;
            $row['worker_data']['photo']['photo_sebelum2'] = $photo_sebelum2;
            $row['worker_data']['photo']['photo_sesudah1'] = $photo_sesudah1;
            $row['worker_data']['photo']['photo_sesudah2'] = $photo_sesudah2;

            $characteristic = getWOCharacter($wo->jobCode);
            $characteristic = $characteristic->groupBy('char_wo_code');

            $field_detail = [];
            $desc = '';
            foreach ($characteristic as $key =>$item) {
                if (empty($characteristic[$key][0]->char_wo_type)) {
                    $field_detail[] = '';
                    $desc = '';
                }else{
                    $row2 = [];
                    $desc = $characteristic[$key][0]->descr;
                    $row2['name'] = $characteristic[$key][0]->char_wo_type;
                    $row2['sort_seq'] = trim($characteristic[$key][0]->char_seq);
                    $row2['char_type_code'] = trim($characteristic[$key][0]->char_wo_code);
                    $row2['char_type_flg'] = trim($characteristic[$key][0]->char_type_flg);

                    $row2['layout']['type'] = $this->trim_type_wo($characteristic[$key][0]->char_type_flg);
                    $row2['mandatory'] = true;

                    $value = [];
                    $var_chal = '';
                    foreach ($characteristic[$key] as $field){

                        if($field->char_value != null){
                            $var_chal = trim($field->char_value);
                            $value[] = [
                                'code' => $var_chal,
                                'desc' => $var_chal
                            ];
                        }
                    }
                    

                    $row2['layout']['value'] = $value;

                    $list_name = $row2['name'] = $characteristic[$key][0]->char_wo_type;
                    $wo_id = WO_Transaction_characteristic::where('no_wo', $wo->no_wo)
                                                            ->where('no_wo_task', $wo->no_wo_task)
                                                            ->get();
                    
                    $user_value = '';
                    foreach ($wo_id as $key => $value) {
                        if($list_name == $value->name_characteristic) {
                            $user_value = $value->value_characteristic;
                            //print_r($user_value);
                        }
                    }    
                             
                    $row2['layout']['user_value'] = $user_value;//$user_value
                    $field_detail[] = $row2;
                }
            }

            if (empty($field_detail[0])) {
                $char_view = response();
            }else{
                $char_view = [
                    'desc' => $desc,
                    'field' => $field_detail
                ];
            }

            $row['worker_data']['characteristic'] = $char_view;


            // cek apakah fa_type memiliki data fa_template_BOM
            // jika kosong maka munculkan nilai nya String kosong
            // jika ada data, munculkan sesuai dengan keinginan
            // masukkan fa_template_bom ke sebuah variable 

            $characteristic_bom = getBOMCharacterWO($wo->jobCode);
            $characteristic_bom = $characteristic_bom->groupBy('template_name');
            $field_bom = [];

            foreach ($characteristic_bom as $key => $item)
            {
                $row3 = [];
                $row3['id'] = $characteristic_bom[$key][0]->wo_template_BOM_id;
                $row3['name'] = $characteristic_bom[$key][0]->template_name;

                $value = [];
                $var_chal = '';
                foreach ($characteristic_bom[$key] as $field){
                    $var_char = trim($field->code);
                    $var_decs = trim($field->name);
                    $value[] = [
                        'itemcode' => $var_char,
                        'itemname' => $var_decs
                    ];
                }

                $row3['bom']['value'] = $value;

                $wo_bom = WO_Template_BOM_Characteristic::where('no_wo', $wo->no_wo)
                                                        ->where('no_wo_task', $wo->no_wo_task)
                                                        ->get();
                $user_value = '';
                foreach ($characteristic_bom[$key] as $field){
                    foreach ($wo_bom as $key => $value) {
                        if ($field->code == $value->code) {
                            $user_value = $value->qty;
                        }
                    }
                }

                $row3['bom']['user_value'] = $user_value;
                $row3['mandatory'] = true;

                $field_bom[] = $row3;
            } 

            $row['worker_data']['bomtemplate'] = $field_bom;

            //cek table foto ttd berdasarkan asset id
            $cekdata = WO_Transaction_ttd::where('no_wo', $wo->no_wo)
                                        ->where('no_wo_task', $wo->no_wo_task)
                                        ->first();
            if($cekdata){
                $signed = $cekdata->photo_url;
            }else{
                $signed = '';
            }

            $row['worker_data']['signed'] = $signed;
            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'Data WO Transaction Tidak Ditemukan',
                'success'=>0,
                'userid'=>$userid,
                'name'=>'',
                'pict'=>''
            ]);
        }

        return response()->json($data);
    }

    protected function trim_type_wo($char_type_flg)
    {
        if(trim($char_type_flg) == 'DFV'){
            $type = 'dropdown';
        }
        if(trim($char_type_flg) == 'ADV')
        {
            $type = 'text';
        }
        if(trim($char_type_flg) == 'CLS'){
            $type = 'text';
        }

        return $type;
    }

    public function send_ontheway_wo_status_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','asset_id'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db
        
        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $level_battery = $request->input('level_battery');
        $level_signal = $request->input('level_signal');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $asset_id = $request->input('asset_id');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $as_id = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();
        $jobCode = trim($as_id->jobCode);

        /**
            1. select worker berdasarkan userid untuk mengambil id nya
            1. select fa transaction berdasarkan id worker
            2. cek assign_status
            3. jika ada yang on the way / started
            4. kasih response gagal
        */
        $worker = Workers::where('user_id', '=', $request->input('userid'))->first();

        $cek_otw = WO_transaction::where('worker_id','=',$worker->id)
                                        ->where('assign_status', '=', 'On The Way')
                                        ->first();

        $cek_start = WO_transaction::where('worker_id','=',$worker->id)
                                        ->where('assign_status', '=', 'Started')
                                        ->first();

        //cek status lock dari mapping type lock wo
        $lock_status = Wo_task::where('job_code', $jobCode)->first();

        if ($lock_status->lock_fa == 'On The Way' || $lock_status->lock_fa == 'Unlock'){
            $url = "http://172.27.1.38:8280/services/DS_Valid_WO_Task?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $inpPlant = trim($as_id->plant);
                $inpWorkOrderNo = trim($as_id->no_wo);
                $inpWorkOrderTaskNo = trim($as_id->no_wo_task);
                $operation = 'validWoTaskOperation';
                $params = array(
                    'inpPlant' => $inpPlant,
                    'inpWorkOrderNo' => $inpWorkOrderNo,
                    'inpWorkOrderTaskNo' => $inpWorkOrderTaskNo
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['validWoTask'] as $key => $value){
                    $status = trim($value);
                    if ($status == 'T') {

                        $trans  = WO_transaction::where('worker_id',$worker->id)
                                    ->where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'On The Way']);

                        //insert log status wo transaction
                        $log = new Log_wo_status;
                        $log->asset_id = $asset_id;
                        $log->no_wo = $no_wo;
                        $log->no_wo_task = $no_wo_task;
                        $log->status = 'On The Way';
                        $log->message = 'Worker on the way to job location';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$worker->id)
                                            ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 

                        if ($trans) {
                            //CODE FOR LOCK WO 
                            return response()->json(['message'=>'','success'=>1]);
                        }else{
                            return response()->json(['message'=>'Status On The Way Gagal Di Simpan','success'=>0]);
                        }
                         
                    }else if ($status == 'F') {

                        $trans  = WO_transaction::where('worker_id',$worker->id)
                                    ->where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$worker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans){
                            //insert log status wo transaction
                            $log = new Log_wo_status;
                            $log->asset_id= $asset_id;
                            $log->no_wo = $no_wo;
                            $log->no_wo_task = $no_wo_task;
                            $log->status = 'Cancel';
                            $log->message = 'WO Transaction ID has been canceled';
                            $log->save();

                            return response()->json(['message'=>'WO Sudah Di Close','success'=>1]);
                        }else{
                            return response()->json(['message'=>'WO Close Gagal Di Update','success'=>0]);
                        }
                    }
                }
            }
        }
        else
        {
            $url = "http://172.27.1.38:8280/services/DS_Valid_WO_Task?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $inpPlant = trim($as_id->plant);
                $inpWorkOrderNo = trim($as_id->no_wo);
                $inpWorkOrderTaskNo = trim($as_id->no_wo_task);
                $operation = 'validWoTaskOperation';
                $params = array(
                    'inpPlant' => $inpPlant,
                    'inpWorkOrderNo' => $inpWorkOrderNo,
                    'inpWorkOrderTaskNo' => $inpWorkOrderTaskNo
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['validWoTask'] as $key => $value){
                    $status = trim($value);
                    if ($status == 'T') {

                        $trans  = WO_transaction::where('worker_id',$worker->id)
                                    ->where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'On The Way']);

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$worker->id)
                                            ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 

                        if ($trans) {

                            //insert log status wo transaction
                            $log = new Log_wo_status;
                            $log->asset_id= $asset_id;
                            $log->no_wo = $no_wo;
                            $log->no_wo_task = $no_wo_task;
                            $log->status = 'On The Way';
                            $log->message = 'Worker on the way to job location';
                            $log->save();

                            return response()->json(['message'=>'','success'=>1]);
                        }else{

                            //insert log status wo transaction
                            $log = new Log_wo_status;
                            $log->asset_id= $asset_id;
                            $log->no_wo = $no_wo;
                            $log->no_wo_task = $no_wo_task;
                            $log->status = 'On The Way';
                            $log->message = 'Gagal On The Way';
                            $log->save();

                            return response()->json(['message'=>'Status On The Way Gagal Di Simpan','success'=>0]);
                        }
                         
                    }else if ($status == 'F') {

                        $trans  = WO_transaction::where('worker_id',$worker->id)
                                    ->where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Closed','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$worker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans){
                            
                            //insert log status wo transaction
                            $log = new Log_wo_status;
                            $log->asset_id= $asset_id;
                            $log->no_wo = $no_wo;
                            $log->no_wo_task = $no_wo_task;
                            $log->status = 'Cancel';
                            $log->message = 'WO Transaction ID has been canceled';
                            $log->save();

                            return response()->json(['message'=>'WO Sudah Di Close','success'=>1]);
                        }else{
                            return response()->json(['message'=>'WO Close Gagal Di Update','success'=>0]);
                        }
                    }
                }
            }
        }
    }

    public function send_start_wo_status_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','asset_id'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $level_battery = $request->input('level_battery');
        $level_signal = $request->input('level_signal');
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $asset_id = $request->input('asset_id');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $as_id = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();
        $jobCode = trim($as_id->jobCode);

        //get data worker
        $getWorker = Workers::where('user_id',$request->userid)->first();

        $lock_status = Wo_task::where('job_code', $jobCode)->first();

        if ($lock_status->lock_fa == 'Started') 
        {
            $url = "http://172.27.1.38:8280/services/DS_Valid_WO_Task?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $inpPlant = trim($as_id->plant);
                $inpWorkOrderNo = trim($as_id->no_wo);
                $inpWorkOrderTaskNo = trim($as_id->no_wo_task);
                $operation = 'validWoTaskOperation';
                $params = array(
                    'inpPlant' => $inpPlant,
                    'inpWorkOrderNo' => $inpWorkOrderNo,
                    'inpWorkOrderTaskNo' => $inpWorkOrderTaskNo
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['validWoTask'] as $key => $value) {
                    $status = trim($value);
                    if ($status == 'T') {
                        
                        //Kirim Lock WO Ke Simpel

                        //kirim status ke wms
                        $trans  = WO_transaction::where('worker_id',$worker->id)
                                    ->where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Started']);

                        //insert log status wo transaction
                        $log = new Log_wo_status;
                        $log->asset_id = $asset_id;
                        $log->no_wo = $no_wo;
                        $log->no_wo_task = $no_wo_task;
                        $log->status = 'Started';
                        $log->message = 'Worker Starting the job';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'WO Close Gagal Di Update','success'=>0]);
                        }
                        
                    }else if($status == 'F'){

                        $trans  = WO_transaction::where('worker_id',$getWorker->id)
                                    ->where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Closed','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status wo transaction
                        $log = new Log_wo_status;
                        $log->asset_id = $asset_id;
                        $log->no_wo = $no_wo;
                        $log->no_wo_task = $no_wo_task;
                        $log->status = 'Cancel';
                        $log->message = 'WO Transaction ID has been canceled';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'WO Sudah Di Close','success'=>1]);
                        }
                        else
                        {
                            return response()->json(['message'=>'WO Close Gagal Di Update','success'=>0]);
                        }
                    }
                }
            }
        }
        else
        {
            $url = "http://172.27.1.38:8280/services/DS_Valid_WO_Task?wsdl";

            if(!filter_var($url, FILTER_VALIDATE_URL))
            {
                return response()->json(['message'=>'Site is Offline','success'=>0]);
            }
            else
            {
                $client = new \nusoap_client($url, 'wsdl');
                $inpPlant = trim($as_id->plant);
                $inpWorkOrderNo = trim($as_id->no_wo);
                $inpWorkOrderTaskNo = trim($as_id->no_wo_task);
                $operation = 'validWoTaskOperation';
                $params = array(
                    'inpPlant' => $inpPlant,
                    'inpWorkOrderNo' => $inpWorkOrderNo,
                    'inpWorkOrderTaskNo' => $inpWorkOrderTaskNo
                );
                $hasil1 = $client->call($operation, $params);

                foreach ($hasil1['validWoTask'] as $key => $value) {
                    $status = trim($value);
                    if ($status == 'T') {
                    
                        $trans  = WO_transaction::where('worker_id',$getWorker->id)
                                    ->where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Started', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status wo transaction
                        $log = new Log_wo_status;
                        $log->asset_id = $asset_id;
                        $log->no_wo = $no_wo;
                        $log->no_wo_task = $no_wo_task;
                        $log->status = 'Started';
                        $log->message = 'Worker Starting the job';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                             return response()->json(['message'=>'','success'=>1]); 
                        }
                        else
                        {
                             return response()->json(['message'=>'WO Close Gagal Di Update', 'success'=>0]);
                        }
                    }else if($status == 'F'){

                        $trans  = WO_transaction::where('worker_id',$getWorker->id)
                                    ->where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->update(['assign_status'=>'Cancel','status'=>'X', 'lat'=>$latitude, 'long'=>$longitude]);

                        //insert log status wo transaction
                        $log = new Log_wo_status;
                        $log->asset_id = $asset_id;
                        $log->no_wo = $no_wo;
                        $log->no_wo_task = $no_wo_task;
                        $log->status = 'Cancel';
                        $log->message = 'WO Transaction ID has been canceled';
                        $log->save();

                        //update level batery dan signal di table worker
                        $upd_lvl_worker = Workers::where('id','=',$getWorker->id)
                                                ->update(['level_battery'=>$level_battery, 'level_signal'=>$level_signal]); 
                        
                        if($trans)
                        {
                            return response()->json(['message'=>'WO Sudah Di Close','success'=>true]);
                        }
                        else
                        {
                            return response()->json(['message'=>'WO Close Gagal Di Update','success'=>false]);
                        }
                    }
                }
            }
        }
    }

    public function post_characteristic_wo_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required', 'no_wo'=>'required', 'no_wo_task'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found', 'success'=>0]);

        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->input('userid');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $asset_id = $request->input('asset_id');
        $data = $request->input('data');
        $decode_data = json_decode($data);

        //get wo data
        $getWO = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();

        //cek apakah asset_id sudah ada di table post_characteristic_wo
        $cek_wo_id = WO_Transaction_characteristic::where('no_wo', $no_wo)
                                                    ->where('no_wo_task', $no_wo_task)
                                                    ->first();
        if (empty($cek_wo_id)) {
            //create data baru
            foreach ($decode_data as $item => $value) {
                $data = new WO_Transaction_characteristic();
                $data->user_id = $userid; 
                $data->asset_id = $asset_id;
                $data->no_wo = $no_wo;
                $data->no_wo_task = $no_wo_task;
                $data->jobCode = $value->jobCode; 
                $data->name_characteristic = $value->name; 
                $data->value_characteristic = $value->user_value;
                $data->sequence = $value->sort_seq;
                $data->char_type_code = $value->char_type_code;
                $data->char_type_flg = $value->char_type_flg;
                $data->save();
            }
            
        }else{
            //Update Data To WMS
            foreach ($decode_data as $item => $value) {
                    $data = WO_Transaction_characteristic::where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->where('name_characteristic', $value->name)
                            ->update([
                            'user_id'=>$userid, 
                            'asset_id'=>$asset_id, 
                            'no_wo'=>$no_wo,
                            'no_wo_task'=>$no_wo_task,
                            'value_characteristic'=>$value->user_value, 
                            'sequence'=>$value->sort_seq,
                            'char_type_code'=>$value->char_type_code,
                            'char_type_flg'=>$value->char_type_flg,
                            'updated_at'=> Carbon::now()
                    ]);
            }
        }

        //jika simpan data ke wms berhasil
        if ($data){
            //cek status worker di wo transaction
            if ($getWO->status_worker < 3) {
                DB::table('wo_transaction')
                    ->where('no_wo', $no_wo)
                    ->where('no_wo_task', $no_wo_task)
                    ->update(['status_worker'=>3]);
            }

            //save status success
            WO_transaction::where('no_wo', $no_wo)
                        ->where('no_wo_task', $no_wo_task)
                        ->update(['status_wms'=>'Success']);

            return response()->json(['message'=>'','success'=>1]);
        }else{
            //save status gagal
            WO_transaction::where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['status_wms'=>'Gagal']);
            return response()->json(['message'=>'Characteristic Gagal Di Kirim Ke WMS', 'success'=>1]);
        }    
    }

    public function upload_photo_wo_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','asset_id'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $asset_id = $request->input('asset_id');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $user_id = $request->input('userid');
        $photo1 = $request->hasfile('photo1');
        $photo2 = $request->hasfile('photo2');
        $photo3 = $request->hasfile('photo3');
        $photo4 = $request->hasfile('photo4');

        $photo1_req = $request->file('photo1');
        $photo2_req = $request->file('photo2');
        $photo3_req = $request->file('photo3');
        $photo4_req = $request->file('photo4');

        $trans = WO_Transaction_photo::where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->first();

        //get data wo
        $getWO = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();

        $this->validate($request, [
            'photo1' => 'image|mimes:jpeg,png,jpg',
            'photo2' => 'image|mimes:jpeg,png,jpg',
            'photo3' => 'image|mimes:jpeg,png,jpg',
            'photo4' => 'image|mimes:jpeg,png,jpg'
        ]);

        if($trans){

            //Hapus Image Sebelumnya 
            $usersImage1 = public_path("uploads/wo_workphotos/{$trans->photo_name1}");
            $usersImage2 = public_path("uploads/wo_workphotos/{$trans->photo_name2}");
            $usersImage3 = public_path("uploads/wo_workphotos/{$trans->photo_name3}");
            $usersImage4 = public_path("uploads/wo_workphotos/{$trans->photo_name4}");

            if (File::exists($usersImage1)) { 
                unlink($usersImage1);
            }
            if (File::exists($usersImage2)) { 
                unlink($usersImage2);
            }
            if (File::exists($usersImage3)) { 
                unlink($usersImage3);
            }
            if (File::exists($usersImage4)) { 
                unlink($usersImage4);
            }

            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($photo1)
            {
                $image = $photo1_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name1 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-01'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name1);
                $photo_url = asset('/uploads/wo_workphotos/'.$name1);

                array_push($images1, $photo_url);
            }

            if($photo2)
            {
                $image = $photo2_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name2 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-02'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name2);
                $photo_url_2 = asset('/uploads/wo_workphotos/'.$name2);

                array_push($images2, $photo_url_2);
            }

            if($photo3)
            {
                $image = $photo3_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name3 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-03'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name3);
                $photo_url_3 = asset('/uploads/wo_workphotos/'.$name3);

                array_push($images3, $photo_url_3);
            }

            if($photo4)
            {
                $image = $photo4_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name4 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-04'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name4);
                $photo_url_4 = asset('/uploads/wo_workphotos/'.$name4);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            DB::table('wo_transaction_photo')
                ->where('no_wo', $no_wo)
                ->where('no_wo_task', $no_wo_task)
                ->update([  'no_wo'=>$no_wo,
                            'no_wo_task'=>$no_wo_task,
                            'photo_name1' => $name1,
                            'photo_name2' => $name2,
                            'photo_name3' => $name3,
                            'photo_name4' => $name4,
                            'photo1' => $image1, 
                            'photo2' => $image2, 
                            'photo3' => $image3, 
                            'photo4' => $image4,
                            'updated_at' => Carbon::now()
                        ]);

            //cek status worker di wo transaction
            if ($getWO->status_worker < 2) {
                DB::table('wo_transaction')
                    ->where('no_wo', $no_wo)
                    ->where('no_wo_task', $no_wo_task)
                    ->update(['status_worker'=>2]);
            }

            return response()->json(['message'=>'','success'=>1, 'img_url'=>['photo1' => $image1, 'photo2' => $image2, 'photo3' => $image3, 'photo4' => $image4]]);

        }else{

            $images1 = [];
            $images2 = [];
            $images3 = [];
            $images4 = [];
            if($photo1)
            {
                $image = $photo1_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name1 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-01'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name1);
                $photo_url = asset('/uploads/wo_workphotos/'.$name1);

                array_push($images1, $photo_url);
            }

            if($photo2)
            {
                $image = $photo2_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name2 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-02'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name2);
                $photo_url_2 = asset('/uploads/wo_workphotos/'.$name2);

                array_push($images2, $photo_url_2);
            }

            if($photo3)
            {  
                $image = $photo3_req; 
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name3 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-03'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name3);
                $photo_url_3 = asset('/uploads/wo_workphotos/'.$name3);

                array_push($images3, $photo_url_3);
            }

            if($photo4)
            {
                $image = $photo4_req;
                $name_file = pathinfo($image, PATHINFO_FILENAME);
                $name4 = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'-04'.'.png';
                $destinationPath = public_path('/uploads/wo_workphotos');
                $image->move($destinationPath, $name4);
                $photo_url_4 = asset('/uploads/wo_workphotos/'.$name4);

                array_push($images4, $photo_url_4);
            }

            $image1 = implode('', $images1);
            $image2 = implode('', $images2);
            $image3 = implode('', $images3);
            $image4 = implode('', $images4);

            $data = new WO_Transaction_photo();
            $data->asset_id = $asset_id;
            $data->no_wo = $no_wo;
            $data->no_wo_task = $no_wo_task;
            $data->user_id = $user_id;
            $data->photo_name1 = $name1;
            $data->photo_name2 = $name2;
            $data->photo_name3 = $name3;
            $data->photo_name4 = $name4;
            $data->photo1 = $image1;
            $data->photo2 = $image2;
            $data->photo3 = $image3;
            $data->photo4 = $image4;
            $data->save();

            //cek status worker di wo transaction
            if ($getWO->status_worker < 2) {
                DB::table('wo_transaction')
                    ->where('no_wo', $no_wo)
                    ->where('no_wo_task', $no_wo_task)
                    ->update(['status_worker'=>2]);
            }

            if ($data) 
            {
                return response()->json([
                                    'message'=>'','success'=>1, 
                                    'img_url'=>
                                        [
                                            'photo1' => $image1, 
                                            'photo2' => $image2, 
                                            'photo3' => $image3, 
                                            'photo4' => $image4
                                        ]
                                    ]);
            }
            else
            {
                return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
            }    
        }
    }

    public function upload_ttd_wo_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required','asset_id'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);
        
        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $asset_id = $request->input('asset_id');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $userid = $request->input('userid');

        //cek apakah asset id exist
        $trans = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();

        //cek data asset_id di table wo_transaction_ttd
        $cek_ttd = WO_Transaction_ttd::where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->first();
        
        if($trans){
            
            //decode string base64 image to image 
            $image = base64_decode($request->input('ttd'));

            //jika kosong create image baru
            if (empty($cek_ttd)) {
                //jika gambar nya kosong
                if (!isset($image) || empty($image)) {
                    return response()->json(['message'=>'Tanda Tangan Belum Ada','success'=>0]);
                }else{
                    //create image name
                    $photo_name = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'.png';
                    $destinationPath = public_path().'/uploads/signedworkers'.$photo_name;
                    Storage::disk('public')->put($photo_name, $image);
                    $img_url = asset('/storage/signedworkers/'.$photo_name);


                    $data = new WO_Transaction_ttd();
                    $data->asset_id = $asset_id;
                    $data->no_wo = $no_wo;
                    $data->no_wo_task = $no_wo_task;
                    $data->user_id = $userid;
                    $data->photo_name = $photo_name;
                    $data->photo_url = $img_url;
                    $data->save();

                    //update status worker
                    if ($trans->status_worker < 6) {
                        DB::table('wo_transaction')
                            ->where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['status_worker'=>6]);
                    }

                    if ($data) {

                        //update status assign to complete
                        DB::table('wo_transaction')
                            ->where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['assign_status'=>'Complete']);

                        return response()->json(['message'=>'','success'=>1,'img_url'=>$img_url]);
                    }else{
                        return response()->json(['message'=>'Foto Gagal Di Upload','error'=>0]);
                    }
                }
            }else{
                //jika gambar nya kosong
                if (!isset($image) || empty($image)) {
                    return response()->json(['message'=>'Tanda Tangan Belum Ada','success'=>0]);
                }else{
                    //create image name
                    $photo_name = $no_wo.'_'.$no_wo_task.'_'.$asset_id.'.png';
                    $destinationPath = public_path().'/uploads/signedworkers'.$photo_name;
                    Storage::disk('public')->put($photo_name, $image);
                    $img_url = asset('/storage/signedworkers/'.$photo_name);

                    //hapus image lama
                    $usersSign = storage_path("public/signedworkers/{$cek_ttd->photo_name}");
                    if (File::exists($usersSign)) { 
                        unlink($usersSign);
                    }

                    //update table berdasarkan id
                    $data = DB::table('wo_transaction_ttd')
                                ->where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->update([
                                    'asset_id'=>$asset_id,
                                    'no_wo'=>$no_wo,
                                    'no_wo_task'=>$no_wo_task,
                                    'user_id'=>$userid,
                                    'photo_name'=>$photo_name,
                                    'photo_url'=>$img_url
                                ]);

                    //update status worker
                    if ($trans->status_worker < 6) {
                        DB::table('wo_transaction')
                            ->where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['status_worker'=>6]);
                    }

                    if ($data) {
                        //update status assign to complete
                        DB::table('wo_transaction')
                            ->where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['assign_status'=>'Complete']);

                        return response()->json(['message'=>'','success'=>1,'img_url'=>$img_url]);
                    }else{

                        //update status assign to complete
                        DB::table('wo_transaction')
                            ->where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->update(['assign_status'=>'Uncompleted']);

                        return response()->json(['message'=>'Foto Signeture Gagal Di Upload','error'=>0]);
                    }
                }
            }
        }
    }

    public function post_bom_wo_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required', 'asset_id'=>'required', 'no_wo'=>'required', 'no_wo_task'=>'required']);
        $gagal = response()->json(['message'=>'WO Transaction Not Found', 'success'=>0]);

        if ($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0]);
        }

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->input('userid');
        $asset_id = $request->input('asset_id');
        $no_wo = $request->input('no_wo');
        $no_wo_task = $request->input('no_wo_task');
        $template_id = $request->input('template_id');
        $data = $request->input('data');
        $decode_data = json_decode($data);

        //cek apakah karakteristik bom sudah ada di dalam table bom karakteristik atau belum
        //jika belum maka lakukan create data baru
        //jika sudah maka lakukan update data lama

        $cek_bom_status = WO_Template_BOM_Characteristic::where('no_wo', $no_wo)
                                                        ->where('no_wo_task', $no_wo_task)
                                                        ->first();

        //get wo
        $getWO = WO_transaction::where('no_wo', $no_wo)
                                ->where('no_wo_task', $no_wo_task)
                                ->first();

        if (empty($cek_bom_status)) {
            foreach ($decode_data as $key => $value) {
                $data = new WO_Template_BOM_Characteristic();
                $data->user_id = $userid;
                $data->asset_id = $asset_id;
                $data->no_wo = $no_wo;
                $data->no_wo_task = $no_wo_task;
                $data->template_bom_id = $template_id;
                $data->code = trim($value->code);
                $data->qty = $value->qty;
                $data->save();
            }
        }else{
            //update characteristic di table characteristic bom
            foreach ($decode_data as $item => $value) {
                    $data = WO_Template_BOM_Characteristic::where('no_wo', $no_wo)
                            ->where('no_wo_task', $no_wo_task)
                            ->where('code', trim($value->code))
                            ->update([
                            'user_id'=>$userid, 
                            'asset_id'=>$asset_id, 
                            'no_wo'=>$no_wo,
                            'no_wo_task'=>$no_wo_task,
                            'template_bom_id'=>$template_id, 
                            'code'=>$value->code,
                            'qty'=>$value->qty,
                            'updated_at'=>Carbon::now()
                    ]);
            }
        }
        
        if ($data) {
            //update status worker
            if ($getWO->status_worker < 4) {
                DB::table('wo_transaction')
                    ->where('no_wo', $no_wo)
                    ->where('no_wo_task', $no_wo_task)
                    ->update(['status_worker'=>4]);
            }
            //SEND TO SUN SERVER
            return response()->json(['status'=>1, 'message'=>'']);
        }else{
            return response()->json(['status'=>1, 'message'=>'Gagal Save Data BOM Ke WMS']);
        }
    }

    public function history_worker_wo(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'WO Transaction Not Found','success'=>0]);

        //status yang tidak boleh muncul
        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $wo_transaction = WO_transaction::with('worker.users')
                    ->whereIn('wo_transaction.assign_status', ['Complete', 'Uncompleted'])
                    ->with('wo_task.wo_char_type')
                    ->whereHas('worker', function ($query) use ($userid){
                        $query->where('user_id','=',$userid);
                    })->get();

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        foreach($wo_transaction as $wo)
        {
            $row['id'] = $wo->id;
            $row['asset_id'] = $wo->asset_id;
            $row['no_wo'] = $wo->no_wo;
            $row['no_wo_task'] = $wo->no_wo_task;
            $row['asset_decs'] = $wo->asset_desc;
            $row['jobCode'] = $wo->jobCode;
            $row['descr'] = $wo->wo_task->descr;
            $row['status'] = $wo->assign_status;

            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'History Transaction Tidak Ditemukan',
                'success'=>0,
            ]);
        }

        return response()->json($data);
    }

    public function history_worker_survey_hh(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);

        $gagal = response()->json(['message'=>'No Survey Not Found','success'=>0]);

        //status yang tidak boleh muncul
        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $survey_transaction = AssignmentSurvey::with('worker.users')
            ->whereIn('survey_transaction.survey_status', ['Finished', 'Unfinished'])
            ->with('survey')
            ->whereHas('worker', function ($query) use ($userid){
                $query->where('user_id','=',$userid);
            })->get();

        // echo json_encode($survey_transaction);
        // exit();

        if($validator->fails())
        {
            return response()->json(['message'=>$validator->errors()->all(),'success'=>0,'userid'=>'','name'=>'','pict'=>'']);
        }

        foreach($survey_transaction as $su)
        {
            $row['survey_id'] = $su->id;
            $row['survey_transaction_id'] = $su->survey_code;
            $row['survey_type'] = $su->survey->survey_name;
            // $row['nomen'] = ($su->nomen) ? $su->nomen : '';
            // $row['asset_id'] = ($su->asset) ? $su->asset : '';
            $row['status'] = $su->survey_status;

            $data[] = $row;
        }

        if(empty($data)){
            return response()->json([
                'message'=>'History Transaction Tidak Ditemukan',
                'success'=>0,
            ]);
        }

        return response()->json($data);
    }

    public function summaryFA(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);//cek user id
        $gagal = response()->json(['message'=>'Tidak Ada Record Data','success'=>0]);//response jika ada yang gagal

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $list_fa = DB::table('fa_transaction as a')
                    ->join('workers as b', 'a.worker_id', 'b.id')
                    ->join('users as c', 'b.user_id', 'c.id')
                    ->whereIn('a.assign_status', ['Complete'])
                    ->where('b.user_id', $userid)
                    ->select('a.fa_type_cd',DB::raw('count(a.fa_type_cd) as total'))
                    ->groupBy('a.fa_type_cd')
                    ->get();

        foreach ($list_fa as $key => $value) {
            $row['fa_type_cd'] = $value->fa_type_cd;
            $row['total'] = $value->total;

            //get list fa berdasarkan user_id;
            /**
            $list_data_fa = getListFA($userid);
            $list_data_fa = $list_data_fa->groupBy('fa_type_cd');

            foreach ($list_data_fa as $key =>$item) {
                $row3['fa_transaction_id'] = $list_data_fa[$key][0]->fa_transaction_id;
                $row3['nomen'] = $list_data_fa[$key][0]->nomen;

                $field_detail[] = $row3;
            }

            $row['list_fa'] = $field_detail;
            **/
            $data[] = $row;
        }

        if(empty($data)){
            return $gagal;
        }

        return response()->json($data);
    }

    public function testapi(Request $request){
        $validator = Validator::make($request->all(),['userid'=>'required']);//cek user id
        $gagal = response()->json(['message'=>'Tidak Ada Record Data','success'=>0]);//response jika ada yang gagal

        $version_code = $request->version_code; //version code yang dikirim dari android
        $check_version = System_parameter::first();//get system parameter data 
        $v_code_db = $check_version->v_code;//echo version code yang ada di db

        if ($version_code != $v_code_db) {
            return response()->json(['message'=>'Versi Aplikasi Terlalu Rendah, Silahkan Melakukan Update Ke Versi Terbaru', 'success'=>0]);
        }

        $userid = $request->userid;
        $list_fa = DB::table('fa_transaction as a')
                    ->join('workers as b', 'a.worker_id', 'b.id')
                    ->join('users as c', 'b.user_id', 'c.id')
                    ->whereIn('a.assign_status', ['Complete'])
                    ->where('b.user_id', $userid)
                    ->select('a.fa_type_cd',DB::raw('count(a.fa_type_cd) as total'))
                    ->groupBy('a.fa_type_cd')
                    ->get();

        foreach ($list_fa as $key => $value) {
            $row['fa_type_cd'] = $value->fa_type_cd;
            $row['total'] = $value->total;

            //get list fa berdasarkan user_id;
            /**
            $list_data_fa = getListFA($userid);
            $list_data_fa = $list_data_fa->groupBy('fa_type_cd');

            foreach ($list_data_fa as $key =>$item) {
                $row3['fa_transaction_id'] = $list_data_fa[$key][0]->fa_transaction_id;
                $row3['nomen'] = $list_data_fa[$key][0]->nomen;

                $field_detail[] = $row3;
            }

            $row['list_fa'] = $field_detail;
            **/
            $data[] = $row;
        }

        if(empty($data)){
            return $gagal;
        }

        return response()->json($data);
    } 
}