<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Fa_type;
use App\Models\Administrator\Fa_type_lang;
use App\Models\Transaction\Fa_transaction;
use App\Models\Administrator\Wo_task;
use App\Models\Transaction\FA_Template_Bom_Code;
use Session;
use DB;
use Carbon\Carbon;

class DashboardController extends Controller
{
    
       //protected $common;
 
   public function __construct() {
       //$this->common = $common;
   }
 
  public function index()
  {   
    return view('home');
  } 

  public function dashboard(){
      $data['listFa'] = Fa_type::all();
      $data['listWo'] = Wo_task::all();

      $listFa = DB::table('fa_type')
                ->join('fa_type_lang', 'fa_type.id', '=', 'fa_type_lang.fa_type_cd')
                ->select('fa_type.id', 'fa_type.fa_type_cd', 'fa_type_lang.descr')
                ->get();


      $data['list_detail_fa'] = [];

      foreach ($listFa as $value) {

        // $data['status'] = FA_transaction::where('fa_type_cd', $value->fa_type_cd)
        //                             ->groupBy('fa_type_cd')
        //                             ->groupBy('assign_status')
        //                             ->select('fa_type_cd', 'assign_status', \DB::raw('count(*) as count'))
        //                             ->get();
        // echo json_encode($status['status']->fa_type_cd);
        // exit();

        $fa_type_cd = $value->fa_type_cd;
        $descr = $value->descr;

        
        /*$c_op_detail = FA_transaction::where('fa_type_cd', $value->fa_type_cd)
                                    ->where('assign_status','like','%Open%')
                                    ->groupBy('fa_type_cd')
                                    ->count();
                                    */

        //get session
        $user = Session::get('userinfo'); 
        $c_op_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('assign_status', '=', 'Open')
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->groupBy('fa_type_cd')
                        ->count();
        $c_ass_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%Assigned%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $c_otw_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%On The Way%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $c_str_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%Started%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $c_cbw_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%Cancel Worker%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $c_cba_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%Cancel Admin%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $c_cls_detail = DB::table('fa_transaction as a')
                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                        ->distinct()
                        ->select('a.*')
                        ->where('b.group_access_id', '=', $user['group_access_id'])
                        ->where('fa_type_cd', $value->fa_type_cd)
                        ->where('assign_status','like','%Complete%')
                        ->groupBy('fa_type_cd')
                        ->count();

        $total = $c_op_detail + $c_ass_detail +  $c_otw_detail +  $c_str_detail +  $c_cbw_detail + $c_cba_detail + $c_cls_detail;                         

        array_push($data['list_detail_fa'], array('fa_type_cd'=>$fa_type_cd, 'descr'=>$descr,'c_op_detail'=>$c_op_detail, 'c_ass_detail'=>$c_ass_detail, 'c_otw_detail'=>$c_otw_detail, 'c_str_detail'=>$c_str_detail, 'c_cbw_detail'=>$c_cbw_detail, 'c_cba_detail'=>$c_cba_detail, 'c_cls_detail'=>$c_cls_detail, 'total'=>$total));
      }
      // print_r($datas[0]['fa_type_cd']);
      // exit();
      //$data['count_open'] = FA_transaction::where('assign_status','like','%Open%')->count();
      $data['count_open'] = DB::table('fa_transaction as a')
                            ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                            ->distinct()
                            ->select('a.*')
                            ->where('b.group_access_id', '=', $user['group_access_id'])
                            ->where('assign_status', '=', 'Open')
                            ->count();
      $data['count_assigned'] = DB::table('fa_transaction as a')
                                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                ->distinct()
                                ->select('a.*')
                                ->where('b.group_access_id', '=', $user['group_access_id'])
                                ->where('assign_status', '=', 'Assigned')
                                ->count();
      $data['count_ontheway'] = DB::table('fa_transaction as a')
                                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                ->distinct()
                                ->select('a.*')
                                ->where('b.group_access_id', '=', $user['group_access_id'])
                                ->where('assign_status', '=', 'On The Way')
                                ->count();
      $data['count_started'] =  DB::table('fa_transaction as a')
                                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                ->distinct()
                                ->select('a.*')
                                ->where('b.group_access_id', '=', $user['group_access_id'])
                                ->where('assign_status', '=', 'Started')
                                ->count();
      $data['count_cancel_worker'] =  DB::table('fa_transaction as a')
                                      ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                      ->distinct()
                                      ->select('a.*')
                                      ->where('b.group_access_id', '=', $user['group_access_id'])
                                      ->where('assign_status', '=', 'Cancel Worker')
                                      ->count();
      $data['count_cancel_admin'] = DB::table('fa_transaction as a')
                                    ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                    ->distinct()
                                    ->select('a.*')
                                    ->where('b.group_access_id', '=', $user['group_access_id'])
                                    ->where('assign_status', '=', 'Cancel Admin')
                                    ->count();
      $data['count_complete'] = DB::table('fa_transaction as a')
                              ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                              ->distinct()
                              ->select('a.*')
                              ->where('b.group_access_id', '=', $user['group_access_id'])
                              ->where('assign_status', '=', 'Complete')
                              ->count();


    //WO SECTION
    $listWo = DB::table('wo_task as a')
                ->select('a.job_code','a.descr')
                ->get();

    $data['list_detail_wo'] = [];

    foreach ($listWo as $key => $value) {
        $jobCode = $value->job_code;
        $descr = $value->descr;
        array_push($data['list_detail_wo'], array('job_code'=>$jobCode, 'descr'=>$descr)); 
    }

      $data['wo_count_open'] = DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Open')
                            ->count();
      $data['wo_count_assigned'] = DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Assigned')
                            ->count();
      $data['wo_count_ontheway'] = DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'On The Way')
                            ->count();
      $data['wo_count_started'] =  DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Started')
                            ->count();
      $data['wo_count_cancel_worker'] =  DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Cancel Worker')
                            ->count();
      $data['wo_count_cancel_admin'] = DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Cancel Admin')
                            ->count();
      $data['wo_count_complete'] = DB::table('wo_transaction as a')
                            ->distinct()
                            ->select('a.*')
                            ->where('assign_status', '=', 'Complete')
                            ->count();

    //END WO SECTION

    //BOM SECTION
    $data['list_bom'] = FA_Template_Bom_Code::all();

    $listBom = DB::table('fa_template_BOM_code as a')
                  ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                  ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                  ->groupBy('a.code','a.name','a.category','a.unit_of_purch')
                  ->get();

    $data['list_detail_bom'] = [];

    foreach ($listBom as $key => $value) {
        $itemCode = $value->code;
        $desc = $value->name;
        $qty = $value->qty;
        $UoM = $value->unit_of_purch;
        $total = $value->qty;

        array_push($data['list_detail_bom'], array('code' => $itemCode, 'desc' => $desc, 'qty' => $qty, 'uom' => $UoM, 'total' => $total));
    }
      
    return view('dashboard', $data);
  }

  public function getData(Request $request){
    $startPeriode = Carbon::createFromFormat('d-m-Y', $request->startPeriode)->format('Y-m-d');
    $endPeriode = Carbon::createFromFormat('d-m-Y', $request->endPeriode)->format('Y-m-d');
       
    if($startPeriode == '' or $endPeriode == ''){
      return redirect()->route('dashboard');

    }else {

        $data['listFa'] = Fa_type::all();
        $data['listWo'] = Wo_task::all();

        $listFa = DB::table('fa_type')
                  ->join('fa_type_lang', 'fa_type.id', '=', 'fa_type_lang.fa_type_cd')
                  ->select('fa_type.id', 'fa_type.fa_type_cd', 'fa_type_lang.descr')
                  ->get();


        $data['list_detail_fa'] = [];

        foreach ($listFa as $value) {

          $fa_type_cd = $value->fa_type_cd;
          $descr = $value->descr;

          //get session
          $user = Session::get('userinfo'); 
          $c_op_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('assign_status', '=', 'Open')
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->groupBy('fa_type_cd')
                          ->count();
          $c_ass_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%Assigned%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $c_otw_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%On The Way%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $c_str_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%Started%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $c_cbw_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%Cancel Worker%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $c_cba_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%Cancel Admin%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $c_cls_detail = DB::table('fa_transaction as a')
                          ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                          ->distinct()
                          ->select('a.*')
                          ->where('b.group_access_id', '=', $user['group_access_id'])
                          ->where('fa_type_cd', $value->fa_type_cd)
                          ->where('assign_status','like','%Complete%')
                          ->groupBy('fa_type_cd')
                          ->count();

          $total = $c_op_detail + $c_ass_detail +  $c_otw_detail +  $c_str_detail +  $c_cbw_detail + $c_cba_detail + $c_cls_detail;                         

          array_push($data['list_detail_fa'], array('fa_type_cd'=>$fa_type_cd, 'descr'=>$descr,'c_op_detail'=>$c_op_detail, 'c_ass_detail'=>$c_ass_detail, 'c_otw_detail'=>$c_otw_detail, 'c_str_detail'=>$c_str_detail, 'c_cbw_detail'=>$c_cbw_detail, 'c_cba_detail'=>$c_cba_detail, 'c_cls_detail'=>$c_cls_detail, 'total'=>$total));
        }

        $data['count_open'] = DB::table('fa_transaction as a')
                              //->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                              ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                              ->distinct()
                              ->select('a.*')
                              ->where('b.group_access_id', '=', $user['group_access_id'])
                              ->where('assign_status', '=', 'Open')
                              ->count();
        $data['count_assigned'] = DB::table('fa_transaction as a')
                                  ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                  ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                  ->distinct()
                                  ->select('a.*')
                                  ->where('b.group_access_id', '=', $user['group_access_id'])
                                  ->where('assign_status', '=', 'Assigned')
                                  ->count();
        $data['count_ontheway'] = DB::table('fa_transaction as a')
                                  ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                  ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                  ->distinct()
                                  ->select('a.*')
                                  ->where('b.group_access_id', '=', $user['group_access_id'])
                                  ->where('assign_status', '=', 'On The Way')
                                  ->count();
        $data['count_started'] =  DB::table('fa_transaction as a')
                                  ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                  ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                  ->distinct()
                                  ->select('a.*')
                                  ->where('b.group_access_id', '=', $user['group_access_id'])
                                  ->where('assign_status', '=', 'Started')
                                  ->count();
        $data['count_cancel_worker'] =  DB::table('fa_transaction as a')
                                        ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                        ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                        ->distinct()
                                        ->select('a.*')
                                        ->where('b.group_access_id', '=', $user['group_access_id'])
                                        ->where('assign_status', '=', 'Cancel Worker')
                                        ->count();
        $data['count_cancel_admin'] = DB::table('fa_transaction as a')
                                      ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                      ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                      ->distinct()
                                      ->select('a.*')
                                      ->where('b.group_access_id', '=', $user['group_access_id'])
                                      ->where('assign_status', '=', 'Cancel Admin')
                                      ->count();
        $data['count_complete'] = DB::table('fa_transaction as a')
                                ->whereBetween('a.assign_date', [$startPeriode, $endPeriode])
                                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                                ->distinct()
                                ->select('a.*')
                                ->where('b.group_access_id', '=', $user['group_access_id'])
                                ->where('assign_status', '=', 'Complete')
                                ->count();

      //get data wo
      $listWo = DB::table('wo_task as a')
                  ->select('a.job_code')
                  ->get();

      $data['list_detail_wo'] = [];

      foreach ($listWo as $key => $value) {
          $jobCode = $value->job_code;

          array_push($data['list_detail_wo'], array('job_code'=>$jobCode)); 
      }

      //get data BOM
      $data['list_bom'] = FA_Template_Bom_Code::all();

      $listBom = DB::table('fa_template_bom_code as a')
                      ->select('a.code', 'a.name')
                      ->get();

      $data['list_detail_bom'] = [];

      foreach ($listBom as $key => $value) {
          $itemCode = $value->code;
          $desc = $value->name;

          array_push($data['list_detail_bom'], array('code' => $itemCode, 'desc' => $desc));
      }
        
      return view('dashboard', $data);
    }
  }
}
