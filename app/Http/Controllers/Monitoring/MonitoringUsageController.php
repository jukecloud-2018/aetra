<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\FA_Template_BOM_code;
use App\Models\Transaction\FA_Template_BOM_characteristic;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;

class MonitoringUsageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all item code in tabel fa_template_BOM_code
        $show['data'] = DB::table('fa_template_BOM_code as a')
                        ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                        ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                        ->groupBy('a.code','a.name','a.category','a.unit_of_purch')
                        ->paginate(200);

        // $data = DB::table('fa_template_BOM_code as a')
        //             ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
        //             ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
        //             ->groupBy('a.code','a.name','a.category','a.unit_of_purch')
        //             ->get();

        // echo json_encode($show);
        // exit();

        return view('monitoring.monitoringUsage.v_index_monitoringUsage', $show);
    }

    public function getDataByDate(Request $request){
        if($request->startDateUsage == null or $request->endDateUsage == null){
            return redirect()->route('monitoring.monitoringUsage.index');
        }else{

            $start_date = Carbon::createFromFormat('d-m-Y', $request->startDateUsage)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->endDateUsage)->format('Y-m-d');

            //get total search
            $total = DB::table('fa_template_BOM_code as a')
                        ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                        ->whereBetween('b.created_at', [$start_date, $end_date])
                        ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                        ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                        ->orderBy('b.updated_at', 'desc')
                        ->get();

                    // DB::table('fa_template_bom_characteristic as a')
                    //     ->leftJoin('fa_template_BOM_code as b', 'a.code', 'b.code')
                    //     ->whereBetween('a.created_at', [$start_date, $end_date])
                    //     ->select('a.qty','b.code','b.name','b.unit_of_purch')
                    //     ->orderBy('a.updated_at', 'desc')
                    //     ->get();



            //get result
            $show['data'] = DB::table('fa_template_BOM_code as a')
                        ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                        ->whereBetween('b.created_at', [$start_date, $end_date])
                        ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                        ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                        ->orderBy('b.updated_at', 'desc')
                        ->paginate(300);

                    // DB::table('fa_template_bom_characteristic as a')
                    //     ->leftJoin('fa_template_BOM_code as b', 'a.code', 'b.code')
                    //     ->whereBetween('a.created_at', [$start_date, $end_date])
                    //     ->select('a.qty','b.code','b.name','b.unit_of_purch')
                    //     ->orderBy('a.updated_at', 'desc')
                    //     ->paginate(300); 

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['cariDari'] = $start_date;
            $show['cariSmp'] = $end_date;
            $show['total'] = count($total);
            $show['ket'] = 'Search Date';

            return view('monitoring.MonitoringUsage.v_search_monitoringUsage', $show);
        }
    }

    public function search(Request $request){
        //get total search
        $total = DB::table('fa_template_BOM_code as a')
                        ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                        ->where('a.code','LIKE','%'.$request->search."%")
                        ->orWhere('a.name','LIKE','%'.$request->search."%")
                        ->orWhere('a.unit_of_purch','LIKE','%'.$request->search."%")
                        ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                        ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                        ->orderBy('b.updated_at', 'desc')
                        ->get();

                    // DB::table('fa_template_BOM_code as a')
                    //     ->select('a.*')
                    //     ->where('a.code','LIKE','%'.$request->search."%")
                    //     ->orWhere('a.name','LIKE','%'.$request->search."%")
                    //     ->orWhere('a.unit_of_purch','LIKE','%'.$request->search."%")
                    //     ->orderBy('updated_at', 'desc')
                    //     ->get();


        //get result
        $show['data'] = DB::table('fa_template_BOM_code as a')
                        ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                        ->where('a.code','LIKE','%'.$request->search."%")
                        ->orWhere('a.name','LIKE','%'.$request->search."%")
                        ->orWhere('a.unit_of_purch','LIKE','%'.$request->search."%")
                        ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                        ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                        ->orderBy('b.updated_at', 'desc')
                        ->paginate(200);

        $limit = 300;//limit untuk continue page number
        $show['num'] = num_row($request->input('page'), $limit);
        $show['total'] = count($total);
        $show['cari'] = $request->search;
        $show['ket'] = 'Search Data';
        return view('monitoring.monitoringUsage.v_search_monitoringUsage', $show);
    }

    public function getMaterialUsage($code){
        $data['data'] = DB::table('fa_template_bom_characteristic as a')
                    ->leftJoin('fa_template_BOM_code as b', 'a.code','b.code')
                    ->join('fa_transaction as c', 'a.fa_transaction_id', 'c.fa_transaction_id')
                    ->where('a.code', $code)                    
                    ->get();

        return view('monitoring.monitoringUsage.v_detailUsage_monitoringUsage', $data);
    }

    public function filterUsage(Request $request){

        if($request->code == null){
            return redirect()->route('monitoring.monitoringUsage.index');
        }else{
            //get total search
            $total = DB::table('fa_template_BOM_code as a')
                            ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                            ->where('a.code','LIKE','%'.$request->code."%")
                            ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                            ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                            ->orderBy('b.updated_at', 'desc')
                            ->get();

            //get result
            $show['data'] = DB::table('fa_template_BOM_code as a')
                            ->leftjoin('fa_template_bom_characteristic as b', 'a.code', '=', 'b.code')
                            ->where('a.code','LIKE','%'.$request->code."%")
                            ->select('a.code','a.name','a.category','a.unit_of_purch', DB::raw("count(b.code) as count"), DB::raw("sum(b.qty) as qty"))
                            ->groupBy('a.code','a.name','a.category','a.unit_of_purch','b.updated_at')
                            ->orderBy('b.updated_at', 'desc')
                            ->paginate(200);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['cari'] = $request->search;
            $show['ket'] = 'Search Data';
            return view('monitoring.monitoringUsage.v_search_monitoringUsage', $show);
        }
    }
}