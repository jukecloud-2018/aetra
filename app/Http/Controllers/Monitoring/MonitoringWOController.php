<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\WO_transaction;
use App\Models\Transaction\WO_Transaction_characteristic;
use App\Models\Transaction\WO_Transaction_photo;
use App\Models\Transaction\WO_Transaction_ttd;
use App\Models\Transaction\WO_Template_BOM_Characteristic;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use DB;
use PDF;

class MonitoringWOController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $show['data'] = DB::table('wo_transaction as a')
                        ->whereNotIn('a.assign_status', ['Open'])
                        ->leftJoin('workers as b','a.worker_id','=','b.id')
                        ->select('a.*','b.worker_name')
                        ->orderBy('updated_at', 'desc')
                        ->paginate(200);
        $show['open'] = WO_transaction::where('assign_status', '=', 'Open')->count();
        $show['assign'] = WO_transaction::where('assign_status', '=', 'Assigned')->count();
        $show['reassign'] = WO_transaction::where('assign_status', '=', 'Re-Assigned')->count();
        $show['ontheway'] = WO_transaction::where('assign_status', '=', 'On The Way')->count();
        $show['started'] = WO_transaction::where('assign_status', '=', 'Started')->count();
        $show['cancel'] = WO_transaction::where('assign_status', '=', 'Cancel Worker')->count();
        $show['complete'] = WO_transaction::where('assign_status', '=', 'Complete')->count();
        $show['total'] = count($show['data']);

        return view('monitoring.monitoringWO.v_index_monitoringWO', $show);
    }

    public function getDetail($no_wo, $no_wo_task){
        $data = WO_transaction::where('no_wo',$no_wo)
                              ->where('no_wo_task', $no_wo_task)
                              ->with('worker')
                              ->get();
        
        return Datatables::of($data)
            ->editColumn('assign_date', function($data) {
                $assign_date = ($data->assign_date==null) ? "" : date('d-m-Y', strtotime($data->assign_date));
                return  $assign_date;
            })
            ->make(true);
    }

    public function getDataBydate(Request $request)
    {
        if($request->is_date_search == 'no')
        {
            $data = WO_transaction::with('worker')
                ->get();
        }else{
            $start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');

            $data = WO_transaction::with('worker')
                ->where('assign_date', '>=', $start_date)
                ->where('assign_date', '<=', $end_date)
                ->get();

            // echo json_encode($data);
            // exit();
        }
        return Datatables::of($data)
            // ->editColumn('descr', function($data) {
            //     return $data->fa_type->fa_type_lang->descr;
            // })
            // ->editColumn('assign_date', function($data) {
            //     $assign_date = ($data->assign_date==null) ? "" : date('d-m-Y', strtotime($data->assign_date));
            //     return  $assign_date;
            // })
            // ->editColumn('created_at', function($data) {
            //     $created_at = ($data->created_at==null) ? "" : date('d-m-Y H:i:s', strtotime($data->created_at));
            //     return  $created_at;
            // })
            ->make(true);
    }

    public function downloadPdf($no_wo){
        $data['list_task'] = WO_transaction::where('no_wo','=',$no_wo)->get();

        // $data['karakteristik'] = FA_Transaction_characteristic::where('fa_transaction_id','=',$fa_transaction_id)
        //     ->with('transaction')
        //     ->get();

        // $data['material'] = FA_Transaction_material_usage::where('fa_transaction_id','=',$fa_transaction_id)
        //     ->with('transaction')
        //     ->get();

        // $data['photo'] = FA_Transaction_photo::where('fa_transaction_id', '=', $fa_transaction_id)
        //     ->with('transaction')
        //     ->get();

        // $data['ttd'] = FA_Transaction_ttd::where('fa_transaction_id', '=', $fa_transaction_id)
        //     ->with('transaction')
        //     ->get();
        // // echo json_encode($data['ttd']);
        // // exit();
        
        $pdf = PDF::setOptions([
                'images' => true
            ])->loadView('monitoring.monitoringWO.v_generate_pdf', $data);

        return $pdf->download('result'.time().'.pdf');
        // return $pdf->download('result.pdf');
    }

    public function search(Request $request){
        //get total search
        $total = DB::table('wo_transaction as a')
                        ->leftJoin('workers as b','a.worker_id','=','b.id')
                        ->select('a.*','b.worker_name')
                        ->where('a.asset_id','LIKE','%'.$request->search."%")
                        ->orWhere('a.jobCode','LIKE','%'.$request->search."%")
                        ->orWhere('a.no_wo','LIKE','%'.$request->search."%")
                        ->orWhere('a.no_wo_task','LIKE','%'.$request->search."%")
                        ->orWhere('a.asset_desc','LIKE','%'.$request->search."%")
                        ->orWhere('a.task_status','LIKE','%'.$request->search."%")
                        ->orWhere('b.worker_name','LIKE','%'.$request->search."%")
                        ->orWhere('a.assign_status','LIKE','%'.$request->search."%")
                        ->orWhere('a.assign_date','LIKE','%'.$request->search."%")
                        ->orderBy('updated_at', 'desc')
                        ->get();

        //get result
        $show['data'] = DB::table('wo_transaction as a')
                        ->leftJoin('workers as b','a.worker_id','=','b.id')
                        ->select('a.*','b.worker_name')
                        ->where('a.asset_id','LIKE','%'.$request->search."%")
                        ->orWhere('a.jobCode','LIKE','%'.$request->search."%")
                        ->orWhere('a.no_wo','LIKE','%'.$request->search."%")
                        ->orWhere('a.no_wo_task','LIKE','%'.$request->search."%")
                        ->orWhere('a.asset_desc','LIKE','%'.$request->search."%")
                        ->orWhere('a.task_status','LIKE','%'.$request->search."%")
                        ->orWhere('b.worker_name','LIKE','%'.$request->search."%")
                        ->orWhere('a.assign_status','LIKE','%'.$request->search."%")
                        ->orWhere('a.assign_date','LIKE','%'.$request->search."%")
                        ->orderBy('updated_at', 'desc')
                        ->paginate(300);

        $limit = 300;//limit untuk continue page number
        $show['num'] = num_row($request->input('page'), $limit);
        $show['total'] = count($total);
        $show['cari'] = $request->search;
        $show['ket'] = 'Search Data';
        return view('monitoring.monitoringWO.v_search_monitoringWO', $show);
    }

    public function getData(Request $request)
    {
        if($request->startDateMonitoring == '' or $request->endDateMonitoring == ''){
            return redirect()->route('monitoring.monitoringWO.index');
        }else {
            $start_date = Carbon::createFromFormat('d-m-Y', $request->startDateMonitoring)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->endDateMonitoring)->format('Y-m-d');

            //get total search
            $total = DB::table('wo_transaction as a')
                        ->leftJoin('workers as b','a.worker_id','=','b.id')
                        ->whereBetween('a.assign_date', [$start_date, $end_date])
                        ->select('a.*','b.worker_name')
                        ->orderBy('updated_at', 'desc')
                        ->get();

            //get result
            $show['data'] = DB::table('wo_transaction as a')
                        ->leftJoin('workers as b','a.worker_id','=','b.id')
                        ->whereBetween('a.assign_date', [$start_date, $end_date])
                        ->select('a.*','b.worker_name')
                        ->orderBy('updated_at', 'desc')
                        ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['cariDari'] = $start_date;
            $show['cariSmp'] = $end_date;
            $show['total'] = count($total);
            $show['ket'] = 'Search Date';
            return view('monitoring.monitoringWO.v_search_monitoringWO', $show);
        }
    }

    public function filterMonitoringWO(Request $request){
        $jobCode = trim($request->jobCode);

        if ($jobCode == null) {
            return redirect()->route('monitoring.monitoringWO.index');
        }else{
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                                ->where('status', '=', 'wo')
                                ->where('worker_status', '=', 'Active')
                                ->get();

            //total row
            $total = DB::table('wo_transaction as a')
                    ->leftJoin('workers as b','a.worker_id','=','b.id')
                    ->where('a.jobCode', $jobCode)
                    ->select('a.*','b.worker_name')
                    ->orderBy('updated_at', 'desc')
                    ->get();

            //result search
            $show['data'] = DB::table('wo_transaction as a')
                              ->leftJoin('workers as b','a.worker_id','=','b.id')
                              ->where('a.jobCode', $jobCode)
                              ->select('a.*', 'b.id as id_worker', 'b.worker_name')
                              ->orderBy('a.updated_at', 'desc')
                              ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['jobCode'] = $jobCode;
            $show['ket'] = 'Search Filter';
            return view('monitoring.monitoringWO.v_search_monitoringWO', $show);
        }
    }

    public function getCharacteristic($no_wo, $no_wo_task){
        $data = WO_Transaction_characteristic::where('no_wo',$no_wo)
                                            ->where('no_wo_task', $no_wo_task)
                                            ->get();
        return Datatables::of($data)->make(true);
    }

    public function getMaterialUsage($no_wo, $no_wo_task){
        $data = DB::table('wo_template_bom_characteristic as a')
                    ->leftJoin('fa_template_BOM_code as b', 'a.code','b.code')
                    ->where('a.no_wo', $no_wo)
                    ->where('a.no_wo_task', $no_wo_task)
                    ->select('a.code','a.qty','b.name','b.unit_of_purch')
                    ->get();

        return Datatables::of($data)->make(true);
    }

    public function getTTD($no_wo, $no_wo_task){
        $data = WO_Transaction_photo::where('no_wo', $no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->get();
        return Datatables::of($data)->make(true);
    }

    public function getphotowo($no_wo, $no_wo_task){
        $data = WO_Transaction_photo::where('no_wo',$no_wo)
                                    ->where('no_wo_task', $no_wo_task)
                                    ->get();
        return Datatables::of($data)->make(true);
    }
}