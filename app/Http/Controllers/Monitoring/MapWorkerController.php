<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;

class MapWorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clientIP = \Request::getClientIp(true);
		$userinfo = Session::get('userinfo');
		$group_access_id = $userinfo['group_access_id'];

		$data = DB::table('fa_group_access as a')
              ->where('a.group_access_id', $group_access_id)
              ->get();

		foreach ($data as $key => $value) {
			$dispatch_group[] = $value->dispatch_group;

		}
		$list_workers = DB::table('workers')
                          ->select('id','worker_id','worker_name')
                          ->get();
		return view('monitoring.mapWorker.v_index_mapWorker')
			->with('ip', $clientIP)
			->with('userinfo', json_encode($dispatch_group))
			->with('workers', $list_workers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
