<?php

namespace App\Http\Controllers\Monitoring;

use App\Exports\GetHistoryFA;
use App\Models\Transaction\FA_Transaction_characteristic;
use App\Models\Transaction\FA_Transaction_material_usage;
use App\Models\Transaction\FA_Transaction_ttd;
use App\Models\Transaction\FA_Transaction_photo;
use App\Models\Administrator\Workers;
use App\models\Administrator\Log_fa_status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction\FA_transaction;
use App\Models\Transaction\Customer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PDF;
use GuzzleHttp\Client;

class MonitoringFAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {    
        $clientIP = \Request::getClientIp(true);
        $user = Session::get('userinfo'); 
        if($user['role_id'] == 1){
            $show['open'] = FA_transaction::where('assign_status','=','Open')->count();
            $show['assigned'] = FA_transaction::where('assign_status','=','Assigned')->count();
            $show['reAssigned'] = FA_transaction::where('assign_status','=','Re-Assigned')->count();
            $show['ontheway'] = FA_transaction::where('assign_status','=','On The Way')->count();
            $show['pickup'] = FA_transaction::where('assign_status','=','Pick Up')->count();
            $show['started'] = FA_transaction::where('assign_status','=','Started')->count();
            $show['cancel'] = FA_transaction::where('assign_status','=','Cancel Worker')->count();
            $show['complete'] = FA_transaction::where('assign_status','=','Complete')->count();
            $show['total'] = FA_transaction::count();
        }else{
            //open
            $open = DB::table('fa_transaction as a')
            ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
            ->distinct()
            ->select('a.*')
            ->where('b.group_access_id', '=', $user['group_access_id'])
            ->where('assign_status', '=', 'Open')
            ->get();
            //assigned
            $assigned = DB::table('fa_transaction as a')
                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                ->distinct()
                ->select('a.*')
                ->where('b.group_access_id', '=', $user['group_access_id'])
                ->where('assign_status', '=', 'Assigned')
                ->get();
            //assigned
            $reassigned = DB::table('fa_transaction as a')
                    ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                    ->distinct()
                    ->select('a.*')
                    ->where('b.group_access_id', '=', $user['group_access_id'])
                    ->where('assign_status', '=', 'Re-Assigned')
                    ->get();
            //ontheway
            $ontheway = DB::table('fa_transaction as a')
                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                ->distinct()
                ->select('a.*')
                ->where('b.group_access_id', '=', $user['group_access_id'])
                ->where('assign_status', '=', 'On The Way')
                ->get();
            //pickup
            $pickup = DB::table('fa_transaction as a')
                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                ->distinct()
                ->select('a.*')
                ->where('b.group_access_id', '=', $user['group_access_id'])
                ->where('assign_status', '=', 'Pick Up')
                ->get();
            //started
            $started = DB::table('fa_transaction as a')
                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                ->distinct()
                ->select('a.*')
                ->where('b.group_access_id', '=', $user['group_access_id'])
                ->where('assign_status', '=', 'Started')
                ->get();
            //cancel worker
            $cancel = DB::table('fa_transaction as a')
                ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                ->distinct()
                ->select('a.*')
                ->where('b.group_access_id', '=', $user['group_access_id'])
                ->where('assign_status', '=', 'Cancel Worker')
                ->get();
            //complete
            $complete = DB::table('fa_transaction as a')
                    ->join('fa_group_access as b', 'a.dispatch_group', '=', 'b.dispatch_group')
                    ->distinct()
                    ->select('a.*')
                    ->where('b.group_access_id', '=', $user['group_access_id'])
                    ->where('assign_status', '=', 'Complete')
                    ->get();
            //total
            $data = DB::table('fa_transaction as a')
                ->whereNotIn('a.assign_status', ['Open'])
				->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
				->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
				->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
				->leftJoin('workers as e','a.worker_id','=','e.id')
				->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->where('g.group_access_id', '=', $user['group_access_id'])
				->get();

        }
        $show['data'] = DB::table('fa_transaction as a')
              ->whereNotIn('a.assign_status', ['Open'])
              ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
              ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
              ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
              ->leftJoin('workers as e','a.worker_id','=','e.id')
              ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
              ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
              ->where('g.group_access_id', '=', $user['group_access_id'])
              ->select(
				'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
				'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
				'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description'
              )->orderBy('a.updated_at', 'desc')->paginate(300);

        $limit = 300;//limit untuk continue page number
		$show['num'] = num_row($request->input('page'), $limit);
		$show['open'] = count($open);
		$show['assigned'] = count($assigned);
		$show['reAssigned'] = count($reassigned);
		$show['ontheway'] = count($ontheway);
		$show['pickup'] = count($pickup);
		$show['started'] = count($started);
		$show['complete'] = count($complete);
		$show['cancel'] = count($cancel);
		$show['total'] = count($data);
      	return view('monitoring.monitoringFA.v_index_monitoringFA',$show)->with('ip', $clientIP);
    }
    
    public function getData(Request $request)
    {
        $clientIP = \Request::getClientIp(true);
		if($request->startDateMonitoring == '' or $request->endDateMonitoring == ''){
			return redirect()->route('monitoring.monitoringFA.index');
		}else {
			$user = Session::get('userinfo');
			$start_date = Carbon::createFromFormat('d-m-Y', $request->startDateMonitoring)->format('Y-m-d');
			$end_date = Carbon::createFromFormat('d-m-Y', $request->endDateMonitoring)->format('Y-m-d');

			//total row
			$total = DB::table('fa_transaction as a')
                ->whereNotIn('a.assign_status', ['Open'])
				->join('customer as b', 'a.fa_transaction_id', '=', 'b.fa_transaction_id')
				->join('fa_type as c', 'a.fa_type_cd', '=', 'c.fa_type_cd')
				->leftJoin('fa_type_lang as d', 'c.id', '=', 'd.fa_type_cd')
				->leftJoin('workers as e', 'a.worker_id', '=', 'e.id')
				->leftJoin('dispatch_group_master as f', 'a.dispatch_group', '=', 'f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->whereBetween('a.assign_date', [$start_date, $end_date])
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer', 'b.customer_name as customer_name', 'b.phone as phone',
					'b.address as address', 'b.pcezbk as pcezbk', 'b.email as email', 'b.city as city', 'b.status as status_customer',
					'b.postal as postal', 'd.descr', 'e.id as id_worker', 'e.worker_name', 'f.description', 'g.group_access_id'
				)->orderBy('a.updated_at', 'desc')->get();

			//result search
			$show['data'] = DB::table('fa_transaction as a')
                ->whereNotIn('a.assign_status', ['Open'])
				->join('customer as b', 'a.fa_transaction_id', '=', 'b.fa_transaction_id')
				->join('fa_type as c', 'a.fa_type_cd', '=', 'c.fa_type_cd')
				->leftJoin('fa_type_lang as d', 'c.id', '=', 'd.fa_type_cd')
				->leftJoin('workers as e', 'a.worker_id', '=', 'e.id')
				->leftJoin('dispatch_group_master as f', 'a.dispatch_group', '=', 'f.dispatch_group')
				->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
				->whereBetween('a.assign_date', [$start_date, $end_date])
				->where('g.group_access_id', '=', $user['group_access_id'])
				->select(
					'a.*', 'b.id as id_customer', 'b.customer_name as customer_name', 'b.phone as phone',
					'b.address as address', 'b.pcezbk as pcezbk', 'b.email as email', 'b.city as city', 'b.status as status_customer',
					'b.postal as postal', 'd.descr', 'e.id as id_worker', 'e.worker_name', 'f.description', 'g.group_access_id'
				)->orderBy('a.updated_at', 'desc')->paginate(300);

			$limit = 300;//limit untuk continue page number
			$show['num'] = num_row($request->input('page'), $limit);
			$show['cariDari'] = $start_date;
			$show['cariSmp'] = $end_date;
			$show['total'] = count($total);
			$show['ket'] = 'Search Date';
			return view('monitoring.monitoringFA.v_search_monitoringFA', $show)->with('ip', $clientIP);
		}
    }

    public function detailNomen($fa_transaction_id){
        $data = Customer::where('fa_transaction_id','=', $fa_transaction_id)->first();
        return response()->json([
            'status' => '200',
            'alert' => 'success',
            'title' => 'Success!',
            'description' => 'Get data nomen success',
            'success' => true,
            'data' => $data
        ]);
    }

    public function getDataBydate(Request $request)
    {
        if($request->is_date_search == 'no')
        {
            $data = FA_transaction::with('customer')
                ->with('fa_type.fa_type_lang')
                ->get();
        }else{
            $start_date = Carbon::createFromFormat('d-m-Y', $request->start_date)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->end_date)->format('Y-m-d');
            $data = FA_transaction::with('customer')
                ->with('fa_type.fa_type_lang')
                ->with('worker')
                ->where('assign_date', '>=', $start_date)
                ->where('assign_date', '<=', $end_date)
                ->get();
        }
        return Datatables::of($data)
            ->editColumn('descr', function($data) {
                return $data->fa_type->fa_type_lang->descr;
            })
            ->editColumn('assign_date', function($data) {
                $assign_date = ($data->assign_date==null) ? "" : date('d-m-Y', strtotime($data->assign_date));
                return  $assign_date;
            })
            ->editColumn('created_at', function($data) {
                $created_at = ($data->created_at==null) ? "" : date('d-m-Y H:i:s', strtotime($data->created_at));
                return  $created_at;
            })
            ->make(true);

    }

    public function getDetail($fa_transaction_id){
        $data = FA_transaction::where('fa_transaction_id','=',$fa_transaction_id)
            ->with('customer')
            ->with('worker')
            //->with('fa_type.fa_type_lang')
            ->get();
        return Datatables::of($data)
            // ->editColumn('descr', function($data) {
            //     return $data->fa_type->fa_type_lang->descr;
            // })
            ->editColumn('assign_date', function($data) {
                $assign_date = ($data->assign_date==null) ? "" : date('d-m-Y', strtotime($data->assign_date));
                return  $assign_date;
            })
            ->make(true);
    }

    public function getCharacteristic($fa_transaction_id){
        $data = FA_Transaction_characteristic::where('fa_transaction_id','=',$fa_transaction_id)
            ->whereNotIn('char_type_cd',['CASE','CMFALOCK'])
            ->with('transaction')
            ->get();
        return Datatables::of($data)->make(true);
    }

    public function getMaterialUsage($fa_transaction_id){
        $data = FA_Transaction_material_usage::where('fa_transaction_id','=',$fa_transaction_id)
            ->with('transaction')
            ->get();
        return Datatables::of($data)->make(true);
    }

    public function getTTD($fa_transaction_id){
        $data = FA_Transaction_ttd::where('fa_transaction_id', '=', $fa_transaction_id)
            ->with('transaction')
            ->get();
        return Datatables::of($data)->make(true);
    }

    public function getphotofa($fa_transaction_id){
        $data = FA_Transaction_photo::where('fa_transaction_id', '=', $fa_transaction_id)
            ->with('transaction')
            ->get();
        return Datatables::of($data)->make(true);
    }

    public function downloadPdf($fa_transaction_id){
        $data['list_job'] = FA_transaction::where('fa_transaction_id','=',$fa_transaction_id)
            ->with('customer')
            ->with('worker')
            ->with('fa_type.fa_type_lang')
            ->get();

        $data['karakteristik'] = FA_Transaction_characteristic::where('fa_transaction_id','=',$fa_transaction_id)
            ->with('transaction')
            ->get();

        $data['material'] = FA_Transaction_material_usage::where('fa_transaction_id','=',$fa_transaction_id)
            ->with('transaction')
            ->get();

        $data['photo'] = FA_Transaction_photo::where('fa_transaction_id', '=', $fa_transaction_id)
            ->with('transaction')
            ->get();

        $data['ttd'] = FA_Transaction_ttd::where('fa_transaction_id', '=', $fa_transaction_id)
            ->with('transaction')
            ->get();
        // echo json_encode($data['ttd']);
        // exit();
        
        $pdf = PDF::setOptions([
                'images' => true
            ])->loadView('monitoring.monitoringFA.v_generate_pdf', $data);
  
        return $pdf->download('result'.time().'.pdf');
        // return $pdf->download('result.pdf');
    }

    public function editCharacteristicById($id){
        $data = FA_Transaction_characteristic::where('id','=',$id)->first();
        return response()->json(['data'=>$data,'status'=>'success']);
    }

    public function updateCharacteristic(Request $request, $id){
        $data = FA_Transaction_characteristic::find($id);
        $data->value_characteristic = $request->value_characteristic;

        if($data->save())
        {
            return response()->json(['status'=>'success']);    
        }
    }

    public function sendBackToSimpel($fa_transaction_id){
        $data = FA_Transaction_characteristic::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->whereNotIn('char_type_cd', ['CASE','CMFALOCK'])
                                            ->get();

        $fa = FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)->first();

        $fa_type_cd = $fa->fa_type_cd;

        // print_r($fa_type_cd);
        // echo json_encode($data);
        // exit();

        if ($data == "[]") { 
            return response()->json(['status'=>'gagal', 'pesan'=>'Characteristic Belum Di Kirim Dari Android Worker']);

        }else{

            $xmlrequest = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:c1c="http://splwg.com/C1CustomerContactMaintenance.xsd">
                   <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                    <wsse:Security>
                        <wsse:UsernameToken>       
                            <wsse:Username>SIMPEL2</wsse:Username>
                            <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>        
                        </wsse:UsernameToken>    
                    </wsse:Security>  
                   </SOAP-ENV:Header>
                    <SOAP-ENV:Body>
                        <C1FieldActivityMaintenance dateTimeTagFormat="CdxDateTime"  transactionType ="UPDATE" >
                            <C1FieldActivityMaintenanceService>
                                <C1FieldActivityMaintenanceHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    
                                /><C1FieldActivityMaintenanceDetails 
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    ActivityType="'.$fa_type_cd.'"  
                                    > 
                                    <FASteps>
                                        <FAStepsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />  
                                    </FASteps>
                                    <FACharacteristics>
                                        <FACharacteristicsHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            />';
                            foreach ($data as $key => $value) {
                            $xmlrequest .= '<FACharacteristicsRow
                                        rowAction="Change" 
                                        FieldActivityID="'.$fa_transaction_id.'" 
                                        CharacteristicType="'.trim($value->char_type_cd).'" 
                                        Sequence="'.trim($value->sequence).'"
                                        SearchCharacteristicValue="'.trim($value->value_characteristic).'" 
                                        ';
                                        if (trim($value->char_type_flg) == 'ADV') {
                                            $xmlrequest .='AdhocCharacteristicValue="'.trim($value->value_characteristic).'">
                                            </FACharacteristicsRow>';
                                        }else{
                                            $xmlrequest .='CharacteristicValue="'.trim($value->value_characteristic).'">
                                            </FACharacteristicsRow>';
                                        }
                            }                                 
                            $xmlrequest .=  '</FACharacteristics>
                            <FaRem>
                                <FaRemHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            </FaRem>
                            <FaLog>
                                <FaLogHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            </FaLog>
                                </C1FieldActivityMaintenanceDetails>
                            </C1FieldActivityMaintenanceService>
                        </C1FieldActivityMaintenance>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>';

                // print_r($xmlrequest);
                // exit();

            $action = 'C1FieldActivityMaintenance';
            //$url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
            $url = 'http://172.27.1.38:8280/services/PS_Field_Activity?wsdl';
            $client = new Client(); // Create the Guzzle Client        
            $response = $client->post($url, [         
                            'headers' => [           
                                'SOAPAction' => $action, // Add the action              
                                'Content-Type' => 'text/xml'          
                            ],          
                            'body' => $xmlrequest // Put the xml in the body        
                        ]);

            if ($response) {
                //return $response->getBody()->getContents();
                $res = $response->getBody()->getContents();
                $doc = new \DOMDocument();
                $doc->loadXML($res);
                $status = $doc->getElementsByTagName('faultcode')->length;
                // print_r($status);
                // print_r($res);
                // exit();
                if ($status == 1) {
                    $err = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                    // $exp = explode(':', $err);
                    // $r_exp = $exp[7];

                    //save log status fa
                    $log = new Log_fa_status;
                    $log->fa_transaction_id= $fa_transaction_id;
                    $log->status = 'Fail Send Characteristic';
                    $log->message = $err;
                    $log->save();

                    FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                        ->update(['status_char_simpel'=>'Gagal']);

                    return response()->json(['status'=>'gagal', 'pesan'=>$err]);
                }else{
                    FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                        ->update(['status_char_simpel'=>'Success']);

                    //save log status fa
                    $log = new Log_fa_status;
                    $log->fa_transaction_id= $fa_transaction_id;
                    $log->status = 'Send Characteristic';
                    $log->message = 'Success';
                    $log->save();

                    $data = Workers::where('id', '=', $fa->worker_id)->first();

                    $deskrip = 'Has been completed by '.$data->worker_id;
                    $worker_id = $data->worker_id;
                    $date = date("Y-m-d h:m:i");

                    $xmlclosefa = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
                        xmlns:c1f="http://splwg.com/C1FACompletionExtSysStruct.xsd">
                            <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                                <wsse:Security>
                                    <wsse:UsernameToken>
                                        <wsse:Username>SIMPEL2</wsse:Username>
                                        <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>
                                    </wsse:UsernameToken>
                                </wsse:Security>       
                            </SOAP-ENV:Header>
                            <SOAP-ENV:Body>
                                <C1FACompletionExtSysStruct dateTimeTagFormat="CdxDateTime"  transactionType ="ADD" >
                                    <C1FACompletionExtSysStructService>
                                        <C1FACompletionExtSysStructHeader
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                                />
                                        <C1FACompletionExtSysStructDetails 
                                            UploadFieldActivityID="'.$fa_transaction_id.'" 
                                            FieldActivityID="'.$fa_transaction_id.'" 
                                            Workedby="'.$worker_id.'" 
                                            WorkDateTime="'.$date.'" 
                                            LongDescription="'.$deskrip.'" 
                                        > 
                                        <FAUploadCharacteristics>
                                            <FAUploadCharacteristicsHeader
                                                FieldActivityID="'.$fa_transaction_id.'" 
                                                />
                                        </FAUploadCharacteristics>
                                        <FAUploadRemarks>
                                            <FAUploadRemarksHeader
                                                FieldActivityID="'.$fa_transaction_id.'" 
                                                />
                                            </FAUploadRemarks>
                                        <MeterInfo>       
                                        </MeterInfo>
                                        <ItemInfo>     
                                        </ItemInfo>
                                    </C1FACompletionExtSysStructDetails>
                                </C1FACompletionExtSysStructService>
                            </C1FACompletionExtSysStruct>
                        </SOAP-ENV:Body>
                    </SOAP-ENV:Envelope>';

                    $action = 'C1FACompletionExtSysStruct';
                    $url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
                    $client = new Client(); // Create the Guzzle Client        
                    $response = $client->post($url, [         
                            'headers' => [           
                                'SOAPAction' => $action, // Add the action              
                                'Content-Type' => 'text/xml'          
                            ],          
                            'body' => $xmlclosefa // Put the xml in the body        
                    ]); 

                    if ($response) {
                        $res = $response->getBody()->getContents();
                        $doc = new \DOMDocument();
                        $doc->loadXML($res);
                        $statusComp = $doc->getElementsByTagName('faultcode')->length;
                        // print_r($res);
                        // exit();
                         if ($statusComp == 1) {
                            $err = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                            // $exp = explode(':', $err);
                            // $r_exp = $exp[7];

                            //save log status fa
                            $log = new Log_fa_status;
                            $log->fa_transaction_id= $fa_transaction_id;
                            $log->status = 'Fail Completion FA';
                            $log->message = $err;
                            $log->save();
                            
                            FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->update(['status_complation_fa'=>'Gagal']);

                            return response()->json(['status'=>'gagal', 'pesan'=>$err]);
                        }else{
                            FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->update(['status_complation_fa'=>'Success','assign_status'=>'Complete', 'status'=>'C']);

                            //save log status fa
                            $log = new Log_fa_status;
                            $log->fa_transaction_id = $fa_transaction_id;
                            $log->status = 'Completion FA';
                            $log->message = 'Success';
                            $log->save();

                            return response()->json(['status'=>'success', 'message'=>'Send To Simpel Berhasil']);
                        }
                    }else{
                        return response()->json(['status'=>'gagal', 'message'=>'Koneksi Ke Simpel Gagal']);
                    }
                }
            }else{
                return response()->json(['status'=>'error']);
            } 
        }  
    }

    public function sendCompBackToSimpel($fa_transaction_id){
        $data = FA_Transaction_characteristic::where('fa_transaction_id', '=', $fa_transaction_id)
                                            ->where('char_type_cd', '=', 'CMREP')
                                            ->first();

        $deskrip = 'Has been completed by '.$data->value_characteristic;
        $worker_id = $data->value_characteristic;
        $date = date("Y-m-d h:m:i");

        $xmlclosefa = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
            xmlns:c1f="http://splwg.com/C1FACompletionExtSysStruct.xsd">
                <SOAP-ENV:Header xmlns:wsse="http://www.w3.org/2001/XMLSchema-instance">
                    <wsse:Security>
                        <wsse:UsernameToken>
                            <wsse:Username>SIMPEL2</wsse:Username>
                            <wsse:Password Type="PasswordText">Ext3ntion</wsse:Password>
                        </wsse:UsernameToken>
                    </wsse:Security>       
                </SOAP-ENV:Header>
                <SOAP-ENV:Body>
                    <C1FACompletionExtSysStruct dateTimeTagFormat="CdxDateTime"  transactionType ="ADD" >
                        <C1FACompletionExtSysStructService>
                            <C1FACompletionExtSysStructHeader
                                FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            <C1FACompletionExtSysStructDetails 
                                UploadFieldActivityID="'.$fa_transaction_id.'" 
                                FieldActivityID="'.$fa_transaction_id.'" 
                                Workedby="'.$worker_id.'" 
                                WorkDateTime="'.$date.'" 
                                LongDescription="'.$deskrip.'" 
                            > 
                            <FAUploadCharacteristics>
                                <FAUploadCharacteristicsHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                            </FAUploadCharacteristics>
                            <FAUploadRemarks>
                                <FAUploadRemarksHeader
                                    FieldActivityID="'.$fa_transaction_id.'" 
                                    />
                                </FAUploadRemarks>
                            <MeterInfo>       
                            </MeterInfo>
                            <ItemInfo>     
                            </ItemInfo>
                        </C1FACompletionExtSysStructDetails>
                    </C1FACompletionExtSysStructService>
                </C1FACompletionExtSysStruct>
            </SOAP-ENV:Body>
        </SOAP-ENV:Envelope>';

        $action = 'C1FACompletionExtSysStruct';
        $url = 'http://172.27.1.38:8280/services/PS_FA_Completion?wsdl';
        $client = new Client(); // Create the Guzzle Client        
        $response = $client->post($url, [         
                'headers' => [           
                    'SOAPAction' => $action, // Add the action              
                    'Content-Type' => 'text/xml'          
                ],          
                'body' => $xmlclosefa // Put the xml in the body        
        ]); 

        if ($response) {
            $res = $response->getBody()->getContents();
            $doc = new \DOMDocument();
            $doc->loadXML($res);
            $statusComp = $doc->getElementsByTagName('faultcode')->length;
            // print_r($res);
            // exit();
             if ($statusComp == 1) {
                $err = $doc->getElementsByTagName('ResponseText')[0]->nodeValue;
                $exp = explode(':', $err);
                $r_exp = $exp[7];
                
                FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                ->update(['status_complation_fa'=>'Gagal']);

                return response()->json(['status'=>'gagal', 'pesan'=>$r_exp]);
            }else{
                 FA_transaction::where('fa_transaction_id', '=', $fa_transaction_id)
                                ->update(['status_complation_fa'=>'Success']);

                return response()->json(['status'=>'success', 'message'=>'Completion Berhasil']);
            }
        }else{
            return response()->json(['status'=>'gagal', 'message'=>'Koneksi Ke Simpel Gagal']);
        }
    }

    public function get_history($fa_id)
    {
        return Excel::download(new GetHistoryFA($fa_id), 'history-'.$fa_id.'.xlsx');
    }

	public function search(Request $request){
		$user = Session::get('userinfo');
        $clientIP = \Request::getClientIp(true); //for relative ip
		//get data search
		$group = $user['group_access_id'];
		//total row
		$total = DB::table('fa_transaction as a')
            ->whereNotIn('a.assign_status', ['Open'])
			->distinct('g.group_access_id')
			->select(
				'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
				'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
				'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
			)
			->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
			->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
			->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
			->leftJoin('workers as e','a.worker_id','=','e.id')
			->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
			->join('fa_group_access as g', function($q) use ($group)
			{
				$q->on('a.dispatch_group', '=', 'g.dispatch_group')
					->where('g.group_access_id', '=', "$group");
			})
			->where('a.nomen','LIKE','%'.$request->search."%")
			->orWhere('a.fa_transaction_id','LIKE','%'.$request->search."%")
			->orWhere('a.dispatch_group','LIKE','%'.$request->search."%")
			->orWhere('a.fa_type_cd','LIKE','%'.$request->search."%")
			->orWhere('a.assign_status','LIKE','%'.$request->search."%")
			->orWhere('a.priority_status','LIKE','%'.$request->search."%")
			->orWhere('a.status_char_simpel','LIKE','%'.$request->search."%")
			->orWhere('a.status_wms','LIKE','%'.$request->search."%")
			->orWhere('a.status_complation_fa','LIKE','%'.$request->search."%")
			->orWhere('b.pcezbk','LIKE','%'.$request->search."%")
			->orWhere('e.worker_name','LIKE','%'.$request->search."%")
			->orWhere('f.description','LIKE','%'.$request->search."%")
			->orderBy('a.updated_at', 'desc')->get();

		//result search
		$show['data'] = DB::table('fa_transaction as a')
            ->whereNotIn('a.assign_status', ['Open'])
			->distinct('g.group_access_id')
			->select(
				'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
				'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
				'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
			)
			->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
			->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
			->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
			->leftJoin('workers as e','a.worker_id','=','e.id')
			->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
			->join('fa_group_access as g', function($q) use ($group)
				{
					$q->on('a.dispatch_group', '=', 'g.dispatch_group')
						->where('g.group_access_id', '=', "$group");
				})
			->where('a.nomen','LIKE','%'.$request->search."%")
			->orWhere('a.fa_transaction_id','LIKE','%'.$request->search."%")
			->orWhere('a.dispatch_group','LIKE','%'.$request->search."%")
			->orWhere('a.fa_type_cd','LIKE','%'.$request->search."%")
			->orWhere('a.assign_status','LIKE','%'.$request->search."%")
			->orWhere('a.priority_status','LIKE','%'.$request->search."%")
			->orWhere('a.status_char_simpel','LIKE','%'.$request->search."%")
			->orWhere('a.status_wms','LIKE','%'.$request->search."%")
			->orWhere('a.status_complation_fa','LIKE','%'.$request->search."%")
			->orWhere('b.pcezbk','LIKE','%'.$request->search."%")
			->orWhere('e.worker_name','LIKE','%'.$request->search."%")
			->orWhere('f.description','LIKE','%'.$request->search."%")
			->orderBy('a.updated_at', 'desc')
			->paginate(300);

		$limit = 300;//limit untuk continue page number
		$show['num'] = num_row($request->input('page'), $limit);
		$show['total'] = count($total);
		$show['cari'] = $request->search;
		$show['ket'] = 'Search Data';
		return view('monitoring.monitoringFA.v_search_monitoringFA', $show)->with('ip', $clientIP);
	}

    public function filterMonitoringFA(Request $request){
        $clientIP = \Request::getClientIp(true); //for relative ip
        $fa_type_cd = trim($request->fa_type_cd);
        $pc_ez_bk = trim($request->pc_ez_bk);
        $assign_status = trim($request->assign_status);
        $user = Session::get('userinfo');

        if ($fa_type_cd == null && $pc_ez_bk == null && $assign_status == null) {
            return redirect()->route('monitoring.monitoringFA.index');

        }else if ($request->fa_type && $request->pcezbk && $request->status) {
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('a.assign_status', '=', $assign_status)
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('a.assign_status', '=', $assign_status)
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['fa_type_cd'] = $fa_type_cd;
            $show['pc_ez_bk'] = $pc_ez_bk;
            $show['status'] = $assign_status;
            $show['ket'] = 'Search All';

        }else if ($request->fa_type && $request->pcezbk) {
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['ket'] = 'Search Duo';

        }else if ($request->fa_type && $request->status) {
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('a.assign_status', '=', $assign_status)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('a.assign_status', '=', $assign_status)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['ket'] = 'Search Duo';

        }
        else if ($request->fa_type) {       
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.fa_type_cd', '=', $fa_type_cd)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['FA_Type'] = $fa_type_cd;
            $show['ket'] = 'Search Fa Type';
            
        }else if ($request->pcezbk) {
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            // $query = '
            // SELECT * FROM customer a
            // CROSS APPLY (
            //  SELECT pcezbk, LEFT(pcezbk,2) AS col
            // ) b
            // LEFT JOIN fa_transaction AS c ON a.fa_transaction_id = c.fa_transaction_id
            // LEFT JOIN fa_type AS d ON c.fa_type_cd = d.fa_type_cd
            // LEFT JOIN fa_type_lang AS e ON d.id = e.fa_type_cd
            // WHERE b.col = 09';
            // $tes = DB::select($query);
            // echo json_encode($tes);
            // echo json_encode(count($tes));
            // exit();

            // SELECT * FROM dbo.customer a
            // CROSS APPLY (
            //  SELECT pcezbk, LEFT(pcezbk,2) AS col
            // ) b
            // WHERE b.col = 09s

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('b.pcezbk','LIKE', $pc_ez_bk.'%')
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['PC_EZ_BK'] = $pc_ez_bk;
            $show['ket'] = 'Search PC_EZ_BK';

        }else if ($request->status) {
            //GET WORKER
            $show['workers'] = DB::table('workers as a')
                ->join('worker_dispatch as b','a.id','=','b.worker_id')
                ->join('fa_group_access as c', 'b.dispatch_group', '=', 'c.dispatch_group')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();

            $show['getWorkers'] = DB::table('worker_dispatch as a')
                ->leftjoin('workers as b','a.worker_id','=','b.id')
                ->join('fa_group_access as c', 'a.dispatch_group', '=', 'c.dispatch_group')
                ->distinct('a.worker_id')
                ->select('a.worker_id', 'b.worker_name')
                ->where('worker_status', '=', 'Active')
                ->where('c.group_access_id', '=', $user['group_access_id'])
                ->get();
            // END GET WORKER

            //total row
            $total = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.assign_status', '=', $assign_status)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )->get();

            //result search
            $show['data'] = DB::table('fa_transaction as a')
                ->join('customer as b','a.fa_transaction_id','=','b.fa_transaction_id')
                ->join('fa_type as c','a.fa_type_cd','=','c.fa_type_cd')
                ->leftJoin('fa_type_lang as d','c.id','=','d.fa_type_cd')
                ->leftJoin('workers as e','a.worker_id','=','e.id')
                ->leftJoin('dispatch_group_master as f','a.dispatch_group','=','f.dispatch_group')
                ->join('fa_group_access as g', 'a.dispatch_group', '=', 'g.dispatch_group')
                ->where('a.assign_status', '=', $assign_status)
                ->where('g.group_access_id', '=', $user['group_access_id'])
                ->select(
                    'a.*', 'b.id as id_customer','b.customer_name as customer_name','b.phone as phone',
                    'b.address as address','b.pcezbk as pcezbk','b.email as email','b.city as city','b.status as status_customer',
                    'b.postal as postal', 'd.descr','e.id as id_worker','e.worker_name','f.description','g.group_access_id'
                )
                ->orderBy('a.updated_at', 'desc')
                ->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['Status'] = $assign_status;
            $show['ket'] = 'Search Status';

        }
        return view('monitoring.monitoringFA.v_search_monitoringFA', $show)->with('ip', $clientIP);
    }
}