<?php

namespace App\Http\Controllers\Monitoring;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Yajra\Datatables\Datatables;
use App\Models\Transaction\Survey_Transaction_Characteristic;
use App\Models\Transaction\AssignmentSurvey;
use App\Models\Administrator\Survey;
use App\Models\Transaction\Survey_Transaction_Photo;
use App\Models\Transaction\Survey_Transaction;
use Carbon\Carbon;
use Validator;

class MonitoringSurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('survey_transaction as a')
        ->join('survey as b','a.survey_id','b.id')
        ->join('workers as c','c.id','a.worker_id')
        ->leftjoin('code_ab as d','d.id','a.AB_id')
        ->leftjoin('code_pc as e','e.id','a.PC_id')
        ->leftjoin('code_ez as f','f.id','a.EZ_id')
        // ->join('code_bk as g','g.id','a.BK_id')
        ->whereNotIn('a.survey_status',['Assigned'])
        ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
            'a.survey_code','a.survey_date',
            'c.worker_name','a.sending_status','a.survey_status','d.code_ab','e.code_pc','f.code_ez','a.worker_id','a.nomen','a.asset')
        ->get();

        $show['list_data'] = DB::table('survey_transaction as a')
            ->join('survey as b','a.survey_id','b.id')
            ->join('workers as c','c.id','a.worker_id')
            ->leftjoin('code_ab as d','d.id','a.AB_id')
            ->leftjoin('code_pc as e','e.id','a.PC_id')
            ->leftjoin('code_ez as f','f.id','a.EZ_id')
            ->whereNotIn('a.survey_status',['Assigned'])
           ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
            'a.survey_code','a.survey_date',
            'c.worker_name','a.sending_status','a.survey_status','d.code_ab','e.code_pc','f.code_ez','a.worker_id','a.nomen','a.asset')
            ->paginate(300);

        $limit = 300;//limit untuk continue page number
        $show['num'] = num_row($request->input('page'), $limit);
        $show['total'] = count($data);
        return view('monitoring.monitoringSurvey.v_index_monitoringSurvey', $show);
    }

    public function getData()
    {
        $data = DB::table('survey_transaction as a')
        ->join('survey as b','a.survey_id','b.id')
        ->join('workers as c','c.id','a.worker_id')
        ->join('code_ab as d','d.id','a.AB_id')
        ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
            'a.survey_code','a.survey_date',
            'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.EZ_id','d.code_ab')
        ->get();
        

         //$data = AssignmentSurvey::all();
         return Datatables::of($data)
         ->make(true);
    }

    public function getCharacteristic($survey_code){
        $data = Survey_Transaction_Characteristic::where('survey_code', $survey_code)->get();
        return Datatables::of($data)->make(true);
    }

    public function getPhotoSurvey($survey_code){
        $data = Survey_Transaction_Photo::where('survey_code', $survey_code)->get();
        return Datatables::of($data)->make(true);
    }

public function getDataBydate(Request $request)
    {
        if($request->startDateMonitoring == null or $request->endDateMonitoring == null){
            return redirect()->route('monitoring.monitoringSurvey.index');
        }else{
            $start_date = Carbon::createFromFormat('d-m-Y', $request->startDateMonitoring)->format('Y-m-d');
            $end_date = Carbon::createFromFormat('d-m-Y', $request->endDateMonitoring)->format('Y-m-d');

            $total = DB::table('survey_transaction as a')
            ->whereNotIn('a.survey_status',['Assigned'])
            ->join('survey as b','a.survey_id','b.id')
            ->join('workers as c','c.id','a.worker_id')
            ->whereDate('a.start_date', '>=', $start_date)
            ->WhereDate('a.end_date',   '<=', $end_date)
            ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                'a.survey_code','a.survey_date',
                'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
            ->orderBy('a.start_date', 'desc')->get();

            //result search
            $show['list_data'] = DB::table('survey_transaction as a')
                ->whereNotIn('a.survey_status',['Assigned'])
                ->join('survey as b','a.survey_id','b.id')
                ->join('workers as c','c.id','a.worker_id')
                ->whereDate('a.start_date', '>=', $start_date)
                ->WhereDate('a.end_date',   '<=', $end_date)
                ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                    'a.survey_code','a.survey_date',
                    'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
                ->orderBy('a.start_date', 'desc')->paginate(300);

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['cariDari'] = $start_date;
            $show['cariSmp'] = $end_date;
            $show['total'] = count($total);
            $show['ket'] = 'Search Date';

            return view('monitoring.monitoringSurvey.v_search_monitoringSurvey', $show);
        }
    }

    public function getDataByStatus(Request $request){
        $validator = Validator::make($request->all(),[
                'status_filter'=>'required'
        ]);

        if($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $survey_status = $request->status_filter;

        $data['list_data'] = AssignmentSurvey::whereIn('survey_status', $survey_status)->get();
        // echo json_encode($data);
        // exit();
        return response()->json(['data'=>$data]);
        // return Datatables::of($data)
        //                 ->make(true);
        //return view('monitoring.monitoringSurvey.v_index_monitoringSurvey', $data);
    }

    public function viewAB($ab_id){
        $data['detail_ab'] = DB::table('code_ab')
                ->where('id', $ab_id)
                ->get();

        return view('monitoring.monitoringSurvey.view_detail_ab', $data);
    }

    public function viewPC($id){
        $data['detail_pc'] = DB::table('survey_transaction as a')
                ->where('a.id', $id)
                ->join('code_pc as b','a.PC_id','b.id')
                ->join('code_ez as c','a.EZ_id','c.id')
                ->select('b.code_pc','c.code_ez')
                ->get();

        return view('monitoring.monitoringSurvey.view_detail_pc', $data);
    }

    public function filterMonitoringSurvey(Request $request){
        $survey_status = $request->status_filter;

        if($survey_status == null){
            return redirect()->route('monitoring.monitoringSurvey.index');
        }else{

            $total = DB::table('survey_transaction as a')
            ->join('survey as b','a.survey_id','b.id')
            ->join('workers as c','c.id','a.worker_id')
            ->whereIn('a.survey_status', $survey_status)
            ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                'a.survey_code','a.survey_date',
                'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
            ->orderBy('a.start_date', 'desc')->get();

            //result search
            $show['list_data'] = DB::table('survey_transaction as a')
                ->join('survey as b','a.survey_id','b.id')
                ->join('workers as c','c.id','a.worker_id')
                ->whereIn('a.survey_status', $survey_status)
                ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                    'a.survey_code','a.survey_date',
                    'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
                ->orderBy('a.start_date', 'desc')->paginate(300);

            // echo json_encode($total);
            // echo json_encode($show['list_data']);
            // exit();

            $limit = 300;//limit untuk continue page number
            $show['num'] = num_row($request->input('page'), $limit);
            $show['total'] = count($total);
            $show['ket'] = 'Search Status';

            return view('monitoring.monitoringSurvey.v_search_monitoringSurvey', $show);
        }
    }

    public function search(Request $request){

        $total = DB::table('survey_transaction as a')
            ->join('survey as b','a.survey_id','b.id')
            ->join('workers as c','c.id','a.worker_id')
            ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                'a.survey_code','a.survey_date',
                'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
            ->where('a.survey_code','LIKE','%'.$request->search."%")
            ->orWhere('a.nomen','LIKE','%'.$request->search."%")
            ->orWhere('a.asset','LIKE','%'.$request->search."%")
            ->orWhere('a.survey_status','LIKE','%'.$request->search."%")
            ->orderBy('a.updated_at', 'desc')->get();

        //result search
        $show['list_data'] =  DB::table('survey_transaction as a')
            ->join('survey as b','a.survey_id','b.id')
            ->join('workers as c','c.id','a.worker_id')
            ->select('a.start_date','a.end_date','b.survey_name','a.id as id',
                'a.survey_code','a.survey_date',
                'c.worker_name','a.sending_status','a.survey_status','a.AB_id','a.PC_id','a.EZ_id','a.worker_id','a.nomen','a.asset')
            ->where('a.survey_code','LIKE','%'.$request->search."%")
            ->orWhere('a.nomen','LIKE','%'.$request->search."%")
            ->orWhere('a.asset','LIKE','%'.$request->search."%")
            ->orWhere('a.survey_status','LIKE','%'.$request->search."%")
            ->orderBy('a.updated_at', 'desc')
            ->paginate(300);

        $limit = 300;//limit untuk continue page number
        $show['num'] = num_row($request->input('page'), $limit);
        $show['total'] = count($total);
        $show['cari'] = $request->search;
        $show['ket'] = 'Search Data';

        return view('monitoring.monitoringSurvey.v_search_monitoringSurvey', $show);
    }

    public function mapSurvey($survey_code){
        $data = Survey_Transaction::where('survey_code', $survey_code)
                                    ->get();
        return response()->json($data);
    }
}
