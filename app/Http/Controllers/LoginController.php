<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Administrator\Users;
use App\Models\Administrator\Admin;
use Illuminate\Support\Facades\Hash;
use Validator;
use Session;
use DB;


Class LoginController extends Controller
{

	function index(Request $request)
	{

		$user = DB::table('users as a')
						->join('admin as b','a.id','=','b.user_id')
						->join('role as c','c.id','=','b.role_id')
						->join('fa_group_access as d', 'b.group_access_id',  '=', 'd.group_access_id')
						->select('a.id as user_id','c.id as role_id','c.role_name','a.password','b.id as admin_id','a.username', 'd.group_access_id', 'd.dispatch_group')
						->where('a.username',$request->input('username'))
						->where('b.admin_status', '=', 'Active')
						->first();

		$validator = Validator::make($request->all(),[
			'username'=>'required',
			'password'=>'required'
		]);
		$gagal =  response()->json(['errors'=>[' Username atau Password salah ']]);	
		if($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		
		if(!$user)
		{
			return response()->json(['errors'=>['Username Tidak Terdaftar Silahkan Hubungi Admin']]);
		}

		$accessModule =	DB::table('users as a')
										->join('admin as b','a.id','=','b.user_id')
										->join('role as c','c.id','=','b.role_id')
										->join('access_modules as d','d.admin_id','=','b.id')
										->join('modules as e','e.id','=','d.module_id')
										->select('d.*','e.*')
										->where('a.id',$user->user_id)
										->get();

		if($user)
		{
			$password =  Hash::check($request->password,$user->password);
			if($password)
			{
				$dataUser['user_id'] = $user->user_id;
				$dataUser['admin_id'] = $user->admin_id;
				$dataUser['role_id'] = $user->role_id;
				$dataUser['role_name'] = $user->role_name;
				$dataUser['username'] = $user->username;
				$dataUser['group_access_id'] = $user->group_access_id;
				$dataUser['dispatch_group'] = $user->dispatch_group;
				$role =[];
				$role[$user->user_id]['home/']  = 'home/'; //awalnya dashboard
				foreach($accessModule as $module)
				{
					$role[$user->user_id][$module->moduleUrl]  = $module->moduleUrl;
 				}

				Session::put('userinfo',$dataUser);
				Session::put('accessModule',$role);
				return response()->json(['success'=>'success']);	
			}
			else
			{
				return $gagal;
			}
		}
		else
		{
			return $gagal;
		}
	}

	function logout(Request $request)
	{
		$request->session()->flush();
        return redirect('/');
	}
}