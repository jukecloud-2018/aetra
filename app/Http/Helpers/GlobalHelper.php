<?php


function getFACharacter($faType=null)
{
return  /*$data = \DB::table('fa_mapping_char_type as a')
        ->join('fa_char_type as b','a.char_type_cd','=','b.char_type_cd')
        ->join('fa_type as c','c.fa_type_cd','=','a.fa_type_cd')
        ->leftJoin('fa_char_value as e','b.char_type_cd','=','e.char_type_cd')
        ->leftJoin('fa_alg_validation as f','b.adhoc_val_alg_cd','=','f.alg_cd')
        ->select('a.fa_type_cd','c.descr','a.char_type_cd','b.descr as charac_desc','a.required_sw','b.char_type_flg','e.char_val','b.adhoc_val_alg_cd','f.descr50 as alg_desc')
        ->where('a.fa_type_cd',$faType)
        ->groupBy('a.fa_type_cd','a.fa_type_cd','c.descr','a.char_type_cd','b.descr', 'a.required_sw','b.char_type_flg','e.char_val','b.adhoc_val_alg_cd','f.descr50')
        ->orderBy('a.fa_type_cd','asc')
        ->get();*/

    $data = \DB::table ('fa_mapping_char_type as a') //ci_chty_faty
        ->select('a.fa_type_cd','a.sort_seq','a.char_type_cd',
                'e.descr','c.descr as charac_desc','a.required_sw','b.char_type_flg',
                'g.char_val','g.descr as description','b.adhoc_val_alg_cd','h.descr50 as alg_desc') //old f.char_val
        ->join('fa_char_type as b','a.char_type_cd','=','b.char_type_cd') //ci_char_type
        ->join('fa_char_type_lang as c','b.char_type_cd','=','c.char_type_cd') //ci_char_type_l
        ->join('fa_type as d','d.fa_type_cd','=','a.fa_type_cd')
        ->join('fa_type_lang as e','e.id','=','d.id') //ci_fa_type_l
        ->leftjoin('fa_char_value as f','b.char_type_cd','=','f.char_type_cd')
        ->leftjoin('fa_alg_validation as h','b.adhoc_val_alg_cd','=','h.alg_cd')
        ->leftjoin('fa_char_val_lang as g', 'b.char_type_cd','=','g.char_type_cd')
        //->leftjoin('fa_char_val_lang as g', 'f.char_val','=','g.char_val')
        ->where('a.fa_type_cd', $faType)
        ->groupBy('a.fa_type_cd','a.sort_seq','a.char_type_cd',
                'e.descr','c.descr','a.required_sw','b.char_type_flg',
                'g.char_val','g.descr','b.adhoc_val_alg_cd','h.descr50') //old f.char_val
        ->orderBy('a.fa_type_cd', 'a.char_type_cd', 'a.sort_seq')
        ->get();
}

function encodePost($post){
    $data = json_encode($post);
    if($data == '[]'){
        $data = '';
    }else{
        $data = str_replace('"', '', json_encode($post));
    }
    return $data;
}

function convertDate($date){
    $get    = date_create($date);
    $date  = date_format($get,"d-m-Y");
    return $date;
}

function num_row($page, $limit){
	if(is_null($page)){
		$page = 1;
	}
	$num = ($page * $limit)-($limit - 1);
	return $num;
}

function getBOMCharacter($fa_type_cd){
    $data = \DB::table('fa_template_BOM as a')
                ->select('a.fa_type_cd', 'a.template_name','b.fa_template_BOM_id','b.code','c.code','c.name','c.category','c.unit_of_purch')
                ->join('fa_template_BOM_detail as b', 'a.id', 'b.fa_template_BOM_id')
                ->join('fa_template_BOM_code as c', 'b.code', 'c.id')
                ->where('a.fa_type_cd', $fa_type_cd)
                ->get();
    return $data;
}

function getBOMCharacterWO($jobCode){
    $data = \DB::table('wo_template_BOM as a')
                ->select('a.jobCode', 'a.template_name','b.wo_template_BOM_id','b.code','c.code','c.name','c.category','c.unit_of_purch')
                ->join('wo_template_BOM_detail as b', 'a.id', 'b.wo_template_BOM_id')
                ->join('fa_template_BOM_code as c', 'b.code', 'c.id')
                ->where('a.jobCode', $jobCode)
                ->get();
    return $data;
}

function getWOCharacter($jobCode){
    $data = \DB::table('wo_task as a')
                ->select('a.job_code','a.descr','b.char_type_flg','b.char_seq','b.char_wo_type','b.char_wo_code','b.char_value')
                ->join('wo_char_type as b', 'a.job_code','b.job_code')
                ->where('a.job_code', $jobCode)
                ->groupBy('a.job_code','a.descr','b.char_type_flg','b.char_seq','b.char_wo_type','b.char_wo_code','b.char_value')
                ->get();

    return $data;
}

function getListFA($userid){
    $data = \DB::table('fa_transaction as a')
                ->join('workers as b', 'a.worker_id', 'b.id')
                ->join('users as c', 'b.user_id', 'c.id')
                ->whereIn('a.assign_status', ['Complete'])
                ->where('b.user_id', $userid)
                ->get();

    return $data;
}