<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class userTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
        	'name'=>'user1',
        	'email'=>'admin@admin1',
        	'password'=>bcrypt('123123'),
        	'group_id'=>1,
        	'username'=>'user1'
        ]);

    }
}
