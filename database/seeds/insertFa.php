<?php

use Illuminate\Database\Seeder;

class insertFa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('fa')->insert([
        	'customer_id'=>'1',
        	'fa_type_code'=>'FA-MREPL',
        	'fa_description'=>'Ganti Meter',
        	'status'=>'P',
        ]);
    }
}
