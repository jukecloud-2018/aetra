<?php

use Illuminate\Database\Seeder;

class insertToModule extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

    	DB::table('modules')->insert([
        	'moduleName'=>'monitoringSurvey',
        	'moduleStatus'=>'1',
        	'moduleParent'=>'2',
        	'moduleRoute'=>'monitoring.monitoringSurvey.index',
            'displayName'=>'Monitoring Survey',
        	'moduleUrl'=>'monitoring/monitoringSurvey',
        ]);

    }
}
