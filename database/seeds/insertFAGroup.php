<?php

use Illuminate\Database\Seeder;

class insertFAGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('fa_group')->insert([
        	'fa_group_name'=>'FA-GROUP-NDT',
        	'description'=>'Perbaikan Bak Meter Pelanggan Key Account',
        ]);
    }
}
