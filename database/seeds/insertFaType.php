<?php

use Illuminate\Database\Seeder;

class insertFaType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('fa_type')->insert([
        	'fa_type'=>'FA-CHMBR',
        	'description'=>'Perbaikan Bak Meter Pelanggan Key Account',
        ]);
    }
}
