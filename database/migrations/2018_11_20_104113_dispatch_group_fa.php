<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DispatchGroupFa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispatch_group_fa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dispatch_group')->unsigned()->index()->nullable();
            $table->text('dispatch_group_description')->nullable();
            $table->string('fa_type_cd')->unsigned()->index()->nullable();
            $table->string('service_point_type')->nullable();
            $table->string('area_code')->nullable();
            $table->string('area_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispatch_group_fa');
    }
}
