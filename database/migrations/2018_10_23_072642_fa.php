<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('customer_id')->index();
            $table->foreign('customer_id')->references('id')->on('customer');
            $table->string('fa_type_code');
            $table->text('fa_description');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa');
    }
}
