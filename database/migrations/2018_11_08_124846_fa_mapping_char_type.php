<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Yajra\Datatables\Datatables;


class FaMappingCharType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa_mapping_char_type', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('fa_type_cd');
            $table->string('char_type_cd');
            $table->string('sort_seq');
            $table->string('required_sw');
            $table->string('default_sw');
            $table->string('version');
            $table->string('char_val');
            $table->string('adhoc_char_val');
            $table->string('char_val_fk1');
            $table->string('char_val_fk2');
            $table->string('char_val_fk3');
            $table->string('char_val_fk4');
            $table->string('char_val_fk5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa_mapping_char_type');
    }
}
