<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SurveyGroupAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_group_access', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_access_id')->unsigned()->index();
            $table->foreign('group_access_id')->references('id')->on('group_access');
            $table->string('code_ab')->nullable();
            $table->enum('transaction',['true','false'])->default('false');
            $table->enum('monitoring',['true','false'])->default('false');
            $table->enum('message',['true','false'])->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_group_access');
    }
}
