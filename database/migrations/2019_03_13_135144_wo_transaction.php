<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WoTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wo_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plant', 50);
            $table->string('no_wo', 50);
            $table->string('no_wo_task', 50);
            $table->string('task_status', 50);
            $table->string('task_desc', 200);
            $table->string('asset_id', 100);
            $table->string('no_account', 200);
            $table->string('no_specification', 100);
            $table->string('asset_desc', 200);
            $table->string('area', 50);
            $table->string('department', 50);
            $table->string('crew', 100);
            $table->string('last_update_user', 100);
            $table->string('job_code', 100);
            $table->string('created_by', 100);
            $table->string('taskPhase', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wo');
    }
}
