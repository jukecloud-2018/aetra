<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SurveyDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_detail', function (Blueprint $table) {
               //
            $table->increments('id');
            $table->string('detail_survey_name');
            $table->integer('survey_id')->unsigned()->index();
            $table->foreign('survey_id')->references('id')->on('survey');
            $table->enum('status',['Active','Non Active']);
            $table->integer('type_data_id')->unsigned()->index();
            $table->foreign('type_data_id')->references('id')->on('type_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_detail');
    }
}
