<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SystemParameter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_parameter', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('range_periode');
            $table->string('assign_task');
            $table->enum('pickup_task',['complete','incomplete']);
            $table->string('lock_fa');
            $table->integer('log_off_web');
            $table->integer('log_off_mobile');
            $table->integer('auto_refresh');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
               Schema::dropIfExists('system_parameter');

    }
}
