<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodeAb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('code_ab', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code_ab')->nullable();
            $table->text('description')->nullable();
            $table->string('sbu_code')->nullable();
            $table->text('sbu_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('code_ab');
    }
}
