<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaAlgorithmValidation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa_alg_validation', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('alg_cd');
            $table->string('language_cd');
            $table->string('descr50');
            $table->string('version');
            $table->string('owner_flg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa_alg_validation');
    }
}
