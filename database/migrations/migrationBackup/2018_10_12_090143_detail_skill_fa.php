<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetailSkillFa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_skill_fa', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('fa_id')->unsigned()->index();
            $table->integer('skill_fa_id')->unsigned()->index();
            $table->foreign('skill_fa_id')->references('id')->on('skill_fa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_skill_fa');
    }
}
