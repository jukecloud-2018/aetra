<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('no_fa',100)->unique();
            $table->string('fa_type_id',100);
            $table->text('fa_comment');
            $table->string('fa_status');
            $table->string('nomen',100)->unique();
            $table->string('cc_pc_ez',100);
            $table->string('bws',100);
            $table->text('latitude');
            $table->text('longitude');
            $table->string('serial',100);
            $table->string('diameter',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa');
    }
}
