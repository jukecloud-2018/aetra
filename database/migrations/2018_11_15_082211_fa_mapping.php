<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaMapping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa_mapping', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fa_type_cd');
            $table->string('assign_date');
            $table->string('sending_status');
            $table->enum('urgent_status',['true','false'])->default('false');
            $table->integer('worker_id');
            $table->string('assignt_status');
            $table->enum('lock_fa',['unlock','assigned','on the way','started'])->default('unlock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fa_mapping');
    }
}
