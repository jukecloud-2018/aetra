<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FaType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fa_type', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->string('fa_type_cd');
            $table->string('elig_dispatch_sw');
            $table->string('fs_cl_cd');
            $table->integer('fa_priority_flg');
            $table->enum('disp_alert_sw',['Y','N'])->default('N');
            $table->integer('version');
            $table->integer('days_alert_nbr');
            $table->string('app_setup_opt_flg');
            $table->string('language_cd');
            $table->text('descr');
            $table->string('alert_text');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExist('fa_type');
    }
}
