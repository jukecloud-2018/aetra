$(document).ready(function()
{
    /*$('.modal-trigger').leanModal({dismissible:false});

    $(function() {
        Materialize.updateTextFields();
    });*/

    $('input').attr('autocomplete','off');

    //Date Today
    var d = new Date();
    var weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var month = d.getMonth()+1;
    var day = d.getDate();
    var nameofday = weekday[d.getDay()];
    var today = (day<10 ? '0' : '') + day + '/' +
        (month<10 ? '0' : '') + month + '/' + d.getFullYear();
    var output = ((''+day).length<2 ? '0' : '') + day + '/'+
        ((''+month).length<2 ? '0' : '') + month + '/' +
        d.getFullYear();
    $('.date-now').val(output);
    //End of Date Today

    /*const optionRp = {
        digitGroupSeparator: '.',
        decimalCharacter: ',',
        decimalPlaces: 0,
        currencySymbol: 'Rp ',
        currencySymbolPlacement: 'p',
        modifyValueOnWheel: false,
        unformatOnSubmit: true
    };*/

    //new AutoNumeric.multiple('.autoformat', optionRp);

    /*$('.datepicker_mask').datetimepicker({
        format: 'd/m/Y',
        timepicker: false,
        mask: true,
    });

    $('.datepicker').datetimepicker({
        format: 'd/m/Y',
        timepicker: false,
        mask: true,
    });

    $('.datepicker_n').datetimepicker({
        format: 'd/m/Y',
        timepicker: false,
        mask: true,
    });

    $('.datepicker_res').datetimepicker({
        timepicker: false,
        mask: true,
        format: 'd/m/Y',
        minDate: 0,
        value: today,
    });

    $('.datepicker_def').datetimepicker({
        timepicker: false,
        mask: true,
        format: 'd/m/Y',
        value: today,
    });

    $('.birth').datetimepicker({
        timepicker: false,
        mask: true,
        format: 'd/m/Y',
        value: '01/01/1988',
    });

    $('.timepicker').datetimepicker({
        datepicker: false,
        mask: '29:59',
        format: 'H:i',
    });

    $('.timepicker').val('');
    $('.datepicker').val('');

    $('input[type="text"]').keyup(function() {
        $(this).val($(this).val().toUpperCase());
    });

    $("#tabsId").on("click", "li", function(evt) {

        evt.preventDefault();
        var id = $(this).data("tabContent");
        $("#tabsContentId")
            .find(".tab-content[data-tab-content='" + id + "']").show()
            .siblings().hide();
    });

    $('.lever').click(function () {
        if($('.js-switch-value').is(":checked")) {
            console.log("No");
        }
        else {
            console.log("Yes");
        }
    });*/
});

function alertLoading(title, text){
    swal({
        title: title,
        text: text,
        imageUrl: "/images/loading_block.gif",
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
}

function alertPopup(title, text, type){
    swal({ title: title,
        text: text,
        type: type,
        showConfirmButton: false,
        timer: 2000
    });
}

/*function alertLogout() {
    swal({
        title: "Are you sure?",
        text: "You want to logout?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Logout",
        closeOnConfirm: false
    }, function(){
        document.getElementById('logout-form').submit();
        swal("Logged Out","", "success");
    });
}

function getDayName(date,type) {
    var from = date.split("/");
    if(type == 'fullLetter'){
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var d = new Date(from[2], from[1], from[0]);
        var dayName = days[d.getDay()];
    }else{
        var d = new Date.parse(from[2], from[1], from[0]);
        var dayName = d.toString().split(' ')[0];
    }

    return dayName;
}

function pad_number(str, max) {
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}*/
