<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    $user = Session::get('userinfo');
    if($user != NULL){
        return redirect('home'); //awalnya dulu dashboard
    }else{
        return view('login');
    }
});
Route::get('/login', function () {
    return view('login');
});


Route::post('/actionLogin','LoginController@index');
Route::get('/logout','LoginController@logout');
Route::get('/home', 'DashboardController@index')->name('home')->middleware(['token_all']);
Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');
Route::get('/dashboard/getData/{type}','DashboardController@getData');

Route::middleware(['token_all'])->prefix('administrator')->namespace('Administrator')->as('administrator.')->group(function (){
//Route::prefix('administrator')->namespace('Administrator')->as('administrator.')->group(function (){

    /* Route User*/
    Route::resource('users', 'UsersController',['except'=>'show']);
    Route::post('users/getData','UsersController@getData')->name('users.getData');
    Route::get('users/updateStatus/{id}','UsersController@updateStatus')->name('users.updateStatus');
    Route::get('users/detailUser/{id}','UsersController@detailUser')->name('users.detailUser');
    Route::post('users/updateUser','UsersController@updateUser');
    Route::get('users/destroy/{id}', 'UsersController@destroy');
    /* /.Route User */

    /* Route Vendor */
    Route::resource('vendors', 'VendorsController');
    Route::get('vendors/{id}/edit', 'VendorsController@edit');
    Route::put('vendors/update/{id}', 'VendorsController@update');
    Route::get('vendors/destroy/{id}', 'VendorsController@destroy');
    Route::get('vendors/viewVendor/{id}', 'VendorsController@viewVendor');
    /* /.Route Vendor */

    Route::resource('groups', 'GroupsController');
    Route::post('groups/getData','GroupsController@getData')->name('groups.getData');
    Route::post('groups/saveSurveyGroup','GroupsController@saveSurveyGroup');
    Route::get('groups/saveAction/{id}','GroupsController@saveAction');
    Route::get('groups/editGroupAccess/{id}','GroupsController@editGroupAccess');
    Route::get('groups/editFAGroup/{id}','GroupsController@editFAGroup');
    Route::get('groups/deleteFAGroup/{id}','GroupsController@destroy');
    Route::get('groups/editSurveyGroup/{id}','GroupsController@editSurveyGroup');
    Route::post('groups/savemodalEditFAGroup','GroupsController@savemodalEditFAGroup');
    Route::post('groups/savemodalEditSurveyGroup','GroupsController@savemodalEditSurveyGroup');


    Route::resource('skillFA', 'SkillFAController');
    Route::get('skillFA/listFa/{id}', 'SkillFAController@listFa');
    Route::post('skillFA/getData','SkillFAController@getData')->name('skillFA.getData');
    Route::post('skillFA/detailFASkill/{id}','SkillFAController@detailFASkill');
    Route::post('skillFA/detailFaType/{id}','SkillFAController@detailFaType');
    Route::post('skillFA/updateFASkill','SkillFAController@updateFASkill');
    Route::post('skillFA/saveFAType','SkillFAController@saveFAType');
    Route::get('skillFA/destroy/{id}', 'SkillFAController@destroy');


    Route::resource('skillWO', 'SkillWOController');
    Route::post('skillWO/store', 'SkillWOController@store')->name('skillWO.store');
    Route::post('skillWO/getData','SkillWOController@getData')->name('skillWO.getData');
    Route::get('skillWO/destroy/{id}', 'SkillWOController@destroy');
    Route::post('skillWO/detailWoType/{id}', 'SkillWOController@detailWoType');
    Route::post('skillWO/detailWOSkill/{id}','SkillWOController@detailWOSkill');
    Route::post('skillWO/updateWOSkill', 'SkillWOController@updateWOSkill');
    Route::post('skillWO/saveWOType','SkillWOController@saveWOType');

    Route::resource('workerLogin', 'WorkerLoginController');
    Route::resource('hhManagement', 'HHManagementController');
    Route::post('hhManagement/add', 'HHManagementController@store')->name('hhManagement.store');
    Route::post('hhManagement/getData','HHManagementController@getData')->name('hhManagement.getData');
    Route::get('hhManagement/{id}/edit', 'HHManagementController@edit');
    Route::put('hhManagement/update/{id}', 'HHManagementController@update');
    Route::get('hhManagement/destroy/{id}', 'HHManagementController@destroy');

    Route::resource('survey', 'SurveyController');
    Route::get('survey/getTypeData/{id}', 'SurveyController@getTypeData')->name('survey.getTypeData');
    Route::get('survey/updateStatus/{id}','SurveyController@updateStatus')->name('survey.updateStatus');
    Route::get('survey/detailSurvey/{id}','SurveyController@detailSurvey')->name('survey.detailSurvey');
    Route::get('survey/deleteDetail/{id}','SurveyController@deleteDetail')->name('survey.deleteDetail');
    Route::post('survey/updateSurvey','SurveyController@updateSurvey');
    Route::get('survey/destroy/{id}', 'SurveyController@destroy');



    Route::get('mappingFAType/view_character/{id}','MappingFATypeController@view_character')->name('mappingFAType.view_character');
    Route::get('mappingFAType/viewBOM/{id}','MappingFATypeController@viewBOM')->name('mappingFAType.viewBOM');
    Route::get('mappingFAType/viewSeqChar/{id}','MappingFATypeController@viewSeqChar')->name('mappingFAType.viewSeqChar');
    Route::get('mappingFAType/saveUrgent/{id}','MappingFATypeController@saveUrgent')->name('mappingFAType.saveUrgent');
    Route::get('mappingFAType/saveLockFA/{id}','MappingFATypeController@saveLockFA')->name('mappingFAType.saveLockFA');
    Route::get('mappingFAType/savePriority/{id}','MappingFATypeController@savePriority')->name('mappingFAType.savePriority');
    Route::get('mappingFAType/getCode/{id}','MappingFATypeController@getCode');
    Route::get('mappingFAType/viewCode/','MappingFATypeController@viewCode');
    Route::get('mappingFAType/edit/{id}','MappingFATypeController@edit');
    Route::get('mappingFAType/editSeqChar/{id}','MappingFATypeController@editSeqChar');
    Route::post('mappingFAType/updateCharSeq/{id}', 'MappingFATypeController@updateCharSeq');


    Route::resource('areaSurvey', 'AreaSurveyController');
    Route::resource('mappingFAType', 'MappingFATypeController');

    Route::resource('mappingWOTask', 'MappingWOTaskController');
    Route::get('mappingWOTask/view_character/{id}','MappingWOTaskController@view_character')->name('mappingWOTask.view_character');
    Route::get('mappingWOTask/savePriority/{id}','MappingWOTaskController@savePriority')->name('mappingWOTask.savePriority');
    Route::get('mappingWOTask/saveUrgent/{id}','MappingWOTaskController@saveUrgent')->name('mappingWOTask.saveUrgent');
    Route::get('mappingWOTask/saveLockWO/{id}','MappingWOTaskController@saveLockWO')->name('mappingWOTask.saveLockWO');
    Route::get('mappingWOTask/viewBOM/{id}','MappingWOTaskController@viewBOM');
    Route::get('mappingWOTask/getCode/{id}','MappingWOTaskController@getCode');
    Route::get('mappingWOTask/edit/{id}','MappingWOTaskController@edit');

    Route::resource('systemParameter', 'SystemParameterController');
    Route::get('getDataModule','UsersController@getDataModule')->name('users.getDataModule');
    Route::get('getDetailModule','UsersController@getDetailModule')->name('users.getDetailModule');



    Route::post('skillWO/getData','SkillWOController@getData')->name('skillWO.getData');
    Route::post('vendors/getData','VendorsController@getData')->name('vendors.getData');
    Route::post('workerLogin/getData','WorkerLoginController@getData')->name('workerLogin.getData');
    Route::get('workerLogin/editSurvey/{id}','WorkerLoginController@editSurvey');
    Route::post('workerLogin/updateListSurvey/{id}','WorkerLoginController@updateListSurvey');
    Route::get('workerLogin/editDispatch/{id}','WorkerLoginController@editDispatch');
    Route::post('workerLogin/updateListFA/{id}','WorkerLoginController@updateListFA');
    Route::get('workerLogin/viewDispatch/{id}','WorkerLoginController@viewDispatch');
    Route::get('workerLogin/viewSkill/{id}','WorkerLoginController@viewSkill');
    Route::get('workerLogin/viewSkillWO/{id}','WorkerLoginController@viewSkillWO');
    Route::get('workerLogin/viewSurvey/{id}','WorkerLoginController@viewSurvey');
    Route::get('workerLogin/editPasword/{user_id}', 'WorkerLoginController@editPassword');
    Route::post('workerLogin/updatePassword/{id}', 'WorkerLoginController@updatePassword');
    Route::get('workerLogin/destroy/{id}', 'WorkerLoginController@destroy');
    Route::get('workerLogin/workerWO/{type}','WorkerLoginController@workerWO');

    Route::post('survey/getData','SurveyController@getData')->name('survey.getData');
    Route::post('areaSurvey/getData','AreaSurveyController@getData')->name('areaSurvey.getData');
    Route::post('mappingFAType/getData','MappingFATypeController@getData')->name('mappingFAType.getData');
    Route::post('mappingWOTask/getData','MappingWOTaskController@getData')->name('mappingWOTask.getData');
    Route::post('systemParameter/getData','SystemParameterController@getData')->name('systemParameter.getData');
});



Route::middleware(['token_all'])->prefix('transaction')->namespace('Transaction')->as('transaction.')->group(function (){

    Route::resource('messaging', 'MessagingController');
    Route::get('messaging/compose/{type}','MessagingController@compose');
    Route::get('messaging/status/{type}','MessagingController@draf');
    Route::get('messaging/destroy/{type}','MessagingController@destroy');
    Route::get('messaging/detail/{type}','MessagingController@detail');
    Route::get('messaging/edit/{type}','MessagingController@edit');
    Route::get('messaging/update/{type}','MessagingController@update');

    Route::resource('assignmentFA', 'AssignmentFAController');
    Route::post('assignmentFA/getDataByFilter','AssignmentFAController@getDataByFilter')->name('assignmentFA.getDataByFilter');
    Route::post('assignmentFA/import','AssignmentFAController@import')->name('import');
    Route::post('assignmentFA/multiSaveWorker','AssignmentFAController@multiSaveWorker');
    Route::get('assignmentFA/getWorker/{id}','AssignmentFAController@getWorker')->name('assignmentFA.getWorker');
    Route::get('assignmentFA/saveWorker/{id}','AssignmentFAController@saveWorker')->name('survey.saveWorker');
    Route::get('assignmentFA/saveAssignDate/{id}','AssignmentFAController@saveAssignDate')->name('survey.saveAssignDate');
    Route::get('assignmentFA/saveReAssignedDate/{id}','AssignmentFAController@saveReAssignedDate')->name('survey.saveReAssignedDate');
    Route::get('assignmentFA/getWorkerOne/{id}','AssignmentFAController@getWorkerOne')->name('survey.getWorkerOne');
    Route::get('assignmentFA/saveUrgent/{id}','AssignmentFAController@saveUrgent')->name('survey.saveUrgent');
    Route::get('assignmentFA/savePriority/{id}','AssignmentFAController@savePriority')->name('survey.savePriority');
    Route::get('assignmentFA/export/{type}','AssignmentFAController@export');
	Route::get('assignmentFA/getData/{type}','AssignmentFAController@getData');
	Route::get('assignmentFA/search/{type}','AssignmentFAController@search');
    Route::get('assignmentFA/filterAssignmentFA/{type}','AssignmentFAController@filterAssignmentFA');
    Route::post('assignmentFA/refresh','AssignmentFAController@refresh_all_fa');
    Route::get('assignmentFA/detailNomen/{id}','AssignmentFAController@detailNomen');

    Route::resource('assignmentWO', 'AssignmentWOController');
    Route::post('assignmentWO/refresh','AssignmentWOController@refresh');
    Route::get('assignmentWO/getData/{type}','AssignmentWOController@getData');
    Route::get('assignmentWO/saveUrgent/{id}','AssignmentWOController@saveUrgent');
    Route::get('assignmentWO/savePriority/{id}','AssignmentWOController@savePriority');
    Route::get('assignmentWO/saveWorker/{id}','AssignmentWOController@saveWorker');
    Route::post('assignmentWO/import','AssignmentWOController@import')->name('importWO');
    Route::get('assignmentWO/export/{type}','AssignmentWOController@export');
    Route::get('assignmentWO/saveAssignDate/{id}','AssignmentWOController@saveAssignDate');
    Route::get('assignmentWO/search/{type}','AssignmentWOController@search');
    Route::get('assignmentWO/filterAssignmentWO/{type}','AssignmentWOController@filterAssignmentWO');

    Route::resource('assignmentSurvey', 'AssignmentSurveyController');
    Route::post('assignmentSurvey/getData','AssignmentSurveyController@getData')->name('assignmentSurvey.getData');
    Route::get('assignmentSurvey/json-pc/{id}','AssignmentSurveyController@pc'); 
    Route::get('assignmentSurvey/json-ez/{id}','AssignmentSurveyController@ez'); 
    Route::get('assignmentSurvey/json-bk/{id}','AssignmentSurveyController@bk'); 
    Route::get('assignmentSurvey/json-worker/{id}','AssignmentSurveyController@selectWorker'); 
    Route::get('assignmentSurvey/viewAB/{id}','AssignmentSurveyController@viewAB');
    Route::get('assignmentSurvey/viewPC/{id}','AssignmentSurveyController@viewPC');

});


Route::middleware(['token_all'])->prefix('monitoring')->namespace('Monitoring')->as('monitoring.')->group(function (){
    Route::resource('monitoringFA', 'MonitoringFAController');
	Route::get('monitoringFA/getData/{type}','MonitoringFAController@getData');
	Route::get('monitoringFA/search/{type}','MonitoringFAController@search');
    Route::get('monitoringFA/detailNomen/{id}','MonitoringFAController@detailNomen');
    Route::post('monitoringFA/getDataBydate','MonitoringFAController@getDataBydate')->name('monitoringFA.getDataBydate');
    Route::post('monitoringFA/getDetail/{id}','MonitoringFAController@getDetail');
    Route::post('monitoringFA/getCharacteristic/{id}','MonitoringFAController@getCharacteristic');
    Route::post('monitoringFA/getMaterialUsage/{id}','MonitoringFAController@getMaterialUsage');
    Route::post('monitoringFA/getTTD/{id}','MonitoringFAController@getTTD');
    Route::get('monitoringFA/getphotofa/{id}', 'MonitoringFAController@getphotofa');
    Route::get('monitoringFA/downloadPdf/{id}', 'MonitoringFAController@downloadPdf');
    Route::post('monitoringFA/updateCharacteristic/{id}', 'MonitoringFAController@updateCharacteristic');
    Route::get('monitoringFA/editCharacteristicById/{id}', 'MonitoringFAController@editCharacteristicById');
    Route::get('monitoringFA/getCharWms/{id}', 'MonitoringFAController@sendBackToSimpel');
    Route::get('monitoringFA/getComp/{id}', 'MonitoringFAController@sendCompBackToSimpel');
    Route::get('monitoringFA/get_history/{id}', 'MonitoringFAController@get_history');
    Route::get('monitoringFA/filterMonitoringFA/{type}','MonitoringFAController@filterMonitoringFA');

    Route::resource('monitoringWO', 'MonitoringWOController');
    Route::post('monitoringWO/getData','MonitoringWOController@getData')->name('monitoringWO.getData');
    Route::post('monitoringWO/getDetail/{id}/{no_wo_task}','MonitoringWOController@getDetail');
    Route::post('monitoringWO/getDataBydate','MonitoringWOController@getDataBydate')->name('monitoringWO.getDataBydate');
    Route::get('monitoringWO/downloadPdf/{id}', 'MonitoringWOController@downloadPdf');
    Route::get('monitoringWO/search/{type}','MonitoringWOController@search');
    Route::get('monitoringWO/getData/{type}','MonitoringWOController@getData');
    Route::get('monitoringWO/filterMonitoringWO/{type}','MonitoringWOController@filterMonitoringWO');
    Route::get('monitoringWO/getCharacteristic/{id}/{no_wo_task}', 'MonitoringWOController@getCharacteristic');
    Route::get('monitoringWO/getMaterialUsage/{id}/{no_wo_task}', 'MonitoringWOController@getMaterialUsage');
    Route::get('monitoringWO/getTTD/{id}/{no_wo_task}', 'MonitoringWOController@getTTD');
    Route::get('monitoringWO/getphotowo/{id}/{no_wo_task}', 'MonitoringWOController@getphotowo');

    Route::resource('monitoringUsage', 'MonitoringUsageController');
    Route::post('monitoringUsage/getData','MonitoringUsageController@getData')->name('monitoringUsage.getData');
    Route::get('monitoringUsage/getDataByDate/{type}','MonitoringUsageController@getDataByDate');
    Route::get('monitoringUsage/search/{type}','MonitoringUsageController@search');
    Route::get('monitoringUsage/getMaterialUsage/{id}','MonitoringUsageController@getMaterialUsage');
    Route::get('monitoringUsage/filterUsage/{type}','MonitoringUsageController@filterUsage');

    Route::resource('monitoringSurvey', 'MonitoringSurveyController');
    Route::post('monitoringSurvey/getData','MonitoringSurveyController@getData')->name('monitoringSurvey.getData');
    Route::post('monitoringSurvey/getCharacteristic/{id}','MonitoringSurveyController@getCharacteristic');
    Route::post('monitoringSurvey/getPhotoSurvey/{id}','MonitoringSurveyController@getPhotoSurvey');
    Route::get('monitoringSurvey/getDataBydate/{type}','MonitoringSurveyController@getDataBydate');
    Route::post('monitoringSurvey/getDataByStatus','MonitoringSurveyController@getDataByStatus');
    Route::get('monitoringSurvey/viewAB/{id}','MonitoringSurveyController@viewAB');
    Route::get('monitoringSurvey/viewPC/{id}','MonitoringSurveyController@viewPC');
    Route::get('monitoringSurvey/filterMonitoringSurvey/{type}','MonitoringSurveyController@filterMonitoringSurvey');
    Route::get('monitoringSurvey/search/{type}','MonitoringSurveyController@search');
    Route::get('monitoringSurvey/mapSurvey/{id}','MonitoringSurveyController@mapSurvey');

    Route::resource('mapStatus', 'MapStatusController');
    Route::resource('mapWorker', 'MapWorkerController');



});
// Auth::routes();
