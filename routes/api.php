<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('signup_hh','ApiController@signup_hh');
Route::post('login_hh','ApiController@login_hh');
Route::post('logout_hh','ApiController@logout_hh');
Route::post('message_hh','ApiController@message_hh');
Route::post('list_fa_hh','ApiController@list_fa_hh');
Route::post('send_pickup_status_hh','ApiController@send_pickup_status_hh');
Route::post('send_start_status_hh','ApiController@send_start_status_hh');
Route::post('send_cancel_status_hh','ApiController@send_cancel_status_hh');
Route::post('send_ontheway_status_hh','ApiController@send_ontheway_status_hh');
Route::post('send_reassigned_status_hh','ApiController@send_reassigned_status_hh');
Route::post('send_assigned_status_hh', 'ApiController@send_assigned_status_hh');
Route::post('send_open_status_hh', 'ApiController@send_open_status_hh');
Route::post('profile','ApiController@profile');
Route::post('post_characteristic','ApiController@post_characteristic');
Route::post('upload_photo_fa_hh', 'ApiController@upload_photo_fa_hh');
Route::post('upload_ttd_fa_hh', 'ApiController@upload_ttd_fa_hh');
Route::post('list_pickup_fa_hh','ApiController@list_pickup_fa_hh');
Route::post('check_status_joblist_hh', 'ApiController@check_status_joblist_hh');
Route::post('list_survey_hh', 'ApiController@list_survey_hh');
Route::post('post_characteristic_survey_hh', 'ApiController@post_characteristic_survey_hh');
Route::post('upload_photo_survey_hh', 'ApiController@upload_photo_survey_hh');
Route::post('post_bom_hh', 'ApiController@post_bom_hh');
Route::post('history_worker', 'ApiController@history_worker');
Route::post('list_wo_hh','ApiController@list_wo_hh');
Route::post('send_ontheway_wo_status_hh','ApiController@send_ontheway_wo_status_hh');
Route::post('send_start_wo_status_hh','ApiController@send_start_wo_status_hh');
Route::post('post_characteristic_wo_hh','ApiController@post_characteristic_wo_hh');
Route::post('upload_photo_wo_hh','ApiController@upload_photo_wo_hh');
Route::post('upload_ttd_wo_hh','ApiController@upload_ttd_wo_hh');
Route::post('post_bom_wo_hh','ApiController@post_bom_wo_hh');
Route::post('history_worker_wo', 'ApiController@history_worker_wo');
Route::post('history_worker_survey_hh', 'ApiController@history_worker_survey_hh');
Route::post('summaryFA', 'ApiController@summaryFA');

//API FOR MAP IN WMS
Route::post('get_all_job_map', 'ApiController@get_all_job_map');
Route::post('search_job_map', 'ApiController@search_job_map');
Route::post('get_all_worker', 'ApiController@get_all_worker');
Route::post('search_worker', 'ApiController@search_worker');
Route::post('update_batery_signal_level', 'ApiController@update_batery_signal_level');
Route::post('testapi', 'ApiController@testapi');