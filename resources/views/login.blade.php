<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="https://fonts.gstatic.com">--}}
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4" style="margin-top: 20px;">
        <div class="card">
        <div class="card-header"> <img src="{{asset('images/logoHLogin.png')}}" style="width:300px;heigh:200px;" alt=""> </div>    
        <div class="border-center"><hr></div>
            <div class="card-body">
                <h3 class="col-md-9  offset-md-2">Sign in</h3>
                <form id="formLogin">
                    @csrf
                    <div class="form-group row">
                    <div class="col-md-9  offset-md-2">
                        <input id="username" type="text"  placeholder="Username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }} logins" name="username" value="{{ old('username') }}" required autofocus>
                        @if ($errors->has('username'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                        @endif
                    </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8  offset-md-2">
                            <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} logins" name="password" required>
                        </div>
                        <span class="co-md-4" onclick="showPass()">
                            <i id="showPass" class="fa fa-eye" style="font-size: 48px; cursor: pointer"></i>
                        </span>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-8 offset-md-4">
                            <button type="button" class="btn btn-primary" id="btnLogin" onclick="actionLogin()">
                                SIGN - IN 
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- is left card -->

    <div class="col-md-1"></div>
    <div class="col-md-6">
        <div class="right-content"> 
            <div class="text-content">
                <h3>Visi Aetra</h3>
                <ul>
                    <li>Penyedia Layanan Air Minum Terdepan Di Indonesia</li>
                </ul>
                <h3>Misi Aetra</h3>
                <ul>
                    <li>Memenuhi Kebutuhan Air Minum Pelanggan Melalui Pelayanan Prima</li>
                    <li>Mengembangkan Perusahaan Yang Sehat Secara Berkelanjutan dan Berwawasan Lingkungan Sehingga Memberikan Nilai Tambah Bagi Pemangku Kepentingan</li>
                    <li>Membangun Lingkungan Kerja Yang Kondusif Untuk Meningkatkan Profesionalisme Dan Kepuasan Karyawan</li>
                </ul>
            </div>
        </div>
    </div><!-- is right card -->
</div>

<!-- Is Row  -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript">
    $(".logins").keyup(function(event) {
        if (event.keyCode === 13) {
            $("#btnLogin").click();
        }
    });
	function actionLogin(){
	    alertLoading('Login', 'Please wait.. don\'t refresh');
		$.ajax({
			url:'actionLogin',
			type:'POST',
			data: $('#formLogin').serialize(),
			dataType:'JSON',
			header:{
				'X-CSRF-TOKEN':'{{csrf_token()}}',
			},
			success:function(data){
				var erroralert='';
				if(data.errors)
				{
					//alertPopup("Error!","Terjadi kesalahan proses login","error");
					alertPopup("Gagal!", data.errors, "error");
				}
				else if(data.success)
				{
					alertPopup("Sukses!","Login Sukses","success");
					window.location.href  = "{{url('/home')}}" //dulunya dashboard
				}
			}
		})
	}

	function showPass() {
        var a = $('#showPass').closest('.fa-eye');
        if(a.length == 1){
            $('#showPass').removeClass('fa-eye').addClass('fa fa-eye-slash');
            $('#password').removeAttr('type','password').attr('type','text');
        }else{
            $('#showPass').removeClass('fa-eye-slash').addClass('fa fa-eye');
            $('#password').removeAttr('type','text').attr('type','password');
        }
	}
</script>
</body>
</html>