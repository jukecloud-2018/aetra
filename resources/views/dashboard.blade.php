@extends('layouts.app')

@section('head')
    Dashboard | {{ env('APP_NAME') }}
@endsection

@section('title')

  <script>
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var c_open = <?= json_encode($count_open) ?>;
      var c_assigned = <?= json_encode($count_assigned) ?>;
      var c_ontheway = <?= json_encode($count_ontheway) ?>;
      var c_started = <?= json_encode($count_started) ?>;
      var c_c_worker = <?= json_encode($count_cancel_worker) ?>;
      var c_c_admin = <?= json_encode($count_cancel_admin) ?>;
      var c_complete = <?= json_encode($count_complete) ?>;

      var c_open_wo = <?= json_encode($wo_count_open) ?>;
      var c_assigned_wo = <?= json_encode($wo_count_assigned) ?>;
      var c_ontheway_wo = <?= json_encode($wo_count_ontheway) ?>;
      var c_started_wo = <?= json_encode($wo_count_started) ?>;
      var c_c_worker_wo = <?= json_encode($wo_count_cancel_worker) ?>;
      var c_c_admin_wo = <?= json_encode($wo_count_cancel_admin) ?>;
      var c_complete_wo = <?= json_encode($wo_count_complete) ?>;

      var data = google.visualization.arrayToDataTable([
          ['Status', 'FA Status', 'WO Status'],
          ['Open', c_open, c_open_wo],
          ['Assigned', c_assigned, c_assigned_wo],
          ['On The Way', c_ontheway, c_ontheway_wo],
          ['Started', c_started, c_started_wo],
          ['Cancel by Worker', c_c_worker, c_c_worker_wo],
          ['Cancel by Admin', c_c_admin, c_c_admin_wo],
          ['Complete', c_complete, c_complete_wo]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                       { calc: "stringify",
                         sourceColumn: 1,
                         type: "string",
                         role: "annotation" },
                       2,
                       { calc: "stringify",
                         sourceColumn: 2,
                         type: "string",
                         role: "annotation" }]);

      var options = {
        width: 1000,
        height: 400,
        bar: {groupWidth: "95%"},
        legend: { position: "right", alignment: 'end' },
      };
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);
  }

function reloadDashboard(){
  alertPopup("Sukses!", "Data Sudah Terupdate", "success");
  window.location.reload();
}
  </script>

  <i class="fa fa-home"></i> Dashboard
@endsection

<meta http-equiv="refresh" content="300">
@section('button')
<div class="row forButtonTop">
    <div class="col-md-12">
        <form method="get" action="{{ URL::to('dashboard/getData/srch')}}">
            {{ csrf_field() }}
            <span>Periode : </span>
            <div style="display: inline-block;width: 100px" >
                <input type="text" class="form-control datepicker" name="startPeriode" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
            <span style="color: #8B9AAD;font-weight: bold; margin-right: 10px;"> > </span>
            <div style="display: inline-block;width: 100px">
                <input type="text" class="form-control datepicker" name="endPeriode" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
            <input type="submit" class="btn btn-primary white btnTop" style="margin-left:5px;" value="OK">
            <!--<a class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalFA">
                <i class="fa fa-filter" aria-hidden="true"></i> Filter
            </a>-->
            <a class="btn btn-primary white btnTop" style="margin-left:5px;" onclick="reloadDashboard()">Refresh</a>
        </form>
    </div>
</div>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <div class="btn-group">
                            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-wrench"></i></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                                <h3 class="text-center"><strong>Status Pekerjaan</strong></h3>
                            <div align="center" id="columnchart_values" ></div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- ./box-body -->
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 col-xs-7">
                        <!--- Box Body --->
                          <div class="box-body">
                            <div class="row">
                                <a href="" data-toggle="modal" data-target="#modalMd"><span class="fa fa-arrow-circle-right fa-3x table-icon-right"></span></a>
                                <table id="tableDashboar1" class="table table-bordered table-striped">
                                  <thead>
                                    <tr style="background-color:#8B9AAD;color:#fff">
                                      <th style="vertical-align: middle!important; text-align: center!important;">No</th>
                                      <th style="vertical-align: middle!important; text-align: center!important;">FA Type</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        @foreach($listFa as $row)
                                        <tr class="tr_dashboard">
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->fa_type_cd}}</td>
                                        </tr>
                                        @endforeach                    
                                  </tbody>
                                </table>
                            </div>
                          </div>
                            
                        </div>
                        
                        <div class="col-sm-4 col-xs-7">
                            <div class="box-body">
                            <div class="row">
                              <a href="" data-toggle="modal" data-target="#modalWO"><span class="fa fa-arrow-circle-right fa-3x table-icon-right"></span></a>
                                <table id="tableDashboar2" class="table table-bordered table-striped">
                                  <thead>
                                    <tr style="background-color:#8B9AAD;color:#fff">
                                      <th style="vertical-align: middle!important; text-align: center!important;">No</th>
                                      <th style="vertical-align: middle!important; text-align: center!important;">Job Code</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        @foreach($listWo as $row)
                                        <tr class="tr_dashboard">
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->job_code}}</td>
                                        </tr>
                                        @endforeach                    
                                  </tbody>
                                </table>
                            </div>
                          </div>
                            
                        </div>
                        
                        <div class="col-sm-4 col-xs-7">
                            <div class="box-body">
                            <div class="row">
                              <a href="" data-toggle="modal" data-target="#modalBOM"><span class="fa fa-arrow-circle-right fa-3x table-icon-right"></span></a>
                                <table id="tableDashboar3" class="table table-bordered table-striped">
                                  <thead>
                                    <tr style="background-color:#8B9AAD;color:#fff">
                                      <th style="vertical-align: middle!important; text-align: center!important;">No</th>
                                      <th style="vertical-align: middle!important; text-align: center!important;">Item Code</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                        @foreach($list_bom as $row)
                                        <tr class="tr_dashboard">
                                            <td>{{$row->id}}</td>
                                            <td>{{$row->code}}</td>
                                        </tr>
                                        @endforeach                    
                                  </tbody>
                                </table>
                            </div>
                          </div>
                            
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->

              <!-- MODAL FA -->
              <div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" style="width:1300px;" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalMdTitle"><center>FA Type</center></h4>
                        </div>
                        <div class="modal-body">
                            <div class="modalError"></div>
                            <div id="modalMdContent">
                                <table id="tableDashboar4" class="table table-bordered table-striped">
                                        <thead>
                                          <tr style="background-color:#8B9AAD;color:#fff">
                                            <th style="vertical-align: middle!important; text-align: center!important;">FA Type</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Descriptions</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Open</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Assigned</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">On The Way</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Started</th>
                                            <!-- <th style="vertical-align: middle!important; text-align: center!important;">Cancel by Worker</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Cancel by Admin</th> -->
                                            <th style="vertical-align: middle!important; text-align: center!important;">Complete</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Total</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                              @foreach($list_detail_fa as $row)
                                              <tr class="tr_dashboard">
                                                  <td>{{ $row['fa_type_cd'] }}</td>
                                                  <td>{{ $row['descr'] }}</td>
                                                  <td>{{ $row['c_op_detail'] }}</td>
                                                  <td>{{ $row['c_ass_detail'] }}</td>
                                                  <td>{{ $row['c_otw_detail'] }}</td>
                                                  <td>{{ $row['c_str_detail'] }}</td>
                                                  <!-- <td></td>
                                                  <td></td> -->
                                                  <td>{{ $row['c_cls_detail'] }}</td>
                                                  <td>{{ $row['total'] }}</td>
                                              </tr>
                                              @endforeach                    
                                        </tbody>
                                      </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- END MODAL FA -->

              <!-- MODAL WO -->
              <div class="modal fade" id="modalWO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" style="width:1250px;" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalMdTitle"><center>WO Task</center></h4>
                        </div>
                        <div class="modal-body">
                            <div class="modalError"></div>
                            <div id="modalMdContent">
                                <table id="tableDashboar5" class="table table-bordered table-striped">
                                        <thead>
                                          <tr style="background-color:#8B9AAD;color:#fff">
                                            <th style="vertical-align: middle!important; text-align: center!important;">Job Code</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Descriptions</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Open</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Assigned</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">On The Way</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Started</th>
                                            <!-- <th style="vertical-align: middle!important; text-align: center!important;">Cancel by Worker</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Cancel by Admin</th> -->
                                            <th style="vertical-align: middle!important; text-align: center!important;">Complete</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Total</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                              @foreach($list_detail_wo as $row)
                                              <tr class="tr_dashboard">
                                                  <td>{{ $row['job_code'] }}</td>
                                                  <td>{{ $row['descr'] }}</td>
                                                  <td>0</td>
                                                  <td>0</td>
                                                  <td>0</td>
                                                  <td>0</td>
                                                  <td>0</td>
                                                  <td>0</td>
                                                  <!-- 
                                                  <td>0</td>
                                                  <td>0</td> -->
                                              </tr>
                                              @endforeach                    
                                        </tbody>
                                      </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- END MODAL WO -->

              <!-- MODAL BOM -->
              <div class="modal fade" id="modalBOM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="modalMdTitle"><center>BOM Code</center></h4>
                        </div>
                        <div class="modal-body">
                            <div class="modalError"></div>
                            <div id="modalMdContent">
                                <table id="tableDashboar6" class="table table-bordered table-striped">
                                        <thead>
                                          <tr style="background-color:#8B9AAD;color:#fff">
                                            <th style="vertical-align: middle!important; text-align: center!important;">Item Code</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Descriptions</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Qty</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">UoM</th>
                                            <th style="vertical-align: middle!important; text-align: center!important;">Total</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                              @foreach($list_detail_bom as $row)
                                              <tr class="tr_dashboard">
                                                  <td>{{ $row['code'] }}</td>
                                                  <td>{{ $row['desc'] }}</td>
                                                  <td>{{ ($row['qty']) ? $row['qty'] : '0' }}</td>
                                                  <td>{{ $row['uom'] }}</td>
                                                  <td>{{ ($row['total']) ? $row['total'] : '0' }}</td>
                                              </tr>
                                              @endforeach                    
                                        </tbody>
                                      </table>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- END MODAL BOM -->

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection

@section('footer')
@endsection