<script type="text/javascript">

/*$(document).ready(function(){
    fetch_data('no');
    $('#getData').click(function(){
        var start_date = $('[name="startDateTrans"]').val();
        var end_date = $('[name="endDateTrans"]').val();

        if(start_date == '' || end_date =='')
        {
            alertPopup('Error',' Please Complete Insert Date ','error');
            return false;
        }
        $('#tableAssignmentFA').dataTable().api().destroy();
        alertLoading('Sedang mengambil data', 'Please wait.. don\'t refresh');

        fetch_data('yes',start_date,end_date);
        swal.close();
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

});*/

function fetch_data()
{
	var start_date = $('[name="startDateTrans"]').val();
	var end_date = $('[name="endDateTrans"]').val();
	var body;
	$.ajax({
	  url:"assignmentFA/getData/"+start_date+"",
    headers: {
    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
  	data:{start_date: $('[name="startDateTrans"]').val(), end_date: $('[name="endDateTrans"]').val()},
	  type:'GET',
	  success:function(data){
	  	//body = data;
	  	//console.log(data);
	  	$('#formUpdateAssigned').html(data);
	  },
		error: function (xhr, ajaxOptions, thrownError) {
			swal("error!", thrownError, "error");
		}
	});
}

function checkAllTrans(cek)
{
  if (cek.checked)
  {
  	$('.rowFATrans').prop('checked', cek.checked);
    $('.selectWorker').attr("disabled", "disabled");
    $('#showWorker').removeAttr("hidden", "hidden");
    var numberOfChecked = $('.rowFATrans').filter(':checked').length;
    var totalOfChecked = $('.rowFATrans').filter(':checkbox').length;
    var numberOfNotChecked = totalOfChecked - numberOfChecked;
    $('#selected_data').text("Selected Data: "+numberOfChecked);
  }
  else
  {
  	$('.rowFATrans').prop('checked', cek.checked);
  	$('#showWorker').attr("hidden", "hidden");
    $('.selectWorker').removeAttr("disabled", "disabled");
    var numberOfChecked = $('.rowFATrans').filter(':checked').length;
    var totalOfChecked = $('.rowFATrans').filter(':checkbox').length;
    var numberOfNotChecked = totalOfChecked - numberOfChecked;
    $('#selected_data').text("Selected Data: "+numberOfChecked);
  }
}

function checkSelection(resp, id)
{
  var numberOfChecked = $('.rowFATrans').filter(':checked').length;
  var totalOfChecked = $('.rowFATrans').filter(':checkbox').length;
  var numberOfNotChecked = totalOfChecked - numberOfChecked;
  $('#selected_data').text("Selected Data: "+numberOfChecked);
  if (resp.checked){
    $('#selectWorker'+id).attr("disabled", "disabled");
    $('#showWorker').removeAttr("hidden", "hidden");
  }else{
    if(numberOfChecked != 0){
      $('#selectWorker'+id).removeAttr("disabled", "disabled");
  	  $('#showWorker').removeAttr("hidden", "hidden");
    }else{
      $('#selectWorker'+id).removeAttr("disabled", "disabled");
  	  $('#showWorker').attr("hidden", "hidden");
    }
  }
}
function resetMark()
{
    $('.rowFATrans').prop('checked', false);
    $('.checkall').prop('checked', false);
    $('#selected_data').text("Selected Data: ");
		$('#showWorker').attr("hidden", "hidden");
    $('.selectWorker').removeAttr("disabled", "disabled");
}

function updateAssigned(){
    var srch = $(".srch").attr("data-srch");
    if(srch == "srch"){
        $.ajax({
            url:"../multiSaveWorker",
            data: $('form#formUpdateAssigned').serialize(),
            type:'POST',
            dataType:'JSON',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                location.reload();
                $('#showWorker').attr("hidden", "hidden");
                if(data.data.reassigned){
                    alertPopup("Sukses", "Berhasil re-assigned worker", "success");
                }else{
                    alertPopup("Sukses", "Berhasil assigned worker", "success");
                }
                $('#assignDate'+id).addClass("datepicker");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
    }else{
        $.ajax({
            url:"assignmentFA/multiSaveWorker",
            data: $('form#formUpdateAssigned').serialize(),
            type:'POST',
            dataType:'JSON',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                location.reload();
                $('#showWorker').attr("hidden", "hidden");
                if(data.data.reassigned){
                    alertPopup("Sukses", "Berhasil re-assigned worker", "success");
                }else{
                    alertPopup("Sukses", "Berhasil assigned worker", "success");
                }
                $('#assignDate'+id).addClass("datepicker");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
    }
}

function saveWorker(id)
{
    worker = $('[name="workerName'+id+'"]').val();
    var srch = $(".srch").attr("data-srch");
    if(srch == "srch"){
    	$.ajax({
	      url:"../saveWorker/"+id+"",
	      data:{
	          'id': $('[name="id_assignmentfa'+id+'"]').val(),
	          'assignDate':$('[name="assignDate'+id+'"]').val(),
	          'worker': worker
	      },
	      dataType:'JSON',
	      type:'GET',
	      success:function(data){
	      	$('#re_'+id).removeAttr("disabled");
	        $('#selectWorker'+id).attr("disabled", "disabled");
	        if(data.data.reassigned){
	          alertPopup("Sukses", "Berhasil re-assigned worker", "success");
	          window.location.reload();
	        }else{
	          alertPopup("Sukses", "Berhasil assigned worker", "success");
	          window.location.reload();
	        }
	        $('#assignDate'+id).addClass("datepicker");
	      },
	      error: function (xhr, ajaxOptions, thrownError) {
	          swal("error!", thrownError, "error");
	      }
    	});
    }else{
	    $.ajax({
	      url:"assignmentFA/saveWorker/"+id+"",
	      data:{
	          'id': $('[name="id_assignmentfa'+id+'"]').val(),
	          'assignDate':$('[name="assignDate'+id+'"]').val(),
	          'worker': worker
	      },
	      dataType:'JSON',
	      type:'GET',
	      success:function(data){
	      	$('#re_'+id).removeAttr("disabled");
	        $('#selectWorker'+id).attr("disabled", "disabled");
	        if(data.data.reassigned){
	          alertPopup("Sukses", "Berhasil re-assigned worker", "success");
	          window.location.reload();
	        }else{
	          alertPopup("Sukses", "Berhasil assigned worker", "success");
	          window.location.reload();
	        }
	        $('#assignDate'+id).addClass("datepicker");
	      },
	      error: function (xhr, ajaxOptions, thrownError) {
	          swal("error!", thrownError, "error");
	      }
	    });
    }
}

function saveAssignDate(id) //jika date nya di isis manual
{
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $.ajax({
            url:"../saveAssignDate/"+id+"",
            data:{
                'assignDate':$('[name="assignDate'+id+'"]').val(),
                'id':id
            },
            dataType:'JSON',
            type:'GET',
            success:function(data){
            alertPopup("Success", "Success setting date", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
        $('#assignDate'+id).addClass("datepicker");
    }else{
        $.ajax({
            url:"assignmentFA/saveAssignDate/"+id+"",
            data:{
                'assignDate':$('[name="assignDate'+id+'"]').val(),
                'id':id
            },
            dataType:'JSON',
            type:'GET',
            success:function(data){
            alertPopup("Success", "Success setting date", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
        $('#assignDate'+id).addClass("datepicker");
    }
}

function saveReAssignedDate(id)
{
  $('#selectWorker'+id).removeAttr("disabled", "disabled");
  /*$.ajax({
      url:"assignmentFA/saveReAssignedDate/"+id+"",
      //data:{'reassignedDate':$('[name="reassignedDate'+id+'"]').val(),'id':id},
       dataType:'JSON',
       type:'GET',
      success:function(data){
	      swal(
	        'Sukses',
	        'Data Reassigned Tersimpan',
	        'success'
	      );
	      window.location.reload(data);
      },
      error: function (xhr, ajaxOptions, thrownError) {
     		swal("error!", thrownError, "error");
    	}
  });*/
}

function saveUrgent(cek,id)
{
	var urgent_status;
	var srch = $(".srch").attr("data-srch");
	if(cek.checked)
	{
		urgent_status = 1;
	}
	else
	{
		urgent_status = 0;
	}
	if(srch == "srch"){
		$.ajax({
			url:"../saveUrgent/"+id+"",
			data:{'urgent_status':urgent_status,'id':id},
			dataType:'JSON',
	    type:'GET',
	    success:function(data){
				alertPopup("Success", "Success update urgent status", "success");
				window.location.reload();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
			}
		});
	}else{
		$.ajax({
			url:"assignmentFA/saveUrgent/"+id+"",
			data:{'urgent_status':urgent_status,'id':id},
			dataType:'JSON',
	    type:'GET',
	    success:function(data){
				alertPopup("Success", "Success update urgent status", "success");
				window.location.reload();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
			}
		});
	}
}

function savePriority(id)
{
    priority = $('[name="priority'+id+'"]').val();
    var srch = $(".srch").attr("data-srch");
    if(srch == "srch"){
        $.ajax({
        url:"../savePriority/"+id+"",
        data:{'priority': priority},
        dataType:'JSON',
        type:'GET',
        success:function(data){
        alertPopup("Success", "Priority update success", "success");
        window.location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
        }
        });
    }
    else{
        $.ajax({
            url:"assignmentFA/savePriority/"+id+"",
            data:{'priority': priority},
            dataType:'JSON',
            type:'GET',
            success:function(data){
                alertPopup("Success", "Priority update success", "success");
                window.location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
    }
}

function download_doc() {
  $.ajax({
   url: 'assignmentFA/download_document_assignment_fa',
   type: 'POST',
    headers: {
    	'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    success:function(data){
       console.log(data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
    	swal("error!", thrownError, "error");
    }
  });
}

function uploadModal(){
    console.log('processing');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(){
            swal("Berhasil", "Update Data Berhasil!", "success");
            window.location.reload();
        },
        error: function(xhr, ajaxOptions, thrownError){
            swal("error!", thrownError, "error");
        }
    });
}

function reloadPage() {
    alertLoading('Mengambil data dari SIMPEL', 'Please wait.. don\'t refresh');
    $.ajax({
        url: 'assignmentFA/refresh',
        type: 'POST',
        dataType: 'JSON',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.data.status = "Update"){
                alertPopup("Success!", "Update Data", "success");
                window.location.reload();
            }else if(data.data.status == "Data Kosong"){
                alertPopup("Success!", "Data Up To Date", "success");
                window.location.reload();
            }
        },
        error: function (xhr, ajaxOptions, thrownError){
            alertPopup("error!", thrownError, "error");
        }
    });
}

function filterModal() {
    var name_search = $("form#formFilterAssignment input[name='name_search']").val();
    var option_checked = $('input[name="optradio"]:checked').val();

    if(( name_search == '') || (option_checked ==  undefined))
    {
        alertPopup('Error',' Please Complete Insert Name or Option Select ','error');
    }

    fetch_data_by_filter(option_checked, name_search);
    swal.close();
    $('#filterModalFA').modal('hide');

}

function fetch_data_by_filter(option_checked, name_search) {
    alertLoading('Sedang mengambil data', 'Please wait.. don\'t refresh');
    $('#tableAssignmentFA').dataTable().api().destroy();
    $('#tableAssignmentFA').dataTable({
        processing: true,
        serverSide: true,
        order: [0, 'desc'],
        ajax: {
            type: 'POST',
            url: '{{route('transaction.assignmentFA.getDataByFilter')}}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data: {
                option_checked: option_checked,
                name_search: name_search
            }
        },
        columns : [
            {
                "render":function(data,type,row,meta)
                {
                    return '<input type="checkbox" name="checkFA" class="rowFATrans" onchange="checkSelection('+row.fa_transaction_id+')">'
                }
            },
            {
                // this for numbering table
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                "mRender" : function(data,type,row,meta)
                {
                    if(row.nomen){
                        return row.nomen + '<button class="btn btn-primary" onclick="detailNomen('+row.nomen+')" data-toggle="modal" data-target="#detailNomen" style="margin-left: 5px;"><i class="fa fa-search-plus"> </button>';
                    }else{
                        return '';
                    }
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                    return row.dispatch_group ? row.dispatch_group : '';
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                return row.dispatch_group_descr ? row.dispatch_group_descr : '';
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                    if(row.customer){
                        return row.customer.address ? row.customer.address+' '+row.customer.city : '';
                    }else{
                        return '';
                    }
                }
            },
            { "data": "fa_transaction_id" },
            { "data": "fa_type_cd" },
            { "data": "descr" },
            {
                "mRender": function (data, type, row, meta) {
                    if(row.customer){
                        return row.customer.pcezbk ? row.customer.pcezbk : '';
                    }else{
                        return '';
                    }
                }
            },
            { "data": "status" },
            { "data": "created_date_fa" },
            {
                "render": function(data, type, row){
                    var $select = $("<select></select>", {
                        "name":'workerName'+row.id,
                        'onchange':'saveWorker('+row.id+')',
                        'class':'form-control',
                        'id':'selectWorker'+row.id
                    });

                    $.ajax({
                        url:"assignmentFA/getWorker/1",
                        data:'kosong',
                        dataType:'JSON',
                        type:'GET',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        async:false,
                        success:function(data){

                            $select.append('<option value=""> - Select Worker  - </option>');

                            $.each(data.worker, function(k,v){
                                var $option = $("<option></option>", {
                                    "text": v.username,
                                    "value": v.id
                                });

                                if(row.worker_id == v.id){
                                    $option.attr("selected", "selected");
                                    $select.attr("disabled", "disabled");
                                }
                                $select.append($option);
                            });
                        }
                    });
                    return $select.prop("outerHTML");
                }
            },
            {
                "mRender" : function(data,type,row,meta)
                {
                    if(row.worker_id){
                        return '<input type="button" class="btn btn-primary" id="re_('+row.id+')" onclick="saveReAssignedDate('+row.id+')" value="RE">';
                    }else{
                        return '<input type="button" class="btn btn-primary" id="re_('+row.id+')" onclick="saveReAssignedDate('+row.id+')" value="RE" disabled="disabled">';
                    }
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                    var assign_date_val;
                    if(row.assign_date == "" || row.assign_date == null)
                    {
                        assign_date_val = "";
                    }
                    else
                    {
                        assign_date_val = row.assign_date;
                    }

                    return "<input type='text' autocomplete='off' name='assignDate"+row.id+"' style='width:150px;' class='form-control datepicker' value='"+assign_date_val+"'  onchange='saveAssignDate("+row.id+")'>";
                }
            },
            {
                "data": "reassigned_date"
            },
            {
                "mRender": function (data, type, row, meta) {
                    return row.assign_status ? row.assign_status : '';
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                    var urgent_status_val;
                    if(row.urgent_status == "" || row.urgent_status == null || row.urgent_status == 0)
                    {
                        urgent_status_val = "";
                    }
                    else
                    {
                        urgent_status_val = 'checked';
                    }

                    return "<input type='checkbox' "+urgent_status_val+" name='urgent_status"+row.id+"' class='' onchange='saveUrgent(this,"+row.id+")'  value='true'>";
                }
            },
            {
                "mRender":function(data,type,row,meta){
                    (row.priority_status == '1') ? priority1= 'selected' : priority1='';
                    (row.priority_status == '2') ? priority2= 'selected' : priority2='';
                    (row.priority_status == '3') ? priority3= 'selected' : priority3='';
                    (row.priority_status == '4') ? priority4= 'selected' : priority4='';
                    (row.priority_status == '5') ? priority5= 'selected' : priority5='';

                    return '<select name="priority'+row.id+'" class="form-control" onchange="savePriority('+row.id+')">'+
                        '<option '+priority1+'> 1  </option>'+
                        '<option  '+priority2+'> 2  </option>'+
                        '<option  '+priority3+'>  3 </option>'+
                        '<option  '+priority4+'>  4 </option>'+
                        '<option  '+priority5+'>  5 </option>'+
                        '</select>';
                }
            },
            {
                "mRender": function (data, type, row, meta) {
                    if(row.sending_status == 'Pending'){
                        return row.sending_status;
                    }else{
                        return 'Received';
                    }
                }
            },
        ],
        "columnDefs": [{
            "targets": 0,
            "orderable": false
        }],
        responsive: true,
        "initComplete": function (settings, json) {
            datePicker();
        }
    });
}

function detailNomen(nomen) {
    $('#detailNomen').modal('hide');
    alertLoading('Sedang mengambil data', 'Please wait.. don\'t refresh');
    $.ajax({
       url: 'assignmentFA/detailNomen/'+nomen,
       type: 'GET',
       dataType: 'JSON',
       success: function (data) {
           $('#name_konsumen').text(data.data.customer_name);
           $('#alamat_nomen').text(data.data.address+' '+data.data.city);
           $('#telp_nomen').text(data.data.phone);
           swal.close();
       },
        error: function (xhr, ajaxOptions, thrownError){
            alertPopup("error!", thrownError, "error");
        }
    });
}
</script>
