@extends('layouts.app') 
@section('head')
    Transaction | Assignment FA
@endsection
@section('title')
<i class="fa fa-home"></i> Transaction  | <span style="color:#2B6B97"> Assignment FA </span>
@endsection
@section('button')
<div class="row forButtonTop">
	<div class="col-md-12">
		<form method="get" action="{{ URL::to('transaction/assignmentFA/getData/srch')}}">
			{{ csrf_field() }}
			<span>Assignment FA Date : </span>
			<div style="display: inline-block;width: 100px" >
				<input type="text" class="form-control datepicker" name="startDateTrans" autocomplete="off" style="border-radius:10px">
			</div>
			<span style="vertical-align: middle;"><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
			<span style="color: #8B9AAD;font-weight: bold; margin-right: 10px;"> > </span>
			<div style="display: inline-block;width: 100px">
				<input type="text" class="form-control datepicker" name="endDateTrans" autocomplete="off" style="border-radius:10px">
			</div>
			<span style="vertical-align: middle;"><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
			<input type="submit" class="btn btn-primary white btnTop" style="margin-left:5px;" value="OK">
			<a  class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalFA">
				<i class="fa fa-filter" aria-hidden="true"></i> Filter
			</a>
			<a href="{{ URL::to('transaction/assignmentFA/export/xlsx' )}}" class="btn btn-primary white btnTop" style="margin-left:5px;">
				<i class="fa fa-download" aria-hidden="true"></i> Download
			</a>
            <a  class="btn btn-primary white btnTop" data-toggle="modal" data-target='#uploadModalFA' style="margin-left:5px;">
                <i class="fa fa-upload" aria-hidden="true"></i> Upload
            </a>
			<a class="btn btn-primary white btnTop" style="margin-left:5px;" onclick="reloadPage()">
				<i class="fa fa-refresh" aria-hidden="true"></i> Refresh
			</a>
		</form>
	</div>
</div>
@endsection

@section('content')
	<section class="content">
	  <!-- /.box-header -->
	    <div class="box box-default">
            {{--<div class="box-header with-border">--}}
            {{--<h3 class="box-title">Assignment FA</h3>--}}
            {{--<div class="box-tools pull-right">--}}
            {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
            {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!-- /Box Header -->
            <!--- Box Body --->
            <div class="box-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3" style="text-align:right;margin-bottom: 15px;">
                            <form method="get" action="{{ URL::to('transaction/assignmentFA/search/srch')}}">
                                <input type="text" class="form-control search" name="search" placeholder="Search"/>
                            </form>
                        </div>
                        <div class="col-md-9">
                            <form id="formUpdateAssigned" class="form-horizontal">
                                <div class="showWorker form-group" id="showWorker" hidden="hidden">
                                    <label class="col-sm-3 control-label" style="text-align: left;padding-right: 0;">Worker to Assigned :</label>
                                    <div class="col-md-3" style="padding-right: 0;">
                                        <select name="worker" class="form-control">
                                            <option> - Select Worker  - </option>
                                            @foreach($getWorkers as $rows)
                                                <option value='{{$rows->worker_id}}'>{{$rows->worker_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-warning" type="button" onclick="resetMark(this)"> Reset </button>
                                        <button class="btn btn-primary" type="button" onclick="updateAssigned()" style="padding-left: 20px;padding-right: 20px;"> Submit FA </button>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <form id="formUpdateAssigned">
                            <table class="tableCus table-striped js-sort-table">
                                <thead class="thead" style="width: 3151px;font-size:14px;">
                                    <tr style="background-color:#8B9AAD;color:#fff;">
                                        <th style="width: 34px;">
{{--                                            <input type="checkbox"  onchange="checkAllTrans(this)"  class="custom-control-input checkall" id="customControlInline">--}}
                                        </th>
                                        <th style="width: 42px;" class="js-sort-number">No</th>
                                        <th style="width: 100px;" class="js-sort-number">Nomen</th>
                                        <th style="width: 100px;">FA Type</th>
                                        <th style="width: 300px;">Description</th>
                                        <th style="width: 85px;" class="js-sort-number">PC-EZ-BK</th>
                                        <th style="width: 170px;">Assign To</th>
                                        <th style="width: 70px"></th>
                                        <th style="width: 191px;" class="js-sort-input">Assign Date/Time</th>
                                        <th style="width: 170px;" class="js-sort-date">Re-Assign Date/Time</th>
                                        <th style="width: 135px;" class="js-sort-string">Dispatch Group</th>
                                        <th style="width: 155px;">Dispatch Group Description</th>
                                        <th style="width: 300px;" class="js-sort-string">Customer Name</th>
                                        <th style="width: 120px;" class="js-sort-number">Phone</th>
                                        <th style="width: 400px;" class="js-sort-string">Address</th>
                                        <th style="width: 100px;" class="js-sort-number">No.FA</th>
                                        <th style="width: 75px;">FA Status</th>
                                        <th style="width: 85px;">Status Customer</th>
                                        <th style="width: 160px;" class="js-sort-date">FA Date/Time</th>
                                        <th style="width: 105px;">Assignment Status</th>
                                        <th style="width: 67px;">Urgent</th>
                                        <th style="width: 75px;">Priority</th>
                                        <th style="width: 90px;">Status HH</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody id="table" class="tbody" style="width: 3149px;font-size:14px;">
                                    @foreach ($data as $key=>$value)
                                        @if($value->assign_status == 'Started' or $value->assign_status == 'Complete' or $value->assign_status == 'Cancel')
                                            <tr role="row" style="background-color:#d4d4d4;border:1px solid #f4f4f4;">
                                                <td style="width: 31px;text-align: center;"><input type="checkbox" disabled></td>
                                                <td style="width: 42px;text-align: center;">{{$num++}}</td>
                                                <td style="width: 100px;">{{$value->nomen}}</td>
                                                <td style="width: 100px;">{{$value->fa_type_cd}}</td>
                                                <td style="width: 300px;">{{$value->descr}}</td>
                                                <td style="width: 85px;">{{$value->pcezbk}}</td>
                                                <td style="width: 170px;">
                                                    <select name="workerName{{$value->id}}" class="form-control" onchange="saveWorker({{$value->id}})" id="selectWorker{{$value->id}}" disabled="disabled">
                                                        <option value="{{$value->worker_id}}">{{$value->worker_name}}</option>
                                                    </select>
                                                </td>
                                                <td style="width: 70px;text-align: center;">
                                                    <input type="button" class="btn btn-primary" value="RE" disabled="disabled">
                                                </td>
                                                <td style="width: 60px;"><input type='text' autocomplete='off' class='form-control datepicker' value='{{$value->assign_date}}'  style='width: 170px;' disabled></td>
                                                <td style="width: 170px;">
                                                    @php
                                                        $reassigned = $value->reassigned_date;
                                                        $date = date_create($reassigned);
                                                        $result = date_format($date,"Ymd H:i:s");
                                                    @endphp
                                                    <span style="display:none;">{{$result}}</span>{{$reassigned}}
                                                </td>
                                                <td style="width: 135px;">{{$value->dispatch_group}}</td>
                                                <td style="width: 155px;">{{$value->description}}</td>
                                                <td style="width: 300px;">{{$value->customer_name}}</td>
                                                <td style="width: 120px;">{{$value->phone}}</td>
                                                <td style="width: 400px;">{{$value->address}}</td>
                                                <td style="width: 100px;">{{$value->fa_transaction_id}}</td>
                                                <td style="width: 75px;text-align: center;">{{$value->status}}</td>
                                                <td style="width: 85px;text-align: center;">{{$value->status_customer}}</td>
                                                <td style="width: 160px;">
                                                    @php
                                                        $created_date_fa = date("d/m/Y H:i:s", strtotime($value->created_at));
                                                        $d = date_create($value->created_at);
                                                        $dhidde = date_format($d,"Ymd H:i:s");
                                                        echo '<span style="display:none;">'.$dhidde.'</span>'.$created_date_fa;
                                                    @endphp
                                                </td>
                                                <td style="width: 105px;">{{$value->assign_status}}</td>
                                                <td style="width: 67px;text-align: center;">

                                                    @php
                                                        if($value->urgent_status == "false" || $value->urgent_status == null || $value->urgent_status == 0){
                                                            $urgent_status_val = "";
                                                        }else{
                                                            $urgent_status_val = 'checked';
                                                        }
                                                        echo "<input type='checkbox' $urgent_status_val disabled>";
                                                    @endphp

                                                </td>
                                                <td style="width: 75px;">
                                                    <select class="form-control" disabled>
                                                        @if($value->priority_status == '')
                                                            @for ($x = 1; $x <= 5; $x++)
                                                            <option value="{{$x}}"> {{$x}}  </option>
                                                            @endfor
                                                        @else
                                                            @for ($x = 1; $x <= 5; $x++)
                                                            <option value="{{$x}}" {{($x == $value->priority_status) ? "selected":""}}> {{$x}}  </option>
                                                            @endfor
                                                        @endif
                                                    </select>
                                                </td>
                                                <td style="width: 90px;">{{$value->sending_status}}</td>
                                            </tr>
                                        @else
                                            <tr role="row">
                                                <td style="width: 31px;text-align: center;"><input type="checkbox" name="checkFA[]" value="{{$value->id}}" data-dispatch="{{$value->dispatch_group}}" class="rowFATrans" onchange="checkSelection(this, {{$value->id}})"></td>
                                                <td style="width: 42px;text-align: center;">{{$num++}}</td>
                                                <td style="width: 100px;">{{$value->nomen}}</td>
                                                <td style="width: 100px;">{{$value->fa_type_cd}}</td>
                                                <td style="width: 300px;">{{$value->descr}}</td>
                                                <td style="width: 85px;">{{$value->pcezbk}}</td>
                                                <td style="width: 170px;">
                                                    @if($value->id_worker == '')
                                                        <select name="workerName{{$value->id}}" class="form-control" onchange="saveWorker('{{$value->id}}')" id="selectWorker{{$value->id}}">
                                                            <option> - Select Worker  - </option>
                                                            @foreach($workers as $rows)
                                                                @if(trim($rows->dispatch_group) == trim($value->dispatch_group))
                                                                    <option value='{{$rows->worker_id}}'>{{$rows->worker_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <select name="workerName{{$value->id}}" class="form-control" onchange="saveWorker('{{$value->id}}')" id="selectWorker{{$value->id}}" disabled="disabled">
                                                            @foreach($workers as $rows)
                                                                @if(trim($rows->dispatch_group) == trim($value->dispatch_group))
                                                                    <option value="{{$rows->worker_id}}" {{($rows->worker_id == $value->worker_id) ? "selected":""}}> {{$rows->worker_name}} </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    @endif
                                                </td>
                                                <td style="width: 70px;text-align: center;">

                                                    @if($value->id_worker)
                                                        <input type="button" class="btn btn-primary" id="re_{{$value->id}}" onclick="saveReAssignedDate('{{$value->id}}')" value="RE">
                                                    @else
                                                        <input type="button" class="btn btn-primary" id="re_{{$value->id}}" onclick="saveReAssignedDate('{{$value->id}}')" value="RE" disabled="disabled">
                                                    @endif

                                                </td>
                                                <td style="width: 189px;">
                                                    @if($value->assign_date == "" || $value->assign_date == null)
                                                        <input type='text' autocomplete='off' name='assignDate{{$value->id}}' class='form-control datepicker' value=''  style='width: 170px;' onchange='saveAssignDate({{$value->id}})'>
                                                    @else
                                                        @php $assign_date = date("d-m-Y", strtotime($value->assign_date)); @endphp
                                                        <input type='text' autocomplete='off' name='assignDate{{$value->id}}' class='form-control datepicker' value='{{$assign_date}}'  style='width: 170px;' onchange='saveAssignDate({{$value->id}})'>
                                                    @endif
                                                </td>
                                                <td style="width: 170px;">
                                                    @php
                                                    $reassigned = $value->reassigned_date;
                                                    $date = date_create($reassigned);
                                                    $result = date_format($date,"Ymd H:i:s");
                                                    @endphp
                                                    <span style="display:none;">{{$result}}</span>{{$reassigned}}

                                                </td>
                                                <td style="width: 135px;">{{$value->dispatch_group}}</td>
                                                <td style="width: 155px;">{{$value->description}}</td>
                                                <td style="width: 300px;">{{$value->customer_name}}</td>
                                                <td style="width: 120px;">{{$value->phone}}</td>
                                                <td style="width: 400px;">{{$value->address}}</td>
                                                <td style="width: 100px;">{{$value->fa_transaction_id}}</td>
                                                <td style="width: 75px;text-align: center;">{{$value->status}}</td>
                                                <td style="width: 85px;text-align: center;">{{$value->status_customer}}</td>
                                                <td style="width: 160px">
                                                    @php
                                                    $created_date_fa = date("d/m/Y H:i:s", strtotime($value->created_at));
                                                    $d = date_create($value->created_at);
                                                    $dhidde = date_format($d,"Ymd H:i:s");
                                                    @endphp
                                                    <span style="display:none;">{{$dhidde}}</span>{{$created_date_fa}}
                                                </td>
                                                <td style="width: 105px;">{{$value->assign_status}}</td>
                                                <td style="width: 67px;text-align: center;">

                                                    @if($value->urgent_status == "false" || $value->urgent_status == null || $value->urgent_status == 0)
                                                        <input type='checkbox' name='urgent_status{{$value->id}}' class='' onchange='saveUrgent(this,"{{$value->id}}")'  value='false'>
                                                    @else
                                                        <input type='checkbox' name='urgent_status{{$value->id}}' class='' onchange='saveUrgent(this,"{{$value->id}}")'  value='true' checked>
                                                    @endif

                                                </td>
                                                <td style="width: 75px;">
                                                    <select name="priority{{$value->id}}" class="form-control" class='srch' data-srch='srch' onchange="savePriority({{$value->id}})">
                                                        @if($value->priority_status == '')
                                                            @for ($x = 1; $x <= 5; $x++)
                                                            <option value="{{$x}}"> {{$x}}  </option>
                                                            @endfor
                                                        @else
                                                            @for ($x = 1; $x <= 5; $x++)
                                                            <option value="{{$x}}" {{($x == $value->priority_status) ? "selected":""}}> {{$x}}  </option>
                                                            @endfor
                                                        @endif
                                                    </select>
                                                </td>
                                                <td style="width: 90px;">{{$value->sending_status}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="col-md-2">
                    <span id="selected_data">Selected Data:   </span>
                </div>
                <div class="col-md-10" style="text-align: right;">
                    <span id="v_open" style="margin-right: 5px;">Open: {{ $open }}</span>
                    <span id="v_assigned" style="margin-right: 5px;">Assigned: {{ $assigned }}</span>
                    <span id="v_ontheway" style="margin-right: 5px;">On the way: {{ $ontheway }}</span>
                    <span id="v_started" style="margin-right: 5px;">Started: {{ $started }}</span>
                    <span id="v_pickup" style="margin-right: 5px;">Pick up: {{ $pickup }}</span>
                    <span id="v_cancel" style="margin-right: 5px;">Cancel by worker: {{ $cancel }}</span>
                    <span id="v_reassigned" style="margin-right: 5px;">Re-Assign:  {{ $reAssigned }}</span>
                    <span id="v_complete" style="margin-right: 5px;">Complete:  {{ $complete }}</span>
                    <span id="v_total" style="margin-right: 5px;">Total: {{ $total }}</span>
                </div>
                <div class="col-md-12">
                    {{ $data->render() }}
                </div>
            </div>
        </div>
	  <!-- /.box body -->
	</section>
	<!-- /section -->

	<!-- Modal   Add -->
	<div class="modal fade" id="filterModalFA" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false">
	    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Filter</h4>
                </div>
                <div class="modal-body"  style="background-color: #FBFBFB">
                    <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                    <form id="formFilterAssignment" method="get" action="{{ URL::to('transaction/assignmentFA/filterAssignmentFA/srch')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="fa_type"> <label style="font-weight: normal; margin-right: 10px;"> FA Type </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Fa Type" name="fa_type_cd">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="pcezbk"> <label style="font-weight: normal; margin-right: 10px;"> PC_EZ_BK </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="PC-EZ-BK" name="pc_ez_bk">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="status"> <label style="font-weight: normal; margin-right: 10px;"> Assignment Status </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Assignment Status" name="assign_status">
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center">
                            <input type="submit" class="btn btn-primary btnFooterModal" value="OK">
                        </div>
                    </form><!-- /form -->
                </div><!-- / Modal Body -->                
            </div>
	    </div>
	</div>

	<!-- MODAL UPLOAD Excel FA -->
	<div class="modal fade" id="uploadModalFA" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="false">
	    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Upload</h4>
                </div>
                <div class="modal-body"  style="background-color: #FBFBFB">
                    <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                    <form method="post" action="{{ route('transaction.import' )}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="file" name="file" class="form-control">
                        <br>
                        <button class="btn btn-primary btnFooterModal">Submit</button>
                    </form><!-- /form -->
                </div><!-- / Modal Body -->
            </div>
	    </div>
	</div>

<!--{{--Modal Nomen Detail--}}
	<div class="modal fade" id="detailNomen" tabindex="-1" role="dialog" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          	<span aria-hidden="true" style="color:#fff">&times;</span>
          </button>
          <h4 class="modal-title" style="text-align:center">Filter</h4>
        </div>
        <div class="modal-body"  style="background-color: #FBFBFB">
          <div class="alert alert-danger" id="error-alert" style="display: none"></div>
          <form id="formFilterAssignment">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">
              <div class="col-sm-4">
              	<label>Nama Konsumen</label>
              </div>
              <div class="col-sm-6">
              	<span id="name_konsumen"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
              	 <label>Alamat</label>
              </div>
              <div class="col-sm-6">
              	<span id="alamat_nomen"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4">
              	<label>No. Telp</label>
              </div>
              <div class="col-sm-6">
              	<span id="telp_nomen"></span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
	</div>-->
@endsection
@section('js')
  @include('transaction.assignmentFA.js_assignmentFA')
@endsection

