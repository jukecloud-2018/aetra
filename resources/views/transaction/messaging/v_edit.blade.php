<form id="formMessage">
	<input type="hidden" name="id" value="{{$getData->id_send}}">
	<input type="hidden" name="status" value="Send">
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> TO </label>
			</div>
			<div class="col-sm-9">
				@if($getData->status_all == 1)
					<input type="checkbox" name="allGroup" id="allGroup" onchange="selectAll(this)" value="all" checked> ALL Group
					<select name="worker_id" id="worker_id" class="form-control" disabled="disabled">
						<option value=""> Pilih Worker </option>
						@foreach($worker as $wo)
						<option value="{{$wo->worker_id}}" {{ ($wo->worker_id == $getData->worker_id) ? "selected" : ""}}> {{$wo->username}}  </option>
						@endforeach
					</select>
				@elseif($getData->status_all == 0)
					<input type="checkbox" name="allGroup" id="allGroup" onchange="selectAll(this)" value="all"> ALL Group
					<select name="worker_id" id="worker_id" class="form-control">
						<option value=""> Pilih Worker </option>
						@foreach($worker as $wo)
						<option value="{{$wo->worker_id}}" {{ ($wo->worker_id == $getData->worker_id) ? "selected" : ""}}> {{$wo->username}}  </option>
						@endforeach
					</select>
				@endif
			</div>
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> Subject </label>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control" id=""  placeholder="Subject" value="{{$getData->subject}}" name="subject">
			</div>
		</div>
	</div>
	
	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> Messagge </label>
			</div>
			<div class="col-sm-9">
				<textarea name="message" rows="10" class="form-control" id="editor">{{$getData->message}}</textarea>
			</div>
		</div>
	</div>
	
	<div class="footer" style="text-align: center">
		<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateMessage('{{$getData->id}}')"> <i class="fa fa-send"></i> Send</button>
		<button type="button" class="btn btn-primary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
	</div>
</form><!-- /form -->