<script type="text/javascript">

//for datatable
$(document).ready(function() {
  var table = $('#tableMessaging').DataTable( {
    "scrollY": 450,
    "scrollX": true
  });
});

//function compose(){
//  $('#compose').modal('show');
//}
function compose()
{
  var body;
  var stat = $("#kirim").attr("data-stat");
  if(stat == 'draf'){
    $.ajax({
      url:'../../messaging/compose/cmps',
      // dataType:'JSON',
      type:'GET',
      success:function(data){
        body = data;
        $('#compose').find('.modal-title').text('Compose Message');
        $('#compose').find('.modal-body').html(body);
        $('#compose').modal('show');
      },
        error: function (xhr, ajaxOptions, thrownError) {
              swal("error!", thrownError, "error");
      }
    });
  }else if(stat == 'send'){
    $.ajax({
      url:'messaging/compose/cmps',
      // dataType:'JSON',
      type:'GET',
      success:function(data){
        body = data;
        $('#compose').find('.modal-title').text('Compose Message');
        $('#compose').find('.modal-body').html(body);
        $('#compose').modal('show');
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      }
    });
  }
}

function editMessage(id){
  var status = 'Send';
  var message = $('[name="message"]').val();
  var subject = $('[name="subject"]').val();
  var worker_id = $('#worker_id').val();
  var allGroup = $('[name="allGroup"]:checked').val();
  $.ajax({
      url:'../../messaging/edit/'+id,
      //data:{'subject':subject,'message':message,'worker_id':worker_id,'status':status,'allGroup':allGroup},
      data:$('form#formMessage').serialize(),
      //dataType:'JSON',
      type:'GET',
      success:function(data){
        body = data;
        $('#compose').find('.modal-title').text('Compose Message');
        $('#compose').find('.modal-body').html(body);
        $('#compose').modal('show');
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      }
    });
}

function detailMessage(id)
{
  var body;
  var stat = $("#kirim").attr("data-stat");
  //var id = $("#detail").attr("data-id");
  if(stat == 'draf'){
    $.ajax({
      url:'../../messaging/detail/'+id,
      type:'GET',
      success:function(data){
        body = data;
        $('#compose').find('.modal-title').text('Message');
        $('#compose').find('.modal-body').html(body);
        $('#compose').modal('show');
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      }
    });
  }else if(stat == 'send'){
    $.ajax({
      url:'messaging/detail/'+id,
      type:'GET',
      success:function(data){
        body = data;
        $('#compose').find('.modal-title').text('Message');
        $('#compose').find('.modal-body').html(body);
        $('#compose').modal('show');
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      }
    });
  }
}

function selectAll(resp){
  if (resp.checked){
    $('#worker_id').attr("disabled", "disabled");
  }else{
    console.log(resp.checked);
    $('#worker_id').removeAttr("disabled", "disabled");
  }
}

function sendMessage()
{
  var status = 'Send';
  var message = $('[name="message"]').val();
  var subject = $('[name="subject"]').val();
  var worker_id = $('#worker_id').val();
  var allGroup = $('[name="allGroup"]:checked').val();
  $.ajax({
    url:"{{route('transaction.messaging.store')}}",
    data:{'subject':subject,'message':message,'worker_id':worker_id,'status':status,'allGroup':allGroup},
    dataType:'JSON',
    type:'POST',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}',
    },
    success:function(data){
      if(data.errors){
        $('.alert-danger').show();
        $.each(data.errors,function(key,value){
          $('.alert-danger').append('<li>'+value+'</li>');
        })
      }else{
        swal('Success','Messaging has been send successfully!','success');
        $(location).attr('href','messaging');
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      swal("error!", thrownError, "error");
    }
  })
}

function draftMessage()
{
  var status = 'Draf';
  var message = $('[name="message"]').val();
  var subject = $('[name="subject"]').val();
  var worker_id = $('#worker_id').val();
  var allGroup = $('[name="allGroup"]:checked').val();
  $.ajax({
    url:"{{route('transaction.messaging.store')}}",
    data:{'subject':subject,'message':message,'worker_id':worker_id,'status':status,'allGroup':allGroup},
    dataType:'JSON',
    type:'POST',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}',
    },
    success:function(data){
      if(data.errors){
        $('.alert-danger').show();
        $.each(data.errors,function(key,value){
          $('.alert-danger').append('<li>'+value+'</li>');
        })
      }else{
        swal('Success','Messaging has been draf successfully!','success');
        $(location).attr('href','messaging');
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      swal("error!", thrownError, "error");
    }
  })
}

function updateMessage(id)
{
  var data = $('#formMessage').serialize();
  $.ajax({
    url:"/transaction/messaging/update/"+id,
    data:data,
    dataType:'JSON',
    type:'GET',
    headers: {
      'X-CSRF-TOKEN' : '{{csrf_token()}}',
    },
    success:function(data){
      if(data.errors)
      {
        $('.alert-danger').show();
        $.each(data.errors,function(key,value){
          $('.alert-danger').append('<li>'+value+'</li>');
        })
      }
      else
      {
        swal('Success','Messaging has been saved successfully!','success');
        $(location).attr('href','/transaction/messaging');
      }
    },
      error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
    }
  })   
}



// for delete message
function deleteMessage(id)
{
  var id_send = $("#id_send").attr("data-idSend");
  swal({
    title: "Are you sure?",
    text: "Delete this data ?",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  },
  function(willDelete){
    if (!willDelete) return;
    $.ajax({
      url:'/transaction/messaging/destroy/'+id,
      data:{'id':id,'id_send':id_send},
      dataType:'JSON',
      type:'GET',
      headers: {
        'X-CSRF-TOKEN' : '{{csrf_token()}}',
      },
      success:function(get){
        if(get.status=='success'){
          swal('Success','Messaging has been delete successfully!','success');
          $(location).attr('href','messaging');   
          location.reload();
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      },
    })
  });
}
</script>