@extends('layouts.app')

@section('head')
    Transaction | Draft
@endsection

@section('title')
  <i class="fa fa-home"></i> Transaction  | <span style="color:#2B6B97"> Draft </span>
@endsection
@section('button')
  <div class="forButtonTop">
    <button id="kirim" class="btn btn-primary white btnTop" onclick="compose(this)" data-stat='draf'>
      <i class="fa fa-edit" aria-hidden="true"></i> Compose
    </button>

    <a href="{{URL::to('transaction/messaging/status/draf')}}" class="btn btn-primary white btnTop" style="margin-left:5px;">
        <i class="fa fa-book" aria-hidden="true"></i> Draft
    </a>

    <a href="{{route('transaction.messaging.index')}}" class="btn btn-primary white btnTop" style="margin-left:5px;">
        <i class="fa fa-send" aria-hidden="true"></i> Send
    </a>
  </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        {{--<div class="box-header with-border">--}}
        {{--<h3 class="box-title">Send</h3>--}}
        {{--<div class="box-tools pull-right">--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- /Box Header -->
    
        <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableMessaging" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                                    </th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">No</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 100px!important;">To</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 350px!important;">Subject</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 20px!important;">Status</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 120px!important;">Date</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 130px!important;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($message as $key=>$value)
                                    <tr role="row">
                                        <td class="sorting_1">
                                            <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        </td>
                                        <td>{{++$key}}</td>
                                        @if($value->status_all == '1')
                                            <td> All Group</td>
                                        @else
                                        <td>
                                            <?php
                                                $to = DB::table('message_to as a')->join('workers as b','a.worker_id','=','b.worker_id')->where('a.id_send',$value->id)->get();
                                                foreach($to as $row){
                                                    echo '<span style="background: #ded6d6;">&nbsp;'.$row->worker_name.'&nbsp;</span> ';
                                                }
                                            ?>
                                        </td>
                                        @endif
                                        <td>{{$value->subject}}</td>
                                        <td>{{$value->status}}</td>
                                        <td>{{$value->created_at}}</td>
                                        <td>
                                            <a class="btn btn-success" id="detail" onclick="detailMessage('{{$value->id}}')" data-stat='draf'><i class="fa fa-search"></i></a> |
                                            @if($stat == 'draf')
                                            <a class="btn btn-success" id="edit" onclick="editMessage('{{$value->id}}')" data-stat='draf'><i class="fa fa-pencil"></i></a> |
                                            @endif
                                            <a class="btn btn-danger" id="id_send" onclick="deleteMessage('{{$value->id}}')" data-idSend='{{$value->id}}'>
                                            <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<div class="modal fade" id="compose" tabindex="-1" aria-hidden="false"  data-backdrop="false" style="top:-100px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 75%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Compose Message</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB"></div>
        </div>
    </div>
</div>
@endsection

@section('js')
  @include('transaction.messaging.js_messaging')
@endsection
