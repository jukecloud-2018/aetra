@foreach($message as $key=>$value)<form>
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> TO </label>
			</div>
			<div class="col-sm-9">
			@if($value->status_all == '1')
				All Group
			@else
				<?php 
					$to = DB::table('message_to as a')->join('workers as b','a.worker_id','=','b.worker_id')->where('a.id_send',$value->id)->get();
					foreach($to as $row){
						echo '<span style="background: #ded6d6;">&nbsp;'.$row->worker_name.'&nbsp;</span> ';
					}
				?>
			@endif
			</div>
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> Subject </label>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control" id=""  placeholder="Subject" value="{{$value->subject}}" name="subject">
			</div>
		</div>
	</div>

	<div class="form-group row">
		<div class="col-sm-12">
			<div class="col-sm-2">
				<label for="" class="col-sm-1 col-form-label"> Messagge </label>
			</div>
			<div class="col-sm-9">
				<textarea name="message" rows="10" class="form-control">{{$value->message}}</textarea>
			</div>
		</div>
	</div>
	<div class="footer" style="text-align: center">
		<button type="button" class="btn btn-primary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
	</div>
</form><!-- /form -->
@endforeach