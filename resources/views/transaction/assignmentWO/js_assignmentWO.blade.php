<script type="text/javascript">
//search data table
var $rows = $('#table tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

//get data WO dari SIMPEL
function reloadPage() {
  alertLoading('Mengambil data dari SIMPEL', 'Please wait.. don\'t refresh');
  $.ajax({
    url: 'assignmentWO/refresh',
    type: 'POST',
    dataType: 'JSON',
    headers: {
      'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    success:function(data){
      if(data.data.status == "Update"){
        alertPopup("Success!", "Update Data", "success");
        window.location.reload();
      }else if(data.data.status == "offline"){
        alertPopup("Offline!", "Site is offline", "error");
        window.location.reload();
      }
    },
    error: function (xhr, ajaxOptions, thrownError){
      alertPopup("error!", thrownError, "error");
    }
  });
}

function reAssign(id)
{
   $('#selectWorker'+id).removeAttr("disabled", "disabled");
}

//cek all WO
function checkAllWO(cek)
{
    if (cek.checked)
    {
        $('.rowWOTrans').prop('checked', cek.checked);
        $('.selectWorker').attr("disabled", "disabled");
        $('#showWorker').removeAttr("hidden", "hidden");
        var numberOfChecked = $('.rowWOTrans').filter(':checked').length;
        var totalOfChecked = $('.rowWOTrans').filter(':checkbox').length;
        var numberOfNotChecked = totalOfChecked - numberOfChecked;
        $('#selected_data').text("Selected Data: "+numberOfChecked);
    }
    else
    {
        $('.rowWOTrans').prop('checked', cek.checked);
        $('#showWorker').attr("hidden", "hidden");
        $('.selectWorker').removeAttr("disabled", "disabled");
        var numberOfChecked = $('.rowWOTrans').filter(':checked').length;
        var totalOfChecked = $('.rowWOTrans').filter(':checkbox').length;
        var numberOfNotChecked = totalOfChecked - numberOfChecked;
        $('#selected_data').text("Selected Data: "+numberOfChecked);
    }
}

function checkSelection(resp, id)
{
    var numberOfChecked = $('.rowWOTrans').filter(':checked').length;
    var totalOfChecked = $('.rowWOTrans').filter(':checkbox').length;
    var numberOfNotChecked = totalOfChecked - numberOfChecked;
    $('#selected_data').text("Selected Data: "+numberOfChecked);
    if (resp.checked){
        $('#selectWorker'+id).attr("disabled", "disabled");
        $('#showWorker').removeAttr("hidden", "hidden");
    }else{
        if(numberOfChecked != 0){
            $('#selectWorker'+id).removeAttr("disabled", "disabled");
            $('#showWorker').removeAttr("hidden", "hidden");
        }else{
            $('#selectWorker'+id).removeAttr("disabled", "disabled");
            $('#showWorker').attr("hidden", "hidden");
        }
    }
}

//save urgent
function saveUrgent(cek,id)
{
	var urgent_status;
	var srch = $(".srch").attr("data-srch");
	if(cek.checked)
	{
		urgent_status = 1;
	}
	else
	{
		urgent_status = 0;
	}
	if(srch == "srch"){
		$.ajax({
			url:"../saveUrgent/"+id+"",
			data:{'urgent_status':urgent_status,'id':id},
			dataType:'JSON',
	    type:'GET',
	    success:function(data){
				alertPopup("Success", "Success update urgent status", "success");
				window.location.reload();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
			}
		});
	}
  else{
		$.ajax({
			url:"assignmentWO/saveUrgent/"+id+"",
			data:{'urgent_status':urgent_status,'id':id},
			dataType:'JSON',
	    type:'GET',
	    success:function(data){
				alertPopup("Success", "Success update urgent status", "success");
				window.location.reload();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
			}
		});
	}
}

//save priority
function savePriority(id)
{
  priority = $('[name="priority'+id+'"]').val();
  var srch = $(".srch").attr("data-srch");
	if(srch == "srch"){
	  $.ajax({
      url:"../savePriority/"+id+"",
      data:{'priority': priority},
      dataType:'JSON',
      type:'GET',
      success:function(data){
      	alertPopup("Success", "Priority update success", "success");
      	window.location.reload();
      },
      error: function (xhr, ajaxOptions, thrownError) {
      	swal("error!", thrownError, "error");
      }
	  });
	}
	else{
		$.ajax({
      url:"assignmentWO/savePriority/"+id+"",
      data:{'priority': priority},
      dataType:'JSON',
      type:'GET',
      success:function(data){
      	alertPopup("Success", "Priority update success", "success");
      	window.location.reload();
      },
      error: function (xhr, ajaxOptions, thrownError) {
      	swal("error!", thrownError, "error");
      }
  });
	}
}

function saveWorker(id)
{
    worker = $('[name="workerName'+id+'"]').val();
    var srch = $(".srch").attr("data-srch");
    if(srch == "srch"){
      $.ajax({
        url:"../saveWorker/"+id+"",
        data:{
            'id': $('[name="id_assignmentwo'+id+'"]').val(),
            'assignDate':$('[name="assignDate'+id+'"]').val(),
            'worker': worker
        },
        dataType:'JSON',
        type:'GET',
        success:function(data){
          $('#re_'+id).removeAttr("disabled");
          $('#selectWorker'+id).attr("disabled", "disabled");
          if(data.data.reassign){
            alertPopup("Sukses", "Berhasil re-assigned worker", "success");
            window.location.reload();
          }else{
            alertPopup("Sukses", "Berhasil assigned worker", "success");
            window.location.reload();
          }
          $('#assignDate'+id).addClass("datepicker");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
      });
    }else{
      $.ajax({
        url:"assignmentWO/saveWorker/"+id+"",
        data:{
            'id': $('[name="id_assignmentfa'+id+'"]').val(),
            'assignDate':$('[name="assignDate'+id+'"]').val(),
            'worker': worker
        },
        dataType:'JSON',
        type:'GET',
        success:function(data){
          $('#re_'+id).removeAttr("disabled");
          $('#selectWorker'+id).attr("disabled", "disabled");
          if(data.data.reassign){
            alertPopup("Sukses", "Berhasil re-assigned worker", "success");
            window.location.reload();
          }else{
            alertPopup("Sukses", "Berhasil assigned worker", "success");
            window.location.reload();
          }
          $('#assignDate'+id).addClass("datepicker");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
      });
    }
}

function saveAssignDate(id) //jika date nya di isis manual
{
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $.ajax({
            url:"../saveAssignDate/"+id+"",
            data:{
                'assignDate':$('[name="assignDate'+id+'"]').val(),
                'id':id
            },
            dataType:'JSON',
            type:'GET',
            success:function(data){
            alertPopup("Success", "Success setting date", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
        $('#assignDate'+id).addClass("datepicker");
    }else{
        $.ajax({
            url:"assignmentWO/saveAssignDate/"+id+"",
            data:{
                'assignDate':$('[name="assignDate'+id+'"]').val(),
                'id':id
            },
            dataType:'JSON',
            type:'GET',
            success:function(data){
            alertPopup("Success", "Success setting date", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
        $('#assignDate'+id).addClass("datepicker");
    }
}

function updateAssigned(){
    var srch = $(".srch").attr("data-srch");
    if(srch == "srch"){
        $.ajax({
            url:"../multiSaveWorker",
            data: $('form#formUpdateAssigned').serialize(),
            type:'POST',
            dataType:'JSON',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                location.reload();
                $('#showWorker').attr("hidden", "hidden");
                if(data.data.reassigned){
                    alertPopup("Sukses", "Berhasil re-assigned worker", "success");
                }else{
                    alertPopup("Sukses", "Berhasil assigned worker", "success");
                }
                $('#assignDate'+id).addClass("datepicker");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
    }else{
        $.ajax({
            url:"assignmentFA/multiSaveWorker",
            data: $('form#formUpdateAssigned').serialize(),
            type:'POST',
            dataType:'JSON',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                location.reload();
                $('#showWorker').attr("hidden", "hidden");
                if(data.data.reassigned){
                    alertPopup("Sukses", "Berhasil re-assigned worker", "success");
                }else{
                    alertPopup("Sukses", "Berhasil assigned worker", "success");
                }
                $('#assignDate'+id).addClass("datepicker");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
            }
        });
    }
}
</script>
