 <div class="alert alert-danger" id="error-alert" style="display: none"></div>
        
           <form id="formSurveyTransaction">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Survey</label>
                </div>
                <div class="col-sm-8">
                    <select class="form-control" name="survey_id">
                      <option value=""> - Choose Survey  -</option>
                     @foreach($survey as $survey)
                        <option value="{{$survey->id}}"  {{($assignmentSurvey->survey_id == $survey->id ) ? "selected":""}}> {{$survey->survey_name}} </option>
                     @endforeach
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Start Date</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" name="start_date" class="form-control datepicker" value="{{$assignmentSurvey->start_date}}" placeholder="Start Date">    
                </div>
              </div>


              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">End Date</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" name="end_date" class="form-control datepicker" value="{{$assignmentSurvey->end_date}}" placeholder="End Date">    
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Nomen</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" name="nomen" class="form-control" placeholder="Nomen" value="{{$assignmentSurvey->nomen}}">    
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Asset</label>
                </div>
                <div class="col-sm-8">
                  <input type="text" name="asset" class="form-control" placeholder="Asset" value="{{$assignmentSurvey->asset}}">    
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">AB</label>
                </div>
                <div class="col-sm-8">
                    <select class="form-control" name="ab_id" id="ab">
                      <option value="0"> - Choose AB  -</option>
                     @foreach($ab as $ab)
                        <option value="{{$ab->id}}"  {{($assignmentSurvey->AB_id == $ab->id ) ? "selected":""}}> {{$ab->code_ab}} </option>
                     @endforeach
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">PC</label>
                </div>
                <div class="col-sm-8">
                    <select class="form-control" name="pc_id" id="pc">
                      <option value="0" disable="true" selected="true"> - Choose PC  -</option>
                      @foreach($pc as $pc)
                        <option value="{{$pc->id}}"  {{($assignmentSurvey->PC_id == $pc->id ) ? "selected":""}}> {{$pc->code_pc}} </option>
                      @endforeach
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">EZ</label>
                </div>
                <div class="col-sm-8">
                    <select class="form-control" name="ez_id" id="ez">
                      <option value="0" disable="true" selected="true"> - Choose EZ  -</option>
                      @foreach($ez as $ez)
                        <option value="{{$ez->id}}"  {{($assignmentSurvey->EZ_id == $ez->id ) ? "selected":""}}> {{$ez->code_ez}} </option>
                      @endforeach
                    </select>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Assign To</label>
                </div>
                <div class="col-sm-8">
                    <select class="form-control" name="worker_id" id="workers">
                     @foreach($workers as $worker)
                        <option value="{{$worker->id}}" {{($assignmentSurvey->worker_id == $worker->id) ? "selected" :"" }}>{{$worker->worker_id}} -- {{$worker->worker_name}} </option>
                     @endforeach
                      
                    </select>
                </div>
              </div>
            </div>
          </form><!-- /form -->

          <script type="text/javascript">
          $('#ab').on('change', function(e){
            console.log(e);
            var ab_id = e.target.value;
            $.get('assignmentSurvey/json-pc/' + ab_id,function(data) {
              console.log(data);
              $('#pc').empty();
              $('#pc').append('<option value="0" disable="true" selected="true"> - Choose PC -</option>');

              $('#ez').empty();
              $('#ez').append('<option value="0" disable="true" selected="true"> - Choose EZ -</option>');

              $('#bk').empty();
              $('#bk').append('<option value="0" disable="true" selected="true"> - Choose BK -</option>');

              $.each(data, function(index, pcObj){
                $('#pc').append('<option value="'+ pcObj.id +'">'+ pcObj.code_pc +'</option>');
              })
            });
          });

          $('#pc').on('change', function(e){
            console.log(e);
            var pc_id = e.target.value;
            $.get('assignmentSurvey/json-ez/' + pc_id,function(data) {
              console.log(data);

              $('#ez').empty();
              $('#ez').append('<option value="0" disable="true" selected="true"> - Choose EZ -</option>');

              $('#bk').empty();
              $('#bk').append('<option value="0" disable="true" selected="true"> - Choose BK -</option>');

              $.each(data, function(index, ezObj){
                $('#ez').append('<option value="'+ ezObj.id +'">'+ ezObj.code_ez +'</option>');
              })
            });
          });

          $('#ez').on('change', function(e){
            console.log(e);
            var ez_id = e.target.value;
            $.get('assignmentSurvey/json-bk/' + ez_id,function(data) {
              console.log(data);

              $('#bk').empty();
              $('#bk').append('<option value="0" disable="true" selected="true"> - Choose BK -</option>');

              $.each(data, function(index, bkObj){
                $('#bk').append('<option value="'+ bkObj.id +'">'+ bkObj.code_bk +'</option>');
              })
            });
          });

          $('#survey_id').on('change', function(e){
            console.log(e);
            var survey_id = e.target.value;
            $.get('assignmentSurvey/json-worker/' + survey_id,function(data) {
              console.log(data);
              $('#workers').empty();
              $('#workers').append('<option value="0" disable="true" selected="true"> - Choose Workers -</option>');

              $.each(data, function(index, workerObj){
                $('#workers').append('<option value="'+ workerObj.id +'">'+ workerObj.worker_id +'-'+ workerObj.worker_name +'</option>');
              })
            });
          });
        </script>