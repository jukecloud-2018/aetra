@extends('layouts.app')

@section('head')
    Transaction | Assignment Survey
@endsection

@section('title')
    <i class="fa fa-home"></i> Transaction  | <span style="color:#2B6B97"> Assignment Survey </span>
@endsection
@section('button')
      <div class="forButtonTop">
          <button href="" class="btn btn-default buttonAE btnTop" onclick="add_assignment_survey()">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </button>
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableAssignmentSurvey" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th>No</th>
                                    <th>No Survey</th>
                                    <th>Survey Type</th>
                                    <th>Survey Date</th>
                                    <th>AB</th>
                                    <th>PC-EZ</th>
                                    <th>Nomen</th>
                                    <th>Asset</th>
                                    <th>Assign</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status Survey</th>
                                    <th>Sending</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($list_survey as $key=>$survey)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$survey->survey_code}}</td>
                                    <td>{{$survey->survey_name}}</td>
                                    <td>{{$survey->survey_date}}</td>
                                    <td>{{($survey->code_ab) ? $survey->code_ab : '-'}}
                                    <!-- <button class="btn btn-primary" style="margin-left: 5px;"> <i class="fa fa-search"></i> </button> -->
                                    </td>
                                    <td>{{($survey->code_pc) ? $survey->code_pc : '-'}}{{$survey->code_ez}}
                                    <!-- <button class="btn btn-primary" style="margin-left: 5px;"> <i class="fa fa-search"></i> </button> -->
                                    </td>
                                    <td>{{($survey->nomen) ? $survey->nomen : '-'}}</td>
                                    <td>{{($survey->asset) ? $survey->asset : '-'}}</td>
                                    <td>{{$survey->worker_name}}</td>
                                    <td>{{$survey->start_date}}</td>
                                    <td>{{$survey->end_date}}</td>
                                    <td>{{$survey->survey_status}}</td>
                                    <td>{{$survey->sending_status}}</td>
                                    <td>
                                    <button class="btn btn-success" onclick="editAssignmentSurvey('{{$survey->id}}')"><i class="fa fa-pencil"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="modalAssignmentSurvey" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Assignment Survey</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
       
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveAssignSurvey()">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal View Detail  -->
<div class="modal fade" id="modalViewDetail" tabindex="-1" role="dialog" aria-labelledby="modalViewDetail" aria-hidden="false" >
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: auto">
            <div class="modal-header">
                <button type="button" class="close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;"></div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center"></div>
        </div>
    </div>
</div>
<!-- end View Detail -->
@endsection

@section('js')
  @include('transaction.assignmentSurvey.js_assignmentSurvey')
@endsection
