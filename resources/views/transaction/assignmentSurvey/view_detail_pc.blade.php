 <form id="formDetailData">
<table class="table table-striped table-bordered" id="detailData"> 
  <thead>
    <tr>
      <th> Detail PC-EZ </th>
    </tr>
  </thead>
  <tbody>
   @foreach($detail_pc as $detail)
    <tr>
      <td width="40%" height="auto">{{($detail->code_pc) ? $detail->code_pc : 'Kosong'}}{{($detail->code_ez) ? $detail->code_ez : 'Kosong'}}</td>
    </tr>
    @endforeach
  </tbody>
</table>  
</form>