<script type="text/javascript">
$(document).ready(function() {
    $('#tableAssignmentSurvey').dataTable({
    });
});
function editAssignmentSurvey(id)
{
    var data = $('#formSurveyTransaction').serialize();
  

    $.ajax({
        url:"assignmentSurvey/"+id+"/edit",
        data:'',
       // dataType:'JSON',
        type:'GET',
        success:function(data){
            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Close</button>';
            footer += '<button type="button" class="btn btn-secondary  btnFooterModal"  onclick="updateAssignmentSurvey('+id+')">Update</button>';
            $('#modalAssignmentSurvey').find('.modal-footer').html(footer);
            $('#modalAssignmentSurvey').find('.modal-body').html(data);
            $('#modalAssignmentSurvey').modal('show');
            datePicker();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })   
}

function updateAssignmentSurvey(id)
{
     var data = $('#formSurveyTransaction').serialize();
  

    $.ajax({
        url:"assignmentSurvey/"+id+"",
        data:data,
        dataType:'JSON',
        type:'PUT',
        headers: {
          'X-CSRF-TOKEN' : '{{csrf_token()}}',
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })

            }
            else
            {
                swal('Success','Survey has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })   
}

function add_assignment_survey()
{
    var data = $('#formSurveyTransaction').serialize();
  

    $.ajax({
        url:"{{route('transaction.assignmentSurvey.create')}}",
        data:'',
       // dataType:'JSON',
        type:'GET',
        success:function(data){
            $('#modalAssignmentSurvey').find('.modal-body').html(data);
            $('#modalAssignmentSurvey').modal('show');
            datePicker();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function saveAssignSurvey()
{

    var data = $('#formSurveyTransaction').serialize();
  

    $.ajax({
        url:"{{route('transaction.assignmentSurvey.store')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        headers: {
          'X-CSRF-TOKEN' : '{{csrf_token()}}',
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
                hidden_error();
                datePicker();

            }
            else
            {
                swal('Success','Survey has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function detail_ab(ab_id){
    $.ajax({
        url:"assignmentSurvey/viewAB/"+ab_id,
        data:'kosong',
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            $('#modalViewDetail').find('.modal-header').html('<button type="button" class="close" onclick="closeDetailData()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Detail AB </h4>'
            );
            $('#modalViewDetail').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeDetailData()">Close</button>')
            $('#modalViewDetail').find('.modal-body').html(data);
            $('#modalViewDetail').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function detail_pc(id){
    $.ajax({
        url:"assignmentSurvey/viewPC/"+id,
        data:'kosong',
        type:'GET',
        success:function(data){
            $('#modalViewDetail').find('.modal-header').html('<button type="button" class="close" onclick="closeDetailData()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Detail PC-EZ </h4>'
            );
            $('#modalViewDetail').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeDetailData()">Close</button>')
            $('#modalViewDetail').find('.modal-body').html(data);
            $('#modalViewDetail').modal('show');
        },
        error:function(xhr, ajaxOptions, thrownError){
            swal("error!", thrownError, "error");
        }
    })
}

function closeDetailData()
{
    $('#modalViewDetail').modal('hide');
}
</script>
