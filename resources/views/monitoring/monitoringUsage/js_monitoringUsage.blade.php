<script type="text/javascript">

function reloadPageMonitoringUsage(){
  alertPopup("Sukses!", "Data Sudah Terupdate", "success");
  window.location.reload();
}

function detailUsage(code){
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $.ajax({
            url:"../getMaterialUsage/"+code,
            data:'kosong',
            type:'GET',
            success:function(data){
                $('#modal_usage').find('.modal-header').html('<button type="button" class="close" onclick="closeDetailData()">'+
                '<span aria-hidden="true" style="color:#fff">&times;</span>'+
                '</button>'+
                '<h4 class="modal-title" style="text-align:center"> Detail Usage </h4>'
                );
                $('#modal_usage').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeDetailData()">Close</button>')
                $('#modal_usage').find('.modal-body').html(data);
                $('#modal_usage').modal('show');
            },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", thrownError, "error");
            }
        })
    }else{
        $.ajax({
            url:"monitoringUsage/getMaterialUsage/"+code,
            data:'kosong',
            type:'GET',
            success:function(data){
                $('#modal_usage').find('.modal-header').html('<button type="button" class="close" onclick="closeDetailData()">'+
                '<span aria-hidden="true" style="color:#fff">&times;</span>'+
                '</button>'+
                '<h4 class="modal-title" style="text-align:center"> Detail Usage </h4>'
                );
                $('#modal_usage').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeDetailData()">Close</button>')
                $('#modal_usage').find('.modal-body').html(data);
                $('#modal_usage').modal('show');
            },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", thrownError, "error");
            }
        })
    }
}

function closeDetailData(){
    $('#modal_usage').modal('hide');
}

</script>