<table id="tableMaterialUsage" class="table table-bordered table-striped tblAssJob">
  <thead>
    <tr style="background-color:#3896D6;">
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">No.</th>
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">FA Transaction</th>
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Nomen</th>
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Fa Type</th>
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Qty</th>
      <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">UoM</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      @foreach($data as $key=>$value)
      <tr>
        <td style="vertical-align: middle; text-align: center;" colspan="5"><center>{{++$key}}</center></td>
        <td style="vertical-align: middle; text-align: center;" colspan="5">{{$value->fa_transaction_id}}</td>
        <td style="vertical-align: middle; text-align: center;" colspan="5">{{$value->nomen}}</td>
        <td style="vertical-align: middle; text-align: center;" colspan="5">{{$value->fa_type_cd}}</td>
        <td style="vertical-align: middle; text-align: center;" colspan="5">{{$value->qty}}</td>
        <td style="vertical-align: middle; text-align: center;" colspan="5">{{$value->unit_of_purch}}</td>
      </tr>
      @endforeach
    </tr>
  </tbody>
</table>