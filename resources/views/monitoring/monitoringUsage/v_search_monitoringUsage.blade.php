@extends('layouts.app')

@section('head')
    Monitoring | Monitoring Usage
@endsection

@section('title')
    <i class="fa fa-home"></i> Monitoring  | <span style="color:#2B6B97"> Monitoring Usage </span>
@endsection
@section('button')
<div class="row forButtonTop">
    <div class="col-md-12">
        <form method="get" action="{{ URL::to('monitoring/monitoringUsage/getDataByDate/srch')}}">
            {{ csrf_field() }}
            <span>Date : </span>
            <div style="display: inline-block;width: 100px" >
                <input type="text" class="form-control datepicker" name="startDateUsage" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
            <span style="color: #8B9AAD;font-weight: bold; margin-right: 10px;"> > </span>
            <div style="display: inline-block;width: 100px">
                <input type="text" class="form-control datepicker" name="endDateUsage" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
            <input type="submit" class="btn btn-primary white btnTop" style="margin-left:5px;" value="OK">
            <a  class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalUsage">
                <i class="fa fa-filter" aria-hidden="true"></i> Filter
            </a>
            <a class="btn btn-primary white btnTop" style="margin-left:5px;" onclick="reloadPageMonitoringUsage()">Refresh</a>
        </form>
    </div>
</div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        @if($ket == 'Search Data')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cari}} </span>
                        @elseif($ket == 'Search Date')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cariDari}} - {{$cariSmp}}</span>
                        @endif
                    </div>
                    <div class="col-md-6"></div>
                    <div class="col-md-3" style="text-align:right;">
                        <form method="get" action="{{ URL::to('monitoring/monitoringUsage/search/srch')}}">
                            <input type="text" class="form-control search" name="search" placeholder="Type to search"/>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <table class="tableCus table-striped js-sort-table">
                        <thead class="thead" style="width: 1242px;font-size:14px;">
                            <tr style="background-color:#8B9AAD;color:#fff">
                                <th style="width: 42px;" class="js-sort-number">No</th>
                                <th style="width: 204px;" class="js-sort-string">Item Code</th>
                                <th style="width: 724px;" class="js-sort-string">Description</th>
                                <th style="width: 100px;" class="js-sort-number">Usage Qty</th>
                                <th style="width: 80px;" class="js-sort-string">UoM</th>
                                <th style="width: 70px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table" class="tbody" style="width: 1242px;font-size:14px;">
                            @foreach($data as $key=>$value)
                                <tr role="row">
                                    <td style="width: 42px;text-align: center;">{{++$key}}</td>
                                    <td style="width: 204px;">{{$value->code}}</td>
                                    <td style="width: 724px;">{{$value->name}}</td>
                                    <td style="width: 100px;">{{($value->qty) ? $value->qty : '0'}}</td>
                                    <td style="width: 80px;">{{$value->unit_of_purch}}</td>
                                    <td style="width: 70px;text-align: center;">
                                        <button class="btn btn-primary srch" data-srch='srch' onclick="detailUsage('{{$value->code}}')"> <i class="fa fa-search"></i> </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12">
                {{ $data->render() }}
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="modal_usage" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Detail Usage</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formGroup">
                    <div class="col-sm-12">
                        <div class="row">
                            <table id="tableMaterialUsage" class="table table-bordered table-striped tblAssJob">
                                <thead>
                                    <tr style="background-color:#3896D6;">
                                        <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">FA Transaction</th>
                                        <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Item Code</th>
                                        <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Fa Type</th>
                                        <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Qty</th>
                                        <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">UoM</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="filterModalUsage" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Filter</h4>
                </div>
                <div class="modal-body"  style="background-color: #FBFBFB">
                    <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                    <form id="formFilterAssignment" method="get" action="{{ URL::to('monitoring/monitoringUsage/filterUsage/srch')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="code"> <label style="font-weight: normal; margin-right: 10px;"> Item Code </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Item Code" name="code">
                            </div>
                        </div><br>
                        <div class="modal-footer" style="text-align: center">
                            <input type="submit" class="btn btn-primary btnFooterModal" value="OK">
                        </div>
                    </form><!-- /form -->
                </div><!-- / Modal Body -->                
            </div>
        </div>
    </div>
@endsection

@section('js')
  @include('monitoring.monitoringUsage.js_monitoringUsage')
@endsection
