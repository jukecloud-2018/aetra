@extends('layouts.app')

@section('head')
    Monitoring | Map Worker
@endsection

@section('title')
    <i class="fa fa-home"></i> Monitoring | Map Worker <span style="color:#2B6B97"> Map Worker </span>
@endsection
@section('button')
<div class="row forButtonTop">
    <div class="col-md-12">
        <div style="display: inline-block;width: 250px">
            <input type="text" class="search-query form-control" placeholder="Search" id="searchtext"/>
        </div>
        <a href="#" id="btnSearch" class="btn btn-danger" type="button" style="height: 34px;margin:-32px 0px -29px -43px;border-radius: 20px;">
            <i class=" glyphicon glyphicon-search"></i>
        </a>
        <a  class="btn btn-primary white btnTop" data-toggle="modal" data-target="#fileterModal" style="margin-left:5px;">
            <i class="fa fa-upload" aria-hidden="true"></i> Filter
        </a>
        <a class="btn btn-primary white btnTop" style="margin-left:5px;" id="btnRefresh">
            <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
        </a>
    </div>
</div>
@endsection

@section('content')
{{--<section class="content">--}}
	<div id="map" style="margin: 0;"></div>
    <!-- /.box body -->
	<div class="modal" id="fileterModal">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Filter</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="typeALLJob">
                                <label class="custom-control-label" for="typeALLJob">All</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="typeFAGroup">
                                <label class="custom-control-label" for="typeFAGroup">FA Group</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="typeWOGroup">
                                <label class="custom-control-label" for="typeWOGroup">WO Group</label>
                            </div>
                                <div class="custom-control custom-checkbox">
                                <input style="width: 100px" class="form-control autocomplete" id="autocomplete" placeholder="Worker.."/>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary  btnFooterModal" data-dismiss="modal" id="btnSubmit">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!--- /Wiem Added --->
{{--</section>--}}
<!-- /section -->
@endsection
@section('js')
  @include('monitoring.mapWorker.js_mapWorker')
@endsection
