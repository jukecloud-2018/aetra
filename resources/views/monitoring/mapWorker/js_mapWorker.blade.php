<script type="text/javascript">

// for style checkbox
$( document ).ready(function() {
  	styleChek();
});

//for datatable 
$('#table').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { "data": "nameGroup" },
        { "data": "description" },
        { "data": "description" },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="detail(`' + row.id + '`)"><i class="fa fa-pencil"></i></button>';
            }
		}
    ],
    responsive: true,
});

// for style checkbox
function styleChek()
{
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass   : 'iradio_minimal-green'
    });
}
    

// for add row table fa
function addRowFa()
{
	var table = $('#tableFA');

	table.append('<tr  class="rowFa">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowFa()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

// for delete row table fa
function deleteRowFa()
{
	document.getElementById("tableFA").deleteRow(1);
}

// for add row table wo
function addRowWO()
{
	var table = $('#tableWO');

	table.append('<tr  class="rowWo">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowWo()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

//// for delete row table wo
function deleteRowWo()
{
	document.getElementById("tableWO").deleteRow(1);
}

/// for save group 
function saveGroup()
{
	var data = $('#formGroup').serialize();

	$.ajax({
		url:"",
		data:data,
		dataType:'JSON',
		type:'POST',
		success:function(data){
			if(data.errors)
			{
				$.each(data.errors,function(key,value){
					$('.alert-danger').show();
					$('.alert-danger').html('<li>'+value+'</li>');
				})

				setTimeout(function() {
					$('#error-alert').fadeOut('fast');
				}, 2000); 
			}
			else
			{
				swal('Success','Group has been saved successfully!','success');
				location.reload();
			}
		},
          error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
          },
	})
}

// wiem added
	  var host = "<?php echo $ip ?>";
	  var dispatch_group = '<?php echo $userinfo ?>';
	  var map;
      const TILE_ORIGIN = [-20037508.34789244, 20037508.34789244];
      const ORIG_X = 0;
      const ORIG_Y = 1;
      const MAP_SIZE = 20037508.34789244 * 2;
      const MINX = 0;
      const MAXX = 1;
      const MINY = 2;
      const MAXY = 3;
	  var markers = [];
	  
	  var availableTags = [
				"ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++",
				"Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran",
				"Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl",
				"PHP", "Python", "Ruby", "Scala", "Scheme"
	];
  
	$(".autocomplete").autocomplete({
		source: availableTags
	});
	
	$('#typeALLJob').on('click change', function(e) {
		if($('#typeALLJob').is(":checked")){
			$( "#typeFAGroup" ).prop( "checked", true );
			$( "#typeWOGroup" ).prop( "checked", true );
			console.log("checked change");
		}
	});

     
      function getBoundingBox(x, y, zoom){
         var bbox = [];
		 
		 tileSize = MAP_SIZE / Math.pow(2, zoom);
         minx = TILE_ORIGIN[ORIG_X] + x * tileSize;
         maxx = TILE_ORIGIN[ORIG_X] + (x+1) * tileSize;
         miny = TILE_ORIGIN[ORIG_Y] - (y+1) * tileSize;
         maxy = TILE_ORIGIN[ORIG_Y] - y * tileSize;

         //bbox = [minx, miny, maxx, maxy];
		 
		 bbox[MINX] = minx;
         bbox[MINY] = miny;
         bbox[MAXX] = maxx;
         bbox[MAXY] = maxy;
         


        return bbox;
      }
      function initMap() {
		 
        map = new google.maps.Map(document.getElementById('map'), {
          // center: {lat: -6.160323, lng: 106.905114},
          center: {lat: -6.21462, lng: 106.84513},
          zoom: 18,
		  disableDefaultUI: true,
        });
		
		var imageMapType = new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
          resultBBox = getBoundingBox(coord.x, coord.y,  zoom);
          //url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          //console.log(url);
		  if(host.startsWith('172.27') == true){
            url = "http://172.27.1.39:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          }else{
            url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          }
          //url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";

          //console.log(url);

              return url;

            },

            tileSize: new google.maps.Size(256, 256)
        });
		
		  map.overlayMapTypes.push(imageMapType);
		  
			var zoomDiv = document.createElement('div');
			var renderZoomControls = new ZoomControl(zoomDiv, map);
			zoomDiv.index = 1;
			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomDiv);

        
			var tileloadedListerner = map.addListener('tilesloaded', function() {
				console.log('Map center change ');
			});
			map.addListener('dragstart', function(){

			if(tileloadedListerner!= null){
				console.log('removeListener ');
				google.maps.event.removeListener(tileloadedListerner);
			}
			});

        
      }
	  
	  function ZoomControl(div, map) {

          // Get the control DIV. We'll attach our control UI to this DIV.
          var controlDiv = div;

          // Set CSS for the controls.
          controlDiv.style.margin = '28px 0px 0px 22px';
          controlDiv.style.cursor = 'pointer';
          controlDiv.style.opacity = "0.8";
          controlDiv.style.backgroundColor = "#00000000";
          controlDiv.style.fontFamily = 'Open Sans';
          controlDiv.style.height = '77px';
          controlDiv.style.width = '36px';
          controlDiv.style.marginRight= '20px';




          var zoomin = document.createElement('div');
          zoomin.title = 'Click to zoom in';
          zoomin.style.display = "block"
          zoomin.style.width = 'auto';
          zoomin.style.height = 'auto';
          zoomin.style.backgroundColor = "#FFFFFF";
          zoomin.style.float='left';

          controlDiv.appendChild(zoomin);

          var zoominIcon = document.createElement('img');
          zoominIcon.src='/dist/img/icon_zoomIn_xxxhdpi.png'
          zoominIcon.style.width = '30px';
          zoominIcon.style.height = '30px';
          zoomin.appendChild(zoominIcon);

          var zoomout = document.createElement('div');
          zoomout.title = 'Click to zoom out';
          zoomout.style.display = "block"
          zoomout.style.borderRight = "1px solid #9e9e9e"
          zoomout.style.width = 'auto';
          zoomout.style.height = 'auto';
          zoomout.style.float='left';
          zoomout.style.backgroundColor = "#FFFFFF";
          zoomout.style.marginTop = '5px';
          controlDiv.appendChild(zoomout);

          var zoomoutIcon = document.createElement('img');
          zoomoutIcon.src='/dist/img/icon_zoomOut_xxxhdpi.png'
          zoomoutIcon.style.width = '30px';
          zoomoutIcon.style.height = '30px';
          zoomout.appendChild(zoomoutIcon);

          // Setup the click event listeners for zoom-in, zoom-out:
          google.maps.event.addDomListener(zoomout, 'click', function() {
           var currentZoomLevel = map.getZoom();
           if(currentZoomLevel != 0){
             map.setZoom(currentZoomLevel - 1);}
          });

           google.maps.event.addDomListener(zoomin, 'click', function() {
           var currentZoomLevel = map.getZoom();
           if(currentZoomLevel != 21){
             map.setZoom(currentZoomLevel + 1);}
          });
		  
		  drawAllWorkers(map, dispatch_group, '', '');
		  
      }
	  
		function clearMarkers() {
			setMapOnAll(null);
		}
		function setMapOnAll(map) {
			for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
			}
		}
		function showMarkers() {
			setMapOnAll(map);
		}
		function deleteMarkers() {
			clearMarkers();
			markers = [];
		}
	  
	  function drawAllWorkers(map, dispatch_gr, filter, worker_name){
		
			$.ajax({
        		url:"/api/get_all_worker",
        		data:{dispatch_group:dispatch_gr, filter: filter, worker_name: worker_name},
        		dataType:'JSON',
        		type:'POST',
        		success:function(data){
        			console.log(data);
					if(data.errors)
        			{
						
        			}
        			else
        			{
						
						createAllWorkerMarker(map, data);
        			}
        		},
            error: function (xhr, ajaxOptions, thrownError) {

            },
        	})
		}
		
		function searchWorkers(map, worker_name){
		
			$.ajax({
        		url:"/api/search_worker",
        		data:{id:worker_name},
        		dataType:'JSON',
        		type:'POST',
        		success:function(data){
        			console.log(data);
					if(data.errors)
        			{
						
        			}
        			else
        			{
						gotoWorker(map, data);
						//createAllJobsMarker(map, data);
        			}
        		},
            error: function (xhr, ajaxOptions, thrownError) {

            },
        	})
		}
		
		function zoomToFitMarkers(map, markers){
			var bounds = new google.maps.LatLngBounds();
			for(i=0;i<markers.length;i++) {
			   bounds.extend(markers[i].getPosition());
			}

			//center the map to a specific spot (city)
			//map.setCenter(center); 

			//center the map to the geometric center of all markers
			map.setCenter(bounds.getCenter());

			map.fitBounds(bounds);

			//remove one zoom level to ensure no marker is on the edge.
			map.setZoom(map.getZoom()-1); 

			// set a minimum zoom 
			// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
			if(map.getZoom()> 15){
			  map.setZoom(15);
			}

			//Alternatively this code can be used to set the zoom for just 1 marker and to skip redrawing.
			//Note that this will not cover the case if you have 2 markers on the same address.
			if(markers.length == 1){
				map.setMaxZoom(15);
				map.fitBounds(bounds);
				map.setMaxZoom(Null)
			}
			
			/*var boundschangedListener = map.addListenerOnce(map, 'bounds_changed', function(event) {
				map.setZoom(map.getZoom()-1);

			  if (map.getZoom() > 15) {
				map.setZoom(15);
			  }
			});*/
			map.fitBounds(bounds);
		}
		
		
		
		function createAllWorkerMarker(map, data){
			deleteMarkers();
			var strData = JSON.stringify(data);
			var objJson = JSON.parse(strData);
			var obj, lat, lon , status, title = "";
			for(i in objJson){
				obj = objJson[i];
				lon = obj.long;
				lat = obj.lat;
				status = obj.status;
				var statusText = '';
				switch(status){
					case 1:
						statusText = 'New';
						break;
					case 2:
						statusText = 'Open';
						break;
					case 3:
						statusText = 'On Progress';
					 break;
				}
				
				title = 'Crew : ' + obj.crew + '\n' +
						obj.id + '\n' +
						statusText + '\n' +
						'Level Battery : ' + obj.battery + '\n' +
						'Level Signal :' + obj.signal;
				var icon = {
							url: "/dist/img/icon_Worker_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
				
				createMarker(map, lat, lon, icon, title);
			}
			
			zoomToFitMarkers(map, markers);
						
		}
		
		$("#btnRefresh").click(function(){
			var worker_name = $('autocomplete').val();
			var typeFilter = '[' + getTypeFilter() + ']';
			console.log(typeFilter.toString());
			drawAllWorkers(map, dispatch_group, typeFilter, worker_name);
		});
		
		function gotoWorker(map, data){
			var strData = JSON.stringify(data);
			var objJson = JSON.parse(strData);
			var obj, lat, lon , status, title = "";
			for(i in objJson){
				obj = objJson[i];
				lon = obj.long;
				lat = obj.lat;
				
				var center = new google.maps.LatLng(lat, lon);
				// using global variable:
				map.panTo(center);
				
			}
			
		}
		
		function createMarker(map, lat, lon, icon, title){
			var position = new google.maps.LatLng(lat, lon);
			
			var marker = new google.maps.Marker({
				position: position,
				icon: icon,
				map: map,
				title: title
           });
		   
			/*var marker = new MarkerWithLabel({
				position: position,
				animation: google.maps.Animation.DROP,
				icon: icon,
				map: map,
				labelContent: title,
				labelAnchor: new google.maps.Point(18, 12),
				labelClass: "my-custom-class-for-label", // the CSS class for the label
				labelInBackground: true
          });*/
		  markers.push(marker);
		}
		
		function getTypeFilter(){
			var arrayResult = [];
			if($('#typeFAGroup').is(":checked")){
				arrayResult.push('fa_group');
			}
			if($('#typeWOGroup').is(":checked")){
				arrayResult.push('wo_group');
			}
			
			return arrayResult;
		}
		
		$("#btnSubmit").click(function(){
			$('#fileterModal').modal('hide');
			var worker_name = $('autocomplete').val();
			var typeFilter = '[' + getTypeFilter() + ']';
			console.log(typeFilter.toString());
			
			drawAllWorkers(map, dispatch_group,typeFilter, worker_name);
			
			//alert('clicked!');
		});
		$("#btnSearch").click(function(){
			var txtSearch = $("#searchtext").val();
			searchWorkers(map, txtSearch);
			
		});
	  
	  
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTwpUvair9cZMOGUcTb28L4o_iV3DObY&callback=initMap"
    async defer></script>