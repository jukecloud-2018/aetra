@extends('layouts.app')

@section('head')
    Monitoring | Monitoring FA
@endsection

@section('title')
  <i class="fa fa-home"></i> Monitoring  | <span style="color:#2B6B97"> <a href="{{ \Illuminate\Support\Facades\URL::to('monitoring/monitoringFA')}}"> Monitoring FA </a></span>
@endsection
<meta http-equiv="refresh" content="300">
@section('button')
<div class="row forButtonTop">
    <div class="col-md-12">
        <form method="get" action="{{URL::to('monitoring/monitoringFA/getData/srch')}}">
            {{ csrf_field() }}
            <span>Assigned Date : </span>
            <div style="display: inline-block;width: 100px" >
                <input type="text" class="form-control datepicker" name="startDateMonitoring" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
            <span style="color: #8B9AAD;font-weight: bold; margin-right: 10px;"> > </span>
            <div style="display: inline-block;width: 100px">
                <input type="text" class="form-control datepicker" name="endDateMonitoring" style="border-radius: 10px;">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
            <input type="submit" class="btn btn-primary white btnTop" style="margin-left:5px;" value="OK">
            <a  class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalFA">
                <i class="fa fa-filter" aria-hidden="true"></i> Filter
            </a>
            <button class="btn btn-primary white btnTop" style="margin-left:5px;" onclick="reloadPageMonitoringFA()">Refresh</button>
        </form>
    </div>
</div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box box-body">
            <div class="col-md-12" style="margin-top: -20px;">
                <div class="row">
                    <div class="col-md-6">
                        @if($ket == 'Search Data')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cari}} </span>
                        @elseif($ket == 'Search Date')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cariDari}} - {{$cariSmp}}</span>
                        @elseif($ket == 'Search Fa Type')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$FA_Type}}</span>
                        @elseif($ket == 'Search PC_EZ_BK')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$PC_EZ_BK}}</span>
                        @elseif($ket == 'Search Status')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$Status}}</span>
                        @elseif($ket == 'Search All')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$fa_type_cd}} {{$pc_ez_bk}} {{$status}}</span>
                        @elseif($ket == 'Search Duo')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}"</span>
                        @endif
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3" style="text-align:right;">
                        <form method="get" action="{{ URL::to('monitoring/monitoringFA/search/srch')}}" style="margin-bottom:10px;">
                            <input type="text" class="form-control search" name="search" placeholder="Type to search"/>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <table class="tableCus table-striped js-sort-table">
                        <thead class="thead" style="width: 2105px;font-size:14px;">
                            <tr style="background-color:#8B9AAD;color:#fff">
                                <th style="width: 42px;" class="js-sort-number">No</th>
                                <th style="width: 80px;" class="js-sort-number">Nomen</th>
                                <th style="width: 91px;" class="js-sort-number">No. FA</th>
                                <th style="width: 85px;" class="js-sort-string">FA Type</th>
                                <th style="width: 300px;">Description </th>
                                <th style="width: 85px;" class="js-sort-number">PC-EZ-BK</th>
                                <th style="width: 80px;" class="js-sort-string">FA Status</th>
                                <th style="width: 170px;" class="js-sort-date">FA Date / Time </th>
                                <th style="width: 100px;">Assign to</th>
                                <th style="width: 130px;">Assigned Date </th>
                                <th style="width: 95px;">Assignment Status</th>
                                <th style="width: 95px;">Urgent</th>
                                <th style="width: 80px;">Priority</th>
                                <th style="width: 115px;">Status Send Characteristic</th>
                                <th style="width: 105px;">Status Send to WMS</th>
                                <th style="width: 100px;">Status FA Completion</th>
                                <th style="width: 90px;text-align: center;">Detail FA</th>
                                <th style="width: 100px;text-align: center;">History Status</th>
                                <th style="width: 140px;text-align: center;">Action</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table" class="tbody" style="width: 2105px;font-size:14px;">
                            @foreach ($data as $key=>$value)
                                <tr role="row">
                                    <td style="width: 42px;text-align: center;">{{$num++}}</td>
                                    <td style="width: 80px;text-align: center;">
                                        <div>{{$value->nomen}}</div>
                                        <button class="btn btn-primary srch" data-srch='srch' onclick="detail_nomen('{{$value->fa_transaction_id}}')" style="margin-left: 5px;"> <i class="fa fa-search"></i> </button>
                                    </td>
                                    <td style="width: 91px;">{{$value->fa_transaction_id}}</td>
                                    <td style="width: 85px;">{{$value->fa_type_cd}}</td>
                                    <td style="width: 300px;">{{$value->descr}}</td>
                                    <td style="width: 85px;">{{$value->pcezbk}}</td>
                                    <td style="width: 80px;text-align: center;">{{$value->status}}</td>
                                    <td style="width: 170px;">{{$value->created_at}}</td>
                                    <td style="width: 100px;">{{$value->worker_name}}</td>
                                    <td style="width: 130px;">{{$value->assign_date}}</td>
                                    <td style="width: 95px;">{{$value->assign_status}}</td>
                                    <td style="width: 95px;text-align: center;">
                                        @php
                                        if($value->urgent_status == '1'){
                                            echo "Urgent";
                                        }else{
                                            echo "Non Urgent";
                                        }
                                        @endphp
                                    </td>
                                    <td style="width: 80px;text-align: center;">{{$value->priority_status}}</td>
                                    <td style="width: 115px;">{{$value->status_char_simpel}}</td>
                                    <td style="width: 105px;">{{$value->status_wms}}</td>
                                    <td style="width: 100px;">{{$value->status_complation_fa}}</td>
                                    <td style="width: 90px;text-align: center;">
                                        <button class="btn btn-primary srch" data-srch='srch' onclick="detail_monitoring('{{$value->fa_transaction_id}}')"> <i class="fa fa-search"></i> </button>
                                    </td>
                                    <td style="width: 100px;text-align: center;">
                                        <a href="{{ URL::to('monitoring/monitoringFA/get_history/'.$value->fa_transaction_id )}}" class="btn btn-primary" style="margin-left:5px;">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    </td>
                                    <td style="width: 140px;">
                                    @if($value->assign_status == 'Complete' || $value->assign_status == 'Cancel')
                                    <button class="btn btn-primary" disabled="disabled">Send To Simpel</button>
                                    @else
                                    <button class="btn btn-primary srch" data-srch='srch' onclick="sendChar('{{$value->fa_transaction_id}}')">Send To Simpel</button>
                                    @endif
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-10" style="text-align: right;">
                <span id="v_total" style="margin-right: 5px;">Total: {{ $total }}</span>
            </div>
            <div class="col-md-12">
                {{ $data->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal Add -->
<div class="modal fade" id="filterModalFA" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Filter</h4>
                </div>
                <div class="modal-body"  style="background-color: #FBFBFB">
                    <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                    <form id="formFilterAssignment" method="get" action="{{ URL::to('monitoring/monitoringFA/filterMonitoringFA/srch')}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="fa_type"> <label style="font-weight: normal; margin-right: 10px;"> FA Type </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Fa Type" name="fa_type_cd">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="pcezbk"> <label style="font-weight: normal; margin-right: 10px;"> PC_EZ_BK </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="PC-EZ-BK" name="pc_ez_bk">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="status"> <label style="font-weight: normal; margin-right: 10px;"> Assignment Status </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Assignment Status" name="assign_status">
                            </div>
                        </div>
                        <div class="modal-footer" style="text-align: center">
                            <input type="submit" class="btn btn-primary btnFooterModal" value="OK">
                        </div>
                    </form><!-- /form -->
                </div><!-- / Modal Body -->                
            </div>
        </div>
    </div>


<!-- Modal Add -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Detail FA</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formGroup">
                    <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
                    <div class="col-sm-12">
                        <div class="row">
                            <table id="tableAssignmentJob" class="table table-bordered table-striped tblAssJob">
                                <thead>
                                <tr style="background-color:#3995CB;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="6">Assignment Job</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No FA</th>
                                    <th style="vertical-align: middle; text-align: center;">FA Type</th>
                                    <!-- <th style="vertical-align: middle; text-align: center;">No FO</th> -->
                                    <th style="vertical-align: middle; text-align: center;">Date Assigned</th>
                                    <th style="vertical-align: middle; text-align: center;">Status</th>
                                    <th style="vertical-align: middle; text-align: center;">Assigned To</th>
                                    <th style="vertical-align: middle; text-align: center;">PDF</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tableCharacteristic" class="table table-bordered table-striped tblChar">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="3">Characteristic</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Nama</th>
                                    <th style="vertical-align: middle; text-align: center;">Nilai</th>
                                    <th style="vertical-align: middle; text-align: center;">Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <table id="tableMaterialUsage" class="table table-bordered table-striped tblBom">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="5">Material Usage</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">No</th>
                                    <th style="vertical-align: middle; text-align: center;">Item Code</th>
                                    <th style="vertical-align: middle; text-align: center;">Description</th>
                                    <th style="vertical-align: middle; text-align: center;">Qty</th>
                                    <th style="vertical-align: middle; text-align: center;">UoM</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tablePhotoBefore" class="table table-bordered table-striped tblPhotoAfterAndBefore">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="2">Photo Before</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Image</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tablePhotoAfter" class="table table-bordered table-striped tblPhotoAfterAndBefore">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="2">Photo After</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Image</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tableSignature" class="table table-bordered table-striped tblSigned">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle; text-align: center; color: white;" colspan="6">Signature</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle; text-align: center;">Image Signature</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        {{--<button type="button" class="btn btn-primary  btnFooterModal" onclick="saveGroup()">Submit</button>--}}
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editCharacteristicModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Edit Characteristic</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formEditCharacteristic">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" >
                    <div class="form-group row">
                    <label for="" class="col-sm-3 col-form-label">Characteristic</label>
                    <div class="col-sm-9">
                        <input type="text" name="value_characteristic" class="form-control" id="" placeholder="">
                    </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveCharacter()">Save</button>
            </div>
        </div>
    </div>
</div>

{{--Modal Nomen Detail--}}
<div class="modal fade" id="detailNomen_monitoring" tabindex="-1" role="dialog" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Filter</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formDetailNomenMonitoring">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Nama Konsumen</label>
                        </div>
                        <div class="col-sm-6">
                            <span id="name_konsumen_monitoring"></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Alamat</label>
                        </div>
                        <div class="col-sm-6">
                            <span id="alamat_nomen_monitoring"></span>
                        </div>
                    </div><div class="row">
                        <div class="col-sm-4">
                            <label>No. Telp</label>
                        </div>
                        <div class="col-sm-6">
                            <span id="telp_nomen_monitoring"></span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
  @include('monitoring.monitoringFA.js_monitoringFA')
@endsection
