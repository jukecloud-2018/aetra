<script type="text/javascript">

function detail_nomen(fa_transaction_id)
{
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $('#detailNomen_monitoring').modal('show');
        $.ajax({
            url: '../detailNomen/'+fa_transaction_id,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                $('#name_konsumen_monitoring').text(data.data.customer_name);
                $('#alamat_nomen_monitoring').text(data.data.address+' '+data.data.city);
                $('#telp_nomen_monitoring').text(data.data.phone);
            },
            error: function () {
                console.log("error");
            }
        });
    }else{
        $('#detailNomen_monitoring').modal('show');
        $.ajax({
            url: 'monitoringFA/detailNomen/'+fa_transaction_id,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                $('#name_konsumen_monitoring').text(data.data.customer_name);
                $('#alamat_nomen_monitoring').text(data.data.address+' '+data.data.city);
                $('#telp_nomen_monitoring').text(data.data.phone);
            },
            error: function () {
                console.log("error");
            }
        });
    }
}

$('#getDataMonitoring').click(function(){
    var start_date = $('[name="startDateMonitoring"]').val();
    var end_date = $('[name="endDateMonitoring"]').val();

    if(start_date == '' || end_date =='')
    {
        return swal('Error',' Please Complete Insert Date ','error');
    }

    $('#tableMonitoringFA').dataTable().api().destroy();
    fetch_data_monitoring_fa('yes',start_date,end_date);
});

function fetch_data_monitoring_fa(is_date_search,start_date = '',end_date = '')
{
    $('#tableMonitoringFA').dataTable({
        processing: true,
        serverSide: true,
        order: [0, 'desc'],
        ajax: {
            method: 'POST',
            url : '{{route('monitoring.monitoringFA.getDataBydate')}}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data:{
                is_date_search:is_date_search,
                start_date:start_date,
                end_date:end_date
            }
        },
        // columns : [
        //     {
        //         // this for numbering table
        //         render: function (data, type, row, meta) {
        //             return meta.row + meta.settings._iDisplayStart + 1;
        //         }
        //     },
        //     {
        //         "mRender": function (data, type, row, meta) {
        //             return row.nomen ? row.nomen +
        //                 '<button class="btn btn-primary" onclick="detail_nomen('+row.fa_transaction_id+')" style="margin-left: 5px;"> <i class="fa fa-search"></i> </button>' : '';
        //         }
        //     },
        //     { "data": "fa_transaction_id" },
        //     { "data": "fa_type_cd" },
        //     { "data": "descr" },
        //     {
        //         "mRender": function (data, type, row, meta) {
        //             if(row.customer){
        //                 return row.customer.pcezbk ? row.customer.pcezbk : '';
        //             }else{
        //                 return '';
        //             }
        //         }
        //     },
        //     { "data": "status" },
        //     { "data": "created_at" },
        //     {
        //         "mRender" : function(data,type,row,meta) {
        //             return row.worker_id ? row.worker.worker_name : '';
        //         }
        //     },
        //     //return new Date(row.assign_date).toJSON().substring(0, 10).split("-").reverse().join("/");
        //     { "data": "assign_date" },
        //     {
        //         "mRender": function (data, type, row, meta) {
        //             return row.assign_status ? row.assign_status : '';
        //         }
        //     },
        //     {
        //         "mRender": function (data, type, row, meta) {
        //             var urgent = '<p class="text-danger">Urgent</p>';
        //             return (row.urgent_status == 1) ? urgent : 'Non Urgent';
        //         }
        //     },
        //     {
        //         "mRender":function(data,type,row,meta){
        //             return row.priority_status ? row.priority_status : '';
        //         }
        //     },
        //     {
        //         "mRender":function(data,type,row,meta){
        //             return '<button class="btn btn-primary" onclick="detail_monitoring('+row.fa_transaction_id+')"> <i class="fa fa-search"></i> </button>'
        //         }
        //     },

        // ],
        // "columnDefs": [ {
        //     "targets": 0,
        //     "orderable": false
        // }],
        // responsive: true,
    });
}

//get ip untuk relative ip
var host = "<?php echo $ip ?>";

function detail_monitoring(fa_transaction_id)
{
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $('#modal_detail').modal('show');
        $('#tableAssignmentJob').DataTable().destroy();
        $('#tableAssignmentJob').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getDetail/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "fa_transaction_id"},
                { "data" : "fa_type_cd"},
                // { "data": "assign_date" }, // no FO ??
                { "data": "assign_date" },
                { "data": "status" }, //fa status
                {
                    "mRender" : function(data,type,row,meta) {
                        return row.worker_id ? row.worker.worker_name : '';
                    }
                },
                {
                    "mRender" : function(data, type,row,meta){
                        return '<a href="{{ URL::to("monitoring/monitoringFA/downloadPdf")}}/'+fa_transaction_id+'" class="btn btn-primary white btnTop">'+
                                '<i class="fa fa-download" aria-hidden="true"></i> Generate PDF'+
                                '</a>'
                    }
                },

            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getCharacteristic/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "name_characteristic"},
                { "data" : "value_characteristic"},
                {
                    "mRender" : function(data, type,row,meta){
                        if (row.transaction.status_char_simpel == 'Success' && row.transaction.status_wms == 'Success' && row.transaction.status_complation_fa == 'Success' && row.transaction.assign_status == 'Complete') {
                            return '<button class="btn btn-primary" disabled="disabled">Edit</button>'
                        }else{
                            return '<a href="javascript:void(0)" class="btn btn-primary" onclick="editChar('+row.id+')">Edit</a>'
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableMaterialUsage').DataTable().destroy();
        $('#tableMaterialUsage').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getMaterialUsage/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                {
                    // this for numbering table
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data" : "item_code"},
                { "data" : "description"},
                { "data" : "qty"},
                { "data" : "uom"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tableSignature').DataTable().destroy();
        $('#tableSignature').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getTTD/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                // {
                //     // this for numbering table
                //     render: function (data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                { 
                    // "data" : "base64(photo_url)",
                    // "render": function(data, type, row) {
                    //     return '<img src="'+data+'" />';
                    // }
                    "render": function (data, type, row) {
                        if(host.startsWith('172.27') == true){
                            return '<img width=200 height=100 src="http://172.27.1.75/storage/signedworkers/'+row.photo_name+'" >';
                        }else{
                            return '<img width=200 height=100 src="' + row.photo_url + '">';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getphotofa/'+fa_transaction_id,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        var photo1 = row.photo1.split("/");
                        var photo2 = row.photo2.split("/");

                        if (host.startsWith('172.27') == true) {
                            if (row.photo1 || row.photo2) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="http://172.27.1.75/uploads/workphotos/'+ photo1[5] +'"></td><td><img width=100 height=100 alt="photo2" src="http://172.27.1.75/uploads/workphotos/'+ photo2[5] +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }else{
                            if (row.photo1 || row.photo2) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getphotofa/'+fa_transaction_id,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        var photo3 = row.photo3.split("/");
                        var photo4 = row.photo4.split("/");
                        
                        if (host.startsWith('172.27') == true) {
                            if (row.photo3 || row.photo4) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="http://172.27.1.75/uploads/workphotos/'+ photo3[5] +'"></td><td><img width=100 height=100 alt="photo2" src="http://172.27.1.75/uploads/workphotos/'+ photo4[5] +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }else{
                            if (row.photo3 || row.photo4) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo4 +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }else{
        $('#modal_detail').modal('show');
        $('#tableAssignmentJob').DataTable().destroy();
        $('#tableAssignmentJob').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getDetail/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "fa_transaction_id"},
                { "data" : "fa_type_cd"},
                // { "data": "assign_date" }, // no FO ??
                { "data": "assign_date" },
                { "data": "status" }, //fa status
                {
                    "mRender" : function(data,type,row,meta) {
                        return row.worker_id ? row.worker.worker_name : '';
                    }
                },
                {
                    "mRender" : function(data, type,row,meta){
                        return '<a href="{{ URL::to("monitoring/monitoringFA/downloadPdf")}}/'+fa_transaction_id+'" class="btn btn-primary white btnTop">'+
				                '<i class="fa fa-download" aria-hidden="true"></i> Generate PDF'+
			                    '</a>'
                    }
                },

            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getCharacteristic/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "name_characteristic"},
                { "data" : "value_characteristic"},
                {
                    "mRender" : function(data, type,row,meta){
                        if (row.transaction.status_char_simpel == 'Success' && row.transaction.status_wms == 'Success' && row.transaction.status_complation_fa == 'Success' && row.transaction.assign_status == 'Complete') {
                            return '<button class="btn btn-primary" disabled="disabled">Edit</button>'
                        }else{
                            return '<a href="javascript:void(0)" class="btn btn-primary" onclick="editChar('+row.id+')">Edit</a>'
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableMaterialUsage').DataTable().destroy();
        $('#tableMaterialUsage').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getMaterialUsage/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                {
                    // this for numbering table
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data" : "item_code"},
                { "data" : "description"},
                { "data" : "qty"},
                { "data" : "uom"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tableSignature').DataTable().destroy();
        $('#tableSignature').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getTTD/'+fa_transaction_id,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                // {
                //     // this for numbering table
                //     render: function (data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                { 
                    // "data" : "base64(photo_url)",
                    // "render": function(data, type, row) {
                    //     return '<img src="'+data+'" />';
                    // }
                    "render": function (data, type, row) {
                        if(host.startsWith('172.27') == true){
                            return '<img width=200 height=100 src="http://172.27.1.75/storage/signedworkers/'+row.photo_name+'" >';
                        }else{
                            return '<img width=200 height=100 src="' + row.photo_url + '">';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getphotofa/'+fa_transaction_id,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        var photo1 = row.photo1.split("/");
                        var photo2 = row.photo2.split("/");

                        if (host.startsWith('172.27') == true) {
                            if (row.photo1 || row.photo2) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="http://172.27.1.75/uploads/workphotos/'+ photo1[5] +'"></td><td><img width=100 height=100 alt="photo2" src="http://172.27.1.75/uploads/workphotos/'+ photo2[5] +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }else{
                            if (row.photo1 || row.photo2) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringFA/getphotofa/'+fa_transaction_id,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        var photo3 = row.photo3.split("/");
                        var photo4 = row.photo4.split("/");
                        
                        if (host.startsWith('172.27') == true) {
                            if (row.photo3 || row.photo4) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="http://172.27.1.75/uploads/workphotos/'+ photo3[5] +'"></td><td><img width=100 height=100 alt="photo2" src="http://172.27.1.75/uploads/workphotos/'+ photo4[5] +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }else{
                            if (row.photo3 || row.photo4) {
                                return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo4 +'"></td></tr>';
                            }else{
                                return '';
                            }
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }
}

function reloadPageMonitoringFA(){
  alertPopup("Sukses!", "Data Sudah Terupdate", "success");
  window.location.reload();
}

function editChar(id){
    // alert($id);
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $.ajax({
            url: '../editCharacteristicById/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(get){
                $('.modal-title').text('Edit Characteristic');
                $('[name="value_characteristic"]').val(get.data.value_characteristic)
                $('[name="id"]').val(get.data.id);
                $('#editCharacteristicModal').modal('show');
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal("error!", "Data Tidak Dapat Di Ambil", "error");
            }
        });
    }else{
        $.ajax({
            url: 'monitoringFA/editCharacteristicById/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(get){
                $('.modal-title').text('Edit Characteristic');
                $('[name="value_characteristic"]').val(get.data.value_characteristic)
                $('[name="id"]').val(get.data.id);
                $('#editCharacteristicModal').modal('show');
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal("error!", "Data Tidak Dapat Di Ambil", "error");
            }
        });
    }
}

function saveCharacter(){
    var data = $('#formEditCharacteristic').serialize();
    var id = $('[name="id"]').val();
    var srch = $(".srch").attr("data-srch");
    if (srch == 'srch') {
        $.ajax({
            url: '../updateCharacteristic/'+id,
            data:data,
            type: 'POST',
            success:function(data){
                swal("success", "Sukses Menyimpan Characteristic", "success");
                window.location.reload();
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal("error!", "Gagal Menyimpan Characteristic", "error");
            }
        });
    }else{
        $.ajax({
            url: 'monitoringFA/updateCharacteristic/'+id,
            data:data,
            type: 'POST',
            success:function(data){
                swal("success", "Sukses Menyimpan Characteristic", "success");
                window.location.reload();
            },
            error:function(xhr, ajaxOptions, thrownError){
                swal("error!", "Gagal Menyimpan Characteristic", "error");
            }
        });
    }
}

function sendChar(fa_transaction_id){
    var srch = $(".srch").attr("data-srch");
    if(srch == 'srch'){
        $.ajax({
            url : '../getCharWms/'+fa_transaction_id,
            data: 'JSON',
            type: 'GET',
            success: function(data){
                if(data.status == 'success'){
                    swal("success", "Sukses Kirim Ke SIMPEL", "success");    
                }else if(data.status == 'gagal'){
                    swal("error!", data.pesan, "error");  
                }
                
            },
            error: function(xhr, ajaxOptions, thrownError){
                swal("error!", "Gagal Koneksi Ke SIMPEL", "error");
            }
        })
    }else{
        $.ajax({
            url : 'monitoringFA/getCharWms/'+fa_transaction_id,
            data: 'JSON',
            type: 'GET',
            success: function(data){
                if(data.status == 'success'){
                    swal("success", "Sukses Kirim Ke SIMPEL", "success");    
                }else if(data.status == 'gagal'){
                    swal("error!", data.pesan, "error");  
                }
                
            },
            error: function(xhr, ajaxOptions, thrownError){
                swal("error!", "Gagal Koneksi Ke SIMPEL", "error");
            }
        })
    }
}

function sendComp(fa_transaction_id){
    $.ajax({
        url : 'monitoringFA/getComp/'+fa_transaction_id,
        data: 'JSON',
        type: 'GET',
        success: function(data){
            if(data.status == 'success'){
                swal("success", "Sukses Completion Ke SIMPEL", "success");    
            }else if(data.status == 'gagal'){
                swal("error!", data.pesan, "error");  
            }
            
        },
        error: function(xhr, ajaxOptions, thrownError){
            swal("error!", "Gagal Koneksi Ke SIMPEL", "error");
        }
    })
}
</script>