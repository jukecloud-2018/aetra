<html>
<head>
    <meta charset="utf-8">
    <title>Rekapitulasi Hasil Field Activity</title>
    <style>
        body {font-family:Arial, Helvetica, sans-serif;}
        table {border-collapse: collapse; width: 100%; }
        table, th, td {border: 1px solid slategray; margin: 5px;}
        .signature { display: inline; width: 30% }
    </style>
</head>
<body>
    <?php
        $path = public_path('/images/aetra.jpg');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $logo = 'data:image/' . $type . ';base64,' . base64_encode($data);
    ?>
    <img src="{{$logo}}" alt="Image" height="auto" width="25%" />
    <h3 style="text-align: center;">BERITA ACARA FIELD ORDER</h3>
    @foreach($list_job as $row)
    <p style="text-align: center;">FA ID {{ $row->fa_transaction_id }}</p>
	<pre>
 Fa Type         : {{$row->fa_type_cd}}/{{$row->descriptions}}
	Nomen           : {{$row->nomen}}
	Nama Customer   : {{$row->customer->customer_name}}
	Alamat          : {{$row->customer->address}}
 Meter Info      : {{$row->customer->meterinfo}}
    </pre>
	@endforeach

    <h5>Characteristik FA</h5>
    <table>
        <tr>
            <th>Char Type Cd</th>
            <th>Deskripsi</th>
            <th>Value</th>
        </tr>
        @foreach($karakteristik as $row2)
        <tr>
            <td>{{ $row2->char_type_cd }}</td>
            <td>{{ $row2->name_characteristic }}</td>
            <td>{{ $row2->value_characteristic }}</td>
        </tr>
        @endforeach
    </table>
    <h5>Photo</h5>
    <?php
        // foreach ($photo as $key => $value) {
        //     $path1 = public_path('/uploads/workphotos/'.$value->photo_name1);
        //     $path2 = public_path('/uploads/workphotos/'.$value->photo_name2);
        //     $path3 = public_path('/uploads/workphotos/'.$value->photo_name3);
        //     $path4 = public_path('/uploads/workphotos/'.$value->photo_name4);
        //     $type1 = pathinfo($path1, PATHINFO_EXTENSION);
        //     $type2 = pathinfo($path2, PATHINFO_EXTENSION);
        //     $type3 = pathinfo($path3, PATHINFO_EXTENSION);
        //     $type4 = pathinfo($path4, PATHINFO_EXTENSION);
        //     $data1 = file_get_contents($path1);
        //     $data2 = file_get_contents($path2);
        //     $data3 = file_get_contents($path3);
        //     $data4 = file_get_contents($path4);
        //     $base64_1 = 'data:image/' . $type1 . ';base64,' . base64_encode($data1);
        //     $base64_2 = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
        //     $base64_3 = 'data:image/' . $type3 . ';base64,' . base64_encode($data3);
        //     $base64_4 = 'data:image/' . $type4 . ';base64,' . base64_encode($data4);
        // }
    ?>

    @foreach($photo as $key => $value)
        @if (empty($value))
             <img src="{{ asset('images/none.png') }}" style="width: 100px; height:100px;"/>
             <img src="{{ asset('images/none.png') }}" style="width: 100px; height:100px;"/>
             <img src="{{ asset('images/none.png') }}" style="width: 100px; height:100px;"/>
             <img src="{{ asset('images/none.png') }}" style="width: 100px; height:100px;"/>
        @else
            <?php
                $path1 = public_path('/uploads/workphotos/'.$value->photo_name1);
                $path2 = public_path('/uploads/workphotos/'.$value->photo_name2);
                $path3 = public_path('/uploads/workphotos/'.$value->photo_name3);
                $path4 = public_path('/uploads/workphotos/'.$value->photo_name4);
                $type1 = pathinfo($path1, PATHINFO_EXTENSION);
                $type2 = pathinfo($path2, PATHINFO_EXTENSION);
                $type3 = pathinfo($path3, PATHINFO_EXTENSION);
                $type4 = pathinfo($path4, PATHINFO_EXTENSION);
                $data1 = file_get_contents($path1);
                $data2 = file_get_contents($path2);
                $data3 = file_get_contents($path3);
                $data4 = file_get_contents($path4);
                $base64_1 = 'data:image/' . $type1 . ';base64,' . base64_encode($data1);
                $base64_2 = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
                $base64_3 = 'data:image/' . $type3 . ';base64,' . base64_encode($data3);
                $base64_4 = 'data:image/' . $type4 . ';base64,' . base64_encode($data4);
            ?>
            <img src="{{ $base64_1 }}" style="width: 100px; height:100px;">
            <img src="{{ $base64_2 }}" style="width: 100px; height:100px;">
            <img src="{{ $base64_3 }}" style="width: 100px; height:100px;">
            <img src="{{ $base64_4 }}" style="width: 100px; height:100px;">
        @endif
    @endforeach

    <h5>Signature :</h5>
    @foreach($ttd as $key => $value)
        @if (empty($value))
            <img src="{{ asset('images/none.png') }}" style="width: 100px; height:100px;"/>
        @else 
            <?php
                $path = storage_path('app/public/signedworkers/'.$value->photo_name);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            ?>
            <img src="{{ $base64 }}" style="width: 100px; height:100px;"/>
        @endif
    @endforeach
</body>
</html>