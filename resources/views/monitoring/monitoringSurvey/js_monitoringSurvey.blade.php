<script type="text/javascript">

$('#getDataMonitoring').click(function(){
    var start_date = $('[name="startDateMonitoring"]').val();
    var end_date = $('[name="endDateMonitoring"]').val();

    if(start_date == '' || end_date =='')
    {
        return swal('Error',' Please Complete Insert Date ','error');
    }

    $('#tableMonitoringSurvey').dataTable().api().destroy();
    fetch_data_monitoring_survey('yes',start_date,end_date);
});

function filterModalSurvey()
{
   $('#filterModalSurvey').modal('show');
}


function detailMonitoring(survey_code)
{
    initMap(survey_code);
    var srch = $(".srch").attr("data-srch");
    if (srch == 'srch') {
        $('#modal_detail').modal('show');
        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getCharacteristic/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "characteristic_name"},
                { "data" : "characteristic_value"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getPhotoSurvey/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo1 || row.photo2) {
                            return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getPhotoSurvey/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo3 || row.photo4) {
                            return '<tr><td><img width=100 height=100 alt="photo3" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo4" src="'+ row.photo4 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }else{
        $('#modal_detail').modal('show');
        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringSurvey/getCharacteristic/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "characteristic_name"},
                { "data" : "characteristic_value"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringSurvey/getPhotoSurvey/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo1 || row.photo2) {
                            return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringSurvey/getPhotoSurvey/'+survey_code,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo3 || row.photo4) {
                            return '<tr><td><img width=100 height=100 alt="photo3" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo4" src="'+ row.photo4 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }
}

function filterBtn(){
    var data = $('#formStatusFilter').serialize();

    $.ajax({
        url:'monitoringSurvey/getDataByStatus',
        data:data,
        dataType:'JSON',
        type:'POST',
        headers: {
          'X-CSRF-TOKEN' : '{{csrf_token()}}',
        },
        success:function(data){
            
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function closeDetailData()
{
    $('#modalViewDetail').modal('hide');
}

function initMap(id) {
    $.getJSON('monitoringSurvey/mapSurvey/'+id, function(data) {
        // // Initialize Google Maps
        // const mapOptions = {
        //     center:new google.maps.LatLng(-6.21462, 106.84513),
        //     zoom: 13
        // }
        // const map = new google.maps.Map(document.getElementById("map"), mapOptions);

        // $.getJSON('monitoringSurvey/mapSurvey/'+id, function(data) {
        // Initialize Google Markers
        for(locationSurvey of data) {
            // Initialize Google Maps
            const mapOptions = {
                center:new google.maps.LatLng(locationSurvey.lat, locationSurvey.long),
                zoom: 18
            }
            const map = new google.maps.Map(document.getElementById("map"), mapOptions);
            // console.log(posisi.lat);
            let marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(locationSurvey.lat, locationSurvey.long),
                title: locationSurvey.survey_code
            })
        }
    });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBS2JeuERDyaoziVulig-3av-Vp22WRv7o&callback=initMap"
    async defer></script>
