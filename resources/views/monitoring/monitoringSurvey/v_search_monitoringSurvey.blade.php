@extends('layouts.app')

@section('head')
    Monitoring | Monitoring Survey
@endsection

@section('title')
    <i class="fa fa-home"></i> Monitoring  |
    <span style="color:#2B6B97">
        <a href="{{ URL::to('monitoring/monitoringSurvey')}}">Monitoring Survey </a>
    </span>
@endsection
@section('button')
    <div class="forButtonTop">
      <form method="get" action="{{ URL::to('monitoring/monitoringSurvey/getDataBydate/srch')}}">
          {{ csrf_field() }}
          <span>Start Date : </span>
          <div style="display: inline-block;width: 100px" >
              <input type="text" class="form-control datepicker" name="startDateMonitoring" style="border-radius:10px!important">
          </div>
          <span><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
          <span>End Date : </span>
          <div style="display: inline-block;width: 100px">
              <input type="text" class="form-control datepicker" name="endDateMonitoring" style="border-radius:10px!important">
          </div>
          <span><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
          <button  class="btn btn-primary white btnTop" id="getDataMonitoring" style="margin-left:5px;">
              <i class="fa fa-ok" aria-hidden="true"></i> OK
          </button>

          <a  class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalSurvey">
                <i class="fa fa-filter" aria-hidden="true"></i> Filter
            </a>

 
          <a href="" class="btn btn-primary white btnTop" style="margin-left:5px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
        </form>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box box-body">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        @if($ket == 'Search Date')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cariDari}} - {{$cariSmp}}</span>
                        @elseif($ket == 'Search Status')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}"</span>
                        @elseif($ket == 'Search Data')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}} {{$cari}}"</span>
                        @endif
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3" style="text-align:right;margin-bottom: 15px;">
                        <form method="get" action="{{ URL::to('monitoring/monitoringSurvey/search/srch')}}">
                            <input type="text" class="form-control search" name="search" placeholder="Type to search"/>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <table class="tableCus table-striped js-sort-table">
                        <thead class="thead" style="width: 1219px;font-size:14px;">
                            <tr style="background-color:#8B9AAD;color:#fff">
                                <th style="width: 42px;">No</th>
                                <th style="width: 85px;">No Survey</th>
                                <th style="width: 85px;">Survey Type</th>
                                <th style="width: 100px;">Survey Date</th>
                                <th style="width: 65px;">AB</th>
                                <th style="width: 70px;">PC-EZ</th>
                                <th style="width: 77px;">Nomen</th>
                                <th style="width: 124px;">Asset</th>
                                <th style="width: 121px;">Assign</th>
                                <th style="width: 85px;">Start Date</th>
                                <th style="width: 85px;">End Date</th>
                                <th style="width: 104px;">Status Survey</th>
                                <th style="width: 42px;">Sending</th>
                                <th style="width: 70px;"></th>
                            </tr>
                        </thead>
                        <tbody id="table" class="tbody" style="width: 1219px;font-size:14px;">
                            @foreach($list_data as $key=>$survey)
                              <tr>
                                  <td style="width: 42px;">{{++$key}}</td>
                                  <td style="width: 85px;">{{$survey->survey_code}}</td>
                                  <td style="width: 85px;">{{$survey->survey_name}}</td>
                                  <td style="width: 100px;">{{$survey->survey_date}}</td>
                                  <td style="width: 42px;">
                                    <button class="btn btn-primary srch" data-srch='srch' onclick="detail_ab('{{$survey->AB_id}}')" style="margin-left: 5px;"> <i class="fa fa-search"></i> </button>
                                  </td>
                                  <td style="width: 70px;">
                                    <button class="btn btn-primary srch" data-srch='srch' onclick="detail_pc('{{$survey->id}}')" style="margin-left: 5px;"> <i class="fa fa-search"></i>
                                    </button>
                                  </td>
                                  <td style="width: 78px;">{{($survey->nomen) ? $survey->nomen : '-'}}</td>
                                  <td style="width: 124px;">{{($survey->asset) ? $survey->asset : '-'}}</td>
                                  <td style="width: 123px;">{{$survey->worker_name}}</td>
                                  <td style="width: 86px;">{{$survey->start_date}}</td>
                                  <td style="width: 86px;">{{$survey->end_date}}</td>
                                  <td style="width: 105px;">{{$survey->survey_status}}</td>
                                  <td style="width: 42px;">{{$survey->sending_status}}</td>
                                  <td>
                                   <button class="btn btn-primary srch" data-srch='srch' onclick="detailMonitoring('{{$survey->survey_code}}')"><i class="fa fa-search"></i></button>
                                  </td>
                                </tr>
                              @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12">
            {{ $list_data->render() }}
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->



<!-- Modal   Add -->
<div class="modal fade" id="filterModalSurvey" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Filter</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
        <div class="alert alert-danger" id="error-alert" style="display: none"></div>
        
           <form id="formStatusFilter" method="get" action="{{ URL::to('monitoring/monitoringSurvey/filterMonitoringSurvey/srch')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">

              <!-- <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Status</label>
                </div>
                <div class="col-sm-8">
                  <input type="checkbox" class="minimal" name="status_filter[]" value="All"> <label style="font-weight: normal; margin-right: 10px;"> All </label>
                </div>
              </div> -->
              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label">Status</label>
                </div>
                <div class="col-sm-8">
                  <input type="checkbox" class="minimal" name="status_filter[]" value="Assigned"> <label style="font-weight: normal; margin-right: 10px;"> Assigned </label>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label"></label>
                </div>
                <div class="col-sm-8">
                  <input type="checkbox" class="minimal" name="status_filter[]" value="Started"> <label style="font-weight: normal; margin-right: 10px;"> Start </label>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label"></label>
                </div>
                <div class="col-sm-8">
                  <input type="checkbox" class="minimal" name="status_filter[]" value="Finished"> <label style="font-weight: normal; margin-right: 10px;"> Finish </label>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-3">
                  <label class="col-sm-12 col-form-label"></label>
                </div>
                <div class="col-sm-8">
                  <input type="checkbox" class="minimal" name="status_filter[]" value="Expired"> <label style="font-weight: normal; margin-right: 10px;"> Expired </label>
                </div>
              </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                  <input type="submit" class="btn btn-primary btnFooterModal" value="OK">
              </div>
          </form><!-- /form -->
      </div><!-- / Modal Body -->
<!--       <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="filterBtn()">Submit</button>
      </div> -->
    </div>
  </div>
</div>


<!-- Modal   Add -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Detail Survey</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
        <div class="alert alert-danger" id="error-alert" style="display: none"></div>
        
           <form id="formGroup">
            <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->

            <!-- Table Karakteristik -->
            <div class="col-sm-4">
                <div class="row">
                    <table id="tableCharacteristic" class="table table-bordered table-striped">
                      <thead>
                      <tr style="background-color:#3896D6;">
                        <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="3">Characteristic</th>
                      </tr>
                      <tr>
                          <th style="vertical-align: middle!important; text-align: center!important;">Nama</th>
                          <th style="vertical-align: middle!important; text-align: center!important;">Nilai</th>
                      </tr>
                      </thead>
                    </table>
                  </div>
            </div>
            <!-- End Table Karakteristik -->

            <!-- Table Photo Before -->
            <div class="col-sm-4">
                <div class="row">
                  <table id="tablePhotoBefore" class="table table-bordered table-striped">
                    <thead>
                    <tr style="background-color:#3896D6;">
                      <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="2">Photo Before</th>
                    </tr>
                    <tr>
                      <th style="vertical-align: middle!important; text-align: center!important;">Image</th>
                    </tr>
                    </thead>
                  </table>
                </div>
            </div>
            <!-- End Table Photo Before -->

            <!-- Table Photo After -->
            <div class="col-sm-4">
              <div class="row">
                <table id="tablePhotoAfter" class="table table-bordered table-striped">
                  <thead>
                  <tr style="background-color:#3896D6;">
                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="2">Photo After</th>
                  </tr>
                  <tr>
                    <th style="vertical-align: middle!important; text-align: center!important;">Image</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div>
            <!-- End Table Photo After -->

            <!-- <div class="col-sm-4">
              <div class="row">
                <table id="tableDownload" class="table table-bordered table-striped">
                  <thead>
                  <tr style="background-color:#3896D6;">
                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="2">Generate PDF</th>
                  </tr>
                  <tr>
                    <th style="vertical-align: middle!important; text-align: center!important;">Image</th>
                  </tr>
                  </thead>
                </table>
              </div>
            </div> -->
          </form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        
      </div>
    </div>
  </div>
</div>

<!-- Modal View Detail  -->
<div class="modal fade" id="modalViewDetail" tabindex="-1" role="dialog" aria-labelledby="modalViewDetail" aria-hidden="false" >
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: auto">
            <div class="modal-header">
                <button type="button" class="close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;"></div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center"></div>
        </div>
    </div>
</div>
<!-- end View Detail -->

@endsection

@section('js')
  @include('monitoring.monitoringSurvey.js_monitoringSurvey')
@endsection
