@extends('layouts.app')

@section('head')
    Monitoring | Map Status
@endsection

@section('title')
  <i class="fa fa-home"></i> Monitoring |  <span style="color:#2B6B97"> Map Status </span>
@endsection
@section('button')
<div class="row forButtonTop">
	<div class="col-md-12">
    <div style="display: inline-block;width: 250px">
      <input type="text" class="search-query form-control" placeholder="Search" id="searchtext"/>
    </div>
    <a href="#" id="btnSearch" class="btn btn-danger" type="button" style="height: 34px;margin:-32px 0px -29px -43px;border-radius: 20px;">
      <i class=" glyphicon glyphicon-search"></i>
    </a>
	  <a  class="btn btn-primary white btnTop" data-toggle="modal" data-target="#fileterModal" style="margin-left:5px;">
  		<i class="fa fa-upload" aria-hidden="true"></i> Filter
    </a>
    <a class="btn btn-primary white btnTop" style="margin-left:5px;" id="btnRefresh">
			<i class="fa fa-refresh" aria-hidden="true"></i> Refresh
		</a>
	</div>
</div>
@endsection

@section('content')
{{--<section class="content">--}}
	<!--- Wiem Added --->
	<div id="map" style="margin: 0;"></div>
	<!-- The Modal -->
    <div class="modal" id="fileterModal">
        <div class="modal-dialog" style="width:300px;">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Filter</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Type :</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="typeFA">
                                <label class="custom-control-label" for="typeFA">FA</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="typeWO">
                                <label class="custom-control-label" for="typeWO">WO</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Status :</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusAssigned">
                                <label class="custom-control-label" for="statusAssigned">Assigned</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusOnTheWay">
                                <label class="custom-control-label" for="statusOnTheWay">On The Way</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusStarted">
                                <label class="custom-control-label" for="statusStarted">Started</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusComplete">
                                <label class="custom-control-label" for="statusComplete">Complete</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusCancelByCrew">
                                <label class="custom-control-label" for="statusCancelByCrew">Cancel By Crew</label>
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" checked="checked" id="statusCancelByAdmin">
                                <label class="custom-control-label" for="statusCancelByAdmin">Cancel By Admin</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary  btnFooterModal" data-dismiss="modal" id="btnSubmit">Submit</button>
                </div>
            </div>
        </div>
    </div>
    <!--- /Wiem Added --->
{{--</section>--}}
<!-- /section -->
@endsection
@section('js')
  @include('monitoring.mapStatus.js_mapStatus')
@endsection
