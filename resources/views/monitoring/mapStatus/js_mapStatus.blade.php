<script type="text/javascript">

// for style checkbox
$( document ).ready(function() {
  	styleChek();
});

//for datatable 
$('#table').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { "data": "nameGroup" },
        { "data": "description" },
        { "data": "description" },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="detail(`' + row.id + '`)"><i class="fa fa-pencil"></i></button>';
            }
		}
    ],
    responsive: true,
});

// for style checkbox
function styleChek()
{
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass   : 'iradio_minimal-green'
    });
}
    

// for add row table fa
function addRowFa()
{
	var table = $('#tableFA');

	table.append('<tr  class="rowFa">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowFa()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

// for delete row table fa
function deleteRowFa()
{
	document.getElementById("tableFA").deleteRow(1);
}

// for add row table wo
function addRowWO()
{
	var table = $('#tableWO');

	table.append('<tr  class="rowWo">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowWo()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

//// for delete row table wo
function deleteRowWo()
{
	document.getElementById("tableWO").deleteRow(1);
}

/// for save group 
function saveGroup()
{
	var data = $('#formGroup').serialize();

	$.ajax({
		url:"",
		data:data,
		dataType:'JSON',
		type:'POST',
		success:function(data){
			if(data.errors)
			{
				$.each(data.errors,function(key,value){
					$('.alert-danger').show();
					$('.alert-danger').html('<li>'+value+'</li>');
				})

				setTimeout(function() {
					$('#error-alert').fadeOut('fast');
				}, 2000); 
			}
			else
			{
				swal('Success','Group has been saved successfully!','success');
				location.reload();
			}
		},
          error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
          },
	})
}
// wiem added
	  var host = "<?php echo $ip ?>";
	  var dispatch_group = '<?php echo $userinfo ?>';
	  //var dispatch_group = "<?php echo serialize($userinfo) ?>";
	  var map;
      const TILE_ORIGIN = [-20037508.34789244, 20037508.34789244];
      const ORIG_X = 0;
      const ORIG_Y = 1;
      const MAP_SIZE = 20037508.34789244 * 2;
      const MINX = 0;
      const MAXX = 1;
      const MINY = 2;
      const MAXY = 3;
	  var markers = [];

     
      function getBoundingBox(x, y, zoom){
         var bbox = [];
		 
		 tileSize = MAP_SIZE / Math.pow(2, zoom);
         minx = TILE_ORIGIN[ORIG_X] + x * tileSize;
         maxx = TILE_ORIGIN[ORIG_X] + (x+1) * tileSize;
         miny = TILE_ORIGIN[ORIG_Y] - (y+1) * tileSize;
         maxy = TILE_ORIGIN[ORIG_Y] - y * tileSize;

         //bbox = [minx, miny, maxx, maxy];
		 
		 bbox[MINX] = minx;
         bbox[MINY] = miny;
         bbox[MAXX] = maxx;
         bbox[MAXY] = maxy;
         


        return bbox;
      }
      function initMap() {
		 
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -6.160323, lng: 106.905114},
          zoom: 18,
		  disableDefaultUI: true,
        });
		
		var imageMapType = new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
          resultBBox = getBoundingBox(coord.x, coord.y,  zoom);
          //url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          //console.log(url);
		  if(host.startsWith('172.27') == true){
            url = "http://172.27.1.39:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          }else{
            url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";
          }
          //url = "http://117.54.100.220:6080/arcgis/services/Asset/AssetMAP_Mobile_WGS84_3857/MapServer/WMSServer?LAYERS=1,2,3,4,5&TILED=true&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image/png&SRS=EPSG:3857&BBOX="+resultBBox[MINX]+","+resultBBox[MINY]+","+resultBBox[MAXX]+","+resultBBox[MAXY]+"&WIDTH=256&HEIGHT=256";

          //console.log(url);

              return url;

            },

            tileSize: new google.maps.Size(256, 256)
        });
		
		  map.overlayMapTypes.push(imageMapType);
		  
		var zoomDiv = document.createElement('div');
        var renderZoomControls = new ZoomControl(zoomDiv, map);
        zoomDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(zoomDiv);

        var legendDiv = document.createElement('div');
        var renderLegendControl = new legendControl(legendDiv, map);
        legendDiv.index = 1;
        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legendDiv);

        var tileloadedListerner = map.addListener('tilesloaded', function() {
          console.log('Map center change ');
        });
        map.addListener('dragstart', function(){

          if(tileloadedListerner!= null){
				console.log('removeListener ');
				google.maps.event.removeListener(tileloadedListerner);
			}
        });

        
      }
	  
	  function ZoomControl(div, map) {

          // Get the control DIV. We'll attach our control UI to this DIV.
          var controlDiv = div;

          // Set CSS for the controls.
          controlDiv.style.margin = '28px 0px 0px 22px';
          controlDiv.style.cursor = 'pointer';
          controlDiv.style.opacity = "0.8";
          controlDiv.style.backgroundColor = "#00000000";
          controlDiv.style.fontFamily = 'Open Sans';
          controlDiv.style.height = '77px';
          controlDiv.style.width = '36px';
          controlDiv.style.marginRight= '20px';




          var zoomin = document.createElement('div');
          zoomin.title = 'Click to zoom in';
          zoomin.style.display = "block"
          zoomin.style.width = 'auto';
          zoomin.style.height = 'auto';
            zoomin.style.backgroundColor = "#FFFFFF";
          zoomin.style.float='left';

          controlDiv.appendChild(zoomin);

          var zoominIcon = document.createElement('img');
          zoominIcon.src='/dist/img/icon_zoomIn_xxxhdpi.png'
          zoominIcon.style.width = '30px';
          zoominIcon.style.height = '30px';
          zoomin.appendChild(zoominIcon);

          var zoomout = document.createElement('div');
          zoomout.title = 'Click to zoom out';
          zoomout.style.display = "block"
          zoomout.style.borderRight = "1px solid #9e9e9e"
          zoomout.style.width = 'auto';
          zoomout.style.height = 'auto';
          zoomout.style.float='left';
          zoomout.style.backgroundColor = "#FFFFFF";
          zoomout.style.marginTop = '5px';
          controlDiv.appendChild(zoomout);

          var zoomoutIcon = document.createElement('img');
          zoomoutIcon.src='/dist/img/icon_zoomOut_xxxhdpi.png'
          zoomoutIcon.style.width = '30px';
          zoomoutIcon.style.height = '30px';
          zoomout.appendChild(zoomoutIcon);

          // Setup the click event listeners for zoom-in, zoom-out:
          google.maps.event.addDomListener(zoomout, 'click', function() {
           var currentZoomLevel = map.getZoom();
           if(currentZoomLevel != 0){
             map.setZoom(currentZoomLevel - 1);}
          });

           google.maps.event.addDomListener(zoomin, 'click', function() {
           var currentZoomLevel = map.getZoom();
           if(currentZoomLevel != 21){
             map.setZoom(currentZoomLevel + 1);}
          });
		  
		  //call drawAllJobs
		  console.log(dispatch_group);
		  drawAllJobs(map, dispatch_group, '[2]', 'fa_group');
		  


        }
		function clearMarkers() {
			setMapOnAll(null);
		}
		function setMapOnAll(map) {
			for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
			}
		}
		function showMarkers() {
			setMapOnAll(map);
		}
		function deleteMarkers() {
			clearMarkers();
			markers = [];
		}


		function drawAllJobs(map, dispatch_gr, filter_st, filter_gr){
		
			$.ajax({
        		url:"/api/get_all_job_map",
        		data:{dispatch_group:dispatch_gr, filter_status: filter_st, filter_group:filter_gr},
        		dataType:'JSON',
        		type:'POST',
        		success:function(data){
        			console.log(data);
					if(data.errors)
        			{
						
        			}
        			else
        			{
						
						createAllJobsMarker(map, data);
        			}
        		},
            error: function (xhr, ajaxOptions, thrownError) {

            },
        	})
		}
		
		function searchJobs(map, fa_transaction_id){
		
			$.ajax({
        		url:"/api/search_job_map",
        		data:{id:fa_transaction_id},
        		dataType:'JSON',
        		type:'POST',
        		success:function(data){
        			console.log(data);
					if(data.errors)
        			{
						
        			}
        			else
        			{
						gotoJob(map, data);
						//createAllJobsMarker(map, data);
        			}
        		},
            error: function (xhr, ajaxOptions, thrownError) {

            },
        	})
		}
		
		function zoomToFitMarkers(map, markers){
			var bounds = new google.maps.LatLngBounds();
			for(i=0;i<markers.length;i++) {
			   bounds.extend(markers[i].getPosition());
			}

			//center the map to a specific spot (city)
			//map.setCenter(center); 

			//center the map to the geometric center of all markers
			map.setCenter(bounds.getCenter());

			map.fitBounds(bounds);

			//remove one zoom level to ensure no marker is on the edge.
			map.setZoom(map.getZoom()-1); 

			// set a minimum zoom 
			// if you got only 1 marker or all markers are on the same address map will be zoomed too much.
			if(map.getZoom()> 15){
			  map.setZoom(15);
			}

			//Alternatively this code can be used to set the zoom for just 1 marker and to skip redrawing.
			//Note that this will not cover the case if you have 2 markers on the same address.
			if(markers.length == 1){
				map.setMaxZoom(15);
				map.fitBounds(bounds);
				map.setMaxZoom(Null)
			}
			
			/*var boundschangedListener = map.addListenerOnce(map, 'bounds_changed', function(event) {
				map.setZoom(map.getZoom()-1);

			  if (map.getZoom() > 15) {
				map.setZoom(15);
			  }
			});*/
			map.fitBounds(bounds);
		}
		
		function createAllJobsMarker(map, data){
			deleteMarkers();
			var strData = JSON.stringify(data);
			var objJson = JSON.parse(strData);
			var obj, lat, lon , status, title = "";
			for(i in objJson){
				obj = objJson[i];
				lon = obj.long;
				lat = obj.lat;
				status = obj.status;
				title = obj.id + '\n' + obj.type;
				var icon = "";
				
				switch(status){
					case 1:
						icon = {
							url: "/dist/img/icon_FA_open_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_open_xxxhdpi.png";
						break;
					case 2:
						icon = {
							url: "/dist/img/icon_FA_Assigned_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_Assigned_xxxhdpi.png";
						break;
					case 3:
						icon = {
							url: "/dist/img/icon_FA_Assigned_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_Assigned_xxxhdpi.png";
						break;
					case 4:
						icon = {
							url: "/dist/img/icon_FA_OnTheWay_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_OnTheWay_xxxhdpi.png";
						break;
					case 5:
						icon = {
							url: "/dist/img/icon_FA_OnTheWay_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_OnTheWay_xxxhdpi.png";
						break;
					case 6:
						icon = {
							url: "/dist/img/icon_FA_Started_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_Started_xxxhdpi.png";
						break;
					case 7:
						icon = {
							url: "/dist/img/icon_FA_CancelWorker_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_CancelWorker_xxxhdpi.png";
						break;
					default:
						icon = {
							url: "/dist/img/icon_FA_CancelAdmin_xxxhdpi.png", // url
							scaledSize: new google.maps.Size(25, 25), // scaled size
							origin: new google.maps.Point(0,0), // origin
							anchor: new google.maps.Point(0, 0) // anchor
						};
						//icon = "/dist/img/icon_FA_CancelAdmin_xxxhdpi.png";
						break;
				}
				createMarker(map, lat, lon, icon, title);
			}
			
			zoomToFitMarkers(map, markers);
						
		}
		
		function gotoJob(map, data){
			var strData = JSON.stringify(data);
			var objJson = JSON.parse(strData);
			var obj, lat, lon , status, title = "";
			for(i in objJson){
				obj = objJson[i];
				lon = obj.long;
				lat = obj.lat;
				
				var center = new google.maps.LatLng(lat, lng);
				// using global variable:
				map.panTo(center);
				
			}
			
		}
		
		function createMarker(map, lat, lon, icon, title){
			var position = new google.maps.LatLng(lat, lon);
			
			var marker = new google.maps.Marker({
				position: position,
				icon: icon,
				map: map,
				title: title
           });
		   
			/*var marker = new MarkerWithLabel({
				position: position,
				animation: google.maps.Animation.DROP,
				icon: icon,
				map: map,
				labelContent: title,
				labelAnchor: new google.maps.Point(18, 12),
				labelClass: "my-custom-class-for-label", // the CSS class for the label
				labelInBackground: true
          });*/
		  markers.push(marker);
		}

        function legendControl(div, map){
           var legendDiv = div;

           legendDiv.style.cursor = 'pointer';
           legendDiv.style.opacity = "0.8";
           legendDiv.style.backgroundColor = "#00000000";
           legendDiv.style.fontFamily = 'Open Sans';
           legendDiv.style.height = '300px';
           legendDiv.style.width = '120px';
           legendDiv.style.marginRight='5px';
           legendDiv.style.marginBottom='20px';

           var faDiv = document.createElement('div');
           faDiv.title = 'Click to zoom in';
           faDiv.style.display = "block"
           faDiv.style.width = '100%';
           faDiv.style.height = 'auto';
           faDiv.style.backgroundColor = "#FFFFFF";
           faDiv.style.float='left';


           legendDiv.appendChild(faDiv);

           var faHeaderText= document.createElement('div');
           faHeaderText.innerHTML='<strong>FA List</strong>';
           faHeaderText.style.color='#ffffff';
           faHeaderText.style.display = "inline-block"
           faHeaderText.style.width = '100%';
           faHeaderText.style.height = '20px';
           faHeaderText.style.backgroundColor = "#4286f4";
           faHeaderText.style.float='left';
           faHeaderText.style.textAlign  = 'center';
           faHeaderText.style.verticalAlign="middle";
           faHeaderText.style.lineHeight="20px";

           faDiv.appendChild(faHeaderText);

           var faStatusOpenDiv = document.createElement('div');
           faStatusOpenDiv.style.width='100%';
           faStatusOpenDiv.style.height='auto';
           faStatusOpenDiv.style.float='left';
           faDiv.appendChild(faStatusOpenDiv);

           var faOpenIcon = document.createElement('img');
           faOpenIcon.style.width='20px';
           faOpenIcon.style.height='20px';
           faOpenIcon.src='/dist/img/icon_FA_open_xxxhdpi.png';
           faStatusOpenDiv.appendChild(faOpenIcon);

           var faOpenText = document.createElement('a');
           faOpenText.innerHTML='Open';
           faOpenText.style.width='auto';
           faOpenText.style.height='auto';
           faStatusOpenDiv.appendChild(faOpenText);

           var faStatusAssignedDiv = document.createElement('div');
           faStatusAssignedDiv.style.width='100%';
           faStatusAssignedDiv.style.height='auto';
           faStatusAssignedDiv.style.float='left';
           faDiv.appendChild(faStatusAssignedDiv);

           var faAssignedIcon = document.createElement('img');
           faAssignedIcon.style.width='20px';
           faAssignedIcon.style.height='20px';
           faAssignedIcon.src='/dist/img/icon_FA_Assigned_xxxhdpi.png';
           faStatusAssignedDiv.appendChild(faAssignedIcon);

           var faAssignedText = document.createElement('a');
           faAssignedText.innerHTML='Assigned';
           faAssignedText.style.width='auto';
           faAssignedText.style.height='auto';
           faStatusAssignedDiv.appendChild(faAssignedText);

           var faStatusOnTheWayDiv = document.createElement('div');
           faStatusOnTheWayDiv.style.width='100%';
           faStatusOnTheWayDiv.style.height='auto';
           faStatusOnTheWayDiv.style.float='left';
           faDiv.appendChild(faStatusOnTheWayDiv);

           var faOnTheWayIcon = document.createElement('img');
           faOnTheWayIcon.style.width='20px';
           faOnTheWayIcon.style.height='20px';
           faOnTheWayIcon.src='/dist/img/icon_FA_OnTheWay_xxxhdpi.png';
           faStatusOnTheWayDiv.appendChild(faOnTheWayIcon);

           var faAOnTheWayText = document.createElement('a');
           faAOnTheWayText.innerHTML='On The Way';
           faAOnTheWayText.style.width='auto';
           faAOnTheWayText.style.height='auto';
           faStatusOnTheWayDiv.appendChild(faAOnTheWayText);

           var faStatusStartedDiv = document.createElement('div');
           faStatusStartedDiv.style.width='100%';
           faStatusStartedDiv.style.height='auto';
           faStatusStartedDiv.style.float='left';
           faDiv.appendChild(faStatusStartedDiv);

           var faStartedIcon = document.createElement('img');
           faStartedIcon.style.width='20px';
           faStartedIcon.style.height='20px';
           faStartedIcon.src='/dist/img/icon_FA_Started_xxxhdpi.png';
           faStatusStartedDiv.appendChild(faStartedIcon);

           var faStartedText = document.createElement('a');
           faStartedText.innerHTML='Started';
           faStartedText.style.width='auto';
           faStartedText.style.height='auto';
           faStatusStartedDiv.appendChild(faStartedText);

           var faStatusCompletedDiv = document.createElement('div');
           faStatusCompletedDiv.style.width='100%';
           faStatusCompletedDiv.style.height='auto';
           faStatusCompletedDiv.style.float='left';
           faDiv.appendChild(faStatusCompletedDiv);

           var faCompletedIcon = document.createElement('img');
           faCompletedIcon.style.width='20px';
           faCompletedIcon.style.height='20px';
           faCompletedIcon.src='/dist/img/icon_FA_Completed_xxxhdpi.png';
           faStatusCompletedDiv.appendChild(faCompletedIcon);

           var faCompletedText = document.createElement('a');
           faCompletedText.innerHTML='Completed';
           faCompletedText.style.width='auto';
           faCompletedText.style.height='auto';
           faStatusCompletedDiv.appendChild(faCompletedText);

           var faStatusCancelWorkerdDiv = document.createElement('div');
           faStatusCancelWorkerdDiv.style.width='100%';
           faStatusCancelWorkerdDiv.style.height='auto';
           faStatusCancelWorkerdDiv.style.float='left';
           faDiv.appendChild(faStatusCancelWorkerdDiv);

           var faCancelWorkerIcon = document.createElement('img');
           faCancelWorkerIcon.style.width='20px';
           faCancelWorkerIcon.style.height='20px';
           faCancelWorkerIcon.src='/dist/img/icon_FA_CancelWorker_xxxhdpi.png';
           faStatusCancelWorkerdDiv.appendChild(faCancelWorkerIcon);

           var faCancelWorkerdText = document.createElement('a');
           faCancelWorkerdText.innerHTML='Cancel Worker';
           faCancelWorkerdText.style.width='auto';
           faCancelWorkerdText.style.height='auto';
           faStatusCancelWorkerdDiv.appendChild(faCancelWorkerdText);

           var faStatusCancelAdmindDiv = document.createElement('div');
           faStatusCancelAdmindDiv.style.width='100%';
           faStatusCancelAdmindDiv.style.height='auto';
           faStatusCancelAdmindDiv.style.float='left';
           faDiv.appendChild(faStatusCancelAdmindDiv);

           var faCancelAdminIcon = document.createElement('img');
           faCancelAdminIcon.style.width='20px';
           faCancelAdminIcon.style.height='20px';
           faCancelAdminIcon.src='/dist/img/icon_FA_CancelAdmin_xxxhdpi.png';
           faStatusCancelAdmindDiv.appendChild(faCancelAdminIcon);

           var faCancelAdminText = document.createElement('a');
           faCancelAdminText.innerHTML='Cancel Admin';
           faCancelAdminText.style.width='auto';
           faCancelAdminText.style.height='auto';
           faStatusCancelAdmindDiv.appendChild(faCancelAdminText);








           var WoDiv = document.createElement('div');
           WoDiv.style.display = "block"
           WoDiv.style.width = '100%';
           WoDiv.style.height = 'auto';
           WoDiv.style.backgroundColor = "#FFFFFF";
           WoDiv.style.float='left';
           WoDiv.style.marginTop= '5px';

           legendDiv.appendChild(WoDiv);

           var woHeaderText= document.createElement('div');
           woHeaderText.innerHTML='<strong>WO List</strong>';
           woHeaderText.style.color='#ffffff';
           woHeaderText.style.display = "inline-block"
           woHeaderText.style.width = '100%';
           woHeaderText.style.height = '20px';
           woHeaderText.style.backgroundColor = "#4286f4";
           woHeaderText.style.float='left';
           woHeaderText.style.textAlign  = 'center';
           woHeaderText.style.verticalAlign="middle";
           woHeaderText.style.lineHeight="20px";



           WoDiv.appendChild(woHeaderText);

           var woStatusOpenDiv = document.createElement('div');
           woStatusOpenDiv.style.width='100%';
           woStatusOpenDiv.style.height='auto';
           woStatusOpenDiv.style.float='left';
           WoDiv.appendChild(woStatusOpenDiv);

           var woOpenIcon = document.createElement('img');
           woOpenIcon.style.width='20px';
           woOpenIcon.style.height='20px';
           woOpenIcon.src='/dist/img/icon_WO_open_xxxhdpi.png';
           woStatusOpenDiv.appendChild(woOpenIcon);

           var woOpenText = document.createElement('a');
           woOpenText.innerHTML='Open';
           woOpenText.style.width='auto';
           woOpenText.style.height='auto';
           woStatusOpenDiv.appendChild(woOpenText);

           var woStatusAssignedDiv = document.createElement('div');
           woStatusAssignedDiv.style.width='100%';
           woStatusAssignedDiv.style.height='auto';
           woStatusAssignedDiv.style.float='left';
           WoDiv.appendChild(woStatusAssignedDiv);

           var woAssignedIcon = document.createElement('img');
           woAssignedIcon.style.width='20px';
           woAssignedIcon.style.height='20px';
           woAssignedIcon.src='/dist/img/icon_WO_Assigned_xxxhdpi.png';
           woStatusAssignedDiv.appendChild(woAssignedIcon);

           var woAssignedText = document.createElement('a');
           woAssignedText.innerHTML='Assigned';
           woAssignedText.style.width='auto';
           woAssignedText.style.height='auto';
           woStatusAssignedDiv.appendChild(woAssignedText);

           var woStatusOnTheWayDiv = document.createElement('div');
           woStatusOnTheWayDiv.style.width='100%';
           woStatusOnTheWayDiv.style.height='auto';
           woStatusOnTheWayDiv.style.float='left';
           WoDiv.appendChild(woStatusOnTheWayDiv);

           var woOnTheWayIcon = document.createElement('img');
           woOnTheWayIcon.style.width='20px';
           woOnTheWayIcon.style.height='20px';
           woOnTheWayIcon.src='/dist/img/icon_WO_OTW_xxxhdpi.png';
           woStatusOnTheWayDiv.appendChild(woOnTheWayIcon);

           var woAOnTheWayText = document.createElement('a');
           woAOnTheWayText.innerHTML='On The Way';
           woAOnTheWayText.style.width='auto';
           woAOnTheWayText.style.height='auto';
           woStatusOnTheWayDiv.appendChild(woAOnTheWayText);

           var woStatusStartedDiv = document.createElement('div');
           woStatusStartedDiv.style.width='100%';
           woStatusStartedDiv.style.height='auto';
           woStatusStartedDiv.style.float='left';
           WoDiv.appendChild(woStatusStartedDiv);

           var woStartedIcon = document.createElement('img');
           woStartedIcon.style.width='20px';
           woStartedIcon.style.height='20px';
           woStartedIcon.src='/dist/img/icon_WO_Started_xxxhdpi.png';
           woStatusStartedDiv.appendChild(woStartedIcon);

           var woStartedText = document.createElement('a');
           woStartedText.innerHTML='Started';
           woStartedText.style.width='auto';
           woStartedText.style.height='auto';
           woStatusStartedDiv.appendChild(woStartedText);

           var woStatusCompletedDiv = document.createElement('div');
           woStatusCompletedDiv.style.width='100%';
           woStatusCompletedDiv.style.height='auto';
           woStatusCompletedDiv.style.float='left';
           WoDiv.appendChild(woStatusCompletedDiv);

           var woCompletedIcon = document.createElement('img');
           woCompletedIcon.style.width='20px';
           woCompletedIcon.style.height='20px';
           woCompletedIcon.src='/dist/img/icon_WO_Completed_xxxhdpi.png';
           woStatusCompletedDiv.appendChild(woCompletedIcon);

           var woCompletedText = document.createElement('a');
           woCompletedText.innerHTML='Completed';
           woCompletedText.style.width='auto';
           woCompletedText.style.height='auto';
           woStatusCompletedDiv.appendChild(woCompletedText);

           var woStatusCancelWorkerdDiv = document.createElement('div');
           woStatusCancelWorkerdDiv.style.width='100%';
           woStatusCancelWorkerdDiv.style.height='auto';
           woStatusCancelWorkerdDiv.style.float='left';
           WoDiv.appendChild(woStatusCancelWorkerdDiv);

           var woCancelWorkerIcon = document.createElement('img');
           woCancelWorkerIcon.style.width='20px';
           woCancelWorkerIcon.style.height='20px';
           woCancelWorkerIcon.src='/dist/img/icon_WO_CancelWorker_xxxhdpi.png';
           woStatusCancelWorkerdDiv.appendChild(woCancelWorkerIcon);

           var woCancelWorkerdText = document.createElement('a');
           woCancelWorkerdText.innerHTML='Cancel Worker';
           woCancelWorkerdText.style.width='auto';
           woCancelWorkerdText.style.height='auto';
           woStatusCancelWorkerdDiv.appendChild(woCancelWorkerdText);

           var woStatusCancelAdmindDiv = document.createElement('div');
           woStatusCancelAdmindDiv.style.width='100%';
           woStatusCancelAdmindDiv.style.height='auto';
           woStatusCancelAdmindDiv.style.float='left';
           WoDiv.appendChild(woStatusCancelAdmindDiv);

           var woCancelAdminIcon = document.createElement('img');
           woCancelAdminIcon.style.width='20px';
           woCancelAdminIcon.style.height='20px';
           woCancelAdminIcon.src='/dist/img/icon_WO_CancelAdmin_xxxhdpi.png';
           woStatusCancelAdmindDiv.appendChild(woCancelAdminIcon);

           var woCancelAdminText = document.createElement('a');
           woCancelAdminText.innerHTML='Cancel Admin';
           woCancelAdminText.style.width='auto';
           woCancelAdminText.style.height='auto';
           woStatusCancelAdmindDiv.appendChild(woCancelAdminText);
			
			/*$.getScript( "/dist/js/gmaps-markerwithlabel-1.9.1.min.js" )
				.done(function( script, textStatus ) {
					console.log( textStatus );
				})
				.fail(function( jqxhr, settings, exception ) {
					console.log("load error");
			});*/
			

        }
		$("#btnRefresh").click(function(){
			var arrayStatus = '[' + getStatusFilter() + ']';
			console.log(arrayStatus);
			var typeFilter = getTypeFilter();;
			console.log(typeFilter.toString());
			drawAllJobs(map, dispatch_group, arrayStatus.toString(), typeFilter);
		});
		$("#btnSubmit").click(function(){
			$('#fileterModal').modal('hide');
			var arrayStatus = '[' + getStatusFilter() + ']';
			console.log(arrayStatus);
			var typeFilter = getTypeFilter();;
			console.log(typeFilter.toString());
			//'CC24-CB'
			drawAllJobs(map, dispatch_group, arrayStatus.toString(), typeFilter);
			
			//alert('clicked!');
		});
		$("#btnSearch").click(function(){
			var txtSearch = $("#searchtext").val();
			searchJobs(map, txtSearch);
			
		});
		
		function getTypeFilter(){
			var result = "";
			var typeFAChecked = $('#typeFA').is(":checked");
			var typeWOChecked = $('typeWO').is(":checked");
			if(typeFAChecked && !typeWOChecked){
				result = "fa_group";
			}else if(!typeFAChecked && typeWOChecked){
				result = "wo_group";
			}
			
			return result;
		}
		
		function getStatusFilter(){
			var arrayResult = [];
			if($('#statusAssigned').is(":checked")){
				arrayResult.push(2);
			}
			if($('#statusOnTheWay').is(":checked")){
				arrayResult.push(4);
			}
			if($('#statusStarted').is(":checked")){
				arrayResult.push(6);
			}
			if($('#statusComplete').is(":checked")){
				arrayResult.push(8);
			}
			if($('#statusCancelByCrew').is(":checked")){
				arrayResult.push(9);
			}
			if($('#statusCancelByAdmin').is(":checked")){
				arrayResult.push(10);
			}
			return arrayResult;
		}
    
    


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTwpUvair9cZMOGUcTb28L4o_iV3DObY&callback=initMap"
    async defer></script>
	

