<script type="text/javascript">

// for style checkbox
$( document ).ready(function() {
  	styleChek();
});

//search data table
var $rows = $('#table tr');
$('#search').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    $rows.show().filter(function() {
        var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
        return !~text.indexOf(val);
    }).hide();
});

// for style checkbox
function styleChek()
{
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass   : 'iradio_minimal-green'
    });
}

function detail_monitoring(no_wo, no_wo_task)
{   
    var srch = $(".srch").attr("data-srch");
    if (srch == 'srch') {
        $('#modal_detail').modal('show');
        $('#tableAssignmentJob').DataTable().destroy();
        $('#tableAssignmentJob').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getDetail/'+no_wo+'/'+no_wo_task,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "no_wo"},
                { "data" : "jobCode"},
                { "data": "assign_date" },
                { "data": "assign_status" }, 
                {
                    "mRender" : function(data,type,row,meta) {
                        return row.worker ? row.worker.worker_name : '';
                    }
                },
                {
                    "mRender" : function(data, type,row,meta){
                        return '<a href="{{ URL::to("monitoring/monitoringWO/downloadPdf")}}/'+no_wo+'" class="btn btn-primary white btnTop">'+
                                    '<i class="fa fa-download" aria-hidden="true"></i> Generate PDF'+
                                    '</a>'
                    }
                },

            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getCharacteristic/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "name_characteristic"},
                { "data" : "value_characteristic"},
                // {
                //     "mRender" : function(data, type,row,meta){
                //         return '<a href="javascript:void(0)" class="btn btn-primary" onclick="editChar('+row.id+')">Edit</a>'
                //     }
                // }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableMaterialUsage').DataTable().destroy();
        $('#tableMaterialUsage').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getMaterialUsage/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                {
                    // this for numbering table
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data" : "code"},
                { "data" : "name"},
                { "data" : "qty"},
                { "data" : "unit_of_purch"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tableSignature').DataTable().destroy();
        $('#tableSignature').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getTTD/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                // {
                //     // this for numbering table
                //     render: function (data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                { 
                    // "data" : "base64(photo_url)",
                    // "render": function(data, type, row) {
                    //     return '<img src="'+data+'" />';
                    // }
                    "render": function (data, type, row) {
                        return '<img width=200 height=100 src="' + row.photo_url + '">';
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getphotowo/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo1 || row.photo2) {
                            return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : '../getphotowo/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo3 || row.photo4) {
                            return '<tr><td><img width=100 height=100 alt="photo3" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo4" src="'+ row.photo4 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }else{
        $('#modal_detail').modal('show');
        $('#tableAssignmentJob').DataTable().destroy();
        $('#tableAssignmentJob').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getDetail/'+no_wo+'/'+no_wo_task,
                method : 'POST',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "no_wo"},
                { "data" : "jobCode"},
                { "data": "assign_date" },
                { "data": "assign_status" }, 
                {
                    "mRender" : function(data,type,row,meta) {
                        return row.worker ? row.worker.worker_name : '';
                    }
                },
                {
                    "mRender" : function(data, type,row,meta){
                        return '<a href="{{ URL::to("monitoring/monitoringWO/downloadPdf")}}/'+no_wo+'" class="btn btn-primary white btnTop">'+
                                    '<i class="fa fa-download" aria-hidden="true"></i> Generate PDF'+
                                    '</a>'
                    }
                },

            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableCharacteristic').DataTable().destroy();
        $('#tableCharacteristic').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getCharacteristic/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { "data" : "name_characteristic"},
                { "data" : "value_characteristic"},
                // {
                //     "mRender" : function(data, type,row,meta){
                //         return '<a href="javascript:void(0)" class="btn btn-primary" onclick="editChar('+row.id+')">Edit</a>'
                //     }
                // }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }

            ]
        });

        $('#tableMaterialUsage').DataTable().destroy();
        $('#tableMaterialUsage').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getMaterialUsage/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                {
                    // this for numbering table
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data" : "code"},
                { "data" : "name"},
                { "data" : "qty"},
                { "data" : "unit_of_purch"}
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tableSignature').DataTable().destroy();
        $('#tableSignature').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getTTD/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                // {
                //     // this for numbering table
                //     render: function (data, type, row, meta) {
                //         return meta.row + meta.settings._iDisplayStart + 1;
                //     }
                // },
                { 
                    // "data" : "base64(photo_url)",
                    // "render": function(data, type, row) {
                    //     return '<img src="'+data+'" />';
                    // }
                    "render": function (data, type, row) {
                        return '<img width=200 height=100 src="' + row.photo_url + '">';
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoBefore').DataTable().destroy();
        $('#tablePhotoBefore').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getphotowo/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo1 || row.photo2) {
                            return '<tr><td><img width=100 height=100 alt="photo1" src="'+ row.photo1 +'"></td><td><img width=100 height=100 alt="photo2" src="'+ row.photo2 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });

        $('#tablePhotoAfter').DataTable().destroy();
        $('#tablePhotoAfter').dataTable({
            paging: false,
            info : false,
            searching: false,
            ajax : {
                url : 'monitoringWO/getphotowo/'+no_wo+'/'+no_wo_task,
                method : 'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                { 
                    "render": function (data, type, row) {
                        if (row.photo3 || row.photo4) {
                            return '<tr><td><img width=100 height=100 alt="photo3" src="'+ row.photo3 +'"></td><td><img width=100 height=100 alt="photo4" src="'+ row.photo4 +'"></td></tr>';
                        }else{
                            return '';
                        }
                    }
                }
            ],
            columnDefs : [
                {
                    "targets" : "_all",
                    "className": "text-center",
                }
            ]
        });
    }
}

$('#getDataMonitoring').click(function(){
    var start_date = $('[name="startDateMonitoring"]').val();
    var end_date = $('[name="endDateMonitoring"]').val();

    if(start_date == '' || end_date =='')
    {
        return swal('Error',' Please Complete Insert Date ','error');
    }

    $('#tableVendor').dataTable().api().destroy();
    fetch_data_monitoring_fa('yes',start_date,end_date);
});

function fetch_data_monitoring_fa(is_date_search,start_date = '',end_date = '')
{
    $('#tableVendor').dataTable({
        processing: true,
        serverSide: true,
        order: [0, 'desc'],
        ajax: {
            method: 'POST',
            url : '{{route('monitoring.monitoringWO.getDataBydate')}}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            data:{
                is_date_search:is_date_search,
                start_date:start_date,
                end_date:end_date
            }
        },
        columns : [
            {
                // this for numbering table
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "asset_id" },
            { "data": "jobCode" },
            { "data": "no_wo" },
            { "data": "no_wo_task" },
            { "data": "task_desc" },
            { "data": "task_status" },
            { "data": "created_at" },
            {
                "mRender" : function(data,type,row,meta) {
                    return row.worker_id ? row.worker.worker_name : '';
                }
            },
            { "data": "assign_date" },
            {
                "mRender": function (data, type, row, meta) {
                    var urgent = '<p class="text-danger">Urgent</p>';
                    return (row.urgent_status == 1) ? urgent : 'Non Urgent';
                }
            },
            {
                "mRender":function(data,type,row,meta){
                    return row.priority_status ? row.priority_status : '';
                }
            },
            {
                "mRender":function(data,type,row,meta){
                    return '<button class="btn btn-primary" onclick="detail_monitoring('+row.id+')"> <i class="fa fa-search"></i> </button>'
                }
            },

        ],
        "columnDefs": [ {
            "targets": 0,
            "orderable": false
        }],
        responsive: true,
    });
}

function reloadPageMonitoringWO(){
  alertPopup("Sukses!", "Data Sudah Terupdate", "success");
  window.location.reload();
}
</script>