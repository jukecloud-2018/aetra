<html>
<head>
    <meta charset="utf-8">
    <title>Rekapitulasi Hasil Field Activity</title>
    <style>
        body {font-family:Arial, Helvetica, sans-serif;}
        table {border-collapse: collapse; width: 100%; }
        table, th, td {border: 1px solid slategray; margin: 5px;}
        .signature { display: inline; width: 30% }
    </style>
</head>
<body>
    <?php
        $path = public_path('/images/aetra.jpg');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $logo = 'data:image/' . $type . ';base64,' . base64_encode($data);
    ?>
    <img src="{{$logo}}" alt="Image" height="auto" width="25%" />
    <h3 style="text-align: center;">BERITA ACARA FIELD ORDER</h3>
    @foreach($list_task as $row)
    <p style="text-align: center;">WO Task {{ $row->no_wo }}</p>
	<pre>
Job Code            : {{$row->jobCode}}/{{$row->task_desc}}
Asset ID            : {{$row->asset_id}}
No Account          : {{$row->no_account}}
No Specification    : {{$row->no_specification}}
Area                : {{$row->area}}
Departement         : {{$row->department}}
    </pre>
	@endforeach

    <h5>Characteristik WO</h5>
    <table>
        <tr>
            <th>Job Code</th>
            <th>Deskripsi</th>
            <th>Quantity</th>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h5>Photo</h5>

    <h5>Signature :</h5>

       

</html>