@extends('layouts.app')
@section('head')
    Monitoring | Monitoring WO
@endsection

@section('title')
<i class="fa fa-home"></i> Monitoring  | <span style="color:#2B6B97"> <a href="{{ URL::to('monitoring/monitoringWO')}}">Monitoring WO </a></span>
@endsection
<meta http-equiv="refresh" content="300">
@section('button')
<div class="row forButtonTop">
    <div class="col-md-12">
        <form method="get" action="{{ URL::to('monitoring/monitoringWO/getData/srch')}}">
            {{ csrf_field() }}
            <span>Assigned Date : </span>
            <div style="display: inline-block;width: 100px" >
                <input type="text" class="form-control datepicker" name="startDateMonitoring" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px; margin-right: 10px;"></i></span>
            <span style="color: #8B9AAD;font-weight: bold; margin-right: 10px;"> > </span>
            <div style="display: inline-block;width: 100px">
                <input type="text" class="form-control datepicker" name="endDateMonitoring" style="border-radius:10px">
            </div>
            <span><i class="fa fa-calendar" style="font-size: 24px;"></i></span>
            <input type="submit" class="btn btn-primary white btnTop" style="margin-left:5px;" value="OK">
            <a  class="btn btn-primary white btnTop" style="margin-left:5px;" data-toggle="modal" data-target="#filterModalWO">
                <i class="fa fa-filter" aria-hidden="true"></i> Filter
            </a>
            <a class="btn btn-primary white btnTop" style="margin-left:5px;" onclick="reloadPageMonitoringWO()">Refresh</a>
        </form>
    </div>
</div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box-body">
            <div class="col-md-12" style="margin-top: -20px;">
                <div class="row">
                    <div class="col-md-6">
                        @if($ket == 'Search Data')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cari}} </span>
                        @elseif($ket == 'Search Date')
                            <span class="col-md-12" style="padding-left: 0;margin-top: 20px;">Result of : "{{$total}}" {{$cariDari}} - {{$cariSmp}}</span>
                        @endif
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3" style="text-align:right;">
                        <form method="get" action="{{ URL::to('monitoring/monitoringWO/search/srch')}}" style="margin-bottom:10px;">
                            <input type="text" class="form-control search" name="search" placeholder="Type to search"/>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <table class="tableCus table-striped js-sort-table">
                        <thead class="thead" style="width: 2353px;font-size:14px;">
                            <tr style="background-color:#8B9AAD;color:#fff">
                                <th style="width: 45px;" class="js-sort-number">No</th>
                                <th style="width: 135px;" class="js-sort-number">Asset ID</th>
                                <th style="width: 100px;" class="js-sort-string">Job Code</th>
                                <th style="width: 100px;" class="js-sort-number">No WO</th>
                                <th style="width: 80px;" class="js-sort-number">No WO Task</th>
                                <th style="width: 80px;" class="js-sort-number">Asset Type</th>
                                <th style="width: 460px;" class="js-sort-string">Asset Description</th>
                                <th style="width: 80px;" class="js-sort-string">Supervisor</th>
                                <th style="width: 90px;" class="js-sort-string">Task Status</th>
                                <th style="width: 140px;" class="js-sort-date">Task Status Date/Time</th>
                                <th style="width: 170px;">Assign TO</th>
                                <th style="width: 70px;"></th>
                                <th style="width: 191px;" class="js-sort-date">Assign Date</th>
                                <th style="width: 170px;" class="js-sort-date">Re-Assign Date</th>
                                <th style="width: 105px;" class="js-sort-string">Worker Status</th>
                                <th style="width: 67px;">Urgent</th>
                                <th style="width: 75px;">Priority</th>
                                <th style="width: 90px;">Sending</th>
                                <th style="width: 70px;"></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table" class="tbody" style="width: 2353px;font-size:14px;">
                            @foreach($data as $key=>$row)
                                <tr role="row">
                                    <td style="width: 40px;text-align: center;">{{++$key}}</td>
                                    <td style="width: 130px;">{{$row->asset_id}}</td>
                                    <td style="width: 100px;">{{$row->jobCode}}</td>
                                    <td style="width: 100px;">{{$row->no_wo}}</td>
                                    <td style="width: 80px;">{{$row->no_wo_task}}</td>
                                    <td style="width: 80px;">{{$row->asset_type}}</td>
                                    <td style="width: 450px;">{{$row->asset_desc}}</td>
                                    <td style="width: 100px;">{{$row->supervisor}}</td>
                                    <td style="width: 100px;">{{$row->task_status}}</td>
                                    <td style="width: 170px;">{{$row->created_at}}</td>
                                    <td style="width: 160px;">{{$row->worker_name}}</td>
                                    <td style="width: 186px;">{{$row->assign_date}}</td>
                                    <td style="width: 170px;">{{$row->reassign_date}}</td>
                                    <td style="width: 105px;">{{$row->assign_status}}</td>
                                    <td style="width: 90px;">
                                        @php
                                            if($row->urgent_status == 1){
                                                echo "Urgent";
                                            }else{
                                                echo "Non Urgent";
                                            }
                                        @endphp
                                    </td>
                                    <td style="width: 90px;text-align: center;">{{$row->priority_status}}</td>
                                    <td style="width: 90px;">{{$row->sending_status}}</td>
                                    <td style="width: 70px;text-align: center;">
                                        <button class="btn btn-primary srch" data-srch='srch' onclick="detail_monitoring('{{$row->no_wo}}', '{{$row->no_wo_task}}')"> <i class="fa fa-search"></i> </button>
                                     </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-10" style="text-align: right;">
            <span id="v_total" style="margin-right: 5px;">Total: {{ $total }}</span>
            </div>
            <div class="col-md-12">
                {{ $data->appends(request()->query())->links() }}
            </div>
        </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="false" data->
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Detail WO</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formGroup">
                    <!-- <input type="hidden" name="_token" value="{{csrf_token()}}"> -->
                    <div class="col-sm-12">
                        <div class="row">
                            <table id="tableAssignmentJob" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3995CB;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="6">Assignment Job</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">No WO</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Job Code</th>
                                    <!-- <th style="vertical-align: middle!important; text-align: center!important;">No FO</th> -->
                                    <th style="vertical-align: middle!important; text-align: center!important;">Date Assigned</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Status</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Assigned To</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">PDF</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tableCharacteristic" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="3">Characteristic</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Nama</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Nilai</th>
                                    <!-- <th style="vertical-align: middle!important; text-align: center!important;">Action</th> -->
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <table id="tableMaterialUsage" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="5">Material Usage</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">No</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Item Code</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Description</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Qty</th>
                                    <th style="vertical-align: middle!important; text-align: center!important;">UoM</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tablePhotoBefore" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="2">Photo Before</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Image</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tablePhotoAfter" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="2">Photo After</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Image</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <table id="tableSignature" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#3896D6;">
                                    <th style="vertical-align: middle!important; text-align: center!important; color: white;" colspan="6">Signature</th>
                                </tr>
                                <tr>
                                    <th style="vertical-align: middle!important; text-align: center!important;">Image Signature</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        {{--<button type="button" class="btn btn-primary  btnFooterModal"></button>--}}
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="filterModalWO" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Filter</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formFilterAssignment" method="get" action="{{ URL::to('monitoring/monitoringWO/filterMonitoringWO/srch')}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="checkbox" class="minimal" name="jobCode"> <label style="font-weight: normal; margin-right: 10px;"> Job Code </label>
                        </div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" placeholder="Job Code" name="jobCode">
                        </div>
                    </div><br>
                    <div class="modal-footer" style="text-align: center">
                        <input type="submit" class="btn btn-primary btnFooterModal" value="OK">
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->                
        </div>
    </div>
</div>

@endsection

@section('js')
  @include('monitoring.monitoringWO.js_monitoringWO')
@endsection
