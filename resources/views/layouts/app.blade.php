<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('head')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('dist/css/skins/_all-skins.css') }}">
    <!-- Morris chart -->
    <!--   <link rel="stylesheet" href="{{ asset('bower_components/morris.js/morris.css') }}">
    -->  <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Time Picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

    <!-- Datatable  -->
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/fixedHeader.bootstrap.min.css') }}">

    <!-- maps -->
    <link rel="stylesheet" href="{{ asset('css/map.css') }}">

    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css')}}">

    <!-- icheck -->
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }} "> 

    <!-- checkbox -->
    <link rel="stylesheet" href="{{ asset('dist/css/beautiful-checkbox.css') }} ">

    <!-- SweetAlert -->
    <link rel="stylesheet" href="{{ asset('bower_components/sweetalert/sweetalert.css') }} ">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    
    <style>
        table{
            overflow-x: auto;
        }
        .th_dashboard{
            text-align: center !important;
        }

        .tr_dashboard{
            text-align: left !important;
        }

        .table-icon-right {
            position: absolute;
            bottom: 250px;
            right: 0px;
            z-index: 1;
            top:260px;
        }

        /** css header fix */
        .tableCus {
            border-collapse: collapse;
            overflow-x: scroll;
            display: block;
        }

        .tableCus td, th {
            padding: 10px;
        }

        .tableCus th {
            text-align: center;
        }
        .thead {
            background-color: #EFEFEF;
        }

        .thead,
        .tbody {
            display: block;
        }

        .tbody {
            overflow-y: scroll;
            overflow-x: hidden;
            /*height: 500px;*/
            height: 390px;
        }

        .tbody td,
        .thead th {
            min-height: 25px;
            border: solid 1px #f4f4f4;
            overflow: hidden;
        }

        .tblAssJob {
            width: 875px !important;
        }

        .tblChar {
            width: 287px !important;
        }

        .tblBom {
            width: 583px !important;
        }

        .tblPhotoAfterAndBefore{
            width: 288px !important;
        }

        .tblSigned {
            width: 288px !important;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <script type="text/javascript" src="{{ asset('js/loader.js') }}"></script>

</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('layouts.partials.top-bar')
    @include('layouts.partials.left-menu')
    <!-- Left side column. contains the logo and sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header" style="background-color:white;padding: 5px 15px 5px 15px;">
            <h4 style="padding: 10px 0 10px 0;margin: 0;font-size: 16px;">
                @yield('title')
            </h4>
            @yield('button')
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                @yield('content')
            </div>
        </section>
        <!-- /.content -->
    </div>
    @include('layouts.partials.footer')
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script> $.widget.bridge('uibutton', $.ui.button);</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('bower_components/raphael/raphael.min.js') }}"></script>
{{-- <script src="{{ asset('bower_components/morris.js/morris.min.js') }}"></script>--}}
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- timepicker -->
<script src="{{ asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>  --}}

<!-- ChartJS -->
<script src="{{ asset('bower_components/chart.js/Chart.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>

<!-- CKEDITOR -->

{{--<script src="https://cdn.ckeditor.com/ckeditor5/11.1.1/classic/ckeditor.js"></script>--}}
{{--<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>--}}
<script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.fixedHeader.min.js') }}"></script>

<!-- ICheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }} "></script>


<!-- Sweet alert -->
{{--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>--}}
<script src="{{ asset('bower_components/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('bower_components/sweetalert/sweetalert-dev.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>


<script type="text/javascript" src="{{ asset('js/sort-table.min.js') }}"></script>


<script type="text/javascript">
    function hidden_error(){
        setTimeout(function() {
            $('#error-alert').fadeOut('fast');
        }, 2000); 
    }
    
    //for key number
    $(".number_valid").on("keypress", function (event) {
        var regex = /[0-9+]/g;
        var key = String.fromCharCode(event.which);
        if (regex.test(key) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
            return true;
        }
        return false;
    });
  
    $('.select2').select2();
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
    });
    function datePicker()
    {
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy',
        })    
    }

    $(document).ready( function () {
        $('#tableDashboar1').dataTable( {
            "bFilter": false,
            // "bPaginate": false
        });
    });

    $(document).ready( function () {
        $('#tableDashboar2').dataTable( {
            "bFilter": false,
            // "bPaginate": false
        });
    });

    $(document).ready( function () {
        $('#tableDashboar3').dataTable( {
            "bFilter": false,
            // "bPaginate": false
        });
    });

    $(document).ready( function () {
        $('#tableDashboar4').dataTable( {
            "bFilter": false,
        });
    });

    $(document).ready( function () {
        $('#tableDashboar5').dataTable( {
            "bFilter": false,
        });
    });

    $(document).ready( function () {
        $('#tableDashboar6').dataTable( {
            "bFilter": false,
        });
    });

    //untuk semua data table 
    $(document).ready(function() {
        var table = $('#tableAssignment').DataTable( {
            "scrollY": 450,
            "scrollX": true
        });
    });

    //untuk databale Mapping FA Type
    $(document).ready(function() {
        $('#tableFAMapping').dataTable({});
    });

    function pageOne(){
        $('#pageOne').show();
    }

    $(document).on('ajaxComplete ready', function () {
        $('.modalMd').off('click').on('click', function () {
            $('#modalMdContent').load($(this).attr('value'));
            $('#modalMdTitle').html($(this).attr('title'));
        });
    });

    //validate number input
    function isInputNumber(evt){
        var ch = String.fromCharCode(evt.which);
        if(!(/[0-9]/.test(ch))){
            evt.preventDefault();
        }
    }
</script>
@yield('js')
</body>
</html>
