  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> Development
    </div>
    <strong>Copyright &copy; {{date('Y')}} <a href="http://www.aetra.co.id/"> Aetra Air Jakarta PT  </a>.</strong> All rights
    reserved.
  </footer>
