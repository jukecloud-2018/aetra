@php $user = Session::get('userinfo'); @endphp
<header class="main-header">
    <a href="{{route('home')}}" class="logo" style="height: 50px;">
        <span class="logo-mini"><b>A</b>etra</span>
        <span class="logo-lg"> <img src="{{asset('images/Logo2.png')}}" alt="" style="vertical-align: unset;width:200px;height:50px;"> </span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" style="padding-top:15px;">
         <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <li class="dropdown tasks-menu"></li>
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                    <span class="hidden-xs">{{ $user['username']  }}</span>
                </a>
                <ul class="dropdown-menu">
                        <li class="user-header">
                        <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                        <p>
                        {{ $user['username']  }}
                        </p>
                    </li>
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{url('logout')}}" class="btn btn-default btn-flat">Logout</a>
                        </div>
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    </nav>
</header>
