<?php use Illuminate\Support\Facades\DB; 
 $parent = DB::table('modules')->where('moduleParent','0')->get(); 
 $userinfo = Session::get('userinfo');
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">    
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!-- <li class="{{ Request::is('dashboard') ? 'active' : '' }}"> -->
        <li>
          <a href="{{route('dashboard')}}">
            <i class="fa fa-th"></i> <span>Dashboard </span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        @foreach($parent as $d)
        <li class="treeview {{ Request::is($d->moduleRoute.'/*') ? 'active' : '' }}">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>{{$d->moduleName}}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <?php $sub = DB::table('modules as a')
          ->join('access_modules as b','a.id','=','b.module_id')
          ->join('admin as c','c.id','=','b.admin_id')
          ->select('a.*')
          ->where('moduleParent',$d->id)
          ->where('c.id',$userinfo['admin_id'])->get(); ?>
          <ul class="treeview-menu">
            @foreach($sub as $s)
            <?php $url = (string)$s->moduleRoute; ?>
            <li  class="{{ Request::is($s->moduleUrl) ? 'active' : '' }}"><a href="<?php echo route($url)  ?>"><i class="fa fa-circle-o"></i> {{$s->displayName}} </a></li>
            @endforeach
          </ul>
        </li>
        @endforeach


 
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>