<script type="text/javascript">
  $(document).ready(function() {
    $('#tableUser').dataTable({
    });
  });

// for get modal data module
function getNewUser(){
  $('#userModal').modal('show');
}
/*
function getDataModule()
{  
    var labelModule = $('.labelModule');
    labelModule.empty();
    var label = '';
    $.ajax({
        url:"{{route('administrator.users.getDataModule')}}",
        data:{'data':'moduleMaster'},
        dataType:'JSON',
        type:'GET',
        success:function(get){ 
            $.each(get.data,function(key,value){
                label = '<div class="col-sm-3">'+
                        '<div style="margin-bottom:28px">'+
                            '<label for="" class="col-sm-1 col-form-label" style=""> '+value.moduleName+' </label>'+
                        '</div>';
                        $.ajax({
                            type: "GET",
                            url: "{{ route('administrator.users.getDetailModule') }}",
                            data:{'id':value.id},
                            async: false,
                            success:function(r){
                                $.each(r.dataDetail,function(index,val){
                                    label += '<div style="margin-top:10px;"> <input type="checkbox" value="'+val.id+'" name="module_id[]"/> '+val.displayName+'  </div>';
                                });        
                            }
                        })
                label += '</div>';
                labelModule.append(label);
            })
            //console.log(label);
        }
    })
    $('#userModal').modal('show');
}*/

// for save name group
function saveGroup()
{
    var data = $('#formUser').serialize();

    $.ajax({
        url:"{{route('administrator.users.store')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Users has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

// for change status with checkbox
function changeStatus(id,status)
{   
  if($('[name="status_check"]').is(':checked'))
  {
    $.ajax({
      url:'users/updateStatus/'+id+'',
      data:{'status':status},
      dataType:'JSON',
      type:'GET',
      success:function(data){
        if(data.status =="success")                    
        {
          swal('Success','Change Status successfully!','success');
          location.reload();
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
      }
    });
  }
}

// for get detail
function detail(id,action)
{
    var body;
    var footer;
    $.ajax({
        url:'users/detailUser/'+id+'',
        data:{'action':action},
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            body = data;
            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Close</button>';
             $('#userDetailModal').find('.modal-title').text('View User Access');
             $('#userDetailModal').find('.modal-body').html(body);
             $('#userDetailModal').find('.modal-footer').html(footer);
             $('#userDetailModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

// for edit user
function edit(id,action)
{
    var body;
    var footer;
    $.ajax({
        url:'users/detailUser/'+id+'',
        data:{'action':action},
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            body = data;
            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Close</button>';
            footer += '<button type="button" class="btn btn-secondary  btnFooterModal"  onclick="update('+id+')">Update</button>';
             $('#userDetailModal').find('.modal-title').text('Edit User Access');
             $('#userDetailModal').find('.modal-body').html(body);
             $('#userDetailModal').find('.modal-footer').html(footer);
             $('#userDetailModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

// for update user
function update(id)
{
    var dataUser = $('#formUserEdit').serialize();
    $.ajax({
        url:'users/updateUser',
        data:dataUser,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            /*swal({
              title: "Access Menu Belum Di Isi",
          });*/
          swal("error!", thrownError, "error");
          }
    });
}

function deleteUser(id)
{
    console.log('Delete processing');

    swal({
      title: "Apakah Data Ingin Di Hapus?",
      text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    },
    function(isConfirm){
        if (!isConfirm) return;
        $.ajax({
            url:'users/destroy/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(response){
                if(response.status == "success"){
                    swal("Berhasil!", "Worker berhasil di hapus", "success");
                    location.reload();
                }else if(response.status == "error"){
                    swal("Error", response.message, "error");
                }
            },
              error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Worker gagal di hapus", "error");
              }
        });
    });
}

</script>