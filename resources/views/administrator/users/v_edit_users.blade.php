<?php 
use Illuminate\Support\Facades\DB; 
use App\Models\Administrator\Modules;
use App\Models\Administrator\Access_modules;
?>

<form id="formUser">
	  <input type="hidden" name="_token" value="{{csrf_token()}}">
	  <div class="form-group row">
	    
	    <div class="col-sm-3">
	      <label for="" class="col-sm-1 col-form-label"> Username </label>
	      <input type="text" class="form-control" id="" placeholder="Username" value="" name="username">
	    </div>

	    <div class="col-sm-3">
	      <label for="" class="col-sm-1 col-form-label"> Name </label>
	      <input type="text" class="form-control" id="" placeholder="Name" value="" name="name">
	    </div>
		
	    <div class="col-sm-3">
	      <label for="" class="col-sm-1 col-form-label"> Phone  </label> 
	      <input type="text" class="form-control" placeholder="Phone" name="phone">
	    </div> 
		
	    <div class="col-sm-3">
	      <label for="" class="col-sm-12 col-form-label"> Group Access </label>
	      <select class="form-control" name="group_id">
	        <option value=""> - Choose Group -</option>
	        @foreach($groups as $group)
	         <option value="{{$group->id}}"> {{$group->group_name}} </option>
	        @endforeach
	      </select>
	    </div>
	  </div>
	  <br>
	<div class="row">
	    <label for="" class="col-sm-6 col-form-label" style="width: 10%"> Access Menu : </label>  
	    <div class="row  labelModule">  
	    	@php 
	    		$Access_modules = Access_modules::where('user_id',$id)->get();
				$count_AM = count($Access_modules);
			@endphp    
		    	@foreach($modules as $module)
			        <div class="col-sm-3">
			        	<div style="margin-bottom: 28px;">
		                  <label for="" class="col-sm-1 col-form-label" style=""> {{$module->moduleName}} </label>
		                </div>
		                  @php $module_detail = Modules::where('moduleParent',$module->id)->get(); @endphp
		                  @foreach($module_detail as $key => $md)
		                  	    @php $Access_modules = Access_modules::where('user_id',$id)->where('module_id',$md->id)->first(); @endphp
		                  		<div style="margin-top:10px;"> <input type="checkbox" value="{{$md->id}}" 
		                  			@php 
		                  				$ck = '';
		                  				if(!empty($Access_modules) && ($md->id==$Access_modules->module_id)): 
		                  				$ck = 'checked'; 
		                  			@endphp
		                  		 	@endif
		                  			<?= $ck; ?> name="module_id[]"/>  {{$md->moduleName}}</div>
		                  @endforeach
		            </div>
		        @endforeach
	    </div>              
	</div>
</form><!-- /form -->