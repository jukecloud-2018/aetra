<?php 
use Illuminate\Support\Facades\DB; 
?>
@extends('layouts.app')

@section('head')
    Administrator | Login User
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Login User </span>
@endsection
@section('button')
    <div class="forButtonTop">
        <button href="#" class="btn btn-default buttonAE btnTop" onclick="getNewUser()">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </button>
        <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
            <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
        </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableUser" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Group Access</th>
                                    <th>Status</th>
                                    <th>Access Menu</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                <tr role="row">
                                    <td class="sorting_1"><center>{{++$key}}</center></td>
                                    <td>{{$value->username}}</td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->phone}}</td>
                                    <td>{{$value->group_name}}</td>
                                    <td>
                                        @if($value->admin_status == 'Active')
                                            <input type="checkbox" name="status_check" id="status_active" onclick="changeStatus('{{$value->admin_id}}', 'Non Active')" checked> Active
                                        @elseif($value->admin_status == 'Non Active')
                                            <input type="checkbox" name="status_check" id="status_active" onclick="changeStatus('{{$value->admin_id}}', 'Active')"> Non Active
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-primary" onclick="detail('{{$value->admin_id}}','detail')"><i class="fa fa-search"></i></button> |
                                        <button class="btn btn-success" onclick="edit('{{$value->admin_id}}','edit')"><i class="fa fa-pencil"></i></button> |
                                        <button class="btn btn-danger" onclick="deleteUser('{{$value->admin_id}}')"><i class="fa fa-trash"></i> </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalCenterTitle" aria-hidden="false"  data-backdrop="false" style="top:-100px;">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 75%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Add New User Access</h4>
      </div>
      <div class="modal-body" style="background-color: #FBFBFB">
        <div class="alert alert-danger" id="error-alert" style="display: none"></div>
				<form id="formUser">
				  <input type="hidden" name="_token" value="{{csrf_token()}}">
				  <div class="form-group row">
				    
				    <div class="col-sm-3">
				      <label for="" class="col-sm-1 col-form-label"> Username </label>
				      <input type="text" class="form-control" id="" placeholder="Username" value="" name="username">
				    </div>
				    
				    <div class="col-sm-3">
				      <label for="" class="col-sm-1 col-form-label"> Password </label>
				      <input type="password" class="form-control" id="" placeholder="Password" value="" name="password">
				    </div>
				    
				    <div class="col-sm-3">
				      <label for="" class="col-sm-1 col-form-label"> Name </label>
				      <input type="text" class="form-control" id="" placeholder="Name" value="" name="name">
				    </div>

				    <div class="col-sm-3">
				      <label for="" class="col-sm-1 col-form-label"> Phone  </label>
				      <input type="tel" class="form-control" placeholder="08xxxxxxxxxx" name="phone" maxlength="15" pattern="^\d{10}$" onkeypress="isInputNumber(event)">
				    </div> 
				  </div>
				  <div class="row">                
				    <div class="col-sm-3">
				      <label for="" class="col-sm-12 col-form-label"> Group Access </label>
				      <select class="form-control" name="group_id">
				        <option value=""> - Choose Group -</option>
				        @foreach($groups as $group)
				         <option value="{{$group->id}}"> {{$group->group_name}} </option>
				        @endforeach
				      </select>
				    </div>
				  </div>
				  <br>
          <div class="row">
              <div class="col-sm-2">
                <label for="" class="col-form-label"> Access Menu :</label>
              </div>
	    	      <div class="col-sm-3">
			        	<div style="margin-bottom: 28px;">
		              <label for="" class="col-sm-1 col-form-label" style=""> Transaction </label>
		            </div>
                <div style="margin-top:10px;"> <input type="checkbox" value="20" name="module_id[]">  assignmentFA</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="21" name="module_id[]">  assignmentWO</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="22" name="module_id[]">  assignmentSurvey</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="23" name="module_id[]">  messaging</div>
		          </div>
		          <div class="col-sm-3">
			        	<div style="margin-bottom: 28px;">
                  <label for="" class="col-sm-1 col-form-label" style=""> Monitoring </label>
                </div>
                <div style="margin-top:10px;"> <input type="checkbox" value="24" name="module_id[]">  monitoringFA</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="25" name="module_id[]">  monitoringWO</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="26" name="module_id[]">  monitoringUsage</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="27" name="module_id[]">  mapStatus</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="28" name="module_id[]">  mapWorker</div>
                <div style="margin-top:10px;"> <input type="checkbox" value="29" name="module_id[]">  monitoringSurvey</div>
		          </div>
		          <div class="col-sm-4">
			        	<div style="margin-bottom: 28px;">
		              <label for="" class="col-sm-1 col-form-label" style=""> Administrator </label>
		            </div>
                <div class="col-sm-6">
                  <div style="margin-top:10px;"> <input type="checkbox" value="4" name="module_id[]">  group</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="5" name="module_id[]">  user</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="6" name="module_id[]">  skillFA</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="7" name="module_id[]">  skillWO</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="8" name="module_id[]">  vendor</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="9" name="module_id[]">  workerLogin</div>
                </div>
                <div class="col-sm-6">
                  <div style="margin-top:10px;"> <input type="checkbox" value="11" name="module_id[]">  hhManagement</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="13" name="module_id[]">  survey</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="14" name="module_id[]">  mappingFAType</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="15" name="module_id[]">  mappingWOTask</div>
                  <div style="margin-top:10px;"> <input type="checkbox" value="16" name="module_id[]">  systemParameter</div>
		            </div>
		          </div>              
          </div>
				</form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveGroup()">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- end Modal -->


<!-- Modal detail -->
<div class="modal fade" id="userDetailModal" tabindex="-1" role="dialog" aria-labelledby="userModalCenterTitle" aria-hidden="false"  data-backdrop="false" style="top:-100px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 75%">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" style="color:#fff">&times;</span>
          </button>
          <h4 class="modal-title" style="text-align:center">View User Access</h4>
        </div>
        <div class="modal-body" style="background-color: #FBFBFB">
          
        </div>
        <div class="modal-footer" style="text-align: center">
          <button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Close</button>
        </div>
    </div>
  </div>
</div>
<!-- end modal detail -->
@endsection

@section('js')
  	@include('administrator.users.js_users')
@endsection
