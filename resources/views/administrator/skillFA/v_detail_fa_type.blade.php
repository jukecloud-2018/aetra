@php
	use App\Models\Administrator\Skill_fa_detail;
@endphp


@foreach($fa_type as $fa)
      @php 
        $dt = Skill_fa_detail::where('fa_type_id',$fa->id)->where('skill_fa_id',$id)->first();
      @endphp

      <tr>
        <td>
        	<input type="checkbox" name="fa_type" 
        	<?php 
        		$ck = '';
        		if(!empty($dt) && ($fa->id==$dt->fa_type_id)):
        			$ck = 'checked';
        	   endif;
          ?>
         <?= $ck ?> class="rowFaType" value="{{$fa->id}}"></td>
        <td>{{$fa->fa_type_cd}}</td>
      </tr>
@endforeach
