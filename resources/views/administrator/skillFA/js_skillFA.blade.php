<script type="text/javascript">
var baris = 0;

$( document ).ready(function() {
    //for datatable 
  $('#tableSkillFA').dataTable({ });
});

/* for style checkbox
$( document ).ready(function() {
    
	//for datatable 
        $('#tableSkillFA').dataTable({
            processing: true,
            serverSide: true,
            order: [0, 'desc'],
            ajax: {
                method: 'POST',
                url : '<?=route('administrator.skillFA.getData');?>',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },
            columns : [
                {
                    // this for numbering table
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { "data": "skill_fa_name" },
          
                {
                    "mRender": function (data, type, row, meta) {
                   
                        return '<button class="btn btn-primary" onclick="detailSkillFA(' + row.id + ',`detail`)"><i class="fa fa-search"></i></button> | <button class="btn btn-success" onclick="detailSkillFA(' + row.id + ',`edit`)"><i class="fa fa-pencil"></i></button> | <button class="btn btn-danger" onclick="detailSkillFA(' + row.id + ')"><i class="fa fa-trash"></i></button>';
                    }
        		}
               
            ],
            responsive: true,
        });
});*/


// for closemodal 
function closeModal()
{
    $('#modalFAType').modal('hide');
}

// for check all fa type
function checkSemua(resp,id)
{
    if (resp.checked)
    {
        $('.rowFaType').prop('checked', resp.checked);
    }
    else
    {
        $('.rowFaType').prop('checked', resp.checked);
    }
}

// for save skill
function saveFA()
{
    $.ajax({
        url:"{{route('administrator.skillFA.store')}}",
        data:$('form#formSkillFA, form#formFaType').serialize(),
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $('.alert-danger').empty();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
            hidden_error();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

// for save  detail skill fa
function saveFAType()
{

    localStorage.clear();
    var checkedValues = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();

    var join_val = checkedValues.join(',');

    localStorage.setItem('join_val',join_val);

     $.ajax({
        url:"skillFA/saveFAType",
        data:{'detail_value':localStorage.getItem('join_val'),'skill_fa_id':$('[name="skill_fa_id"]').val()},
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            $('#modalFAType').modal('hide');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })

}

// for show modal add form
function modalFAType()
{
    $('#modalFAType').modal('show');
}
/*
function modalSkillFA()
{
      var body = '<div class="alert alert-danger" id="error-alert" style="display: none"></div>'+
              '<form id="formSkillFA">'+
              '<input type="hidden" name="_token" value="{{csrf_token()}}">'+
               '<div class="form-group row">'+
                  '<label  class="col-sm-3 col-form-label">Group Skill Name</label>'+
                  '<div class="col-sm-8">'+
                    '<input type="text" name="skill_fa_name" class="form-control" placeholder="Group Skill Name">'+
                  '</div>'+
                  '<div class="col-sm-1"  style="margin-top:7px;">'+
                    '<a href="javascript:void(0)" onclick="modalFAType()"> <i class="fa fa-plus"></i> </a>'+
                  '</div>'+
                '</div>'+
          '</form>';
        var footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal"onclick="window.location.reload()">Cancel</button>'+
        '<button type="button" class="btn btn-primary  btnFooterModal" onclick="saveFA()">Save</button>';
    hidden_error();

    $('#modalSkillFA').find('.modal-title').text('Add New Group FA Skill');
    $('#modalSkillFA').find('.modal-footer').html(footer);
    $('#modalSkillFA').find('.modal-body').html(body);
    $('#modalSkillFA').modal('show');   


}*/

// for action detail and action edit
function detailSkillFA(id,action)
{
    var body;
    var footer;
     $.ajax({
        url:"skillFA/detailFASkill/"+id,
        data:{'action':action},
        //dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
             body ='<div class="alert alert-danger" id="error-alert" style="display: none"></div>';
             body += data;
             if(action == 'detail')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>';   
                var title = 'Detail Group Skill FA';
             }
             else if(action == 'edit')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>'+
                    '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="updateFASkill()">Update</button>';   
                title = 'Edit Group Skill FA';
             }

             

             $('#modalSkillFA').find('.modal-title').text(title);
             $('#modalSkillFA').find('.modal-body').html(body);
             $('#modalSkillFA').find('.modal-footer').html(footer);
             $('#modalSkillFA').modal('show');

        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

// for action  detail skill fa  and edit skill fa
function detailFaType(id,action)
{
    var body;
    var footer;
     $.ajax({
        url:"skillFA/detailFaType/"+id,
        data:{'page':'detail'},
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
             body = data;

              if(action == 'detail')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="">Cancel</button>';   
             }
             else
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="closeModal()">Cancel</button>'+
                    '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="saveFAType()">Update</button>';   
             }
             $('[name="skill_fa_id"]').val(id);
             $('#modalFAType').find('tbody').html(body);
             $('#modalFAType').find('.modal-footer').html(footer);
             $('#modalFAType').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

// for update skill FA
function updateFASkill()
{

    $.ajax({
        url:"skillFA/updateFASkill",
        data:{'skill_fa_name':$('[name="skill_fa_name"]').val(),'fa_id':$('[name="fa_id"]').val(),'detail_value':localStorage.getItem('join_val')},
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $('.alert-danger').empty();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
            hidden_error();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}


// for delete skill fa 
function deleteSkillFA(id)
{
    console.log('Delete processing');
    
    swal({
      title: "Apakah Data Ingin Di Hapus?",
      text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    }, function(isConfirm){
        if (!isConfirm) return;
        $.ajax({
            url:'skillFA/destroy/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(data){
                swal("Berhasil!", "Skill FA berhasil di hapus", "success");
                location.reload();
            },
              error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Skill FA gagal di hapus", "error");
              }
        });
    });
}
</script>