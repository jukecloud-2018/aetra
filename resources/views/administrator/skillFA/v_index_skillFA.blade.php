@extends('layouts.app')

@section('head')
    Administrator | Skill FA
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Skill FA </span>
@endsection
@section('button')
  <div class="forButtonTop">
    <a href="" class="btn btn-default buttonAE btnTop" data-toggle="modal" data-target="#modalSkillFA">
      <i class="fa fa-plus" aria-hidden="true"></i> Add New
    </a>
    <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
        <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
    </a>
  </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        {{--<div class="box-header with-border">--}}
        {{--<h3 class="box-title">Skill FA</h3>--}}
        {{--<div class="box-tools pull-right">--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- /Box Header -->
    
        <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableSkillFA" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 9px!important;">No</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 650px!important;">Group Skill</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 80px!important;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($fa as $key => $value)
                            <tr role="row">
                                <td style="text-align: center;">{{++$key}}</td>
                                <td>{{$value->skill_fa_name}}</td>
                                <td>
                                    <button class="btn btn-primary" onclick="detailSkillFA({{$value->id}},'detail')"><i class="fa fa-search"></i></button> |
                                    <button class="btn btn-success" onclick="detailSkillFA({{$value->id}},'edit')"><i class="fa fa-pencil"></i></button> |
                                    <button class="btn btn-danger" onclick="deleteSkillFA({{$value->id}})"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="modalSkillFA" tabindex="-1" role="dialog" aria-labelledby="modalSkillFACenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Add New Group Skill FA</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formSkillFA">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group row">
                        <label  class="col-sm-3 col-form-label">Group Skill Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="skill_fa_name" class="form-control" placeholder="Group Skill Name">
                        </div>
                        <div class="col-sm-1"  style="margin-top:7px;">
                            <a href="javascript:void(0)" onclick="modalFAType()"> <i class="fa fa-plus"></i> </a>
                        </div>
                    </div>
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal"onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveFA()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<!-- Modal   list fa type -->
<div class="modal fade" id="modalFAType" tabindex="-1" role="dialog" aria-labelledby="modalFATypeCenterTitle" aria-hidden="false"  data-backdrop="false" style="margin-top:50px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <h4 class="modal-title" style="text-align:center">List FA Type</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: scroll;">
                <form id="formFaType">
                    <input type="hidden" name="skill_fa_id">
                    <table class="table table-striped table-bordered" id="listFAType">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="allCheck" onchange="checkSemua(this)" value="0"> All </th>
                            <th>FA Type</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($type as $value)
                            <tr>
                                <td><input type="checkbox" name="faType[]" class="rowFaType" value="{{$value->id}}"></td>
                                <td>{{$value->fa_type_cd}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModal()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="closeModal()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal list fa type-->
@endsection

@section('js')
  	@include('administrator.skillFA.js_skillFA')
@endsection
