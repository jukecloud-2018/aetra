  <form id="formSkillFA">
      <input type="hidden" name="_token" value="{{csrf_token()}}">
       <div class="form-group row">
          <label  class="col-sm-3 col-form-label">Group Skill Name</label>
          <div class="col-sm-8">
            <input type="hidden" name="fa_id" value="{{$skill_fa->id}}">
            <input type="text" name="skill_fa_name" class="form-control" value="{{$skill_fa->skill_fa_name}}" placeholder="Group Skill Name">
          </div>
          <div class="col-sm-1"  style="margin-top:7px;">
            <a href="javascript:void(0)" onclick="detailFaType('{{$skill_fa->id}}','{{$action}}')"> <i class="fa fa-edit"></i> </a>
          </div>
        </div>
  </form><!-- /form -->