<script type="text/javascript">
var baris = 0;
$( document ).ready(function() {
    //for datatable 
  $('#tableSkillWO').dataTable({ });
});
/*
function addRowSkillWO()
{
    baris++;
    var _div = $('#formSkillWO');
    _div.append('<div class="form-group row rowSkillWO_'+baris+'">'+
                  '<label for="" class="col-sm-3 col-form-label"></label>'+
                  '<div class="col-sm-8">'+
                      '<select class="form-control" name="skillWOType">'+
                          '<option>Skill WO</option>'+
                          '<option>Skill WO</option>'+
                          '<option>Skill WO</option>'+
                        '</select>'+
                  '</div>'+
                  '<div class="col-sm-1"  style="margin-top:7px;">'+
                    '<a href="javascript:void(0)" onclick="deleteRowSkillWO()" style="color:red"> <i class="fa fa-minus"></i> </a>'+
                  '</div>'+
                '</div>');
    

}

function deleteRowSkillWO()
{   
    $('.rowSkillWO_'+baris+'').remove();
    baris--;
}

function modalSkillWO()
{
      var body = '<div class="alert alert-danger" id="error-alert" style="display: none"></div>'+
              '<form id="formSkillWO">'+
              '<input type="hidden" name="_token" value="{{csrf_token()}}">'+
               '<div class="form-group row">'+
                  '<label  class="col-sm-3 col-form-label">Group Skill Name</label>'+
                  '<div class="col-sm-8">'+
                    '<input type="text" name="skill_wo_name" class="form-control" placeholder="Group Skill Name">'+
                  '</div>'+
                  '<div class="col-sm-1"  style="margin-top:7px;">'+
                    '<a href="javascript:void(0)" onclick="modalWOType()"> <i class="fa fa-edit"></i> </a>'+
                  '</div>'+
                '</div>'+
          '</form>';
        var footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal"onclick="window.location.reload()">Cancel</button>'+
        '<button type="button" class="btn btn-primary  btnFooterModal" onclick="saveWO()">Save</button>';
    hidden_error();

    $('#modalSkillWO').find('.modal-title').text('Add New Group WO Skill');
    $('#modalSkillWO').find('.modal-footer').html(footer);
    $('#modalSkillWO').find('.modal-body').html(body);
    $('#modalSkillWO').modal('show');   


}*/

function modalWOType(){
    $('#modalWOType').modal('show');
}

function ambilSemua(resp){
  if (resp.checked)
  {
     $('.rowWoType').prop('checked', resp.checked);
  }
  else
  {
    $('.rowWoType').prop('checked', resp.checked);
  }
}

function closeModal()
{
    $('#modalWOType').modal('hide');
}

function saveWO(){
    console.log('processing');
    var data = $('#formSkillWO').serialize();
    $.ajax({
        url:"{{route('administrator.skillWO.store')}}",
        //data:{'skill_wo_name':$('[name="skill_wo_name"]').val(),'detail_value':localStorage.getItem('join_val')},
        data:$('form#formSkillWO, form#formwoTask').serialize(),
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $('.alert-danger').empty();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
            hidden_error();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function detailSkillWO(id,action){
    var body;
    var footer;
     $.ajax({
        url:"skillWO/detailWOSkill/"+id,
        data:{'action':action},
        //dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
             body ='<div class="alert alert-danger" id="error-alert" style="display: none"></div>';
             body += data;
             if(action == 'detail')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>';   
                var title = 'Detail Group Skill WO';
             }
             else if(action == 'edit')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>'+
                    '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="updateWOSkill()">Update</button>';   
                title = 'Edit Group Skill WO';
             }

             

             $('#modalSkillWO').find('.modal-title').text(title);
             $('#modalSkillWO').find('.modal-body').html(body);
             $('#modalSkillWO').find('.modal-footer').html(footer);
             $('#modalSkillWO').modal('show');

        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

function detailWoType(id,action)
{
    var body;
    var footer;
     $.ajax({
        url:"skillWO/detailWoType/"+id,
        data:{'page':'detail'},
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
             body = data;

              if(action == 'detail')
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="">Cancel</button>';   
             }
             else
             {
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal" onclick="closeModal()">Cancel</button>'+
                    '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="saveWOType()">Update</button>';   
             }
             $('[name="skill_wo_id"]').val(id);
             $('#modalWOType').find('tbody').html(body);
             $('#modalWOType').find('.modal-footer').html(footer);
             $('#modalWOType').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function updateWOSkill(){
    $.ajax({
        url:"skillWO/updateWOSkill",
        data:{'skill_wo_name':$('[name="skill_wo_name"]').val(),'wo_id':$('[name="wo_id"]').val(),'detail_value':localStorage.getItem('join_val')},
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $('.alert-danger').empty();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
            hidden_error();
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

function saveWOType()
{
    localStorage.clear();
    var checkedValues = $('input:checkbox:checked').map(function() {
        return this.value;
    }).get();

    var join_val = checkedValues.join(',');

    localStorage.setItem('join_val',join_val);

     $.ajax({
        url:"skillWO/saveWOType",
        data:{'detail_value':localStorage.getItem('join_val'),'skill_wo_id':$('[name="skill_wo_id"]').val()},
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
         
                $('#modalWOType').modal('hide');

        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })

}

function deleteSkillWO(id){
    console.log('Delete processing');
    
    swal({
      title: "Apakah Data Ingin Di Hapus?",
      text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    }, function(isConfirm){
        if (!isConfirm) return;
        $.ajax({
            url:'skillWO/destroy/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(data){
                swal("Berhasil!", "Skill WO berhasil di hapus", "success");
                location.reload();
            },
              error: function (xhr, ajaxOptions, thrownError) {
                    swal("Gagal!", "Skill WO gagal di hapus", "error");
              }
        });
    });
}
</script>