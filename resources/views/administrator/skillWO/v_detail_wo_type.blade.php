@php
	use App\Models\Administrator\Skill_wo_detail;
@endphp

@foreach($wo_type as $wo)
      @php $dt = Skill_wo_detail::where('job_code_id',$wo->id)->where('skill_wo_id',$id)->first(); @endphp

      <tr>
        <td>
        	<input type="checkbox" name="wo_task" 
        	<?php 
        		$ck = '';
        		if(!empty($dt) && ($wo->id==$dt->job_code_id)):
        			$ck = 'checked';
        	   endif;
          ?>
         <?= $ck ?> class="rowWoType" value="{{$wo->id}}"></td>
        <td>{{$wo->job_code}}</td>
      </tr>
@endforeach
