@extends('layouts.app')

@section('head')
    Administrator | Skill WO
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Skill WO </span>
@endsection
@section('button')
    <div class="forButtonTop">
        <!--<button class="btn btn-default buttonAE btnTop" data-target="#modalSkillWO" onclick="modalSkillWO()">-->
        <a href="" class="btn btn-default buttonAE btnTop" data-toggle="modal" data-target="#modalSkillWO">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>
        <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
            <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
        </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableSkillWO" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 9px!important;">No</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 660px!important;">Group Skill</th>
                                    <th style="vertical-align: middle!important; text-align: center!important; width: 80px!important;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($wo as $key => $value)
                                <tr role="row">
                                    <td style="text-align: center;">{{++$key}}</td>
                                    <td>{{$value->skill_wo_name}}</td>
                                    <td>
                                        <button class="btn btn-primary" onclick="detailSkillWO({{$value->id}},'detail')"><i class="fa fa-search"></i></button> |
                                        <button class="btn btn-success" onclick="detailSkillWO({{$value->id}},'edit')"><i class="fa fa-pencil"></i></button> |
                                        <button class="btn btn-danger" onclick="deleteSkillWO({{$value->id}})"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="modalSkillWO" tabindex="-1" role="dialog" aria-labelledby="skillWOModalCenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
            </button>
            <h4 class="modal-title" style="text-align:center">Add New Group Skill Wo</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB">
            <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formSkillWO">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group row">
                        <label  class="col-sm-3 col-form-label">Group Skill Name</label>
                        <div class="col-sm-8">
                            <input type="text" name="skill_wo_name" class="form-control" placeholder="Group Skill Name">
                        </div>
                        <div class="col-sm-1"  style="margin-top:7px;">
                            <a href="javascript:void(0)" onclick="modalWOType()"> <i class="fa fa-edit"></i> </a>
                        </div>
                    </div>
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" data-dismiss="modal"onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveWO()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->

<!-- Modal   list fa type -->
<div class="modal fade" id="modalWOType" tabindex="-1" role="dialog" aria-labelledby="modalWOTypeCenterTitle" aria-hidden="false"  data-backdrop="false" style="margin-top:50px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <h4 class="modal-title" style="text-align:center">List WO Type</h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: scroll;">
                <form id="formwoTask">
                    <input type="hidden" name="skill_wo_id">
                    <table class="table table-striped table-bordered" id="listWOType">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="allCheck" onchange="ambilSemua(this)" value="0"> All </th>
                                <th>WO Type</th>
                            </tr>
                        </thead>
                            <tbody>
                            @foreach($task as $wo)
                            <tr>
                                <td><input type="checkbox" name="woTask[]" class="rowWoType" value="{{$wo->id}}"></td>
                                <td>{{$wo->job_code}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModal()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="closeModal()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal list fa type-->
@endsection

@section('js')
    @include('administrator.skillWO.js_skillWO')
@endsection
