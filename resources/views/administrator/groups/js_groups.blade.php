<script type="text/javascript">
var baris = 1;
//for datatable 
$('#tableGroup').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '<?=route('administrator.groups.getData');?>',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { "data": "group_name" },
        {
            "mRender": function (data, type, row, meta) {
                var nama = '';
                $.each(row.fa_group_access , function (index, item) {
                    nama+= nama ? ', ' : '';
                    nama+= item.dispatch_group;
                });
                return nama;
            }
        },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="editGroupAccess(`' + row.id + '`)"><i class="fa fa-pencil"></i></button> | <button class="btn btn-danger" onclick="deleteGroupAccess(`' + row.id + '`)"><i class="fa fa-trash"></i></button>';
            }
		}
    ],
    responsive: true,
});

// for delete group access
function deleteGroupAccess(id)
{
     /*swal({
      title: "Are you sure?",
      text: "Delete this data ?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
         $.ajax({
            url:'groups/'+id,
            data:{'status':'true'},
            dataType:'JSON',
            type:'DELETE',
            headers: {
{{--               'X-CSRF-TOKEN': '{{ csrf_token() }}'--}}
            },
            success:function(get){
                if(get.status=='success')
                {
                    swal('Success','Group has been saved successfully!','success');
                    location.reload();
                }
               
            },
              error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", thrownError, "error");
              },
        })
      } 
    });*/

    swal({
        title: "Apakah Data Ingin Di Hapus?",
        text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
    }, function(isConfirm){
        if (!isConfirm) return;
        $.ajax({
            url:'groups/deleteFAGroup/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(response){
                if(response.status == "success"){
                    swal("Berhasil!", "Group Akses berhasil di hapus", "success");
                    location.reload();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Gagal!", "Group akses gagal di hapus", "error");
            }
        });
    });
}


// for Survey Group
function SurveyGroup()
{
    $('#modalSurveyGroup').modal('show');
}

// this function for button edit 
function editFAGroup(id)
{
    var body;
    var footer;
    $.ajax({
        url:'groups/editFAGroup/'+id+'',
        data:{'page':'edit'},
        type:'GET',
        success:function(data){
            body = data;

            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closemodalEditFAGroup()">Cancel</button>'+
                      '<button type="button" class="btn btn-primary  btnFooterModal" onclick="savemodalEditFAGroup()">Update</button>';
              $('#modalEditFAGroup').find('.modal-title').html('Edit FA Access');
              $('#modalEditFAGroup').find('.modal-body').html(body);
              $('#modalEditFAGroup').find('.modal-footer').html(footer);
              $('#modalEditFAGroup').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}


// this function for update data 
function savemodalEditFAGroup()
{
    var body;
    var footer;
    $.ajax({
        url:'{{url('administrator/groups/savemodalEditFAGroup')}}',
        data: $('form#formEditAccess').serialize(),
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.success=="success")
            {
                $('#modalEditFAGroup').modal('hide');
            }
            else if(data.gagal=="gagal")
            {
                swal('FA Access Harus Di Pilih');
            }
            else
            {
                swal('Failed','Cannot be save!','error');
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
              //swal("FA Access Harus Di Pilih");
              swal("error", thrownError, "error");
        }
    });
}


// this function for update data 
function savemodalEditSurveyGroup()
{
    var body;
    var footer;
    $.ajax({
        url:'{{url('administrator/groups/savemodalEditSurveyGroup')}}',
        data: $('form#formEditSurvey').serialize(),
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.success=="success")
            {
                $('#modalEditFAGroup').modal('hide');
            }
            else
            {
                swal('Failed','Cannot be save!','error');
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("Survey Group Harus Di Pilih");
        }
    });
}

// this function for get data in modal edit 
function editGroupAccess(id)
{
    var body;
    var footer;
    $.ajax({
        url:'groups/editGroupAccess/'+id+'',
        data:{'page':'edit'},
        type:'GET',
        success:function(data){
            body = data;
              $('#modalEditGroups').find('.modal-title').html('Edit Group Access');
              $('#modalEditGroups').find('.modal-body').html(body);
              $('#modalEditGroups').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

function updateFAGroup()
{
   var dataUser = $('#formGroupEdit').serialize();
   var id = $('[name="group_id"]').val();
    $.ajax({
        url:'groups/'+id,
        data:dataUser,
        dataType:'JSON',
        type:'PUT',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Group has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

/// for save new group access
function saveGroup()
{
  $.ajax({
    url:"{{route('administrator.groups.store')}}",
    data: $('form#formGroup, form#formSurveyGroup ,form#formFAGroup').serialize(),
    dataType:'JSON',
    type:'POST',
    headers: 
    {
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    success:function(data){
      if(data.errors)
      {
        $.each(data.errors,function(key,value){
          $('.alert-danger').show();
          $('.alert-danger').html('<li>'+value+'</li>');
        })

        setTimeout(function() {
          $('#error-alert').fadeOut('fast');
        }, 2000); 
      }
      else
      {
        swal('Success','Group has been saved successfully!','success');
        location.reload();
      }
    },
          error: function (xhr, ajaxOptions, thrownError) {
        swal("error!", thrownError, "error");
          },
  })
}

  /* ========== This for close modal ================ */

//for FA Group
function FAGroup()
{
  $('#modalAddFA').modal('show');
}


//transaction
function faTrnSelecta(cek, id)
{ 
  if(cek.checked){
    $('#fa_group_type_trn_ck_edit_b'+id+'').prop('checked', false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_trn_ck_edit_a'+id+'').prop('checked', true);
  }
}
function faTrnSelectb(cek, id)
{
  if(cek.checked){
    $('#fa_group_type_trn_ck_edit_a'+id+'').prop('checked', false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_trn_ck_edit_b'+id+'').prop('checked', true);
  }
}

//monitoring
function faMntSelecta(cek, id)
{ 
  if(cek.checked){
    $('#fa_group_type_mnt_ck_edit_b'+id+'').prop('checked',false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_mnt_ck_edit_a'+id+'').prop('checked',true);
  }
}
function faMntSelectb(cek, id)
{
  if(cek.checked){
    $('#fa_group_type_mnt_ck_edit_a'+id+'').prop('checked',false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_mnt_ck_edit_b'+id+'').prop('checked',true);
  }
}

//message
function faMsgSelecta(cek, id)
{ 
  if(cek.checked){
    $('#fa_group_type_msg_ck_edit_b'+id+'').prop('checked',false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_msg_ck_edit_a'+id+'').prop('checked',true);
  }
}
function faMsgSelectb(cek, id)
{
  if(cek.checked){
    $('#fa_group_type_msg_ck_edit_a'+id+'').prop('checked',false);
  }else{
    swal('Harus dipilih');
    $('#fa_group_type_msg_ck_edit_b'+id+'').prop('checked',true);
  }
}

/// button save fa group for close modal fa
function saveFAGroup()
{
    $('#modalAddFA').modal('hide');
}

/// button save fa group for close modal fa
function closeSurveyGroup()
{
    $('#modalSurveyGroup').modal('hide');
}


// for closemodal 
function closeModalFA()
{
    $('#modalAddFA').modal('hide');
}

// for close modal edit 
function closeModalEdit()
{
    $('#modalEditGroups').modal('hide'); 
}

/// for close modal edit fa group
function closemodalEditFAGroup()
{
    $('#modalEditFAGroup').modal('hide'); 
}


/// for close modal edit fa group
function closemodalEditSurveyGroup()
{
    $('#modalEditFAGroup').modal('hide'); 
}

/* ================================= */


/* ============ this for handle ceklist in modall add new ======= */

// disaat checkbox TRN perbaris di ceklist
function faGroupTrn(cek,id)
{ 
    if (cek.checked)
    {
        $('#fa_group_type_trn_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#fa_group_type_trn_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox MNT perbaris di ceklist
function faGroupMnt(cek,id)
{ 
    if (cek.checked)
    {
        $('#fa_group_type_mnt_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#fa_group_type_mnt_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox MNT perbaris di ceklist
function faGroupMsg(cek,id)
{ 
    if (cek.checked)
    {
        $('#fa_group_type_msg_'+id+'').attr('disabled',true);
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#fa_group_type_msg_'+id+'').attr('disabled',false); 
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',false); 
    }
}
/*====================================== */





/* ============ this for handle ceklist in modall add new ======= */

// disaat checkbox survey TRN perbaris di ceklist
function SurveyGroupTrn(cek,id)
{ 
    if (cek.checked)
    {
        $('#survey_group_type_trn_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_trn_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox survey MNT perbaris di ceklist
function SurveyGroupMnt(cek,id)
{ 
    if (cek.checked)
    {
        $('#survey_group_type_mnt_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_mnt_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox survey MNT perbaris di ceklist
function SurveyGroupMsg(cek,id)
{ 
    if (cek.checked)
    {
        $('#survey_group_type_msg_'+id+'').attr('disabled',true);
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_msg_'+id+'').attr('disabled',false); 
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',false); 
    }
}


// disaat checkbox survey TRN perbaris di ceklist
function SurveyGroupTrnEdit(cek,id)
{ 
    if (cek.checked)
    {
        $('#survey_group_type_trn_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_trn_edit_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox survey MNT perbaris di ceklist
function SurveyGroupMntEdit(cek,id)
{ 
    if (cek.checked)
    {
        $('#survey_group_type_mnt_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_mnt_edit_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox survey MNT perbaris di ceklist
function SurveyGroupMsgEdit(cek,id)
{ 


    if (cek.checked)
    {
        $('#survey_group_type_msg_'+id+'').attr('disabled',true);
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#survey_group_type_msg_'+id+'').attr('disabled',false); 
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',false); 
    }
}
/*====================================== */


/* ============ this for handle ceklist in modall edit======= */

// disaat checkbox TRN perbaris di ceklist
function faGroupTrnEdit(cek,id)
{
  if(cek.checked)
  {
    $('#fa_group_type_trn_edit_'+id+'').attr('disabled',true);  
  }
  else
  {
    $('#fa_group_type_trn_edit_'+id+'').attr('disabled',false);   
  }
}


// disaat checkbox MNT perbaris di ceklist
function faGroupMntEdit(cek,id)
{ 
    if (cek.checked)
    {
        $('#fa_group_type_mnt_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#fa_group_type_mnt_edit_'+id+'').attr('disabled',false); 
    }
}

// disaat checkbox MNT perbaris di ceklist

function faGroupMsgEdit(cek,id)
{ 
    if (cek.checked)
    {
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',false); 
    }
}

/*=====================================*/


/*  this function for checklist per row [Add New] */
function faGroupCheck(cek,id)
{
    if (cek.checked)
    { 
        //checkbox
        $('#fa_group_type_msg_ck_'+id+'').attr('disabled',false);
        $('#fa_group_type_trn_ck_'+id+'').attr('disabled',false);
        $('#fa_group_type_mnt_ck_'+id+'').attr('disabled',false);


        // text box
        $('#fa_group_type_trn_'+id+'').attr('disabled',true);
        $('#fa_group_type_mnt_'+id+'').attr('disabled',true);
        $('#fa_group_type_msg_'+id+'').attr('disabled',true);
    }
    else
    {
        ///checkbox
        $('#fa_group_type_msg_ck_'+id+'').attr('disabled',true);
        $('#fa_group_type_trn_ck_'+id+'').attr('disabled',true);
        $('#fa_group_type_mnt_ck_'+id+'').attr('disabled',true);

        // text box
        $('#fa_group_type_trn_'+id+'').attr('disabled',false);
        $('#fa_group_type_mnt_'+id+'').attr('disabled',false);
        $('#fa_group_type_msg_'+id+'').attr('disabled',false);
    } 
}

/*================================= */



/*  this function for checklist per row [Add New] */
function SurveyGroupCheck(cek,id)
{
    if (cek.checked)
    { 
        //checkbox
        $('#survey_group_type_msg_ck_'+id+'').attr('disabled',false);
        $('#survey_group_type_trn_ck_'+id+'').attr('disabled',false);
        $('#survey_group_type_mnt_ck_'+id+'').attr('disabled',false);


        // text box
        $('#survey_group_type_trn_'+id+'').attr('disabled',true);
        $('#survey_group_type_mnt_'+id+'').attr('disabled',true);
        $('#survey_group_type_msg_'+id+'').attr('disabled',true);
    }
    else
    {
        ///checkbox
        $('#survey_group_type_msg_ck_'+id+'').attr('disabled',true);
        $('#survey_group_type_trn_ck_'+id+'').attr('disabled',true);
        $('#survey_group_type_mnt_ck_'+id+'').attr('disabled',true);

        // text box
        $('#survey_group_type_trn_'+id+'').attr('disabled',false);
        $('#survey_group_type_mnt_'+id+'').attr('disabled',false);
        $('#survey_group_type_msg_'+id+'').attr('disabled',false);
    } 
}

/*================================= */

/*  this function for checklist per row [Add New] */
function SurveyGroupCheckEdit(cek,id)
{
    if (cek.checked)
    { 
        //checkbox
        $('#survey_group_type_msg_ck_edit_'+id+'').attr('disabled',false);
        $('#survey_group_type_trn_ck_edit_'+id+'').attr('disabled',false);
        $('#survey_group_type_mnt_ck_edit_'+id+'').attr('disabled',false);


        // text box
        $('#survey_group_type_trn_edit_'+id+'').attr('disabled',true);
        $('#survey_group_type_mnt_edit_'+id+'').attr('disabled',true);
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        ///checkbox
        $('#survey_group_type_msg_ck_edit_'+id+'').attr('disabled',true);
        $('#survey_group_type_trn_ck_edit_'+id+'').attr('disabled',true);
        $('#survey_group_type_mnt_ck_edit_'+id+'').attr('disabled',true);

        // text box
        $('#survey_group_type_trn_edit_'+id+'').attr('disabled',false);
        $('#survey_group_type_mnt_edit_'+id+'').attr('disabled',false);
        $('#survey_group_type_msg_edit_'+id+'').attr('disabled',false);
    } 
}

/*================================= */

/*  this function for checklist per row [Edit] */
function faGroupCheckEdit(cek,id)
{
    if (cek.checked)
    { 
        //checkbox
        $('#fa_group_type_msg_ck_edit_a'+id+'').attr('disabled',false);
        $('#fa_group_type_msg_ck_edit_b'+id+'').attr('disabled',false);
        $('#fa_group_type_trn_ck_edit_a'+id+'').attr('disabled',false);
        $('#fa_group_type_trn_ck_edit_b'+id+'').attr('disabled',false);
        $('#fa_group_type_mnt_ck_edit_a'+id+'').attr('disabled',false);
        $('#fa_group_type_mnt_ck_edit_b'+id+'').attr('disabled',false);


        // text box
        $('#fa_group_type_trn_edit_'+id+'').attr('disabled',true);
        $('#fa_group_type_mnt_edit_'+id+'').attr('disabled',true);
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',true);
    }
    else
    {
        ///checkbox
        $('#fa_group_type_msg_ck_edit_a'+id+'').attr('disabled',true);
        $('#fa_group_type_msg_ck_edit_b'+id+'').attr('disabled',true);
        $('#fa_group_type_trn_ck_edit_a'+id+'').attr('disabled',true);
        $('#fa_group_type_trn_ck_edit_b'+id+'').attr('disabled',true);
        $('#fa_group_type_mnt_ck_edit_a'+id+'').attr('disabled',true);
        $('#fa_group_type_mnt_ck_edit_b'+id+'').attr('disabled',true);

        // text box
        $('#fa_group_type_trn_edit_'+id+'').attr('disabled',false);
        $('#fa_group_type_mnt_edit_'+id+'').attr('disabled',false);
        $('#fa_group_type_msg_edit_'+id+'').attr('disabled',false);
    } 
}

/*================================= */



function editSurveyGroup(id)
{
    var body;
    var footer;
    $.ajax({
        url:'groups/editSurveyGroup/'+id+'',
        data:{'page':'edit'},
        type:'GET',
        success:function(data){
            body = data;

            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closemodalEditSurveyGroup()">Cancel</button>'+
                      '<button type="button" class="btn btn-primary  btnFooterModal" onclick="savemodalEditSurveyGroup()">Update</button>';
              $('#modalEditFAGroup').find('.modal-title').html('Edit Group Access');
              $('#modalEditFAGroup').find('.modal-body').html(body);
              $('#modalEditFAGroup').find('.modal-footer').html(footer);
              $('#modalEditFAGroup').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}


// this function for checklist all in add new modal
function allFAGroup(resp)
{
    var count = '{{$count}}';
    if (resp.checked)
    {
        $('.rowFAGroup').prop('checked', resp.checked);
        for(i=0;i<count;i++)
        {
            //checkbox
            $('#fa_group_type_msg_ck_'+i+'').attr('disabled',false);
            $('#fa_group_type_trn_ck_'+i+'').attr('disabled',false);
            $('#fa_group_type_mnt_ck_'+i+'').attr('disabled',false);
        }
    }
    else
    {
        $('.rowFAGroup').prop('checked', resp.checked);
        for(i=0;i<count;i++)
        {
            //checkbox
            $('#fa_group_type_msg_ck_'+i+'').attr('disabled',true);
            $('#fa_group_type_trn_ck_'+i+'').attr('disabled',true);
            $('#fa_group_type_mnt_ck_'+i+'').attr('disabled',true);
        }
    }
}


// this function for checklist all in add new modal
function allSurveyGroup(resp)
{
    var count = '{{$count}}';
    if (resp.checked)
    {
        $('.rowSurveyGroup').prop('checked', resp.checked);
        for(i=0;i<count;i++)
        {
            //checkbox
            $('#survey_group_type_msg_ck_'+i+'').attr('disabled',false);
            $('#survey_group_type_trn_ck_'+i+'').attr('disabled',false);
            $('#survey_group_type_mnt_ck_'+i+'').attr('disabled',false);
        }
    }
    else
    {
        $('.rowSurveyGroup').prop('checked', resp.checked);
        for(i=0;i<count;i++)
        {
            //checkbox
            $('#survey_group_type_msg_ck_'+i+'').attr('disabled',true);
            $('#survey_group_type_trn_ck_'+i+'').attr('disabled',true);
            $('#survey_group_type_mnt_ck_'+i+'').attr('disabled',true);
        }
    }
}


// this function for checklist all in Edit modal
function allFAGroupEdit(resp)
{
    var countEdit = '{{$count}}';
    if (resp.checked)
    {
        $('.rowFAGroupEdit').prop('checked', resp.checked);
        for(z=0;z<countEdit;z++)
        {
            //checkbox
            $('#fa_group_type_msg_ck_edit_a'+z+'').attr('disabled',false);
            $('#fa_group_type_msg_ck_edit_b'+z+'').attr('disabled',false);
            $('#fa_group_type_trn_ck_edit_a'+z+'').attr('disabled',false);
            $('#fa_group_type_trn_ck_edit_b'+z+'').attr('disabled',false);
            $('#fa_group_type_mnt_ck_edit_a'+z+'').attr('disabled',false);
            $('#fa_group_type_mnt_ck_edit_b'+z+'').attr('disabled',false);
        }
    }
    else
    {
        $('.rowFAGroupEdit').prop('checked', resp.checked);
        for(z=0;z<countEdit;z++)
        {
            //checkbox
            $('#fa_group_type_msg_ck_edit_a'+z+'').attr('disabled',true);
            $('#fa_group_type_msg_ck_edit_b'+z+'').attr('disabled',true);
            $('#fa_group_type_trn_ck_edit_a'+z+'').attr('disabled',true);
            $('#fa_group_type_trn_ck_edit_b'+z+'').attr('disabled',true);
            $('#fa_group_type_mnt_ck_edit_a'+z+'').attr('disabled',true);
            $('#fa_group_type_mnt_ck_edit_b'+z+'').attr('disabled',true);
        }
    }
}


// this function for checklist all in Edit modal
function allSurveyGroupEdit(resp)
{
    var countEdit = '{{$count}}';
    if (resp.checked)
    {
        $('.rowSurveyGroupEdit').prop('checked', resp.checked);
        for(z=0;z<countEdit;z++)
        {
            //checkbox
            $('#survey_group_type_msg_ck_edit_'+z+'').attr('disabled',false);
            $('#survey_group_type_trn_ck_edit_'+z+'').attr('disabled',false);
            $('#survey_group_type_mnt_ck_edit_'+z+'').attr('disabled',false);
        }
    }
    else
    {
        $('.rowSurveyGroupEdit').prop('checked', resp.checked);
        for(z=0;z<countEdit;z++)
        {
            //checkbox
            $('#survey_group_type_msg_ck_edit_'+z+'').attr('disabled',true);
            $('#survey_group_type_trn_ck_edit_'+z+'').attr('disabled',true);
            $('#survey_group_type_mnt_ck_edit_'+z+'').attr('disabled',true);
        }
    }
}





</script>