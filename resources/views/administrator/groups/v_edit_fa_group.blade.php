@php
	use App\Models\Administrator\FA_Group_Access;
@endphp
<form id="formEditAccess">
<table class="table table-striped table-bordered" id="listFAGroup"> 
	<thead>
		<tr>
			<th rowspan="2">
			@php
			if($count_dispatch_group_master == $count_dispatch_group_user){
			echo '<input type="checkbox" name="allCheck" value="0" disabled>';
			}else{
			echo '<input type="checkbox" name="allCheck" onchange="allFAGroupEdit(this)" value="0">';
			}
			@endphp
			All 
			</th>
			<th rowspan="2">Dispatch Group </th>
			<th colspan="2"><center>TRN</center></th>
			<th colspan="2"><center>MNT</center></th>
			<th colspan="2"><center>MSG</center></th>
		</tr>
		<tr> 
			<th>Yes</th> 
			<th>No</th> 
			<th>Yes</th> 
			<th>No</th> 
			<th>Yes</th> 
			<th>No</th> 
		</tr>
	</thead>
	<tbody>
	<input type="hidden" name="group_access_id" value="{{$id}}">
	@foreach($faGroup as $in => $fa)
		@php 
			$groupAccess = FA_Group_Access::where('group_access_id',$id)->where('dispatch_group',$fa->dispatch_group)->first();
		@endphp
		<tr>
			<td>
				<input type="checkbox"  name="dispatch_group[]" class="rowFAGroupEdit" value="{{$fa->dispatch_group}}"  id="rowFAGroupEdit_{{$in}}" onchange="faGroupCheckEdit(this,{{$in}})" 
				@php 
				$ckRow = '';
				$disabled = 'disabled';
				$disabledForm = '';
				if(!empty($groupAccess->group_access_id)):
				$ckRow = 'checked';
				$disabled = '';
				$disabledForm='disabled';
				@endphp
				@endif
				{{$ckRow}}>
			</td>
			<td>{{$fa->dispatch_group}}</td>
			@if(!empty($groupAccess) && ($groupAccess->transaction == 'true'))
				<td><input type="checkbox" name="fa_group_type_trn[]" onchange="faTrnSelecta(this,{{$in}})" id="fa_group_type_trn_ck_edit_a{{$in}}" value="true" {{$disabled}} checked></td>
				<td><input type="checkbox" name="fa_group_type_trn[]" onchange="faTrnSelectb(this,{{$in}})" id="fa_group_type_trn_ck_edit_b{{$in}}" value="false" {{$disabled}}></td>
			@else
				<td><input type="checkbox" name="fa_group_type_trn[]" onchange="faTrnSelecta(this,{{$in}})" id="fa_group_type_trn_ck_edit_a{{$in}}" value="true" {{$disabled}}></td>
				<td><input type="checkbox" name="fa_group_type_trn[]" onchange="faTrnSelectb(this,{{$in}})" id="fa_group_type_trn_ck_edit_b{{$in}}" value="false" {{$disabled}} checked></td>
			@endif
			@if(!empty($groupAccess) && ($groupAccess->monitoring == 'true'))
				<td style="border-left: 2px solid #cecece;"><input type="checkbox" name="fa_group_type_mnt[]" onchange="faMntSelecta(this,{{$in}})" id="fa_group_type_mnt_ck_edit_a{{$in}}" value="true" {{$disabled}} checked></td>
				<td><input type="checkbox" name="fa_group_type_mnt[]" onchange="faMntSelectb(this,{{$in}})" id="fa_group_type_mnt_ck_edit_b{{$in}}" value="false" {{$disabled}}></td>
			@else
				<td style="border-left: 2px solid #cecece;"><input type="checkbox" name="fa_group_type_mnt[]" onchange="faMntSelecta(this,{{$in}})" id="fa_group_type_mnt_ck_edit_a{{$in}}" value="true" {{$disabled}}></td>
				<td><input type="checkbox" name="fa_group_type_mnt[]" onchange="faMntSelectb(this,{{$in}})" id="fa_group_type_mnt_ck_edit_b{{$in}}" value="false" {{$disabled}} checked></td>
			@endif
			@if(!empty($groupAccess) && ($groupAccess->message == 'true'))
				<td style="border-left: 2px solid #cecece;"><input type="checkbox" name="fa_group_type_msg[]" onchange="faMsgSelecta(this,{{$in}})" id="fa_group_type_msg_ck_edit_a{{$in}}" value="true" {{$disabled}} checked></td>
				<td><input type="checkbox" name="fa_group_type_msg[]" onchange="faMsgSelectb(this,{{$in}})" id="fa_group_type_msg_ck_edit_b{{$in}}" value="false" {{$disabled}}></td>
			@else
				<td style="border-left: 2px solid #cecece;"><input type="checkbox" name="fa_group_type_msg[]" onchange="faMsgSelecta(this,{{$in}})" id="fa_group_type_msg_ck_edit_a{{$in}}" value="true" {{$disabled}}></td>
				<td><input type="checkbox" name="fa_group_type_msg[]" onchange="faMsgSelectb(this,{{$in}})" id="fa_group_type_msg_ck_edit_b{{$in}}" value="false" {{$disabled}} checked></td>
			@endif
		</tr>
	@endforeach
	</tbody>
</table>  
</form>