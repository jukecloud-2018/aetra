@extends('layouts.app')
@section('head')
    Administrator | Group Access
@endsection
@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Group Access </span>
@endsection
@section('button')
    <div class="forButtonTop">
          <a href="" class="btn btn-default buttonAE btnTop" data-toggle="modal" data-target="#modalGroup">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </a>
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection
@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        {{--<div class="box-header with-border">--}}
        {{--<h3 class="box-title">Group Access</h3>--}}
        {{--<div class="box-tools pull-right">--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
        {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- /Box Header -->
        <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableGroup" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th>No</th>
                                    <th style="width: 90px;">Nama Group</th>
                                    <th>Dispatch Group</th>
                                    <th style="width: 65px;"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->
<!-- Modal   Add -->
<div class="modal fade" id="modalGroup" tabindex="-1" role="dialog" aria-labelledby="modalGroupCenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 66%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Add New Group Access</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formGroup">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="form-group row">
                        <label for="" class="col-sm-1 col-form-label"> Group </label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" placeholder="Group" value="" name="group_name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-4 col-form-label"> Access Data </label>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <table id="tableFA">
                                <tr class="rowFA_0">
                                    <td width="85px"><label for="" class="col-form-label"> Dispatch Group </label></td>
                                    <td align="center">
                                        <div>
                                            <a href="javascript:void(0)" onclick="FAGroup()"> <i class="fa fa-plus"></i> </a>
                                        </div>
                                    </td>
                                </tr> 
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table id="tableWO">
                                <tr class="rowWO_0">
                                    <td width="85px"><label for="" class="col-form-label"> WO Group </label></td>
                                    <td align="center">
                                        <div>
                                            <a href="javascript:void(0)" onclick="WOGroup()"> <i class="fa fa-plus"></i> </a>
                                        </div>
                                    </td>
                                </tr> 
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table id="tableSurvey">
                                <tr class="rowSurvey_0">
                                    <td width="100px"><label for="" class="col-form-label"> Survey Group </label></td>
                                    <td align="center">
                                        <div>
                                            <a href="javascript:void(0)" onclick="SurveyGroup()"> <i class="fa fa-plus"></i> </a>
                                        </div>
                                    </td>
                                </tr> 
                            </table>
                        </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveGroup()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
<!-- Modal  FA Group  -->
<div class="modal fade" id="modalAddFA" tabindex="-1" role="dialog" aria-labelledby="modalAddFA" aria-hidden="false"  data-backdrop="false" style="margin-top:50px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModalFA()">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center"> Dispatch Group </h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: scroll;">
                <form id="formFAGroup">
                    <table class="table table-striped table-bordered" id="listFAGroup"> 
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" name="allCheck" onchange="allFAGroup(this)" value="0"> All 
                                </th>
                                <th>Dispatch Group </th>
                                <th>TRN </th>
                                <th>MNT </th>
                                <th>MSG </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($faGroup as $in => $fa)
                            <tr>
                                <td><input type="checkbox"  name="dispatch_group[]" class="rowFAGroup" value="{{$fa->dispatch_group}}" id="rowFAGroup_{{$in}}" onchange="faGroupCheck(this,'{{$in}}')" ></td>
                                <td>{{$fa->dispatch_group}}</td>
                                <td>
                                    <input type="checkbox"  name="fa_group_type_trn[]" class="" disabled value="true" id="fa_group_type_trn_ck_{{$in}}"  onchange="faGroupTrn(this,'{{$in}}')">
                                    <input type='hidden' value='false' name="fa_group_type_trn[]" id="fa_group_type_trn_{{$in}}">
                                </td>
                                <td>
                                    <input type="checkbox"  name="fa_group_type_mnt[]" value="true" disabled onchange="faGroupMnt(this,'{{$in}}')"  id="fa_group_type_mnt_ck_{{$in}}">
                                    <input type='hidden' value='false' name="fa_group_type_mnt[]"  id="fa_group_type_mnt_{{$in}}" >
                                </td>
                                <td>
                                    <input type="checkbox"   id="fa_group_type_msg_ck_{{$in}}" name="fa_group_type_msg[]" value="true" disabled onchange="faGroupMsg(this,'{{$in}}')">
                                    <input type='hidden' value='false' name="fa_group_type_msg[]"  id="fa_group_type_msg_{{$in}}" >
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>  
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModalFA()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveFAGroup()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal fa group-->
<!-- Modal  edit group  -->
<div class="modal fade" id="modalEditGroups" tabindex="-1" role="dialog" aria-labelledby="modalEditGroups" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 66%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModalEdit()">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center"></h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB"></div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="window.location.reload()"">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="updateFAGroup()">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal edit group -->
<!-- Modal  edit detail  -->
<div class="modal fade" id="modalEditFAGroup" tabindex="-1" role="dialog" aria-labelledby="modalEditFAGroup" aria-hidden="false"  data-backdrop="false" style="margin-top:50px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <button type="button" class="close" onclick="closemodalEditFAGroup()">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center"></h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: scroll;"></div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center"></div>
        </div>
    </div>
</div>
<!-- end Modal edit detail -->
<!-- Modal  Survey Group  -->
<div class="modal fade" id="modalSurveyGroup" tabindex="-1" role="dialog" aria-labelledby="modalSurveyGroup" aria-hidden="false"  data-backdrop="false" style="margin-top:50px;">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeSurveyGroup()">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center"> Survey Group </h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: scroll;">
                <form id="formSurveyGroup">
                    <table class="table table-striped table-bordered" id="listFAGroup"> 
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="allCheck" onchange="allSurveyGroup(this)" value="0"> All </th>
                                <th>Survey Group </th>
                                <th>TRN </th>
                                <th>MNT </th>
                                <th>MSG </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($surveyGroup as $index => $su)
                            <tr>
                                <td>
                                    <input type="checkbox"  name="code_ab[]" class="rowSurveyGroup" value="{{$su->code_ab}}" id="rowSurveyGroup_{{$index}}" onchange="SurveyGroupCheck(this,'{{$index}}')" >
                                </td>
                                <td>{{$su->code_ab.' -  '. $su->description }}</td>
                                <td>
                                    <input type="checkbox"  name="survey_group_type_trn[]" class="" disabled value="true" id="survey_group_type_trn_ck_{{$index}}"  onchange="SurveyGroupTrn(this,'{{$index}}')">
                                    <input type='hidden' value='false' name="survey_group_type_trn[]" id="survey_group_type_trn_{{$index}}">
                                </td>
                                <td>
                                    <input type="checkbox"  name="survey_group_type_mnt[]" value="true" disabled onchange="SurveyGroupMnt(this,'{{$index}}')"  id="survey_group_type_mnt_ck_{{$index}}">
                                    <input type='hidden' value='false' name="survey_group_type_mnt[]"  id="survey_group_type_mnt_{{$index}}" >
                                </td>
                                <td>
                                    <input type="checkbox"   id="survey_group_type_msg_ck_{{$index}}" name="survey_group_type_msg[]" value="true" disabled onchange="SurveyGroupMsg(this,'{{$index}}')">
                                    <input type='hidden' value='false' name="survey_group_type_msg[]"  id="survey_group_type_msg_{{$index}}" >
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>  
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeSurveyGroup()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="closeSurveyGroup()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal Survey group-->
@endsection
@section('js')
  @include('administrator.groups.js_groups')
@endsection
