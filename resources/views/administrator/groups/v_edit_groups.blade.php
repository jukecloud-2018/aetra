<form id="formGroupEdit">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="form-group row">
        <label for="" class="col-sm-1 col-form-label"> Group </label>
        <div class="col-sm-6">
            <input type="text" class="form-control" placeholder="Group" value="{{$groups->group_name}}" name="group_name">
            <input type="hidden" class="form-control" placeholder="Group" value="{{$groups->id}}" name="group_id">
        </div>
    </div> 
    <div class="form-group row">
        <label for="" class="col-sm-4 col-form-label"> Access Data </label>
    </div>
    <div class="row">
        <div class="col-md-4">
            <table id="tableFA" class="tableFA" style="width:100%;">
                <tr>
                    <td style="width:30%;"><label for="" class="col-form-label"> Dispatch </label></td>
                    <td style="width:70%;" class="rowFA_0" name="rowFA_0">
                        <div>
                            <a href="javascript:void(0)" onclick="editFAGroup('{{$groups->id}}')"> <i class="fa fa-edit"></i> </a>
                        </div>
                        <!--<input type="checkbox" name="fa_group_type_trn[]" value="All">
                        <label>All</label><br>
                        <select name="" style="width:100%;">
                        @foreach($faGroup as $value)
                        <option>{{$value->dispatch_group}}</option>
                        @endforeach
                        </select><br>
                        <input type="checkbox" name="fa_group_type_trn[]" value="All" ><label class="col-form-label"> TRN </label>&nbsp;&nbsp;
                        <input type="checkbox" name="fa_group_type_mnt[]" value="All" ><label class="col-form-label"> MNT </label>&nbsp;&nbsp;
                        <input type="checkbox" name="fa_group_type_msg[]" value="All" ><label class="col-form-label"> MSG </label><br>
                        <a href="javascript:void(0)" onclick="addRowFaGroup()"> <i class="fa fa-plus"></i> Add FA Group </a>-->
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table id="tableWO">
                <tr class="rowWO_0">
                    <td width="85px"><label for="" class="col-form-label"> WO Group </label></td>
                    <td align="center">
                        <div>
                            <a href="javascript:void(0)" onclick="editWOGroup('{{$groups->id}}')"> <i class="fa fa-edit"></i> </a>
                        </div>
                    </td>
                </tr> 
            </table>
        </div>
        <div class="col-md-4">
            <table id="tableSurvey">
                <tr class="rowSurvey_0">
                    <td width="100px"><label for="" class="col-form-label"> Survey Group </label></td>
                    <td align="center">
                        <div>
                            <a href="javascript:void(0)" onclick="editSurveyGroup('{{$groups->id}}')"> <i class="fa fa-edit"></i> </a>
                        </div>
                    </td>
                </tr> 
            </table>
        </div>
    </div>
</form><!-- /form -->

<!-- Add Row Group FA 
<script type="text/javascript">
function addRowFaGroup(){
baris++
var option =  '<tr> <td style="width:30%;"></td>'+
'<td style="width:70%;" class="rowFA_'+baris+'">'+
'<select class="form-control" name="template_code[]" style="width:100%;">'+
'<option>---Select---</option>'+
'@foreach ($faGroup as $value)'+
'<option value="{{$value->dispatch_group}}" data="'+baris+'">{{$value->dispatch_group}}</option>'+
'@endforeach'+
'</select><br>'+
'<input type="checkbox" name="fa_group_type_trn[]"><label class="col-form-label"> TRN </label>&nbsp;&nbsp;'+
'<input type="checkbox" name="fa_group_type_mnt[]"><label class="col-form-label"> MNT </label>&nbsp;&nbsp;'+
'<input type="checkbox" name="fa_group_type_msg[]"><label class="col-form-label"> MSG </label><br>'+
'<a href="javascript:void(0)" onclick="deleteRowFA('+baris+')" class="remove" style="color:red"> <i class="fa fa-minus"></i> </a>'+
'</td></tr>';
$('.tableFA').append(option);
}
</script>-->