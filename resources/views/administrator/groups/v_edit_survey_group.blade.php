<form id="formEditSurvey">
	<table class="table table-striped table-bordered" id="listSurveyGroup"> 
		<thead>
			<tr>
			<th><input type="checkbox" name="allCheck" onchange="allSurveyGroupEdit(this)" value="0"> All </th>
			<th>Dispatch Group </th>
			<th>TRN </th>
			<th>MNT </th>
			<th>MSG </th>
			</tr>
		</thead>
		<tbody>
			<input type="hidden" name="group_access_id" value="{{$id}}">
			@foreach($code_ab as $in => $cd)
			@php $groupAccess = DB::table('survey_group_access')->where('group_access_id',$id)->where('code_ab',$cd->code_ab)->first(); @endphp   
			<tr>
			<td>
				<input type="checkbox" name="code_ab[]" class="rowSurveyGroupEdit" value="{{$cd->code_ab}}" id="rowSurveyGroupEdit_{{$in}}" onchange="SurveyGroupCheckEdit(this,{{$in}})" 
				@php 
				$ckRow = '';
				$disabled = 'disabled';
				$disabledForm = '';
				if(!empty($groupAccess->group_access_id)):
				$ckRow = 'checked';
				$disabled = '';
				$disabledForm='disabled';
				@endphp
				@endif
				{{$ckRow}}>
			</td>
			<td>{{$cd->code_ab.' - '.$cd->description}}</td>
			<td>
				<input type="checkbox"  name="survey_group_type_trn[]"   onchange="SurveyGroupTrnEdit(this,'{{$in}}')"  id="survey_group_type_trn_ck_edit_{{$in}}"
				@php 
				$ckTrn = '';
				if(!empty($groupAccess) &&  ($groupAccess->transaction=='true')):
				$ckTrn ='checked';
				@endphp
				@endif
				value="true" {{$ckTrn}} {{$disabled}}> 
				<input type='hidden' value='false' name="survey_group_type_trn[]" id="survey_group_type_trn_edit_{{$in}}" {{$disabledForm}}>
			</td>
			<td>
				<input type="checkbox"  name="survey_group_type_mnt[]"  onchange="SurveyGroupMntEdit(this,'{{$in}}')"  id="survey_group_type_mnt_ck_edit_{{$in}}"
				@php 
				$ckMnt = '';
				if(!empty($groupAccess) &&  ($groupAccess->monitoring=='true')):
				$ckMnt ='checked';
				@endphp
				@endif
				value="true" {{$ckMnt}} {{$disabled}}>
				<input type='hidden' value='false' name="survey_group_type_mnt[]"  id="survey_group_type_mnt_edit_{{$in}}" {{ $disabledForm }}>
			</td>
			<td>
				<input type="checkbox"  name="survey_group_type_msg[]"   onchange="SurveyGroupMsgEdit(this,'{{$in}}')"  id="survey_group_type_msg_ck_edit_{{$in}}"
				@php 
				$ckMsg = '';
				if(!empty($groupAccess) &&  ($groupAccess->message=='true')):
				$ckMsg ='checked';
				@endphp
				@endif
				value="true" {{$ckMsg}} {{$disabled}}>
				<input type='hidden' value='false' name="survey_group_type_msg[]"  id="survey_group_type_msg_edit_{{$in}}" {{ $disabledForm }}>
			</td>
			</tr>
			@endforeach
		</tbody>
	</table>  
</form>