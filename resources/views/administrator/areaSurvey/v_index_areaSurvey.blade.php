@extends('layouts.app')

@section('head')
    Administrator | Area Survey
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Area Survey </span>
@endsection
@section('button')
    <div class="forButtonTop">
          <a href="" class="btn btn-default buttonAE btnTop" data-toggle="modal" data-target="#groupModal">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </a>
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Area Survey</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
    <!-- /Box Header -->
    
    <!--- Box Body --->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
            <div class="table-responsive">
              <table id="tableVendor" class="table table-bordered table-striped">
                <thead>
                  <tr style="background-color:#8B9AAD;color:#fff">
                    <th>No</th>
                    <th>CC</th>
                    <th>PC</th>
                    <th>EZ</th>
                    <th>AB</th>
                    <th></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="groupModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" style="text-align:center">Add New Area Survey</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
        <div class="alert alert-danger" id="error-alert" style="display: none"></div>
        
           <form id="formGroup">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
             <div class="form-group row">
              <label for="" class="col-sm-3 col-form-label">Username</label>
              <div class="col-sm-9">
                <input type="username" class="form-control" id="" placeholder="Username">
              </div>
            </div>
          </form><!-- /form -->
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveGroup()">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- end Modal -->
@endsection

@section('js')
  @include('administrator.areaSurvey.js_areaSurvey')
@endsection