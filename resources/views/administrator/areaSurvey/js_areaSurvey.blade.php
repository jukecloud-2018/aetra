<script type="text/javascript">

// for style checkbox
$( document ).ready(function() {
  	styleChek();
});

//for datatable 
$('#table').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { "data": "nameGroup" },
        { "data": "description" },
        { "data": "description" },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="detail(`' + row.id + '`)"><i class="fa fa-pencil"></i></button>';
            }
		}
    ],
    responsive: true,
});

// for style checkbox
function styleChek()
{
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass   : 'iradio_minimal-green'
    });
}
    

// for add row table fa
function addRowFa()
{
	var table = $('#tableFA');

	table.append('<tr  class="rowFa">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                        '<option value="">Fa</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowFa()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

// for delete row table fa
function deleteRowFa()
{
	document.getElementById("tableFA").deleteRow(1);
}

// for add row table wo
function addRowWO()
{
	var table = $('#tableWO');

	table.append('<tr  class="rowWo">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowWo()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

//// for delete row table wo
function deleteRowWo()
{
	document.getElementById("tableWO").deleteRow(1);
}

/// for save group 
function saveGroup()
{
	var data = $('#formGroup').serialize();

	$.ajax({
		url:"",
		data:data,
		dataType:'JSON',
		type:'POST',
		success:function(data){
			if(data.errors)
			{
				$.each(data.errors,function(key,value){
					$('.alert-danger').show();
					$('.alert-danger').html('<li>'+value+'</li>');
				})

				setTimeout(function() {
					$('#error-alert').fadeOut('fast');
				}, 2000); 
			}
			else
			{
				swal('Success','Group has been saved successfully!','success');
				location.reload();
			}
		},
          error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
          },
	})
}

</script>