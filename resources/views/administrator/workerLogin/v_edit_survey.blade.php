<form id="formEditSurveyGroup">
<table class="table table-striped table-bordered" id="listFAGroup"> 
  <thead>
    <tr>
      <th><input type="checkbox" name="allCheck" onchange="allSurveyGroup(this)" value="0"> All </th>
      <th>Survey Group </th>
    </tr>
  </thead>
  <tbody>
   @foreach($surveyGroup as $index => $su)
   	 @php  
   	 $data = DB::table('worker_survey')->where(['worker_id'=>$worker_id,'code_ab'=>$su->code_ab])->first(); 
   	 @endphp
    <tr>
      <td><input type="checkbox" 

      			@php 
      				$ck = '';
      			if(!empty($data) &&  ($data->code_ab==$su->code_ab)):
      					$ck = 'checked';
      			@endphp
      			@endif

      	name="code_ab[]" class="rowSurveyGroup" {{$ck}} value="{{$su->code_ab}}" id="rowSurveyGroup_{{$index}}" ></td>
      <td>{{$su->code_ab.' -  '. $su->description }}</td>
    </tr>
    @endforeach
  </tbody>
</table>  
</form>