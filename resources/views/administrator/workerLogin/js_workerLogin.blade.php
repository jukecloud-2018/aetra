<script type="text/javascript">

//for datatable 

  $(document).ready(function() {
    var table = $('#tableWorker').DataTable( {
      "scrollY": 450,
      "scrollX": true
    });
  });

function saveWorker()
{
   var data = $('form#formWorker, form#formFAGroup, form#formSurveyGroup').serialize();
    $.ajax({
        url:"{{route('administrator.workerLogin.store')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        headers: {
          'X-CSRF-TOKEN' : '{{csrf_token()}}',
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').html('<li>'+value+'</li>');
                })
                hidden_error();
            }
            else
            {
                swal('Success','Worker Login has been saved successfully!','success');
                location.reload();
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", 'Jangan Ada Data Yang Kosong', "error");
        }
    })
}

function addWorker()
{
    var body;
    var worker = $(".worker").attr("data-worker");
    if(worker == 'wo'){
        $.ajax({
            url:'../create',
            data:{'page':'detail'},
            type:'GET',
            success:function(data){
                body = data;
                $('#workerModal').find('.modal-body').html(body);
                $('#workerModal').modal('show');
                datePicker()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }else if(worker == 'fa'){
        $.ajax({
            url:'workerLogin/create',
            data:{'page':'detail'},
            type:'GET',
            success:function(data){
                body = data;
                $('#workerModal').find('.modal-body').html(body);
                $('#workerModal').modal('show');
                datePicker()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }
}

function editWorker(id)
{
    var body;
    var worker = $(".worker").attr("data-worker");
    if(worker == 'wo'){
        $.ajax({
            url:'../'+id+'',
            data:{'page':'detail'},
            type:'GET',
            success:function(data){
                body = data;
                footer = '<button type="button" class="btn btn-secondary btnFooterModal"'+
                'data-dismiss="modal"onclick="window.location.reload()">Cancel</button>'+
                '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateWorker('+id+')">Update</button>';
                $('#editworkerModal').find('.modal-body').html(body);
                $('#editworkerModal').find('.modal-footer').html(footer);
                $('#editworkerModal').modal('show');
                datePicker()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }else if(worker == 'fa'){
        $.ajax({
            url:'workerLogin/'+id+'',
            data:{'page':'detail'},
            type:'GET',
            success:function(data){
                body = data;
                footer = '<button type="button" class="btn btn-secondary btnFooterModal"'+
                'data-dismiss="modal"onclick="window.location.reload()">Cancel</button>'+
                '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateWorker('+id+')">Update</button>';
                $('#editworkerModal').find('.modal-body').html(body);
                $('#editworkerModal').find('.modal-footer').html(footer);
                $('#editworkerModal').modal('show');
                datePicker()
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }
}

function deleteWorker(id)
{
    var worker = $(".worker").attr("data-worker");
    if(worker == 'wo'){
        console.log('Delete processing');
        swal({
        title: "Apakah Data Ingin Di Hapus?",
        text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
        }, function(isConfirm){
            if (!isConfirm) return;
            $.ajax({
                url:'../destroy/'+id,
                data:id,
                dataType:'JSON',
                type:'GET',
                success:function(response){
                    if(response.status == "success"){
                        swal("Berhasil!", "Worker berhasil di hapus", "success");
                        location.reload();
                    }else if(response.status == "error"){
                        swal("Error", response.message, "error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Worker gagal di hapus", "error");
                }
            });
        });
    }else if(worker == 'fa'){
        console.log('Delete processing');
        swal({
        title: "Apakah Data Ingin Di Hapus?",
        text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
        }, function(isConfirm){
            if (!isConfirm) return;
            $.ajax({
                url:'workerLogin/destroy/'+id,
                data:id,
                dataType:'JSON',
                type:'GET',
                success:function(response){
                    if(response.status == "success"){
                        swal("Berhasil!", "Worker berhasil di hapus", "success");
                        location.reload();
                    }else if(response.status == "error"){
                        swal("Error", response.message, "error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swal("Gagal!", "Worker gagal di hapus", "error");
                }
            });
        });
    }
}

function updateWorker(id)
{
    var satatus = document.getElementById("status").value;
    if(satatus == 'fa'){
        var dataUser = $('#formWorkerEdit').serialize();
        $.ajax({
            url:'workerLogin/'+id,
            data:dataUser,
            dataType:'JSON',
            type:'PUT',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                if(data.status=='success')
                {
                    swal('Success','Worker has been upated successfully!','success');
                    location.reload();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }else if(satatus == 'wo'){
        var dataUser = $('#formWorkerEdit').serialize();
        $.ajax({
            url:'../'+id,
            data:dataUser,
            dataType:'JSON',
            type:'PUT',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(data){
                if(data.status=='success')
                {
                    swal('Success','Worker has been upated successfully!','success');
                    location.reload();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        });
    }
}



function addRowSurvey(id)
{
    $('#modalSurveyGroup').modal('show');
}


function closeSurveyGroup(id)
{
    $('#modalSurveyGroup').modal('hide');
}

function closeShowGeneral(id)
{
    $('#modalShowGeneral').modal('hide');
}

function closeResetPassword(user_id)
{
    $('#modalPasswordReset').modal('hide');
}


function addFAGroup(id)
{
    $('#modalAddFA').modal('show');
    datePicker();
}


function closeModalFA(id)
{
    $('#modalAddFA').modal('hide');
}

function saveFAGroup()
{
    $('#modalAddFA').modal('hide');   
}


// this function for checklist all in add new modal
function allSurveyGroup(resp)
{
    var count = '{{$count}}';
    if (resp.checked)
    {
        $('.rowSurveyGroup').prop('checked', resp.checked);
    }
    else
    {
        $('.rowSurveyGroup').prop('checked', resp.checked);
    }
}

function faGroupCheck(cek,id)
{
    if (cek.checked)
    { 
        $('#start_date_'+id+'').attr('disabled',false);
        $('#end_date_'+id+'').attr('disabled',false);
    }
    else
    {
        $('#start_date_'+id+'').attr('disabled',true);
        $('#end_date_'+id+'').attr('disabled',true);
    }
}

function editSurvey(id)
{ 
    var worker = $(".worker").attr("data-worker");
    if(worker == 'fa'){
        $.ajax({
            url:"workerLogin/editSurvey/"+id,
            data:'kosong',
            // dataType:'JSON',
            type:'GET',
            success:function(data){
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeSurveyGroup()">Cancel</button>'+
                            '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateListSurvey('+id+')">Update</button>';
                $('#modalSurveyGroup').find('.modal-body').html(data);
                $('#modalSurveyGroup').find('.modal-footer').html(footer);
                $('#modalSurveyGroup').find('.modal-title').html('Edit Survey');
                $('#modalSurveyGroup').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }else if(worker == 'wo'){
        $.ajax({
            url:"../editSurvey/"+id,
            data:'kosong',
            // dataType:'JSON',
            type:'GET',
            success:function(data){
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeSurveyGroup()">Cancel</button>'+
                            '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateListSurvey('+id+')">Update</button>';
                $('#modalSurveyGroup').find('.modal-body').html(data);
                $('#modalSurveyGroup').find('.modal-footer').html(footer);
                $('#modalSurveyGroup').find('.modal-title').html('Edit Survey');
                $('#modalSurveyGroup').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }
}

function updateListSurvey(id)
{   
    var dataSurvey = $('#formEditSurveyGroup').serialize();
    $.ajax({
        url:"workerLogin/updateListSurvey/"+id,
        data:dataSurvey,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.success)
            {
                $('#modalSurveyGroup').modal('hide');
            }
            else
            {
                swal("error!", 'Error', "error");
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
        }
    })
}
function editDispatch(id)
{ 
    var worker = $(".worker").attr("data-worker");
    if(worker == 'fa'){
        $.ajax({
            url:"workerLogin/editDispatch/"+id,
            data:'kosong',
            type:'GET',
            success:function(data){
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModalFA()">Cancel</button>'+
                            '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateListFA('+id+')">Update</button>';
                $('#modalAddFA').find('.modal-body').html(data);
                $('#modalAddFA').find('.modal-footer').html(footer);
                $('#modalAddFA').modal('show');
                $('#modalAddFA').find('.modal-title').html('Edit Dispatch Group');
                datePicker();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }else if(worker == 'wo'){
        $.ajax({
            url:"../editDispatch/"+id,
            data:'kosong',
            type:'GET',
            success:function(data){
                footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModalFA()">Cancel</button>'+
                            '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateListFA('+id+')">Update</button>';
                $('#modalAddFA').find('.modal-body').html(data);
                $('#modalAddFA').find('.modal-footer').html(footer);
                $('#modalAddFA').modal('show');
                $('#modalAddFA').find('.modal-title').html('Edit Dispatch Group');
                datePicker();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }
}

function faGroupCheckEdit(cek,id)
{
     if (cek.checked)
    { 
        $('#start_date_edit_'+id+'').attr('disabled',false);
        $('#end_date_edit_'+id+'').attr('disabled',false);
    }
    else
    {
        $('#start_date_edit_'+id+'').attr('disabled',true);
        $('#end_date_edit_'+id+'').attr('disabled',true);
    }
}

function updateListFA(id)
{   
    var dataSurvey = $('#formEditFAGroup').serialize();
    $.ajax({
        url:"workerLogin/updateListFA/"+id,
        data:dataSurvey,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.success)
            {
                $('#modalAddFA').modal('hide');
            }
            else
            {
                swal("error!", 'Error', "error");
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", 'Jangan Ada Data Yang Kosong', "error");
        }
    })
}



function viewDispatch(id)
{
    $.ajax({
        url:"workerLogin/viewDispatch/"+id,
        data:'kosong',
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            $('#modalShowGeneral').find('.modal-header').html('<button type="button" class="close" onclick="closeShowGeneral()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Dispatch Group</h4>'
            )

            $('#modalShowGeneral').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeShowGeneral()">Close</button>')
            $('#modalShowGeneral').find('.modal-body').html(data);
            $('#modalShowGeneral').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function viewSkill(id)
{
    $.ajax({
        url:"workerLogin/viewSkill/"+id,
        data:'kosong',
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            $('#modalShowGeneral').find('.modal-header').html('<button type="button" class="close" onclick="closeShowGeneral()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Skill </h4>'
            );
            $('#modalShowGeneral').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeShowGeneral()">Close</button>')
            $('#modalShowGeneral').find('.modal-body').html(data);
            $('#modalShowGeneral').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

function viewSkillWO(id)
{
    $.ajax({
        url:"../viewSkillWO/"+id,
        data:'kosong',
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            $('#modalShowGeneral').find('.modal-header').html('<button type="button" class="close" onclick="closeShowGeneral()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Skill WO </h4>'
            );
            $('#modalShowGeneral').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeShowGeneral()">Close</button>')
            $('#modalShowGeneral').find('.modal-body').html(data);
            $('#modalShowGeneral').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}


function viewSurvey(id)
{
    var worker = $(".worker").attr("data-worker");
    if(worker == 'wo'){
        $.ajax({
            url:"../viewSurvey/"+id,
            data:'kosong',
            type:'GET',
            success:function(data){
                $('#modalShowGeneral').find('.modal-header').html('<button type="button" class="close" onclick="closeShowGeneral()">'+
                '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Survey  </h4>'
            )
                $('#modalShowGeneral').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeShowGeneral()">Close</button>')
                $('#modalShowGeneral').find('.modal-body').html(data);
                $('#modalShowGeneral').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", thrownError, "error");
            }
        })
    }else if(worker == 'fa'){
        $.ajax({
            url:"workerLogin/viewSurvey/"+id,
            data:'kosong',
            type:'GET',
            success:function(data){
                $('#modalShowGeneral').find('.modal-header').html('<button type="button" class="close" onclick="closeShowGeneral()">'+
                '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Survey  </h4>'
            )
                $('#modalShowGeneral').find('.modal-footer').html('<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeShowGeneral()">Close</button>')
                $('#modalShowGeneral').find('.modal-body').html(data);
                $('#modalShowGeneral').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", thrownError, "error");
            }
        })
    }
}

function editPassword(user_id)
{
    var worker = $(".worker").attr("data-worker");
    if(worker == 'wo'){
        $.ajax({
            url:"../editPasword/"+user_id,
            data:user_id,
            type:'GET',
            success:function(data){
                $('#modalPasswordReset').find('.modal-header').html('<button type="button" class="close" onclick="closeResetPassword()">'+
                    '<span aria-hidden="true" style="color:#fff">&times;</span>'+
                    '</button>'+
                    '<h4 class="modal-title" style="text-align:center"> Reset Password  </h4>'
                )
                $('#modalPasswordReset').find('.modal-footer');
                $('#modalPasswordReset').find('.modal-body').html(data);
                $('#modalPasswordReset').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }else if (worker == 'fa'){
        $.ajax({
            url:"workerLogin/editPasword/"+user_id,
            data:user_id,
            type:'GET',
            success:function(data){
                $('#modalPasswordReset').find('.modal-header').html('<button type="button" class="close" onclick="closeResetPassword()">'+
                '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button>'+
            '<h4 class="modal-title" style="text-align:center"> Reset Password  </h4>'
            )
                $('#modalPasswordReset').find('.modal-footer');
                $('#modalPasswordReset').find('.modal-body').html(data);
                $('#modalPasswordReset').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", 'Jangan Ada Data Yang Kosong', "error");
            }
        })
    }
}

function savePassword(id)
{
    var dataPassword = $('#formResetPassword').serialize();
    var worker = $(".worker").attr("data-worker");
    if (worker == 'fa'){
        $.ajax({
            url:"workerLogin/updatePassword/"+id,
            data:dataPassword,
            dataType:'JSON',
            type:'post',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(response){
                if(response.status == "success"){
                    swal("Berhasil!", "Password Berhasil Di Ubah", "success");
                    location.reload();
                }
            },
            error:function(xhr,thrownError,ajaxOptions){
                swal("Error", "Password Gagal Di Update", "Error");
            }
        }); 
    }else if (worker == 'wo'){
        $.ajax({
            url:"../updatePassword/"+id,
            data:dataPassword,
            dataType:'JSON',
            type:'post',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success:function(response){
                if(response.status == "success"){
                    swal("Berhasil!", "Password Berhasil Di Ubah", "success");
                    location.reload();
                }
            },
            error:function(xhr,thrownError,ajaxOptions){
                swal("Error", "Password Gagal Di Update", "Error");
            }
        }); 
    }
}
</script>