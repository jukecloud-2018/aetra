<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form id="formWorkerEdit">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Username </label>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12" style="padding-bottom: 10px;">
                            <input type="hidden" name="user_id" value="{{$worker->user_id}}">
                            <input type="hidden" id="status" name="status" value="{{$worker->status}}">
                            <input type="text" class="form-control" name="username" value="{{$users->username}}" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Worker Name </label>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12" style="padding-bottom: 10px;">
                            <input type="text" class="form-control" value="{{$worker->worker_name}}" name="worker_name" placeholder="Worker Name">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control datepicker" name="worker_start_date" placeholder="Start Date" value="{{ \Carbon\Carbon::parse($worker_date->start_date)->format('d-m-Y') }}">                    
                        </div>
                        <div class="col-md-6">
                        <input type="text" class="form-control datepicker" name="worker_end_date" placeholder="End Date" value="{{\Carbon\Carbon::parse($worker_date->end_date)->format('d-m-Y')}}">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-3">
                        <label class="col-form-label" style="padding: 10px;"> KTP </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="no_ktp" value="{{$worker->no_ktp}}" placeholder="KTP" maxlength="25" pattern="^\d{10}$" onkeypress="isInputNumber(event)">                    
                    </div>
                </div>
            </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-3">
                            <label for="" class="col-form-label" style="padding: 10px;"> Worker ID </label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="worker_id" placeholder="Worker ID" value="{{$worker->worker_id}}" disabled="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-3">
                            <label class="col-form-label" style="padding: 10px;"> Email </label>
                        </div>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="email" value="{{$worker->email}}" placeholder="Email">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6" style="padding-bottom: 10px;">
                        <div class="col-md-3">
                            <label for="" class="col-form-label" style="padding: 10px;"> Phone </label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="no_hp" value="{{$worker->no_hp}}" placeholder="08xxxxxxxxxx" maxlength="15" pattern="^\d{10}$" onkeypress="isInputNumber(event)">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-3">
                            <label class="col-form-label" style="padding: 10px;"> Vendor </label>                    
                        </div>
                        <div class="col-md-9">
                            <select name="vendor" class="form-control">
                                <option value="">- Pilih Vendor -</option>
                                @foreach($vendors as $vendor)
                                <option value="{{$vendor->id}}" {{($vendor->id == $worker->vendor_id) ? "selected":""}}> {{$vendor->vendor_name}} </option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Survey </label>
                    </div>
                    <div class="col-md-9">
                        @if($worker->status == 'fa')
                            <a href="javascript:void(0)" class="worker" data-worker='fa' onclick="editSurvey('{{$worker->id}}')" style="margin-right: 10px;margin-left: 10px;">
                                <i class="fa fa-edit"></i>
                            </a>
                        @elseif($worker->status == 'wo')
                            <a href="javascript:void(0)" class="worker" data-worker='wo' onclick="editSurvey('{{$worker->id}}')" style="margin-right: 10px;margin-left: 10px;">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endif
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>

            @if($worker->status == 'fa')
                <div class="row">
                    <div class="col-md-6">
                        <center><label><input type="radio" name="fa" onclick="WorkerFA()" checked> WOrker FA</label></center>
                        <fieldset id="fa">
                            <input type="hidden" value="fa" name="status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                    <label class="col-form-label"> Skill FA </label>
                                    </div>
                                    <div class="col-md-9">
                                        <select name="skillFA" class="form-control">
                                            <option value="">- Pilih Skill FA -</option>
                                            @foreach($skill_fa as $skillfa)
                                            <option value="{{$skillfa->id}}" {{($skillfa->id == $worker->skill_fa_id) ? "selected":""}}> {{$skillfa->skill_fa_name}} </option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="col-form-label"> Dispatch Group </label>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="javascript:void(0)" id="faDispatch" class="worker" data-worker='fa' onclick="editDispatch('{{$worker->id}}')" style="margin-right: 10px;margin-left: 10px;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <center><label><input type="radio" name="fa" onclick="WorkerWO()"> WOrker WO</label></center>
                        <fieldset id="wo" disabled>
                            <input type="hidden" value="wo" name="status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="col-form-label"> Skill WO </label>
                                    </div>
                                    <div class="col-md-9">
                                        <select name="skillWO" class="form-control">
                                            <option value="">- Pilih Skill WO -</option>
                                            @foreach($skill_wo as $skillwo)
                                            <option value="{{$skillwo->id}}" {{($skillwo->id == $worker->skill_wo_id) ? "selected":""}}> {{$skillwo->skill_wo_name}} </option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            @elseif($worker->status == 'wo')
                <div class="row">
                    <div class="col-md-6">
                        <center><label><input type="radio" name="fa" onclick="WorkerFA()"> WOrker FA</label></center>
                        <fieldset id="fa" disabled>
                            <input type="hidden" value="fa" name="status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                    <label class="col-form-label"> Skill FA </label>
                                    </div>
                                    <div class="col-md-9">
                                        <select name="skillFA" class="form-control">
                                            <option value="">- Pilih Skill FA -</option>
                                            @foreach($skill_fa as $skillfa)
                                            <option value="{{$skillfa->id}}" {{($skillfa->id == $worker->skill_fa_id) ? "selected":""}}> {{$skillfa->skill_fa_name}} </option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="col-form-label"> Dispatch Group </label>
                                    </div>
                                    <div class="col-md-9">
                                        <a href="javascript:void(0)" id="faDispatch" class="worker" data-worker='wo' onclick="" style="margin-right: 10px;margin-left: 10px;">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <center><label><input type="radio" name="fa" onclick="WorkerWO()" checked> WOrker WO</label></center>
                        <fieldset id="wo">
                            <input type="hidden" value="wo" name="status">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="col-form-label"> Skill WO </label>
                                    </div>
                                    <div class="col-md-9">
                                        <select name="skillWO" class="form-control">
                                            <option value="">- Pilih Skill WO -</option>
                                            @foreach($skill_wo as $skillwo)
                                            <option value="{{$skillwo->id}}" {{($skillwo->id == $worker->skill_wo_id) ? "selected":""}}> {{$skillwo->skill_wo_name}} </option>
                                            @endforeach;
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            @endif
            <!-- <table id="tableWOGroup">
            <tr class="">
                <td width="85px"><label  style="margin-top:50px;" class="col-form-label"> WO Group </label></td>
                <td  width="230px">
                <div  style="margin-top:50px;">
                    <select name="faGroup" class="form-control">
                    <option>WO Group</option>
                    <option>WO Group</option>
                    </select>
                </div>
                </td>
                <td width="120px">
                <div style="margin-top:50px;margin-left: 10px;">
                    <input type="text" class="form-control" name="">
                </div>
                </td>
                <td  width="120px">
                <div style="margin-top:50px;margin-left: 10px;">
                    <input type="text" class="form-control" name="">
                </div>
                </td>
                <td>
                <div style="margin-top:50px;">
                    <a href="javascript:void(0)" onclick="addWOGroup()" style="margin-right: 10px;margin-left: 10px;"> <i class="fa fa-plus"></i> </a>  
                </div>
                </td>
            </tr>
            </table> -->
        </div>
    </div>
</form><!-- /form -->

<script>
    function WorkerFA() {
        document.getElementById("wo").disabled = true;
        document.getElementById("fa").disabled = false;
        document.getElementById("faDispatch").setAttribute("onclick", "editDispatch('{{$worker->id}}')");
    }

    function WorkerWO() {
        document.getElementById("wo").disabled = false;
        document.getElementById("fa").disabled = true;
        document.getElementById("faDispatch").removeAttribute("onclick");
    }
</script>