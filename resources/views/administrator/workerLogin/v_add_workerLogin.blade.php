<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form autocomplete="off" id="formWorker">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Username </label>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12" style="padding-bottom: 10px;">
                            <input type="text" class="form-control" name="username" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Worker Name </label>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-12" style="padding-bottom: 10px;">
                            <input type="text" class="form-control" name="worker_name" placeholder="Worker Name">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control datepicker" name="worker_start_date" value="<?php echo date('d-m-Y'); ?>" placeholder="Start Date">                    
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control datepicker" name="worker_end_date" placeholder="End Date">                    
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-3">
                        <label class="col-form-label" style="padding: 10px;"> KTP </label>                    
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="no_ktp" placeholder="KTP" maxlength="25" pattern="^\d{10}$" onkeypress="isInputNumber(event)">                    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding: 10px;"> Worker ID </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="worker_id" placeholder="Worker ID">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-3">
                        <label class="col-form-label" style="padding: 10px;"> Email </label>
                    </div>
                    <div class="col-md-9">
                        <input type="email" class="form-control" name="email" placeholder="Email">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding: 10px;"> Phone </label>
                    </div>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="no_hp" placeholder="08xxxxxxxxxx" maxlength="15" pattern="^\d{10}$" onkeypress="isInputNumber(event)">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-3">
                        <label class="col-form-label" style="padding: 10px;"> Vendor </label>
                    </div>
                    <div class="col-md-9">
                        <select name="vendor" class="form-control">
                            <option value="">- Pilih Vendor -</option>
                            @foreach($vendors as $vendor)
                            <option value="{{$vendor->id}}"> {{$vendor->vendor_name}} </option>
                            @endforeach;
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6" style="padding-bottom: 10px;">
                    <div class="col-md-3">
                        <label for="" class="col-form-label" style="padding-left: 10px;"> Survey </label>
                    </div>
                    <div class="col-md-9">
                        <a href="javascript:void(0)" onclick="addRowSurvey()" style="margin-right: 10px;margin-left: 10px;"> 
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <center><label><input type="radio" name="fa" onclick="WorkerFA()"> WOrker FA</label></center>
                    <fieldset id="fa" disabled>
                        <input type="hidden" value="fa" name="status">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                <label class="col-form-label"> Skill FA </label>
                                </div>
                                <div class="col-md-9">
                                    <select name="skillFA" class="form-control">
                                        <option value="">- Pilih Skill FA -</option>
                                        @foreach($skill_fa as $skillfa)
                                        <option value="{{$skillfa->id}}"> {{$skillfa->skill_fa_name}} </option>
                                        @endforeach;
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="col-form-label"> Dispatch Group </label>
                                </div>
                                <div class="col-md-9">
                                    <a href="#" id="faDispatch" disabled>
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <center><label><input type="radio" name="fa" onclick="WorkerWO()"> WOrker WO</label></center>
                    <fieldset id="wo" disabled>
                        <input type="hidden" value="wo" name="status">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <label class="col-form-label"> Skill WO </label>
                                </div>
                                <div class="col-md-9">
                                    <select name="skillWO" class="form-control">
                                        <option>- Pilih Skill WO -</option>
                                        @foreach($skill_wo as $skillWo)
                                        <option value="{{$skillWo->id}}">{{$skillWo->skill_wo_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <!-- <table id="tableWOGroup">
                <tr class="">
                    <td width="85px"><label  style="margin-top:50px;" class="col-form-label"> WO Group </label></td>
                    <td  width="230px">
                        <div  style="margin-top:50px;">
                            <select name="faGroup" class="form-control">
                                <option>WO Group</option>
                                <option>WO Group</option>
                            </select>
                        </div>
                    </td>
                    <td width="120px">
                        <div style="margin-top:50px;margin-left: 10px;">
                        <input type="text" class="form-control" name="">
                        </div>
                    </td>
                    <td  width="120px">
                        <div style="margin-top:50px;margin-left: 10px;">
                            <input type="text" class="form-control" name="">
                        </div>
                    </td>
                    <td>
                        <div style="margin-top:50px;">
                        <a href="javascript:void(0)" onclick="addWOGroup()" style="margin-right: 10px;margin-left: 10px;"> <i class="fa fa-plus"></i> </a>  
                        </div>
                    </td>
                </tr>
            </table> -->
        </div>
    </div>
</form><!-- /form -->


<script>
    function WorkerFA() {
        document.getElementById("wo").disabled = true;
        document.getElementById("fa").disabled = false;
        document.getElementById("faDispatch").setAttribute("onclick", "addFAGroup()");
    }

    function WorkerWO() {
        document.getElementById("wo").disabled = false;
        document.getElementById("fa").disabled = true;
        document.getElementById("faDispatch").removeAttribute("onclick");
    }
</script>