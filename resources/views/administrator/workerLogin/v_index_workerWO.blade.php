@extends('layouts.app')

@section('head')
    Administrator | Worker Login
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Worker Login WO</span>
@endsection
@section('button')
    <div class="forButtonTop">
          <button href="" class="btn btn-default buttonAE worker" data-worker='wo' onclick="addWorker()">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </button>
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title" style="color:#2B6B97"><a href="{{ URL::to('administrator/workerLogin')}}">Worker Login FA</a> | Worker Login WO</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
        </div>
      </div>
    <!-- /Box Header -->
    
    <!--- Box Body --->
      <div class="box-body">
        <div class="row">
          <div class="col-md-12">
          <div class="table-responsive">
            <table id="tableWorker" class="table table-bordered table-striped">
              <thead>
                <tr style="background-color:#8B9AAD;color:#fff">
                  <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">Username</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">Worker Name</th>                 
                  <th style="vertical-align: middle!important; text-align: center!important; width: 70px!important;">Phone</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 60px!important;">KTP</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">Skill WO</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 19px!important;">Survey</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 150px!important;">Vendor</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 50px!important;">Start Date</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 50px!important;">End Date</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 50px!important;">Worker Status</th>
                  <th style="vertical-align: middle!important; text-align: center!important; width: 130px!important;"></th>
                </tr>
              </thead>
              <tbody>
                @foreach($worker as $key=>$value)
                <tr role="row">
                  <td>{{$value->username}}</td>
                  <td>{{$value->worker_name}}</td>
                  <td>{{$value->no_hp}}</td>
                  <td>{{$value->no_ktp}}</td>
                  <td>
                    <button class="btn btn-primary" onclick="viewSkillWO('{{$value->skill_wo_id}}')"><i class="fa fa-search"></i></button>
                  </td>
                  <td>
                    <button class="btn btn-primary worker" data-worker='wo' onclick="viewSurvey('{{$value->id}}')"><i class="fa fa-search"></i></button>
                  </td>
                  <td>{{$value->vendor_name}}</td>
                  <td>{{$value->start_date}}</td>
                  <td>{{$value->end_date}}</td>
                  <td>{{$value->worker_status}}</td>
                  <td>
                    <button class="btn btn-primary worker" data-worker='wo' onclick="editPassword('{{$value->user_id}}')">Reset</button> | 
                    <button class="btn btn-success worker" data-worker='wo' onclick="editWorker('{{$value->id}}')"><i class="fa fa-pencil"></i></button> | 
                    <button class="btn btn-danger worker" data-worker='wo' onclick="deleteWorker('{{$value->id}}')"><i class="fa fa-trash"></i></button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal Add -->
<div class="modal fade" id="workerModal" tabindex="-1" role="dialog" aria-labelledby="workerModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 78%">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Add New Worker Login</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
       
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveWorker()">Save</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="editworkerModal" tabindex="-1" role="dialog" aria-labelledby="workerModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 78%">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Edit Worker Login</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
       
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveWorker()">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- end Modal -->

<!-- Modal FA Group  -->
<div class="modal fade" id="modalAddFA" tabindex="-1" role="dialog" aria-labelledby="modalAddFA" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
        <div class="modal-content" style="height: 85%;">
            <div class="modal-header">
                <button type="button" class="close" onclick="closeModalFA()">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center"> Dispatch Group </h4>
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;">
                <form id="formFAGroup">
                    <table class="table table-striped table-bordered" id="listFAGroup"> 
                        <thead>
                            <tr>
                            <th>Dispatch Group </th>
                            <th> Start Date </th>
                            <th> End Date </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($faGroup as $in => $fa)
                            <tr>
                            <td width="40%"><input type="checkbox"  name="dispatch_group[]" value="{{$fa->dispatch_group}}" id="rowFAGroup_{{$in}}" onchange="faGroupCheck(this,'{{$in}}')" > {{$fa->dispatch_group}}</td>
                            <td  width="30%"><input type="text" name="start_date[]"  class="form-control  datepicker" id="start_date_{{$in}}" disabled></td>
                            <td  width="30%"><input type="text" name="end_date[]" class="form-control  datepicker" id="end_date_{{$in}}" disabled></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>  
                </form>
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModalFA()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveFAGroup()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal fa group-->

<!-- Modal  Survey Group  -->
<div class="modal fade" id="modalSurveyGroup" tabindex="-1" role="dialog" aria-labelledby="modalSurveyGroup" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
    <div class="modal-content" style="height: auto;">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeSurveyGroup()">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center"> Survey Group </h4>
      </div>
      <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;">
        <form id="formSurveyGroup">
        <table class="table table-striped table-bordered" id="listFAGroup"> 
          <thead>
            <tr>
              <th><input type="checkbox" name="allCheck" onchange="allSurveyGroup(this)" value="0"> All </th>
              <th>Survey Group </th>
            </tr>
          </thead>
          <tbody>
           @foreach($surveyGroup as $index => $su)
            <tr>
              <td><input type="checkbox"  name="code_ab[]" class="rowSurveyGroup" value="{{$su->code_ab}}" id="rowSurveyGroup_{{$index}}" ></td>
              <td>{{$su->code_ab.' -  '. $su->description }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>  
        </form>
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeSurveyGroup()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="closeSurveyGroup()">Save</button>
      </div>
    </div>
  </div>
</div>
<!-- end Modal Survey group-->

<!-- Modal  Reset Password  -->
<div class="modal fade" id="modalPasswordReset" tabindex="-1" role="dialog" aria-labelledby="modalPasswordReset" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: 85%;">
    <div class="modal-content" style="height: auto">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeResetPassword()">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;">
        
      </div><!-- / Modal Body -->
    </div>
  </div>
</div>
<!-- end Modal Password-->

<!-- Modal Show General -->
<div class="modal fade" id="modalShowGeneral" tabindex="-1" role="dialog" aria-labelledby="modalShowGeneral" aria-hidden="false" >
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 30%;height: auto;">
        <div class="modal-content" style="height: auto;">
            <div class="modal-header">
            </div>
            <div class="modal-body" style="background-color: #FBFBFB;max-height: calc(100% -  120px);overflow-y: auto;">
  
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
            </div>
        </div>
    </div>
</div>
<!-- End Modal Show General -->

@endsection

@section('js')
  @include('administrator.workerLogin.js_workerLogin')
@endsection