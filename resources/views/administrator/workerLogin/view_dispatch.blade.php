 @php 
 	use Carbon\Carbon;
 @endphp
 <form id="formEditFAGroup">
<table class="table table-striped table-bordered" id="listFAGroup"> 
  <thead>
    <tr>
      <th> Dispatch Group </th>
    </tr>
  </thead>
  <tbody>
   @foreach($faGroup as $in => $fa)
    <tr>
      <td width="40%"> {{$fa->dispatch_group}}</td>
   
    </tr>
    @endforeach
  </tbody>
</table>  
</form>