<form id="formEditFAGroup">
    <table class="table table-striped table-bordered" id="listFAGroup"> 
        <thead>
            <tr>
                <th> Survey </th>
            </tr>
        </thead>
        <tbody>
            @php
            if($count > 0){
                foreach($survey as $in => $su){
                    echo '<tr><td width="40%">'.$su->code_ab.' - '.$su->description.'</td></tr>';
                }
            }else{
                echo '<tr><td width="40%">No Data Survey</td></tr>';
            }
            @endphp
        </tbody>
    </table>  
</form>