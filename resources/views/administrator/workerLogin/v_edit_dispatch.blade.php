 @php 
 	use Carbon\Carbon;
 @endphp
 <form id="formEditFAGroup">
    <table class="table table-striped table-bordered" id="listFAGroup"> 
        <thead>
            <tr>
            <th>Dispatch Group </th>
            </tr>
        </thead>
        <tbody>
            @foreach($faGroup as $in => $fa)
                @php  
                    $data = DB::table('worker_dispatch')->where(['worker_id'=>$worker_id,'dispatch_group'=>$fa->dispatch_group])->first(); 
                    $getDis = json_decode(json_encode($data), True);
                @endphp
                <tr>
                    <td width="40%">
                        <input type="checkbox" 
                            @php 
                                $ck='';$dis = 'disabled'; $start='';$end = '';
                                if(!empty($getDis)){
                                    $ck = 'checked';
                                    $dis = '';
                                    $start = convertDate($getDis['start_date']);
                                    $end = convertDate($getDis['end_date']);
                                }
                            @endphp
                        name="dispatch_group[]" value="{{$fa->dispatch_group}}" {{$ck}} id="rowFAGroup_{{$in}}" onchange="faGroupCheckEdit(this,'{{$in}}')" > {{$fa->dispatch_group}}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</form>