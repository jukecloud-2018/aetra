<script type="text/javascript">

function saveSystemParameter()
{
    var data = $('#formSystemParameter').serialize();
    $.ajax({
        url:"{{route('administrator.systemParameter.store')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        headers: {
          'X-CSRF-TOKEN' : '{{csrf_token()}}',
        },
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
            }
            else
            {
                swal('Success','Users has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}


$('#timepicker2').timepicker({
    minuteStep: 1,
    template: 'modal',
    appendWidgetTo: 'body',
    showSeconds: true,
    showMeridian: false,
    defaultTime: false
});
</script>