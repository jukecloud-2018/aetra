@extends('layouts.app')

@section('head')
    Administrator | System Parameter
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> System Parameter </span>
@endsection
@section('button')
    <div class="forButtonTop">
          <!-- <a href="" class="btn btn-default buttonAE btnTop" data-toggle="modal" data-target="#groupModal">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </a> -->
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')

@php
// echo '<pre>';print_r($system_parameter->range_periode);exit()
@endphp
<section class="col-lg-3 connectedSortable">
</section>
<section class="col-lg-6 connectedSortable">  
  <!-- System Parameter -->
  <div class="box box-default">
    <div class="box-header with-border">
      <h3 class="box-title"></h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <form class="form-horizontal" id="formSystemParameter">
          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Range Periode </label>
            <div class="col-sm-3">
                <input type="text" name="range_periode" class="form-control number_valid" value="{{ (empty($system_parameter->range_periode)) ? "":$system_parameter->range_periode }}">
            </div>
            <label class="label-col-label col-sm-3"> Days </label>
          </div>

          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Assign Task </label>
            <div class="col-sm-3">
                <input type="text" name="assign_task" class="form-control number_valid" value="{{ (empty($system_parameter->assign_task))  ? "":$system_parameter->assign_task }}">
            </div>
            <label class="label-col-label col-sm-3"> Task / Days </label>
          </div>

          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Pickup Task </label>
            <div class="col-sm-2">
                <input type="radio" name="pickup_task" value="complete" {{ (!empty($system_parameter->pickup_task) && $system_parameter->pickup_task == 'complete' )  ? "checked":""}}>
                Complete 
            </div>
            <div class="col-sm-3">
                <input type="radio" name="pickup_task" value="incomplete" {{ (!empty($system_parameter->pickup_task) && $system_parameter->pickup_task == 'incomplete' )  ? "checked":""}} >
                Incomplete
            </div>
          </div>


          <!-- <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Lock FA </label>
            <div class="col-sm-3">
                <select class="form-control" name="lock_fa">
                  <option value="unclock" {{ (!empty($system_parameter->lock_fa) && $system_parameter->lock_fa == 'unclock' )  ? "selected":""}}>Unlock</option>
                  <option value="assigned"  {{ (!empty($system_parameter->lock_fa) && $system_parameter->lock_fa == 'assigned' )  ? "selected":""}} >Assigned</option>
                  <option value="on_the_way"  {{ (!empty($system_parameter->lock_fa) && $system_parameter->lock_fa == 'on_the_way' )  ? "selected":""}} >On The Way</option>
                  <option value="started"  {{ (!empty($system_parameter->lock_fa) && $system_parameter->lock_fa == 'started' )  ? "selected":""}}>Started</option>
                </select>
            </div>
          </div> -->


          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Log Off Web </label>
            <div class="col-sm-3">
                <input type="text"  class="form-control number_valid" name="log_off_web" value="{{(empty($system_parameter->log_off_web))  ? "":$system_parameter->log_off_web}}">
            </div>
            <label class="label-col-label col-sm-3"> Minutes </label>
          </div>

          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Log Off Mobile </label>
            <div class="col-sm-3">
                <input type="text" class="form-control number_valid" name="log_off_mobile" value="{{(empty($system_parameter->log_off_mobile))  ? "":$system_parameter->log_off_mobile}}">
            </div>
            <label class="label-col-label col-sm-3"> Minutes </label>
          </div>


          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Auto Refresh Data WMS </label>
            <div class="col-sm-3">
                <input type="text" class="form-control number_valid" name="auto_refresh" value="{{(empty($system_parameter->auto_refresh))  ? "":$system_parameter->auto_refresh}}">
            </div>
            <label class="label-col-label col-sm-3"> Second </label>
          </div>

          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> Close Work </label>
              <div class="col-sm-3 bootstrap-timepicker timepicker">
                  <input id="timepicker2" type="text" class="form-control input-small" placeholder="15:00:00" name="close_time" value="{{(empty($system_parameter->close_time))  ? "":$system_parameter->close_time}}">
              </div>

              <div class="col-sm-2">
                <input type="radio" name="close_time_status" value="Active" {{ (!empty($system_parameter->close_time_status) && $system_parameter->close_time_status == 'Active' )  ? "checked":""}}>
                Active 
              </div>
              <div class="col-sm-3">
                  <input type="radio" name="close_time_status" value="Non-Active" {{ (!empty($system_parameter->close_time_status) && $system_parameter->close_time_status == 'Non-Active' )  ? "checked":""}} >
                  Non-Active
              </div>
          </div>

          <div class="form-group">
            <label class="col-sm-5" style="text-align: right;"> App Version </label>
            <div class="col-sm-3">
                <input type="text" class="form-control number_valid" name="v_code" placeholder="Version Code" value="{{(empty($system_parameter->v_code))  ? "":$system_parameter->v_code}}">
            </div>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="v_name" placeholder="Version Name" value="{{(empty($system_parameter->v_name))  ? "":$system_parameter->v_name}}">
            </div>
          </div>


        </form>  
      </div>
    </div>
    <!-- /.box-body -->
    
    <!-- /.box-footer -->
     <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveSystemParameter()">Save</button>
      </div>
  </div>

</section>
<section class="col-lg-3 connectedSortable">
</section>
<!-- /section -->

@endsection

@section('js')
  @include('administrator.systemParameter.js_systemParameter')
@endsection