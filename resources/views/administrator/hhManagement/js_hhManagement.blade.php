<script type="text/javascript">

  //for datatable 
  $('#tableHHManagement').dataTable({
      processing: true,
      serverSide: true,
      order: [0, 'desc'],
      ajax: {
          method: 'POST',
          url : '<?=route('administrator.hhManagement.getData');?>',
          headers: {
              'X-CSRF-TOKEN': '{{ csrf_token() }}'
          }
      },
      columns : [
          {
              // this for numbering table
              render: function (data, type, row, meta) {
                  return meta.row + meta.settings._iDisplayStart + 1;
              }
          },
          { data: "no_imei" },
          { data : "username" },
          { data : "mode"},
          {
              mRender: function (data, type, row, meta) {
                  return '<button class="btn btn-success" onclick="editImei(' + row.id + ')"><i class="fa fa-pencil"></i></button> | <button class="btn btn-danger" onclick="deleteImei(' + row.id + ')"><i class="fa fa-trash"></i></button>';
                  
              }
      },
      ],
      responsive: true,
  });

  function modulAddHH()
  {
    $('[name="no_imei"]').val('');
    $('[name="username"]').val('');
    $('[name="imei_id"]').val('');
    $('#addHHModal').find('.modal-title').text('Add New IMEI');
    $('#addHHModal').modal('show');
  }

  function saveHH()
{
    var data = $('#formHH').serialize();
    var id = $('[name="imei_id"]').val();
    var url;
    var sendType;
    if(id == '')
    {
        url = "{{route('administrator.hhManagement.store')}}";
        sendType = 'POST';
    }
    else
    {
        url = 'hhManagement/update/'+id;
        sendType = 'PUT';
    }
    

    //ajax for save
    $.ajax({
        url:url,
        data:data,
        dataType:'JSON',
        type:sendType,
        success:function(data){
            if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                })

                setTimeout(function() {
                    $('#error-alert').fadeOut('fast');
                }, 2000); 
            }
            else if(data.status=='success')
            {
                swal('Success','IMEI has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {

               get = $.parseJSON(xhr.responseText);
               swal("error!", "IMEI Sudah Ada, Silahkan Masukkan Vendor Dengan Nama Lain", "error");
          },
    })
}

  

function editImei(id)
  {
      $.ajax({
          url:'hhManagement/'+id+'/edit',
          data:id,
          dataType:'JSON',
          type:'GET',
          success:function(get){
              $('.modal-title').text('Update Master Vendor');
              $('[name="no_imei"]').val(get.data.no_imei);
              $('[name="username"]').val(get.data.username)
              $('[name="imei_id"]').val(get.data.id);
              $('#addHHModal').modal('show');
          },
            error: function (xhr, ajaxOptions, thrownError) {
                  swal("error!", thrownError, "error");
            },
      })
  }

  function deleteImei(id)
  {
      console.log('Delete processing');
      
      swal({
        title: "Apakah Data Ingin Di Hapus?",
        text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      }, function(isConfirm){
          if (!isConfirm) return;
          $.ajax({
              url:'hhManagement/destroy/'+id,
              data:id,
              dataType:'JSON',
              type:'GET',
              success:function(){
                  swal("Done!", "IMEI has been deleted!", "success");
                  location.reload();
              },
                error: function (xhr, ajaxOptions, thrownError) {
                      swal("error!", "IMEI failed to delete", "error");
                },
          });
      });
  }
</script>