@extends('layouts.app')

@section('head')
    Administrator | HandHeld Management
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> HandHeld Management </span>
@endsection
@section('button')
    <div class="forButtonTop">
          <button href="" class="btn btn-primary white btnTop" style="margin-left:10px;" onclick="modulAddHH()">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </button>

          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
    <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableHHManagement" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th style="width: 15px;">No</th>
                                    <th>No IMEI</th>
                                    <th>Username</th>
                                    <th>Mode</th>
                                    <th style="width: 65px;"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="addHHModal" tabindex="-1" role="dialog" aria-labelledby="imeiModalCenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Add New IMEI HH</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>

                <form id="formHH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="imei_id" >
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">No IMEI</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_imei" class="form-control" id="" placeholder="Nomer IMEI">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" id="" placeholder="Username">
                        </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveHH()">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal   Edit -->
<div class="modal fade" id="editHHModal" tabindex="-1" role="dialog" aria-labelledby="imeiModalCenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Add New IMEI HH</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formeditHH">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="imei_id" >
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">No IMEI</label>
                        <div class="col-sm-9">
                            <input type="text" name="no_imei" class="form-control" id="" placeholder="Nomer IMEI">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" id="" placeholder="Username">
                        </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveEdit()">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- end Modal -->
@endsection

@section('js')
  @include('administrator.hhManagement.js_hhManagement')
@endsection
