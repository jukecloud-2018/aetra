 <form id="formSurveyEdit">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
     <div class="row">
      <div class="col-md-12">
        <table>
           <tr class="">
              <td width="85px"><label for="" class="col-form-label"> Survey Type </label></td>
              <td  width="300px">
                <div style="margin-left: 10px;">
                  <input type="text" class="form-control" name="survey_name" placeholder="Survey Type" value="{{$survey->survey_name}}">
                  <input type="hidden" name="survey_id" value="{{$survey->id}}">
                </div>
              </td>
              <tr>
                <td width="85px"><label for="" class="col-form-label"> Object Survey </label></td>
                  <td>
                    <div style="margin-left: 10px;">
                      <input type="radio"  name="object_survey" value="Asset" {{($survey->object_survey == 'Asset') ? 'checked': '' }}> Asset
                    </div>

                    <div style="margin-left: 10px;">
                      <input type="radio"  name="object_survey" value="Nomen" {{($survey->object_survey == 'Nomen') ? 'checked': '' }}> Nomen
                    </div>

                    <div style="margin-left: 10px;">
                      <input type="radio"  name="object_survey" value="Area" {{($survey->object_survey == 'Area') ? 'checked': '' }}> Area
                    </div>
                  </td>
              </tr>
              <td>
                  <div style="margin-left: 10px;">
                    <input type="checkbox"  name="status_survey" value="{{$survey->status}}"  {{($survey->status=='Active') ? 'checked': '' }}  > Active
                  </div>
              </td>
            </tr>
          </table> 
          <table>
            <td>
              <label style="margin-top:20px;" class="col-form-label"> Detail Survey : </label>
            </td>
          </table>
        <table id="tableDetailSurveyEdit">
          <?php $countRow = count($detail[0]->survey_detail); ?>
          <input type="hidden" name="countRow" value="{{$countRow}}">
          @foreach($detail[0]->survey_detail  as $key=>  $detail_s)
          <tr class="rowSurvey_0">
             <td  width="300px">
                <div  style="margin-top:20px;margin-left: 10px;">
                   <input type="text" class="form-control" name="survey_detail_name[]" placeholder="Detail Survey" value="{{$detail_s->survey_detail_name}}">
                </div>
            </td>
            <td width="70px" colspan="2">
                <div  style="margin-top:20px;margin-left: 10px;">
                  <input type="checkbox"   name="status_detail_survey[]" value="{{$detail_s->status}}" {{($detail_s->status=='Active') ? 'checked': '' }} > Active
                </div>
            </td>
            <td  width="200px">
              <div style="margin-top:20px;margin-left: 10px;">
               <select name="type_data_id[]" class="form-control">
                  <option value=""> - Choose Type Data - </option>
                  @foreach($type_data as $td)
                    <option value="{{$td->id}}" {{($td->id == $detail_s->type_data_id) ? 'selected':''}}>{{$td->type_data}}</option>
                  @endforeach
                </select>
              </div>
            </td>
            @if($page == 'edit')
            <td>
              <div style="margin-top:20px;">
                @if($key == 0)
                <a href="javascript:void(0)" onclick="addRowSurveyEdit()" style="margin-right: 10px;margin-left: 10px;"> <i class="fa fa-plus"></i> </a>   Add
                @else
                <a href="javascript:void(0)" onclick="deleteRowSurveyEdit('<?= $detail_s->id ?>')" style="margin-right: 10px;margin-left: 10px;color:red"> <i class="fa fa-minus"></i> </a>   
                @endif
              </div>
            </td>
            @endif
          </tr>
          @endforeach

        </table>

      </div>
    </div>
</form><!-- /form -->