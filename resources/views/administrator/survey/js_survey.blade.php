<script type="text/javascript">

//for datatable 
$('#tableSurvey').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '<?=route('administrator.survey.getData');?>',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { "data": "survey_name" },
        { "data": "object_survey" },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-primary" onclick="detailSurvey(`' + row.id + '`)"><i class="fa fa-search"></i></button>';
            }
        },
        { "data": "status" },
        {
            "mRender": function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="editSurvey(`' + row.id + '`)"><i class="fa fa-pencil"></i></button> |  <button class="btn btn-danger" onclick="deleteSurvey(`' + row.id + '`)"><i class="fa fa-trash"></i></button>';
            }

		    }
    ],
    responsive: true,
});




// for add row table 
function addRowSurvey()
{
	var table = $('#tableDetailSurvey');
  var select;
  var row;
  row = '<tr class="rowSurvey_0">'+
                      '<td  width="300px">'+
                       '<div  style="margin-top:20px;margin-left: 10px;">'+
                           '<input type="text" class="form-control" name="survey_detail_name[]" placeholder="Detail Survey">'+
                        '</div>'+
                      '</td>'+
                      '<td width="70px" colspan="2">'+
                          '<div  style="margin-top:20px;margin-left: 10px;">'+
                            '<input type="checkbox"  name="status_detail_survey[]"> Active'+
                          '</div>'+
                      '</td>'+
                      '<td  width="200px">'+
                        '<div style="margin-top:20px;margin-left: 10px;">'+
                          '<select name="type_data_id[]" class="form-control typeData">'+
                          $.ajax({
                            url:"survey/getTypeData/all",
                            type:'GET',
                            dataType:'JSON',
                            success:function(data){
                              var tp = $('.typeData'); tp.empty();
                              tp.append('<option value=""> - Choose Type Data - </option>')
                                data.data.forEach(function(e){
                                    tp.append('<option value='+e.id+'>'+e.type_data+'</option>')
                                })
                                
                            }
                          })
                   row+=  '</select>'+
                        '</div>'+
                      '</td>'+
                      '<td>'+
                        '<div style="margin-top:20px;">'+
                          '<a href="javascript:void(0)" onclick="deleteRowSurvey()" style="margin-right: 10px;margin-left: 10px;color:red;"> <i class="fa fa-minus"></i> </a>'+  
                        '</div>'+
                      '</td>'+
                    '</tr>'
	table.append(row);
	styleChek();
}




// for add row table 
function addRowSurveyEdit()
{
  var table = $('#tableDetailSurveyEdit');
  var select;
  var row;
  row = '<tr class="rowSurvey_0">'+
                      '<td  width="300px">'+
                       '<div  style="margin-top:20px;margin-left: 10px;">'+
                           '<input type="text" class="form-control" name="survey_detail_name[]" placeholder="Detail Survey">'+
                        '</div>'+
                      '</td>'+
                      '<td width="70px" colspan="2">'+
                          '<div  style="margin-top:20px;margin-left: 10px;">'+
                            '<input type="checkbox"  name="status_detail_survey[]"> Active'+
                          '</div>'+
                      '</td>'+
                      '<td  width="200px">'+
                        '<div style="margin-top:20px;margin-left: 10px;">'+
                          '<select name="type_data_id[]" class="form-control typeData">'+
                          $.ajax({
                            url:"survey/getTypeData/all",
                            type:'GET',
                            dataType:'JSON',
                            success:function(data){
                              var tp = $('.typeData'); tp.empty();
                              tp.append('<option value=""> - Choose Type Data - </option>')
                                data.data.forEach(function(e){
                                    tp.append('<option value='+e.id+'>'+e.type_data+'</option>')
                                })
                                
                            }
                          })
                   row+=  '</select>'+
                        '</div>'+
                      '</td>'+
                      '<td>'+
                        '<div style="margin-top:20px;">'+
                          '<a href="javascript:void(0)" onclick="deleteRowEdit()" style="margin-right: 10px;margin-left: 10px;color:red;"> <i class="fa fa-minus"></i> </a>'+  
                        '</div>'+
                      '</td>'+
                    '</tr>'
  table.append(row);
  styleChek();
}

//// for delete row table wo
function deleteRowSurvey()
{
  document.getElementById("tableDetailSurvey").deleteRow(1);
}


//// for delete row table wo
function deleteRowEdit()
{
  var row = $('[name="countRow"]').val();

	document.getElementById("tableDetailSurveyEdit").deleteRow(row);
}


// for delete survey
function deleteRowSurveyEdit(id)
{
   swal({
        title: "Apakah Data Ingin Di Hapus?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      }, function(isConfirm){
          if (!isConfirm) return;
          $.ajax({
              url:'survey/deleteDetail/'+id,
              data:id,
              dataType:'JSON',
              type:'GET',
              success:function(){
                  swal("Done!", "Detail has been deleted!", "success");
                  location.reload();
              },
                error: function (xhr, ajaxOptions, thrownError) {
                      swal("error!", "Detail failed to delete", "error");
                },
          });
      });
}

// for add modal form
function addSurvey()
{
  $('#surveyModal').modal('show');
}

// for save survey
function saveSurvey()
{
    var data = $('#formSurvey').serialize();

    $.ajax({
        url:"{{route('administrator.survey.store')}}",
        data:data,
        dataType:'JSON',
        type:'POST',
        success:function(data){
            if(data.errors)
            {
                $('.alert-danger').show();
                $('.alert-danger').empty();
                $.each(data.errors,function(key,value){
                    $('.alert-danger').append('<li>'+value+'</li>');
                })
                hidden_error();
            }
            else
            {
                swal('Success','Survey has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })
}

// for change status with checkbox
function changeStatus(id,status)
{   
      if($('[name="status_check"]').is(':checked'))
      {
            $.ajax({
                url:'survey/updateStatus/'+id+'',
                data:{'status':status},
                dataType:'JSON',
                type:'GET',
                success:function(data){
                    if(data.status =="success")                    
                    {
                        swal('Success','Change Status successfully!','success');
                        location.reload();
                    }
                },
                  error: function (xhr, ajaxOptions, thrownError) {
                        swal("error!", thrownError, "error");
                }
            });
      }
}

// for get detail survey by id
function detailSurvey(id)
{
    var body;
    $.ajax({
        url:'survey/detailSurvey/'+id+'',
        data:{'page':'detail'},
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            body = data;
             $('#surveyDetailModal').find('.modal-body').html(body);
             $('#surveyDetailModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}


// for edit survey
function editSurvey(id)
{
    var body;
    var footer;
    $.ajax({
        url:'survey/detailSurvey/'+id+'',
        data:{'page':'edit'},
        // dataType:'JSON',
        type:'GET',
        success:function(data){
            body = data;

            footer = '<button type="button" class="btn btn-secondary btnFooterModal"'+
            'data-dismiss="modal"onclick="window.location.reload()">Cancel</button>'+
            '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateSurvey()">Update</button>';

             $('#surveyDetailModal').find('.modal-title').html('Survey Type');
             $('#surveyDetailModal').find('.modal-body').html(body);
             $('#surveyDetailModal').find('.modal-footer').html(footer);
             $('#surveyDetailModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

// for update survey
function updateSurvey()
{
    var dataUser = $('#formSurveyEdit').serialize();
    $.ajax({
        url:'survey/updateSurvey',
        data:dataUser,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Survey has been upated successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    });
}

//for delete Survey
function deleteSurvey(id)
{

  console.log('Delete processing');
      
  swal({
        title: "Apakah Data Ingin Di Hapus?",
        text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus",
        closeOnConfirm: false
      }, function(isConfirm){
          if (!isConfirm) return;
          $.ajax({
              url:'survey/destroy/'+id,
              data:id,
              dataType:'JSON',
              type:'GET',
              success:function(){
                  swal("Done!", "Survey has been deleted!", "success");
                  location.reload();
              },
                error: function (xhr, ajaxOptions, thrownError) {
                      swal("error!", "Survey failed to delete", "error");
                },
          });
      });
}
</script>