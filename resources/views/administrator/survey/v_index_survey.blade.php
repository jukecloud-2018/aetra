@extends('layouts.app')

@section('head')
    Administrator | Master Survey
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Master Survey </span>
@endsection
@section('button')
    <div class="forButtonTop">
        <button href="" class="btn btn-default buttonAE btnTop" onclick="addSurvey()">
          <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </button>
        <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
          <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
        </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
        <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableSurvey" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th>No</th>
                                    <th>Survey Type</th>
                                    <th>Object Survey</th>
                                    <th>Detail</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="surveyModal" tabindex="-1" role="dialog" aria-labelledby="surveyModalCenterTitle" aria-hidden="false"  data-backdrop="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Add Survey Type</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
            <div class="alert alert-danger" id="error-alert" style="display: none"></div>
            <form id="formSurvey">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                <div class="col-md-12">
                <table>
                <tr class="">
                <td width="85px"><label for="" class="col-form-label"> Survey Type </label></td>
                <td  width="300px">
                <div style="margin-left: 10px;">
                <input type="text" class="form-control" name="survey_name" placeholder="Survey Type">
                </div>
                </td>
                </tr>
                <tr>
                <td width="85px"><label for="" class="col-form-label"> Object Survey </label></td>
                <td>
                <div style="margin-left: 10px;">
                <input type="radio"  name="object_survey" value="Asset"> Asset
                </div>

                <div style="margin-left: 10px;">
                <input type="radio"  name="object_survey" value="Nomen"> Nomen
                </div>

                <div style="margin-left: 10px;">
                <input type="radio"  name="object_survey" value="Area"> Area
                </div>
                </td>
                </tr>
                </table>

                <table id="tableDetailSurvey">
                <td width="85px"><label  style="margin-top:20px;" class="col-form-label"> Detail Survey : </label></td>
                <tr class="rowSurvey_0">
                <td  width="300px">
                <div  style="margin-top:20px;margin-left: 10px;">
                <input type="text" class="form-control" name="survey_detail_name[]" placeholder="Detail Survey">
                </div>
                </td>
                <td width="70px" colspan="2">
                <div  style="margin-top:20px;margin-left: 10px;">
                <input type="checkbox"   name="status_detail_survey[]" value="Active"> Active
                </div>
                </td>
                <td  width="200px">
                <div style="margin-top:20px;margin-left: 10px;">
                <select name="type_data_id[]" class="form-control">
                <option value=""> - Choose Type Data - </option>
                @foreach($type_data as $td)
                <option value="{{$td->id}}">{{$td->type_data}}</option>
                @endforeach
                </select>
                </div>
                </td>
                <td>
                <div style="margin-top:20px;">
                <a href="javascript:void(0)" onclick="addRowSurvey()" style="margin-right: 10px;margin-left: 10px;"> <i class="fa fa-plus"></i> </a>Add
                </div>
                </td>
                </tr>
                </table>

                </div>
                </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveSurvey()">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->



<!-- Modal   Detail -->
<div class="modal fade" id="surveyDetailModal" tabindex="-1" role="dialog" aria-labelledby="surveyDetailModalCenterTitle" aria-hidden="false"  data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 50%;">
    <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
        </button>
        <h4 class="modal-title" style="text-align:center">Add Survey Type</h4>
      </div>
      <div class="modal-body"  style="background-color: #FBFBFB">
        <div class="alert alert-danger" id="error-alert" style="display: none"></div>
        
      </div><!-- / Modal Body -->
      <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- end detail -->


@endsection

@section('js')
  @include('administrator.survey.js_survey')
@endsection
