<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form id="formUpdateBOM">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <!-- Name Template BOM -->
    <div class="form-group row">
        <div class="col-sm-3"><label for="" class="col-form-label"> Header Template </label></div>
        <div class="col-sm-7">
            <input type="hidden" name="jobCode" value="{{$wo_template_BOM->jobCode}}">
            <input type="text" class="form-control" placeholder="Header Template" value="{{$wo_template_BOM->template_name}}" name="template_name">
        </div>
        <div class="col-sm-2"></div>
    </div> 
    <fieldset>
        <legend style="font-size: 14px;font-weight: bold;">Detail:</legend>
        <div class="row">
            <div class="col-md-4">
                <label class="col-sm-12 col-form-label"> Item Code </label>
            </div>
            <div class="col-md-6">
                <label class="col-sm-4 col-form-label"> Item Name </label>
            </div>
            <div class="col-md-2"></div>
        </div>
    </fieldset>
    @if($count > 0)
        @php $i = 1; @endphp
        @foreach($detail as $detail)
            <input type="hidden" class="count" name="count" value="{{$count}}">
            <input type="hidden" name="id_detail" value="{{$detail->id_detail}}">
            <div class="form-group row rowBOM_{{$i}}" name="rowBOM_{{$i}}">
                <div class="form-group">
                    <!-- Item Code -->
                    <div class="col-md-4">
                        <select onchange="getCode(this);" class="form-control" name="template_code[]" id="template_code">
                            <option disabled>---Select---</option>
                            @foreach ($code as $value[0])
                            <option value="{{ $value[0]->id}}" {{($value[0]->id == $detail->id) ? "selected":""}} data="{{$i}}">{{$value[0]->code}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- End Item Code -->
                    <!-- Item Name -->
                    <div class="col-md-7">
                        <input class="form-control nama{{$i}}" placeholder="Item Name" value="{{$detail->name}}" name="template_name_code" id="template_name_code" readonly>
                    </div>
                    <!-- End Item Name -->
                    <!-- Add Row -->
                    <div class="col-md-1" style="padding:0px;">
                        @if($i < 1)
                        @else
                            @if($i == 1)
                                <div class="col-sm-1"  style="padding:0px;margin-top:5px;" id="tes">
                                    <a href="javascript:void(0)" onclick="addRowBom()"> <i class="fa fa-plus"></i> </a>
                                </div>
                            @else
                                <div class="col-sm-1"  style="padding:0px;margin-top:7px;">
                                    <a href="javascript:void(0)" onclick="deleteRowBom('.$i.')" style="color:red"> <i class="fa fa-minus"></i> </a>
                                </div>
                            @endif
                        @endif
                    </div>
                    <!-- End Add Row -->
                </div>
            </div>
            
        @php $i++; @endphp
        @endforeach
    @else
        <div class="form-group row rowBOM_0" name="rowBOM_0">
            <div class="col-md-4">
                <select onchange="getCodeAdd(this)" class="form-control" name="template_code[]" id="template_code">
                    <option>---Select---</option>
                    @php $i = 1; @endphp
                    @foreach ($code as $value[0])
                    <option value="{{ $value[0]->id}}" data="{{$i}}">{{$value[0]->code}}</option>
                    @php $i++; @endphp
                    @endforeach
                </select>
            </div>
            <div class="col-md-7">
                <input class="form-control nama" placeholder="Item Name" value="" name="template_name_code" id="template_name_code" disabled>
            </div>
            <div class="col-sm-1" style="padding:0px;margin-top:5px;">
                <a href="javascript:void(0)" onclick="addRowNewBom()"> <i class="fa fa-plus"></i> </a>
            </div>
        </div>
    @endif
</form><!-- /form -->
<script type="text/javascript">
function addRowBom(){
	var count = $('[name="count"]').val();
	baris++
	var hsl = baris + Number(count);
	var option ='<div class="form-group row rowBOM_'+hsl+'">'+
                    '<div class="col-md-4">'+
                        '<select onchange="getCode(this);" class="form-control" name="template_code[]">'+
                            '<option>---Select---</option>'+
                            '@foreach ($code as $value[0])'+
                            '<option value="{{ $value[0]->id}}" data="'+hsl+'">{{$value[0]->code}}</option>'+
                            '@endforeach'+
                        '</select>'+
                    '</div>'+
                    '<div class="col-md-7">'+
                        '<input type="text" class="form-control nama'+hsl+'" placeholder="Item Name" value="" name="template_name_code[]" readonly>'+
                    '</div>'+
                    '<div class="col-sm-1"  style="padding:0px;margin-top:7px;">'+
                        '<a href="javascript:void(0)" onclick="deleteRowBom('+hsl+')" class="remove" style="color:red"> <i class="fa fa-minus"></i> </a>'+
                    '</div>'+
                '</div>';
	$('#formUpdateBOM').append(option);
}
</script>

<script type="text/javascript">
    function addRowNewBom(){
        baris++
        var option ='<div class="form-group row rowBOM_'+baris+'">'+
                        '<div class="col-md-4">'+
                            '<select onchange="getCode(this);" class="form-control" name="template_code[]">'+
                                '<option>---Select---</option>'+
                                '@foreach ($coba as $value[0])'+
                                '<option value="{{ $value[0]->id}}" data="'+baris+'">{{$value[0]->code}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-7">'+
                            '<input type="text" class="form-control nama'+baris+'" placeholder="Item Name" value="" name="template_name_code[]">'+
                        '</div>'+
                        '<div class="col-sm-1"  style="padding:0px;margin-top:7px;">'+
                            '<a href="javascript:void(0)" onclick="deleteRowBom('+baris+')" class="remove" style="color:red"> <i class="fa fa-minus"></i> </a>'+
                        '</div>'+
                    '</div>';
        $('#formUpdateBOM').append(option); 
    }
</script>