@extends('layouts.app')

@section('head')
    Administrator | Mapping WO Task
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Mapping WO Task </span>
@endsection
@section('button')
    <div class="forButtonTop">
        <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
            <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
        </a>
    </div>
@endsection

@section('content')
    <section class="content">
        <!-- /.box-header -->
        <div class="box box-default">
            <!--- Box Body --->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="tableWOMapping" class="table table-bordered table-striped">
                                <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th>No</th>
                                    <th>Job Code</th>
                                    <th>Description</th>
                                    <th>Lock WO</th>
                                    <th>Charateristic</th>
                                    <th>Priority</th>
                                    <th>Urgent</th>
                                    <th>BOM</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($list_wo_task as $key=>$value)
                                    <tr role="row">
                                        <td style="text-align: center">{{++$key}}</td>
                                        <td>{{$value->job_code}}</td>
                                        <td>{{$value->descr}}</td>
                                        <td>
                                            <select name="lock_wo{{$value->id}}" class="form-control" onchange="saveLockWO('{{$value->id}}','{{$value->job_code}}')">
                                                <option value="Unlock" {{("Unlock" == $value->lock_status) ? "selected":""}}> Unlock  </option>
                                                <option value="Assigned" {{("Assigned" == $value->lock_status) ? "selected":""}}> Assigned  </option>
                                                <option value="On The Way" {{("On The Way" == $value->lock_status) ? "selected":""}}> On The Way  </option>
                                                <option value="Started" {{("Started" == $value->lock_status) ? "selected":""}}> Started  </option>
                                            </select>
                                        </td>
                                        <td style="text-align: center">
                                            <button class="btn btn-primary" onclick="view_character('{{$value->job_code}}')"><i class="fa fa-search"></i></button>
                                        </td>
                                        <td>
                                            <select name="priority{{$value->id}}" class="form-control" onchange="savePriority('{{$value->id}}','{{$value->job_code}}')">
                                                @for($x=1; $x <=5; $x++)
                                                    <option value="{{$x}}" {{($x == $value->priority) ? "selected":""}}> {{$x}} </option>
                                                @endfor
                                            </select>
                                        </td>
                                        @if($value->urgent_status == '1')
                                        <td style="text-align: center;">
                                            <input type="checkbox" name="urgent_status{{$value->id}}" onchange="saveUrgent(this, '{{$value->id}}')" checked value="{{$value->job_code}}">
                                        </td>
                                        @else
                                            <td style="text-align: center;">
                                                <input type="checkbox" name="urgent_status{{$value->id}}" onchange="saveUrgent(this, '{{$value->id}}')" value="{{$value->job_code}}">
                                            </td>
                                        @endif
                                        <td>
                                        <button class="btn btn-info" onclick="viewBOM('{{$value->job_code}}')">
                                            <i class="fa fa-eye"></i>
                                        </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box body -->
    </section>
    <!-- /section -->

    <!-- Modal Add -->
    <div class="modal fade" id="modalChar" tabindex="-1" role="dialog" aria-labelledby="modalCharCenterTitle" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 20%;height: 85%;">
            <div class="modal-content" style="height: 85%;">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-align:center"></h4>
                </div>
                <div class="modal-body" style="max-height: calc(100% -  120px);overflow-y: auto;">

                    <form id="formGroup">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <table id="mappingWOType" class="table table-bordered">
                            <thead>

                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </form><!-- /form -->
                </div><!-- / Modal Body -->

            </div>
        </div>
    </div>
    <!-- end Modal -->

    <!-- Modal View BOM -->
    <div class="modal fade" id="modalBOM" tabindex="-1" role="dialog" aria-labelledby="modalBOMCenterTitle" aria-hidden="false" style="">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 50%;">
            <div class="modal-content" style="">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeModalBOM()">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">  </h4>
                </div>
                <div class="modal-body" style="background-color:  #FBFBFB;">
                    <table class="table table-striped table-bordered" id="listBOM"> 
                        <thead>
                            <tr>
                                <th> NO </th>
                                <th>Header Template</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div><!-- / Modal Body -->
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeModalBOM()">Cancel</button>
                    <button type="button" class="btn btn-primary  btnFooterModal" onclick="addBOM()">Add</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end Modal view BOM-->

    <!-- Modal add BOM -->
    <div class="modal fade" id="addModalBOM" tabindex="-1" role="dialog" aria-labelledby="addModalBOMCenterTitle" aria-hidden="false"  data-backdrop="false" style="">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 60%;">
            <div class="modal-content" style="">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeAddModalBOM()">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Add New Template BOM</h4>
                </div>
                <div class="modal-body" style="background-color: #FBFBFB;"></div><!-- / Modal Body -->
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>
                    <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveBOM()">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal add BOM-->

    <!-- Modal Edit BOM -->
    <div class="modal fade" id="editModalBOM" tabindex="-1" role="dialog" aria-labelledby="editModalBOMCenterTitle" aria-hidden="false"  data-backdrop="false" style="">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 60%;">
            <div class="modal-content" style="">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeAddModalBOM()">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Edit Template BOM</h4>
                </div>
                <div class="modal-body" style="background-color: #FBFBFB;"></div><!-- / Modal Body -->
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>
                    <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveBOM()">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal add BOM-->

<!-- Modal Edit BOM -->
<div class="modal fade" id="editCharacteristicBOM" tabindex="-1" role="dialog" aria-labelledby="editCharacteristicBOMCenterTitle" aria-hidden="false" style="">
        <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 60%;">
            <div class="modal-content" style="">
                <div class="modal-header">
                    <button type="button" class="close" onclick="closeAddModalBOM()">
                        <span aria-hidden="true" style="color:#fff">&times;</span>
                    </button>
                    <h4 class="modal-title" style="text-align:center">Edit Characteristic Sequences</h4>
                </div>
                <div class="modal-body" style="background-color: #FBFBFB;">
                    <table class="table table-striped table-bordered" id="listBOM"> 
                        <thead>
                            <tr>
                                <th>NO</th>
                                <th>Char Type</th>
                                <th>Sequence</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div><!-- / Modal Body -->
                <div class="modal-footer" style="text-align: center">
                    <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>
                    <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveCharWO()">Save</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal add BOM-->
@endsection

@section('js')
    @include('administrator.mappingWOTask.js_mappingWOTask')
@endsection
