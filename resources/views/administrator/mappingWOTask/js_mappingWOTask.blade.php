<script type="text/javascript">
var baris = 0;
// for style checkbox
$( document ).ready(function() {
  	styleChek();
});

//for datatable 
$(document).ready(function() {
    $('#tableWOMapping').DataTable();
});

// for style checkbox
function styleChek()
{
	$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-green',
        radioClass   : 'iradio_minimal-green'
    });
}


// for add row table wo
function addRowWO()
{
	var table = $('#tableWO');

	table.append('<tr  class="rowWo">'+
                    '<td width="85px"><label for="staticEmail" class="col-form-label">  </label></td>'+
                    '<td width="230px">'+
                      '<select name="" id="" class="form-control">'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                        '<option value="">Wo</option>'+
                      '</select>'+
                    '</td>'+
                    '<td width="73px" align="center">'+
                      '<input type="checkbox" class="minimal"> TRN</td>'+
                    '<td width="">'+
                     '<input type="checkbox" class="minimal"> MNT'+
                    '</td>'+
                    '<td width="30px" align="center">'+
                       '<a href="javascript:void(0)" class="" style="color:red;"  onclick="deleteRowWo()"> <i class="fa fa-minus"></i> </a>'+
                    '</td>'+
                  '</tr>');
	styleChek();
}

//// for delete row table wo
function deleteRowWo()
{
	document.getElementById("tableWO").deleteRow(1);
}

/// for save group 
function saveGroup()
{
	var data = $('#formGroup').serialize();

	$.ajax({
		url:"",
		data:data,
		dataType:'JSON',
		type:'POST',
		success:function(data){
			if(data.errors)
			{
				$.each(data.errors,function(key,value){
					$('.alert-danger').show();
					$('.alert-danger').html('<li>'+value+'</li>');
				})

				setTimeout(function() {
					$('#error-alert').fadeOut('fast');
				}, 2000); 
			}
			else
			{
				swal('Success','Group has been saved successfully!','success');
				location.reload();
			}
		},
          error: function (xhr, ajaxOptions, thrownError) {
				swal("error!", thrownError, "error");
          },
	})
}

function view_character(id)
{
     $.ajax({
        type: 'GET',
        url : '/administrator/mappingWOTask/view_character/'+id,
        data:"",
        success:function(e){
            table = $('#mappingWOType tbody');
            table.empty();
            e.data.forEach(function(z){
                var trim_char = $.trim(z.char_type_flg);
                if(trim_char == 'CLS') 
                {
                  table.append('<tr> ' +
                        '<td> '+'Tidak Ada Karakteristik'+' </td>'+
                        '</tr>')
                }
                if(trim_char == 'ADV')
                {
                    table.append('<tr> ' +
                        '<td> '+z.char_wo_type+' </td>' +
                        '<td> <input type="text" pattern="[a-zA-Z0-9\\s]+" class="form-control input-sm"> </td>' +
                        '</tr>')
                }if(trim_char == 'DFV')
                {
                    table.append('<tr> ' +
                        '<td> '+z.char_wo_type+' </td>' + 
                        '<td> <input type="text" pattern="[a-zA-Z0-9\\s]+" class="form-control input-sm" value="'+z.char_value+'"> </td>' +
                        '</tr>')
                }
                /*table.append('<tr> ' +
                    '<td> '+z.charac_desc+' </td>' +
                    '<td> '+z.char_val+' </td>' +
                    '<td> '+z.sort_seq+' </td>' +
                    '</tr>')*/
            });
            $('#modalChar').find('.modal-header').html('<button type="button" class="close" onclick="closeModal()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button><h4> Charateristic '+id+' </h4>');
            $('#modalChar').modal('show');
        }
    })
}

function closeModal()
{
      $('#modalChar').modal('hide');
}

function savePriority(id,job_code)
{
  $.ajax({
    url : '/administrator/mappingWOTask/savePriority/'+id,
    data:{'priority_status':$('[name="priority'+id+'"]').val(),'job_code':job_code,'id':id},
    dataType:'JSON',
    type:'GET',
    success:function(data){
      if(data.status=='success')
      {
        swal('Success','Priority has been update successfully!','success');
      }
    },
    error: function (xhr, ajaxOptions, thrownError) {
      swal("error!", "Gagal Setting Priority", "error");
    }
  })
}

function saveUrgent(cek,id)
{
  var urgent_status;
  if(cek.checked == true)
  {
    //urgent_status = 'true';
    urgent_status = '1';
  }
  else
  {
    //urgent_status = 'false';
    urgent_status = '0';
  }
  $.ajax({
    url : '/administrator/mappingWOTask/saveUrgent/'+id,
    data:{'urgent_status':urgent_status,'job_code':cek.value},
      dataType:'JSON',
      type:'GET',
    success:function(data){
      if(data.status=='success'){
        swal('Success','Urgen has been update successfully!','success');
      }
    },
      error: function (xhr, ajaxOptions, thrownError) {
      swal("error!", "Gagal Setting Urgent", "error");
    }
  })
}

function saveLockWO(id,job_code)
{
  $.ajax({
        url : '/administrator/mappingWOTask/saveLockWO/'+id,
        data:{'val_lock_wo':$('[name="lock_wo'+id+'"]').val(),'job_code':job_code},
         dataType:'JSON',
         type:'GET',
        success:function(data){
          if(data.status=='success')
          {
            swal('Success','Lock WO has been update successfully!','success');
          }
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
        }
    })   
}

function viewBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingWOTask/viewBOM/'+id,
        data:'kosong',
        success:function(e){
                //data.worker.forEach(function(e){
            table = $('#listBOM tbody');
            table.empty();
            e.data.forEach(function(currentValue,index){
                table.append('<tr> <td> '+ (parseInt(index) + parseInt(1)) +' </td>'+
                        '<td> '+currentValue.template_name+' </td>'+
                        '<td> <button class="btn btn-primary" onclick="detailBOM(`'+currentValue.id+'`)"><i class="fa fa-search"></i>  </button> '+
                        '|  <button class="btn btn-success" onclick="editBOM(`'+currentValue.id+'`)"><i class="fa fa-pencil"></i>  </button> </td>'+
                        '</tr>');
            });
             $('#modalBOM').find('.modal-header').html('<input type="hidden" name="jobCode" value="'+id+'"><button type="button" class="close" onclick="closeModalBOM()">'+
            '<span aria-hidden="true" style="color:#fff;">&times;</span>'+
            '</button><h4 style="text-align:center;"> List Template BOM  '+id+' </h4>');
            $('#modalBOM').modal('show');       
        }
    });    
}

function closeModalBOM()
{
    $('#modalBOM').modal('hide');
}

function closeAddModalBOM()
{
    $('#addModalBOM').modal('hide');
    $('#editModalBOM').modal('hide');
}

function addBOM()
{   
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingWOTask/create',
        data:{jobCode: $('[name="jobCode"]').val() },
        success:function(e){        
            footer ='<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>'+
                    '<button type="button" class="btn btn-primary  btnFooterModal" onclick="saveBOM()">Save</button>';
            $('#addModalBOM').find('.modal-footer').html(footer);
            $('#addModalBOM').find('.modal-body').html(e);
            $('#addModalBOM').modal('show');
        }
    });
}

function getCode(obj){
    var id = obj.options[obj.selectedIndex].getAttribute('value');
    var baris = obj.options[obj.selectedIndex].getAttribute('data');
    if(id) {
        $.ajax({
            type: 'GET',
            url : '/administrator/mappingWOTask/getCode/'+id,
            data:{jobCode: $('[name="'+id+'"]').val() },
            dataType:'json',
            success:function(e){
                $('.nama'+baris).val(e.name);
            }
        });
    }
}

function getCodeAdd(id){
    var id = id.value;
    if(id) {
        $.ajax({
            type: 'GET',
            url : '/administrator/mappingWOTask/getCode/'+id,
            data:{jobCode: $('[name="'+id+'"]').val() },
            dataType:'json',
            success:function(e){
                $('.nama').val(e.name);
            }
        });
    }
}

function deleteRowBom(baris)
{
    $('.rowBOM_'+baris+'').remove();
    baris--;
}
function deleteRowBomadd()
{   
    $('.rowBOM_'+baris+'').remove();
    baris--;
}

function saveBOM()
{
    var dataBOM= $('#formBOM').serialize();
    $.ajax({
        url:'mappingWOTask',
        data:dataBOM,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Group has been saved successfully!','success');
                closeAddModalBOM();
                viewBOM(data.jobCode);
            }
            else if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                });
            }
            hidden_error();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
    });
}

function detailBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingWOTask/'+id,
        data:{jobCode: $('[name="jobCode"]').val() },
        success:function(e){
            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>';
            $('#addModalBOM').find('.modal-footer').html(footer)
            $('#addModalBOM').find('.modal-body').html(e);
            $('#addModalBOM').modal('show');
        }
    });
}

function editBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingWOTask/edit/'+id,
        data:{jobCode: $('[name="jobCode"]').val() },
        success:function(e){
            footer ='<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>'+
                    '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateBOM('+id+')">Update</button>';
            $('#editModalBOM').find('.modal-footer').html(footer)
            $('#editModalBOM').find('.modal-body').html(e);
            $('#editModalBOM').modal('show');
        }
    });
}

function updateBOM(id)
{
    var dataBOM= $('#formUpdateBOM').serialize();
    //console.log(dataBOM); 
    $.ajax({
        url:'mappingWOTask/'+id,
        data:dataBOM,
        dataType:'JSON',
        type:'PUT',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Group has been saved successfully!','success');
                closeAddModalBOM();
                viewBOM(data.jobCode);
            }
            else if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                });
            }
            hidden_error();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
    });
}
</script>