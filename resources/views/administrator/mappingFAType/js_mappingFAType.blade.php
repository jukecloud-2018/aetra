<script type="text/javascript">
var baris = 0;

function view_character(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/view_character/'+id,
        data:"",
        success:function(e){
            table = $('#mappingFAType tbody');
            table.empty();
            e.data.forEach(function(z){
                var trim_char = $.trim(z.char_type_flg);
                if(trim_char == 'FKV')
                {
                    table.append('<tr> ' +
                        '<td> '+z.sort_seq+' </td>' +
                        '<td> '+z.charac_desc+' </td>' +
                        '<td> <input type="text" disabled="disabled" class="form-control input-sm"> </td>' +
                        '</tr>')
                }
                if(trim_char == 'ADV')
                {
                    table.append('<tr> ' +
                        '<td> '+z.sort_seq+' </td>' +
                        '<td> '+z.charac_desc+' </td>' +
                        '<td> <input type="text" pattern="[a-zA-Z0-9\\s]+" class="form-control input-sm"> </td>' +
                        '</tr>')
                }
                if(trim_char == 'DFV')
                {
                    table.append('<tr> ' +
                        '<td> '+z.sort_seq+' </td>' +
                        '<td> '+z.charac_desc+' </td>' +
                        /*'<td> ' +
                            '<select class="form-control">' +
                            '<option>'+z.char_val+'</option>'+
                            '</select>' +
                        '</td>' +*/
                        '<td> <input type="text" pattern="[a-zA-Z0-9\\s]+" class="form-control input-sm" value="'+z.char_val+'"> </td>' +
                        '</tr>')
                }
                /*table.append('<tr> ' +
                    '<td> '+z.charac_desc+' </td>' +
                    '<td> '+z.char_val+' </td>' +
                    '<td> '+z.sort_seq+' </td>' +
                    '</tr>')*/
            });
            $('#modalChar').find('.modal-header').html('<button type="button" class="close" onclick="closeModal()">'+
            '<span aria-hidden="true" style="color:#fff">&times;</span>'+
            '</button><h4> Charateristic '+id+' </h4>');
            $('#modalChar').modal('show');
        }
    });
}

function closeModal()
{
    $('#modalChar').modal('hide');
}

function viewBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/viewBOM/'+id,
        data:'kosong',
        success:function(e){
                //data.worker.forEach(function(e){
            table = $('#listBOM tbody');
            table.empty();
            e.data.forEach(function(currentValue,index){
                table.append('<tr> <td> '+ (parseInt(index) + parseInt(1)) +' </td>'+
                        '<td> '+currentValue.template_name+' </td>'+
                        '<td> <button class="btn btn-primary" onclick="detailBOM(`'+currentValue.id+'`)"><i class="fa fa-search"></i>  </button> '+
                        '|  <button class="btn btn-success" onclick="editBOM(`'+currentValue.id+'`)"><i class="fa fa-pencil"></i>  </button> </td>'+
                        '</tr>');
            });
             $('#modalBOM').find('.modal-header').html('<input type="hidden" name="fa_type_cd" value="'+id+'"><button type="button" class="close" onclick="closeModalBOM()">'+
            '<span aria-hidden="true" style="color:#fff;">&times;</span>'+
            '</button><h4 style="text-align:center;"> List Template BOM  '+id+' </h4>');
            $('#modalBOM').modal('show');       
        }
    });    
}

function closeModalBOM()
{
    $('#modalBOM').modal('hide');
}
function closeAddModalBOM()
{
    $('#addModalBOM').modal('hide');
    $('#editModalBOM').modal('hide');
}

function addBOM()
{   
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/create',
        data:{fa_type_cd: $('[name="fa_type_cd"]').val() },
        success:function(e){        
            footer ='<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>'+
                    '<button type="button" class="btn btn-primary  btnFooterModal" onclick="saveBOM()">Save</button>';
            $('#addModalBOM').find('.modal-footer').html(footer);
            $('#addModalBOM').find('.modal-body').html(e);
            $('#addModalBOM').modal('show');
        }
    });
}
function getCode(obj){
    var id = obj.options[obj.selectedIndex].getAttribute('value');
    var baris = obj.options[obj.selectedIndex].getAttribute('data');
    if(id) {
        $.ajax({
            type: 'GET',
            url : '/administrator/mappingFAType/getCode/'+id,
            data:{fa_type_cd: $('[name="'+id+'"]').val() },
            dataType:'json',
            success:function(e){
                $('.nama'+baris).val(e.name);
            }
        });
    }
}
function getCodeAdd(id){
    var id = id.value;
    if(id) {
        $.ajax({
            type: 'GET',
            url : '/administrator/mappingFAType/getCode/'+id,
            data:{fa_type_cd: $('[name="'+id+'"]').val() },
            dataType:'json',
            success:function(e){
                $('.nama').val(e.name);
            }
        });
    }
}

function deleteRowBom(baris)
{
    $('.rowBOM_'+baris+'').remove();
    baris--;
}
function deleteRowBomadd()
{   
    $('.rowBOM_'+baris+'').remove();
    baris--;
}

function saveBOM()
{
    var dataBOM= $('#formBOM').serialize();
    $.ajax({
        url:'mappingFAType',
        data:dataBOM,
        dataType:'JSON',
        type:'POST',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Data has been saved successfully!','success');
                closeAddModalBOM();
                viewBOM(data.fa_type_cd);
            }
            else if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                });
            }
            hidden_error();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", "Jangan Ada Data Yang Kosong", "error");
        }
    });
}

function detailBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/'+id,
        data:{fa_type_cd: $('[name="fa_type_cd"]').val() },
        success:function(e){
            footer = '<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>';
            $('#addModalBOM').find('.modal-footer').html(footer)
            $('#addModalBOM').find('.modal-body').html(e);
            $('#addModalBOM').modal('show');
        }
    });
}

function editBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/edit/'+id,
        data:{fa_type_cd: $('[name="fa_type_cd"]').val() },
        success:function(e){
            footer ='<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>'+
                    '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateBOM('+id+')">Update</button>';
            $('#editModalBOM').find('.modal-footer').html(footer)
            $('#editModalBOM').find('.modal-body').html(e);
            $('#editModalBOM').modal('show');
        }
    });
}

function updateBOM(id)
{
    var dataBOM= $('#formUpdateBOM').serialize();
    //console.log(dataBOM); 
    $.ajax({
        url:'mappingFAType/'+id,
        data:dataBOM,
        dataType:'JSON',
        type:'PUT',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Group has been saved successfully!','success');
                closeAddModalBOM();
                viewBOM(data.fa_type_cd);
            }
            else if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                });
            }
            hidden_error();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
    });
}

function saveUrgent(cek,id)
{
    var urgent_status;
    if(cek.checked == true)
    {
        //urgent_status = 'true';
        urgent_status = '1';
    }
    else
    {
        //urgent_status = 'false';
        urgent_status = '0';
    }
    $.ajax({
        url : '/administrator/mappingFAType/saveUrgent/'+id,
        data:{'urgent_status':urgent_status,'fa_type_cd':cek.value},
        dataType:'JSON',
        type:'GET',
        success:function(data){
            if(data.status=='success'){
                swal('Success','Urgen has been update successfully!','success');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
    });
}

function saveLockFA(id,fa_type_cd)
{
     $.ajax({
        url : '/administrator/mappingFAType/saveLockFA/'+id,
        data:{'val_lock_fa':$('[name="lock_fa'+id+'"]').val(),'fa_type_cd':fa_type_cd},
         dataType:'JSON',
         type:'GET',
        success:function(data){
            if(data.status=='success')
            {
                swal('Success','Lock FA has been update successfully!','success');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            swal("error!", thrownError, "error");
        }
    }); 
}


function savePriority(id,fa_type_cd)
{
	$.ajax({
		url : '/administrator/mappingFAType/savePriority/'+id,
		data:{'priority_status':$('[name="priority'+id+'"]').val(),'fa_type_cd':fa_type_cd,'id':id},
		dataType:'JSON',
		type:'GET',
		success:function(data){
			if(data.status=='success')
			{
				swal('Success','Priority has been update successfully!','success');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			swal("error!", thrownError, "error");
		}
	});
}

function closeModalSeqChar()
{
    $('#editCharacteristicBOM').modal('hide');
}

function editCharacteristicBOM(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/viewSeqChar/'+id,
        data:'kosong',
        success:function(e){
                //data.worker.forEach(function(e){
            table = $('#listSeq tbody');
            table.empty();
            e.data.forEach(function(currentValue,index){
                table.append('<tr> <td> '+ (parseInt(index) + parseInt(1)) +' </td>'+
                        '<td> '+currentValue.char_type_cd+' </td>'+
                        '<td> '+currentValue.sort_seq+' </td>'+
                        '<td> '+currentValue.descr+' </td>'+
                        '<td><button class="btn btn-success" onclick="editSeqChar(`'+currentValue.id+'`)"><i class="fa fa-pencil"></i>  </button> </td>'+
                        '</tr>');
            });
             $('#editCharacteristicBOM').find('.modal-header').html('<input type="hidden" name="fa_type_cd" value="'+id+'"><button type="button" class="close" onclick="closeModalSeqChar()">'+
            '<span aria-hidden="true" style="color:#fff;">&times;</span>'+
            '</button><h4 style="text-align:center;"> Edit Characteristic Sequences </h4>');
            $('#editCharacteristicBOM').modal('show');       
        }
    });    
}

function editSeqChar(id)
{
    $.ajax({
        type: 'GET',
        url : '/administrator/mappingFAType/editSeqChar/'+id,
        data:'kosong',
        success:function(e){
            footer ='<button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeAddModalBOM()">Cancel</button>'+
                    '<button type="button" class="btn btn-primary  btnFooterModal" onclick="updateBOM('+id+')">Update</button>';
            $('#editModalSeqChar').find('.modal-footer').html(footer)
            $('#editModalSeqChar').find('.modal-body').html(e);
            $('#editModalSeqChar').modal('show');
        }
    });
}

function closeEditSeq()
{
    $('#editModalSeqChar').modal('hide');
}

function saveCharSeq(id){
    var dataSeq = $('#formUpdateCharType').serialize();
    $.ajax({
        url:"/administrator/mappingFAType/updateCharSeq/"+id,
        data:dataSeq,
        dataType:'JSON',
        type:'post',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success:function(response){
            if(response.status == "success"){
                swal("Berhasil!", "Sequence Berhasil Di Ubah", "success");
                location.reload();
            }
        },
        error:function(xhr,thrownError,ajaxOptions){
            swal("Error", "Sequence Gagal Di Update", "Error");
        }
    }); 
}

</script>