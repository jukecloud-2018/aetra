<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form id="formBOM">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <!-- Nama Template BOM -->
    <div class="form-group row">
        <div class="col-sm-3"><label for="" class="col-form-label"> Header Template </label></div>
        <div class="col-sm-7">
            <input type="hidden" name="fa_type_cd" value="{{$fa_type_cd}}">
            <input type="text" class="form-control" placeholder="Header Template" value="" name="template_name">
        </div>
        <div class="col-sm-2"></div>
    </div>
    <fieldset>
        <legend style="font-size: 14px;font-weight: bold;">Detail:</legend>
    </fieldset>
    <div class="form-group row rowBOM_0" name="rowBOM_0">
        <div class="col-md-4">
            <label class="col-form-label"> Item Code </label>
            <select onchange="getCodeAdd(this)" class="form-control" name="template_code[]" id="template_code">
                <option value="">---Select---</option>
                @php $i = 1; @endphp
                @foreach ($code as $code)
                <option value="{{ $code->id }}" data="1">{{ $code->code }}</option>
                @php $i++; @endphp
                @endforeach
            </select>
        </div>
        <div class="col-md-7">
            <label class="col-sm-4 col-form-label"> Item Name </label>
            <input class="form-control nama" placeholder="Item Name" value="" name="template_name_code" id="template_name_code" disabled>
        </div>
        <div class="col-sm-1" style="padding:0px;margin-top:31px;">
            <a href="javascript:void(0)" onclick="addRowBom()"> <i class="fa fa-plus"></i> </a>
        </div>
    </div>
</form><!-- /form -->
<script type="text/javascript">
    function addRowBom(){
        baris++
        var option ='<div class="form-group row rowBOM_'+baris+'">'+
                        '<div class="col-md-4">'+
                            '<select onchange="getCode(this);" class="form-control" name="template_code[]">'+
                                '<option value="">---Select---</option>'+
                                '@foreach ($coba as $value[0])'+
                                '<option value="{{ $value[0]->id}}" data="'+baris+'">{{$value[0]->code}}</option>'+
                                '@endforeach'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-7">'+
                            '<input type="text" class="form-control nama'+baris+'" placeholder="Item Name" value="" name="template_name_code[]">'+
                        '</div>'+
                        '<div class="col-sm-1" style="padding:0px;margin-top:7px;">'+
                            '<a href="javascript:void(0)" onclick="deleteRowBom('+baris+')" class="remove" style="color:red"> <i class="fa fa-minus"></i> </a>'+
                        '</div>'+
                    '</div>';
        $('#formBOM').append(option); 
    }
</script>