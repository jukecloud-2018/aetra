<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form id="formUpdateBOM">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="form-group row">
        <label for="" class="col-sm-3 col-form-label"> Header Template </label>
        <div class="col-sm-7">
        <input type="hidden" name="fa_type_cd" value="{{$fa_template_BOM->id}}">
        <!--<input type="text" class="form-control" placeholder="Header Template"  name="template_name" value="{{$fa_template_BOM->template_name}}">-->
        <span class="form-control">{{$fa_template_BOM->template_name}}</span>
        </div>
        <div class="col-sm-7"></div>
    </div> 
    <fieldset>
        <legend style="font-size: 14px;font-weight: bold;">Detail:</legend>
        <div class="row">
            <div class="col-md-4">
                <label class="col-sm-12 col-form-label"> Item Code </label>
            </div>
            <div class="col-md-6">
                <label class="col-sm-4 col-form-label"> Item Name </label>
            </div>
            <div class="col-md-2"></div>
        </div>
    </fieldset>
    @foreach($detail as $detail)
        <div class="row">
            <div class="col-md-4">
                <div style="margin-top:5px;">
                <span class="form-control">{{$detail->code}}</span>
                </div>
            </div>
            <div class="col-md-7">
                <div style="margin-top:5px;">
                    <span class="form-control">{{$detail->name}}</span>
                </div>
            </div>
        </div>
    @endforeach
</form>
<!-- /form -->