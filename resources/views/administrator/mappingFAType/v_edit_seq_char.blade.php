<div class="alert alert-danger" id="error-alert" style="display: none"></div>
<form id="formUpdateCharType">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <!-- Name Template BOM -->
    <div class="form-group row">
        <div class="col-sm-3"><label class="col-form-label"> Char Type : {{trim($seqChar->char_type_cd)}} - {{trim($seqChar->descr)}} </label></div>
        <div class="col-sm-7">
            <input type="hidden" name="id" value="{{$seqChar->id}}">
            <input type="text" class="form-control" value="{{$seqChar->sort_seq}}" name="sort_seq">
        </div>
    </div>  
    <div class="modal-footer" style="text-align: center">
        <button type="button" class="btn btn-secondary  btnFooterModal" onclick="closeEditSeq()">Cancel</button>
        <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveCharSeq('{{$seqChar->id}}')">Save</button>
     </div>  
</form><!-- /form -->
