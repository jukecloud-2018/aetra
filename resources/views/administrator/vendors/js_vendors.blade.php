<script type="text/javascript">


//for datatable 
$('#tableVendor').dataTable({
    processing: true,
    serverSide: true,
    order: [0, 'desc'],
    ajax: {
        method: 'POST',
        url : '<?=route('administrator.vendors.getData');?>',
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    },
    columns : [
        {
            // this for numbering table
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        },
        { data: "vendor_name" },
        {
            mRender: function (data, type, row, meta) {
                return '<button class="btn btn-success" onclick="editVendor(' + row.id + ')"><i class="fa fa-pencil"></i></button> | <button class="btn btn-danger" onclick="deleteVendor(' + row.id + ')"><i class="fa fa-trash"></i></button> | <button class="btn btn-info" onclick="viewVendor(' + row.id + ')"><i class="fa fa-eye"></i></button>';
                
            }
		},
    ],
    responsive: true,
});


// for form add vendor
function addVendor()
{
    $('[name="vendor_name"]').val('');
    $('[name="vendor_id"]').val('');
    $('#vendorModal').find('.modal-title').text('Add New Master Vendor');
    $('#vendorModal').modal('show');
}


/// for save and update vendor
function saveVendor()
{
    var data = $('#formVendor').serialize();
    var id = $('[name="vendor_id"]').val();
    var url;
    var sendType;
    if(id == '')
    {
        url = "{{route('administrator.vendors.store')}}";
        sendType = 'POST';
    }
    else
    {
        url = 'vendors/update/'+id;
        sendType = 'PUT';
    }
    

    //ajax for save
    $.ajax({
        url:url,
        data:data,
        dataType:'JSON',
        type:sendType,
        success:function(data){
            if(data.errors)
            {
                $.each(data.errors,function(key,value){
                    $('.alert-danger').show();
                    $('.alert-danger').html('<li>'+value+'</li>');
                })

                setTimeout(function() {
                    $('#error-alert').fadeOut('fast');
                }, 2000); 
            }
            else if(data.status=='success')
            {
                swal('Success','Vendor has been saved successfully!','success');
                location.reload();
            }
        },
          error: function (xhr, ajaxOptions, thrownError) {

               get = $.parseJSON(xhr.responseText);
               swal("error!", "Vendor Sudah Ada, Silahkan Masukkan Vendor Dengan Nama Lain", "error");
          },
    })
}


/// for edit vendor
function editVendor(id)
{

    $.ajax({
        url:'vendors/'+id+'/edit',
        data:id,
        dataType:'JSON',
        type:'GET',
        success:function(get){
            $('.modal-title').text('Update Master Vendor');
            $('[name="vendor_name"]').val(get.data.vendor_name);
            $('[name="vendor_id"]').val(get.data.id);
            $('#vendorModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
          },
    })
}

function viewVendor(id)
{

    $.ajax({
        url:'vendors/viewVendor/'+id,
        data:id,
        dataType:'JSON',
        type:'GET',
        success:function(get){
            $('.modal-title').text('View Vendor');
            $('[name="vendor_name"]').val(get.data.vendor_name);
            $('[name="vendor_id"]').val(get.data.id);
            $('#vendorViewModal').modal('show');
        },
          error: function (xhr, ajaxOptions, thrownError) {
                swal("error!", thrownError, "error");
          },
    })
}

//for delete vendor
function deleteVendor(id)
{
    console.log('Delete processing');
    
    swal({
      title: "Apakah Data Ingin Di Hapus?",
      text: "Jika Data Sudah Di Hapus, Tidak Dapat Di Kembalikan",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Hapus",
      closeOnConfirm: false
    }, function(isConfirm){
        if (!isConfirm) return;
        $.ajax({
            url:'vendors/destroy/'+id,
            data:id,
            dataType:'JSON',
            type:'GET',
            success:function(){
                swal("Done!", "Vendor has been deleted!", "success");
                location.reload();
            },
              error: function (xhr, ajaxOptions, thrownError) {
                    swal("error!", "Vendor failed to delete", "error");
              },
        });
    });
}
</script>