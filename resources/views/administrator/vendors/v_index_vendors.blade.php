@extends('layouts.app')

@section('head')
    Administrator | Master Vendor
@endsection

@section('title')
    <i class="fa fa-home"></i> Administrator  | <span style="color:#2B6B97"> Master Vendor </span>
@endsection
@section('button')
    <div class="forButtonTop">
          <button href="" class="btn btn-default buttonAE" onclick="addVendor()">
              <i class="fa fa-plus" aria-hidden="true"></i> Add New
          </button>
          <a href="" class="btn btn-primary white btnTop" style="margin-left:10px;">
              <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
          </a>
    </div>
@endsection

@section('content')
<section class="content">
    <!-- /.box-header -->
    <div class="box box-default">
    <!--- Box Body --->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="tableVendor" class="table table-bordered table-striped">
                            <thead>
                                <tr style="background-color:#8B9AAD;color:#fff">
                                    <th style="width: 20px;">No</th>
                                    <th>Vendor</th>
                                    <th style="width: 110px;"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box body -->
</section>
<!-- /section -->

<!-- Modal   Add -->
<div class="modal fade" id="vendorModal" tabindex="-1" role="dialog" aria-labelledby="vendorModalCenterTitle" aria-hidden="false">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="color:#fff">&times;</span>
            </button>
            <h4 class="modal-title" style="text-align:center">Add New Master Vendor</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formVendor">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="vendor_id" >
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Vendor Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="vendor_name" class="form-control" id="" placeholder="Vendor Name">
                        </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
                <button type="button" class="btn btn-primary  btnFooterModal" onclick="saveVendor()">Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal   Add -->
<div class="modal fade" id="vendorViewModal" tabindex="-1" role="dialog" aria-labelledby="vendorModalCenterTitle" aria-hidden="false" >
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="width: 40%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color:#fff">&times;</span>
                </button>
                <h4 class="modal-title" style="text-align:center">Master Vendor</h4>
            </div>
            <div class="modal-body"  style="background-color: #FBFBFB">
                <div class="alert alert-danger" id="error-alert" style="display: none"></div>
                <form id="formVendor">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="vendor_id" >
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Vendor Name</label>
                        <div class="col-sm-9">
                            <input type="text" name="vendor_name" class="form-control" disabled="">
                        </div>
                    </div>
                </form><!-- /form -->
            </div><!-- / Modal Body -->
            <div class="modal-footer" style="text-align: center">
            <button type="button" class="btn btn-secondary btnFooterModal" data-dismiss="modal" onclick="window.location.reload()">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
@endsection

@section('js')
  @include('administrator.vendors.js_vendors')
@endsection
